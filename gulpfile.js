const elixir = require('laravel-elixir');

const gulptasks = require('./gulptasks');

// require('laravel-elixir-jade');
require('laravel-elixir-vue-2');
require('laravel-blade-jade');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

gulptasks.tasks(elixir);

elixir(mix => {
    mix.sass(['app.scss', 'admin_style.scss']); //.webpack('app.js');
    
    mix.sass([
        './html/sass/components/*.scss'
    ], 'public/css/front_components.css');

    mix.sass([
        './html/sass/style/style.scss',
        './html/sass/modules/*.scss',
        './html/sass/partials/*.scss'
    ], 'public/css/front.css',
        './html');

    /* fancytree */
    mix.copy('public/bower_components/fancytree/dist/skin-win8/icons.gif', 'public/css/icons.gif'); //build
    /* datatables */
    mix.copy('public/bower_components/datatables.net-dt/images/*.*', 'public/images'); //build
    /* fontawesome */
    mix.copy('public/bower_components/fontawesome/fonts', 'public/fonts'); //build

    mix.copy('public/bower_components/lightbox2/dist/images', 'public/images');
    mix.copy('public/bower_components/lightslider/dist/img', 'public/img');
    mix.copy('resources/assets/js/front/library/ResponsiveImageGallery/images', 'public/images');

    mix.copy('resources/assets/js/front/library/slick/fonts', 'public/css/fonts');
    mix.copy('resources/assets/js/front/library/slick/ajax-loader.gif', 'public/css/ajax-loader.gif');

    mix.copy('public/bower_components/uploadify/uploadify-cancel.png', 'public/img/uploadify-cancel.png'); //build

    mix.styles([
        'admin/library/Intimidatetime.css',
        'admin/library/HoldOn.min.css',
        '../../../public/bower_components/croppie/croppie.css',
        '../../../public/bower_components/fancytree/dist/skin-win8-n/ui.fancytree.min.css',
        '../js/admin/library/jquery-toggles/css/toggles.css',
        '../js/admin/library/jquery-toggles/css/themes/toggles-light.css',
        '../../../public/bower_components/fontawesome/css/font-awesome.min.css',
        '../js/admin/library/uploadify/uploadifive.css',
        '../js/admin/library/select2/dist/css/select2.min.css',
        '../js/admin/library/mcustom-scrollbar/jquery.mCustomScrollbar.css',
        'admin/library/awesome-bootstrap-checkbox.css',
        'admin/library/tippy.css',
        '../../../public/bower_components/owl.carousel/dist/assets/owl.carousel.min.css'
    ], 'public/css/app_admin.css');

    /**
     * biblioteki dla admin
     */

    mix.scripts([
        '../../../public/bower_components/jquery/jquery.js',
        '../../../public/bower_components/bootstrap/dist/js/bootstrap.min.js',
        '../../../public/bower_components/jquery-validation/dist/jquery.validate.js',
        '../../../public/bower_components/handlebars/handlebars.min.js',
        '../../../public/bower_components/tinymce/tinymce.min.js',
        '../../../public/bower_components/tinymce/themes/modern/theme.js',
        '../../../public/bower_components/croppie/croppie.js',
        '../../../public/bower_components/jquery-ui/jquery-ui.min.js',
        '../../../public/bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.js',
        '../../../public/bower_components/fancytree/dist/jquery.fancytree.min.js',
        '../../../public/bower_components/fancytree/dist/src/jquery.fancytree.dnd.js',
        'admin/library/jquery-toggles/toggles.js',
        'admin/library/Intimidatetime.js',
        'admin/library/HoldOn.min.js',
        'admin/library/uploadify/jquery.uploadifive.min.js',
        'admin/library/select2/dist/js/select2.full.min.js',
        'admin/library/mcustom-scrollbar/jquery.mCustomScrollbar.js',
        'admin/library/tippy.js',
        '../../../public/bower_components/owl.carousel/dist/owl.carousel.min.js'
    ], 'public/js/app_admin.js');

    /**
     * wlasne dla admina
     */
    mix.scripts([
        'admin/list.js', 'admin/post.js', 'admin/popup.js', 'admin/validator.js', 'admin/rest.js',
        'admin/galleryvideo.js',
        'admin/slider.js', 'admin/tree.js', 'admin/newsletter.js', 'admin/galleryphoto.js',
        'admin/page.js', 'admin/category.js', 'admin/user.js', 'admin/editor.js', 'admin/download.js',
        'admin/language.js', 'admin/openinghours.js', 'admin/productoffer.js'
    ], 'public/js/admin.js');

    //----------------------------------------------------------------------------------------------

    mix.styles([
        '../../../public/bower_components/magnific-popup/dist/magnific-popup.css',
        '../../../public/bower_components/slick-carousel/slick/slick.css',
        '../../../public/bower_components/normalize-css/normalize.css',
    ], 'public/css/app_front.css');

    mix.styles([
        'front/front.css'
    ], 'public/css/front.css');
    /**
     * biblioteki dla front
     */

    mix.scripts([
        '../../../public/bower_components/jquery/jquery.js',
        '../../../public/bower_components/jquery-validation/dist/jquery.validate.js',
        '../../../public/bower_components/magnific-popup/dist/jquery.magnific-popup.js',
        '../../../public/bower_components/slick-carousel/slick/slick.js',
        '../../../public/bower_components/js-cookie/src/js.cookie.js',
    ], 'public/js/app_front.js');

    /**
     * wlasne dla front
     */
    mix.scripts([
        'front/newsletter.js', 'front/rest.js', 'front/category.js', 
        './html/js/modules/photo_lightbox.js',
        './html/js/modules/video_playlist.js',
        './html/js/modules/content.js',
        './html/js/partials/cookies.js',
        './html/js/partials/popup.js',
        './html/js/partials/ajaxnews.js',
        './html/js/partials/mobile_menu.js',
        
    ], 'public/js/front.js');


    mix.scripts([
        'common.js'
    ], 'public/js/common.js');

    // mix.version(['js/admin.js', 'js/app_admin.js', 'css/app_admin.css',
    //     'js/front.js', 'js/app_front.js', 'css/app_front.css',
    //     'js/common.js'
    // ]);

    mix.assets();
    mix.iconfont();
    mix.jadeHandlebars();

    //mix.fonts();
});