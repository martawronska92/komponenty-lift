<?php

namespace App\Observers;

use App\Models\Category;

class CategoryObserver
{
    /**
     * Listen to the Category deleting event.
     *
     * @param  Category  $category
     * @return void
     */
    public function deleting(Category $category)
    {
          $posts = \App\Models\Post::where('category_id','=',$category->id)->pluck('id')->toArray();
          \App\Models\PagePostLanguage::where('entity_type','=','App\Models\Post')->whereIn('entity_id',$posts)->delete();
          \App\Models\PagePostModule::where('entity_type','=','App\Models\Post')->whereIn('entity_id',$posts)->delete();

          $childrenCategories = [];
          $parentCategories = [$category->id];
          do{
            $categories = \App\Models\Category::whereIn('category_parent_id',$parentCategories)->pluck('id')->toArray();
            $parentCategories = $categories;
            $childrenCategories = array_merge($childrenCategories,$categories);
          }while(count($categories) > 0);

          $childrenCategories = array_unique($childrenCategories);

          foreach($childrenCategories as $categoryId){
            $posts = \App\Models\Post::where('category_id','=',$categoryId)->pluck('id')->toArray();
            \App\Models\PagePostLanguage::where('entity_type','=','App\Models\Post')->whereIn('entity_id',$posts)->delete();
            \App\Models\PagePostModule::where('entity_type','=','App\Models\Post')->whereIn('entity_id',$posts)->delete();
          }
    }
}
