<?php

namespace App\Http\Middleware;

use Closure;

class Nonwww
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $host = $request->header('host');
        // if (substr($host, 0, 4) != 'www.') {
        //   $request->headers->set('host', 'www.'.$host);
        //   return \Redirect::to($request->path());
        // }
        return $next($request);
    }
}
