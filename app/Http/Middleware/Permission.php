<?php

namespace App\Http\Middleware;

use Closure;

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $moduleSymbol = '')
    {
        if(!\App\Models\User::canAccess($moduleSymbol)){
          abort(403,"",[
            'header' => trans('menu.'.$moduleSymbol)
          ]);
        }else{
          return $next($request);
        }
    }
}
