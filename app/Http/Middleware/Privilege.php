<?php

namespace App\Http\Middleware;

use Closure;

class Privilege
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$privilegeSymbol = '')
    {
      if(!\App\Models\User::hasPrivilege($privilegeSymbol)){
        abort(403, "",[
          "header" => trans('menu.'.$privilegeSymbol)
        ]);
      }else{
        return $next($request);
      }
    }
}
