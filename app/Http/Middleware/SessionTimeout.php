<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Session\Store;

class SessionTimeout
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

       $cookies = \Cookie::get();
       $rememberMe = false;
       foreach($cookies as $name=>$value){
            preg_match('/remember\_web\_.*$/',$name,$matches);
            if(count($matches) > 0)
              $rememberMe = true;
        }
        $request->session()->put('rememberMe',(int)$rememberMe);

        if(!$rememberMe){
          $isLoggedIn = $request->path() != 'dashboard/logout';
          if(!session('lastActivityTime'))
            $request->session()->put('lastActivityTime', time());
          elseif(time() - $request->session()->get('lastActivityTime') > \Config::get('cms.admin_timeout')){
            $request->session()->forget('lastActivityTime');
            $email = $request->user()->email;
            auth()->logout();

            $msg = trans('admin.user.no_activity',['period'=>\Config::get('cms.admin_timeout')/60]);
            \App\Components\MessageBox::set(1,$msg,'main');
            return redirect()
                ->route('login')
                ->withInput(compact('email'));
          }
          $isLoggedIn ? $request->session()->put('lastActivityTime', time()) : $request->session()->forget('lastActivityTime');
        }
        return $next($request);
    }
}
