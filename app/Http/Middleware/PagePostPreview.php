<?php

namespace App\Http\Middleware;

use Closure;

class PagePostPreview
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $entityType = $request->segment(2);
        $hash = $request->segment(4);

        $entityId = \DB::table($entityType)->where('draft','=',$hash)->pluck('id');

        if(count($entityId)>0){
            \App\Models\PagePostLanguage::where('entity_id','=',$entityId)->where('entity_type','=','App\Models\\'.ucfirst($entityType))->delete();
            $pagePostModules = \App\Models\PagePostModule::with(['module'])->where('entity_id','=',$entityId)->where('entity_type','=','App\Models\\'.ucfirst($entityType))->get();
            foreach($pagePostModules as $module){
              if($module->draft){
                $entityModel = new $module->module->module_entity_model();
                $method = "deleteObject";
                if(method_exists($entityModel,$method)){
                  $isPreview = strpos($request->route()->getName(),'preview')!==FALSE;
                  $entityModel->$method($module->module_entity_id,$isPreview);
                }
              }
              \App\Models\PagePostModule::where('id','=',$module->id)->delete();
            }
            \DB::table($entityType)->where('id','=',$entityId)->delete();
        }

        return $response;
    }
}
