<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\SettingsGroup;

class SettingsController extends AdminController
{
    public function __construct(){
    	parent::__construct();
      $this->middleware('privilege:settings');
      $this->setActiveMenu(['settings']);
    }

    public function index(){
      $this->setBreadcrumbs('settings');
      $model = new \App\Models\SettingsGroup();
      return view('admin.settings.index')->with([
        'header' => trans('menu.settings'),
        'list' => $model->getAll(),
        'langs' => \App\Models\Language::getActive(),
        'openinghours' => \App\Models\OpeningHours::all()
      ]);
    }

    public function save(Request $request){
      $model = new \App\Models\Settings();
      $result = $model->saveSettings($request->input());
      return $this->setMessageBoxAfterRedirect((int)$result, route('settings'), 'admin.update_'.($result ? 'success' : 'failure'));
    }
}
