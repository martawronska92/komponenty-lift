<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\ModuleController;

set_time_limit(0);

class OfferController extends AdminController
{
    var $moduleCategoryController;
    var $modulePostController;

    public function __construct(){
      parent::__construct();
      $this->moduleCategoryController = new \App\Http\Controllers\Admin\ModuleCategoryController(['offer'],'offer');
      $this->modulePostController = new \App\Http\Controllers\Admin\ModulePostController(['offer'],'offer','product');
    }

    public function generateList($rows,$categoryParentId = false){

      $list = new \App\Components\Clist\Clist();
      $list->header = trans('admin.'.$this->moduleCategoryController->module.'.header');
      $list->sortable = "category";
      $list->buttons = [
        new \App\Components\Clist\ListButtonAdd('admin.'.$this->moduleCategoryController->module.'.add_button',"",$this->moduleCategoryController->module."/create".($categoryParentId ? "/".$categoryParentId : "")),
        new \App\Components\Clist\ListButton('admin.offer.import',$this->moduleCategoryController->module."/import","button button--small button--blue button--withIcon button--iconDownload"),
        new \App\Components\Clist\ListButton('admin.offer.export',$this->moduleCategoryController->module."/export","button button--small button--blue button--withIcon button--iconUpload")
      ];
      $list->actions = [
        new \App\Components\Clist\ListActionEdit($this->moduleCategoryController->module),
        new \App\Components\Clist\ListAction('admin.'.$this->moduleCategoryController->module.'.showposts',$this->moduleCategoryController->module.'/category/{id}','button button--small button--blue button--withIcon button--iconList',false,''),
        new \App\Components\Clist\ListAction('admin.'.$this->moduleCategoryController->module.'.showsubcategory',$this->moduleCategoryController->module.'/{id}','button button--small button--blue button--withIcon button--iconTree',false,''),
        new \App\Components\Clist\ListActionDelete($this->moduleCategoryController->module,
          trans('admin.'.$this->moduleCategoryController->module.'.delete_category').\App\Helpers\Format::confirmDeleteName(' {name}')." ? ".trans('admin.category.delete_category_warning'))
      ];
      $list->columns = [
        new \App\Components\Clist\ListColumn('name','admin.list.name'),
        new \App\Components\Clist\ListColumn('post_counter','admin.'.$this->moduleCategoryController->module.'.post_counter',false,function($row){
          echo \App\Components\Clist\ListColumnFormat::showBadge($row[0]->post_counter);
        }),
        new \App\Components\Clist\ListColumn('subcategory_counter','admin.'.$this->moduleCategoryController->module.'.subcategory_counter',false,function($row){
          echo \App\Components\Clist\ListColumnFormat::showBadge($row[0]->subcategory_counter);
        })
      ];
      $list->rows = $rows;
      return $list->render();
    }

    public function index(Request $request){
      //return $this->moduleCategoryController->index($request);
      $this->setBreadcrumbs($this->moduleCategoryController->module,['categoryParentId' => $request->categoryParentId]);
      return view('admin.components.clist.index')->with([
        'list' => $this->generateList($this->moduleCategoryController->model->getList($this->moduleCategoryController->module,$request->categoryParentId),$request->categoryParentId)
      ]);
    }

    public function create($categoryParentId = false){
      return $this->moduleCategoryController->create($categoryParentId);
    }

    public function edit($id){
      return $this->moduleCategoryController->edit($id);
    }

    public function delete($id){
      return $this->moduleCategoryController->delete($id);
    }

    public function store(Request $request,$id = false){
      return $this->moduleCategoryController->store($request,$id);
      //return $this->setMessageBoxAfterRedirect($response->result, $response->route, $response->msg);
    }

    public function category($id){
      return $this->modulePostController->index($id);
    }

    public function productcreate($id){
      return $this->modulePostController->create($id);
    }

    public function productedit($id){
      return $this->modulePostController->edit($id);
    }

    public function productstore(Request $request, $id = false){
      return $this->modulePostController->store($request,$id);
    }

    public function productdelete($id){
      return $this->modulePostController->delete($id);
    }

    public function import(){
      $this->setBreadcrumbs('offer_import');
      return view('admin.offer.import',[
        'header' => trans('admin.offer.import')
      ]);
    }

    public function saveImport(Request $request){
      $model = new \App\Models\PostData();
      $response = $model->import($request->all());
      return $this->setMessageBoxAfterRedirect($response->code,route('offer'),$response->msg);
    }

    public function export(){
      $model = new \App\Models\Offer();
      return \Response::download($model->export())->deleteFileAfterSend(true);
    }
}
