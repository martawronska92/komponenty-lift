<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{

  var $commonModel;

  public function __construct(){
    $this->middleware(function ($request, $next) {
      \App\Components\MessageBox::get();
      return $next($request);
    });
    $this->commonModel = new \App\Models\Base();
    \View::share("title",\App\Models\Settings::getBySymbol('contact_name'));
  }


  /**
   * nadaje active dla menu
   * @param array $items tablica aliasów z menu
   */
  public function setActiveMenu($items){
    \View::share('activeMenu',$items);
  }

  /**
   * ustawia breadcrumbsy
   * @param string  $name  nazwa zarejestrowanego breadcrumbsa
   * @param boolean/object $object opcjonalny obiekt do pobierania danych do breadcrumbsa
   */
  public function setBreadcrumbs($name,$object = false){
    \View::share('breadcrumbsName',$name);
    if($object!=false){
      \View::share('breadcrumbsObject',$object);
    }
  }

  /**
   * pokazuje box z wiadomością po przekierowaniu
   * @param int $msgType  0 - error, 1 - success, 2 - info
   * @param string $url url na który przekierowanie
   * @param string $msg   wiadomość do tłumaczenia
   * @param string $viewType miejsce wyświetlenia wiadomości main/list
   */
  public function setMessageBoxAfterRedirect($msgType,$url,$msg,$viewType = 'main'){
    \App\Components\MessageBox::set((int)$msgType,$msg,$viewType);
    return redirect($url)->withInput();
  }

  /**
   * ustawia tytuł strony
   * @param string $title tytuł
   */
  public function setTitle($title = ""){
    \View::share('metaTitle',trans($title));
  }
}
