<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagController extends AdminController
{

  public function __construct(){
    parent::__construct();
    $this->middleware('permission:tag');
    $this->setActiveMenu(['tag']);
    $this->model = new \App\Models\Tag();
  }

  public function generateList($rows){
    $list = new \App\Components\Clist\Clist();
    $list->header = trans('admin.tag.header');
    $list->buttons = [
      new \App\Components\Clist\ListButtonAdd('admin.tag.add_button','tag'),
    ];
    $list->actions = [
      new \App\Components\Clist\ListActionEdit('tag'),
      new \App\Components\Clist\ListActionDelete('tag',
          trans('admin.tag.delete_tag').\App\Helpers\Format::confirmDeleteName(' {name}')." ?")
    ];
    $list->columns = [
      new \App\Components\Clist\ListColumn('name','admin.list.name')
    ];
    $list->rows = $rows;
    return $list->render();
  }

  public function index(Request $request){
    return view('admin.components.clist.index')->with([
      'breadcrumbs' => $this->setBreadcrumbs('tag'),
      'list' => $this->generateList($this->model->getList($request->input()))
    ]);
  }

  public function getCommonData(){
    return [
      'langs' => \App\Models\Language::getActive()
    ];
  }

  public function create(){
      $this->setBreadcrumbs('tag_create');
      $params = $this->getCommonData();
      $params['header'] = trans('admin.tag.new_tag');
      return view('admin.tag.add')->with($params);
  }

  public function edit($id){
    if(!$this->commonModel->checkIfExists($id,'tag')){
      return $this->setMessageBoxAfterRedirect(0, route('tag'), 'admin.tag.no_exists','list');
    }else{
      $params = $this->getCommonData();
      $params['row'] = \App\Models\Tag::where('id','=',$id)->with(['language'])->first();
      $params['header'] = trans('admin.tag.edit_tag');
      $this->setBreadcrumbs('tag_edit',['tag' => $params['row']]);
      return view('admin.tag.add')->with($params);
    }
  }

  public function store(Request $request,$id = false){
      $data = $request->all();

      foreach($request->get('name') as $key => $val)
      {
        $rules['name.'.$key] = 'required|max:255';
      }

      \Validator::make($data,$rules)->setAttributeNames($this->model->getAttributesNames())->validate();

      $result = $this->model->storeTag($data,$id);
      $msg = $result->code ? trans('admin.update_success') : trans('admin.update_failure');

      if($result->code){
        \App\Models\Sitemap::generate();
      }

      return $this->setMessageBoxAfterRedirect($result->code, route('tag'), $result->msg,'list');
  }

  public function storeFromPost(Request $request){
      parse_str($request->input('params'),$params);
      $result = $this->model->storeTag($params);
      if($result->code){
          $result->tags = \App\Models\Tag::getInCurrentLang();
      }
      return response()->json($result);
  }

  public function delete($id){
      $response = $this->commonModel->deleteMulti($id,'tag');
      $route = route('tag');

      if($response->code){
        \App\Models\Sitemap::generate();
      }

      return $this->setMessageBoxAfterRedirect($response->code, $route , 'admin.tag.delete_'.($response->code ? 'success' : 'failure').'|'.$response->itemCounter,'list');
  }
}
