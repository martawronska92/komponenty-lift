<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\SliderItem;

class SliderController extends AdminController
{
    private $model;

    public function __construct(){
      parent::__construct();
      $this->middleware('permission:slider');
      $this->setActiveMenu(['module','slider']);
      $this->model = new \App\Models\Slider();
    }

    public function index(){
      $rows = $this->model->getList();

      $list = new \App\Components\Clist\Clist();
      $list->header = trans('menu.slider');
      $list->actions = [
        new \App\Components\Clist\ListAction('admin.slider.show_slides','slider/slides/{id}','button button--small button--blue button--withIcon button--iconEye',false,''),
        new \App\Components\Clist\ListAction('admin.slider.add_slide','slider/create/{id}','button button--small button--green button--withIcon button--iconAdd',false,''),
      ];
      $list->columns = [
        new \App\Components\Clist\ListColumn('name','admin.list.name',false,function($row){
          echo trans($row[0]->name);
        }),
        new \App\Components\Clist\ListColumn('slides_counter','admin.slider.slides_counter',false,function($row){
		  echo \App\Components\Clist\ListColumnFormat::showBadge($row[0]->slides_counter);
        })
      ];
      $list->rows = $rows;

      $this->setBreadcrumbs('slider');
      return view('admin.components.clist.index')->with([
        'list' => $list->render()
      ]);
    }

    public function slides($sliderId){
      if(!$this->commonModel->checkIfExists($sliderId,'slider')){
        return $this->setMessageBoxAfterRedirect(0, route('slider'), 'admin.slider.no_exists','list');
      }else{
        $rows = $this->model->getSlidesFromSlider($sliderId);

        $list = new \App\Components\Clist\Clist();
        $list->sortable = 'slider';
        // $list->multiActions = [
        //   new \App\Components\Clist\ListMultiAction(route('slider_delete'),'admin.delete_multi','admin.slider.delete_multi_question')
        // ];
        $list->buttons = [
          new \App\Components\Clist\ListButton('admin.slider.add_slide','slider/create/'.$sliderId,'button button--small button--green button--withIcon button--iconAdd')
        ];
        $list->actions = [
          new \App\Components\Clist\ListActionEdit('slider'),
          new \App\Components\Clist\ListActionDelete('slider',
              trans('admin.slider.delete_item').\App\Helpers\Format::confirmDeleteName(' {title_1}')." ?")
        ];
        $list->columns = [
          new \App\Components\Clist\ListColumn('ext','admin.list.preview',false,function($row){
            echo "<img width='140' src='/img/slider/".$row[0]->id."/thumbnail.".$row[0]->ext."'/>";
          }),
          new \App\Components\Clist\ListColumn('title_1','admin.list.title',false,function($row){
            $title = "";
            $title.= $row[0]->title_1;
            if($row[0]->title_1!="" && $row[0]->title_2!="") $title.= "<br/>";
            $title.= $row[0]->title_2;
            echo trim($title);
          }),
          new \App\Components\Clist\ListColumn('link','admin.list.link',false,function($row){
            $type = (int)$row[0]->external;
            if($type == -1){
              echo trans('admin.none');
            }else if($row[0]->external)
              echo $row[0]->link;
            else{
              echo \App\Models\Page::getById($row[0]->link)->language[0]->title;
            }
          }),
          new \App\Components\Clist\ListColumn('active','admin.list.active',false,function($row){
            echo \App\Components\Clist\ListColumnFormat::switchValue($row[0]->active,'slider_item',$row[0]->id,'active');
          })
        ];
        $list->rows = $rows;

        $slider = \App\Models\Slider::find($sliderId);

        $list->header = ucfirst(trans($slider->name));

        $this->setBreadcrumbs('slides',['slider' => $slider]);
        return view('admin.components.clist.index')->with([
          'list' => $list->render(),
          'slider' => $slider
        ]);
      }
    }

    protected function getCommonData(){
      $pageModel = new \App\Models\Page();
      return [
        'langs' => \App\Models\Language::getActive(),
        'pages' => $pageModel->getForSelect(),
        'settings' => \App\Models\Settings::getByGroup('images')
      ];
    }

    public function create($sliderId){
      if(!$this->commonModel->checkIfExists($sliderId,'slider')){
        return $this->setMessageBoxAfterRedirect(0, route('slider'), 'admin.slider.no_exists','list');
      }else{
        $slider = \App\Models\Slider::where('id','=',$sliderId)->first();
        $this->setBreadcrumbs('addslider',['slider' => $slider]);
        $params = $this->getCommonData();
        $params['header'] = trans('admin.slider.header_add');
        $params['sliderId'] = $sliderId;
        $params['slider'] = $slider;

        return view('admin.slider.add')->with($params);
      }
    }

    public function edit($id){
      if(!$this->commonModel->checkIfExists($id,'slider_item')){
        return $this->setMessageBoxAfterRedirect(0, route('slider'), 'admin.slider.slide_no_exists','list');
      }else{
        $params = $this->getCommonData();
        $params['id'] = $id;
        $params['row'] = \App\Models\SliderItem::where('id','=',$id)->with('language')->first();
        $params['slider'] = \App\Models\Slider::where('id','=',$params['row']->slider_id)->first();
        $params['header'] = trans('admin.slider.header_edit');
        $this->setBreadcrumbs('editslider',['slider' => $params['slider'], 'slide' => $params['row']]);
        return view('admin.slider.add')->with($params);
      }
    }

    public function store(Request $request,$id = false){

      $data = $request->input();
      $rules = [];

      if((int)$data['external']==0){
        $rules = [
          'internal_content' => 'required'
        ];
      }else if((int)$data['external']==1){
        $rules = [
          'external_content' => 'required'
        ];
      }

      if(!$id){
        $rules['thumbnail_encoded'] = 'required';
      }

      \Validator::make($request->input(),$rules)->setAttributeNames($this->model->getAttributesNames())->validate();

      $result = $this->model->store($request->input(),$id);

      $route = route('slides',['id'=>$result->sliderId]);
      $msg = 'admin.'.($id ? 'update' : 'slider.add').'_';

      return $this->setMessageBoxAfterRedirect($result->code,$route, $msg.($result->code ? 'success' : 'failure'),'list');
    }

    public function delete($id){
      $ids = is_array($id) ? $id[0] : explode(",",$id);

      $sliderId = $this->model->getSliderId($ids);

      $imagesToDelete = [];
      foreach($ids as $id){
          $item = \App\Models\SliderItem::where('id','=',$id)->first();
          if($item){
            $ext = explode("?",$item->ext);
            $directoriesToDelete[] = public_path()."/img/slider/".$id;
          }
      }

      $response = $this->commonModel->deleteMulti($ids,'slider_item');

      if($response->code){
        foreach($directoriesToDelete as $dir){
          \File::deleteDirectory($dir);
        }
      }

      $route = route('slides',['sliderId'=>$sliderId]);
      return $this->setMessageBoxAfterRedirect($response->code, $route , 'admin.slider.delete_'.($response->code ? 'success' : 'failure').'|'.$response->itemCounter,'list');
    }

    public function setposition(Request $request){
      $data = $request->input();
      foreach($data['ids'] as $position=>$slideId)
        \App\Models\SliderItem::where('id','=',$slideId)->update(['position'=>$position]);
    }
}
