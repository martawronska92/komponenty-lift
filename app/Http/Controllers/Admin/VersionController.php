<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

set_time_limit(0);

class VersionController extends AdminController
{
    var $model;
    var $component;

    public function __construct(){
      parent::__construct();
      $this->middleware('permission:user');
      $this->model = new \App\Models\VersionUpdate();
      $this->setBreadcrumbs('version_update');
    }

    public function update(){
      if($_SERVER['HTTP_HOST'] == \Config::get('cms.update_host')){
        $results = $this->model->updateSystem();
        return view('admin.version.update',[
          'header' => trans('admin.version.updates'),
          'results' => $results
        ]);
      }else{
        return redirect()->route('dashboard');
      }
    }

    public function migrate(){
      $this->model->migrate();
    }
}
