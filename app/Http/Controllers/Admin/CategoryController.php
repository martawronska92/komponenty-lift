<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

set_time_limit(0);

class CategoryController extends AdminController
{

  private $model;

  public function __construct(){
    parent::__construct();
    $this->model = new \App\Models\Category();
  }

  public function getCommonData($categoryViews = "category/view", $postViews = "post/view"){
    return [
      'category_view' => $this->commonModel->getViews($categoryViews),
      'post_view' => $this->commonModel->getViews($postViews),
      'langs' => \App\Models\Language::getActive(),
      'loading_types' => ['show_all_posts','show_button','show_swipe','show_number']
    ];
  }

  public function edit($pagePostModuleId,$id){
    if(!$this->commonModel->checkIfExists($pagePostModuleId,'page_post_module')){
      return $this->setMessageBoxAfterRedirect(0, route('dashboard'), 'admin.module.no_exists');
    }
    else{
      $params = $this->getCommonData();
      $params['redirect_url'] = \App\Models\PagePostModule::getRedirectUrl($pagePostModuleId);

      if(!$this->commonModel->checkIfExists($id,'category')){
        return $this->setMessageBoxAfterRedirect(0, $params['redirect_url'], 'admin.category.no_exists');
      }else{
        $params['row'] = \App\Models\Category::where('id','=',$id)->with(['language','field'])->first();

        foreach($params['row']->language as $lang){
          if($lang->language->symbol == \App::getLocale())
            $params['header'] = $lang->name;
        }

        $params['id'] = $id;
        $params['pagePostModuleId'] = $pagePostModuleId;
        $this->setBreadcrumbs('category_edit',$this->createBreadcrumbsObject($pagePostModuleId,$id));
        return view('admin.category.add')->with($params);
      }
    }
  }

  public function createBreadcrumbsObject($pagePostModuleId,$id){
    return [
      'pagePostModuleId' => $pagePostModuleId,
      'category_id' => $id
    ];
  }

  public function store(Request $request,$pagePostModuleId,$id = false){
    $data = $request->input();

    $rules = [
      'per_page' => 'required|numeric|min:1',
      'loading_type' => 'required'
    ];

    if(array_key_exists('module_id',$data)){
      $rules['name'] = 'required|max:255';
      foreach($request->get('name') as $key => $val)
      {
        $rules['name.'.$key] = 'required|max:255';
      }
    }

    \Validator::make($request->all(),$rules)->setAttributeNames($this->model->getAttributesNames())->validate();

    $result = $this->model->saveCategory($data,$id);
    $msg = 'admin.'.($id ? 'update' : 'category.add').'_';
    $redirect_url = array_key_exists('save_and_stay',$data) ? route('category_edit',['pagePostModuleId'=>$pagePostModuleId,'id'=>$result->id]) : \App\Models\PagePostModule::getRedirectUrl($pagePostModuleId);

    if($result->code){
      \App\Models\Sitemap::generate();
    }

    return $this->setMessageBoxAfterRedirect($result->code, $redirect_url , $msg.($result->code ? 'success' : 'failure'));
  }

  public function delete($pagePostModuleId,$id){
    if(!$this->commonModel->checkIfExists($id,'category')){
      $redirect_url = \App\Models\PagePostModule::getRedirectUrl($pagePostModuleId);
      return $this->setMessageBoxAfterRedirect(0, $redirect_url, 'admin.category.no_exists');
    }else{
      try{
        $result = $this->model->deleteObject($id);

        if($result){
          \App\Models\Sitemap::generate();
        }

        return $this->setMessageBoxAfterRedirect($result, route('category'), 'admin.category.delete_'.($result ? 'success' : 'failure'),'list');
      }catch(Exception $e){
        return $this->setMessageBoxAfterRedirect(0, route('category'), 'admin.category.delete_failure','list');
      }
    }
  }

  public function setposition(Request $request){
    $ids = $request->input('ids');
    $all = count($ids);
    foreach($ids as $key=>$id){
      $object = \App\Models\Category::find($id);
      $object->position = $all-$key;
      $object->save();
    }
  }

}
