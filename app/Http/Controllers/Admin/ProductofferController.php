<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductofferController extends AdminController
{
  var $model;

  public function __construct(){
    parent::__construct();
    $this->model = new \App\Models\Productoffer();
  }

  protected function getCommonData(){
    return [
      'langs' => \App\Models\Language::getActive(),
      'settings' => \App\Models\Settings::getAll(),
      'products' => []
    ];
  }

  public function generateList($rows,$variables = []){
    $list = new \App\Components\Clist\Clist();
    $list->header = $variables['list_header'];
    $list->columns = [
      new \App\Components\Clist\ListColumn('title','admin.list.title',true),
      new \App\Components\Clist\ListColumn('category_name','admin.post.category',true)
    ];

    $list->rows = $rows;
    $list->view = new \App\Components\Clist\ListView("admin/productoffer/list",$variables);

    $list->search = [
      new \App\Components\Clist\ListSearchColumn('title','admin.list.title'),
      new \App\Components\Clist\ListSearchColumn('category_language.name','admin.post.category')
    ];
    return $list->render();
  }

  public function edit($pagePostModuleId,$id,Request $request){
    if(!$this->commonModel->checkIfExists($pagePostModuleId,'page_post_module')){
      return $this->setMessageBoxAfterRedirect(0, route('dashboard'), 'admin.module.no_exists');
    }
    else{
      $params = $this->getCommonData();
      $moduleDetails = \App\Models\PagePostModule::getDetails($pagePostModuleId);
      if($moduleDetails['entity_type'] == "page") $this->setActiveMenu(['page']);

      $params['redirect_url'] = $moduleDetails['redirect_url'];
      if(!$this->commonModel->checkIfExists($id,'product_offer')){
        return $this->setMessageBoxAfterRedirect(0, $params['redirect_url'], 'admin.productoffer.no_exists');
      }else{
        $params['list_header'] = trans('admin.productoffer.header');
        $params['products'] = \App\Models\ProductofferProduct::where('product_offer_id','=',$id)->get()->pluck('post_id')->toArray();
        $params['list'] = $this->generateList($this->model->getProducts($request->input()),$params);
        $params['row'] = \App\Models\Productoffer::with(['language'])->where('id','=',$id)->first();
        $params['pagePostModuleId'] = $pagePostModuleId;
        $params['id'] = $id;

        $this->setBreadcrumbs('module_edit',[
          'entity_type' => $moduleDetails['entity_type'],
          'entity_id' => $moduleDetails['entity_id'],
          'entity_name' => 'productoffer',
          'pagePostModuleId' => $pagePostModuleId,
          'id' => 0,
          'from_module' => $moduleDetails['from_module'],
          $moduleDetails['entity_type'].'_id' => $moduleDetails['entity_id']
        ]);

        return view('admin.productoffer.add',$params)->render();
      }
    }
  }

  public function store(Request $request,$pagePostModuleId,$id){
    $result = $this->model->saveProducts($request->input(),$pagePostModuleId,$id);

    $moduleDetails = \App\Models\PagePostModule::getDetails($pagePostModuleId);

    return $this->setMessageBoxAfterRedirect($result,$moduleDetails['redirect_url'], 'admin.update_'.($result ? 'success' : 'failure'));
  }

  public function generatepreview(Request $request){
    $response = \App\Models\PagePostModule::duplicateEntity($request->input('pagePostModuleId'));

    $data = \App\Helpers\Parse::parseSerialize($request->input('form'));

    $newProductOfferId = $this->model->createEmpty();
    $result = $this->model->saveProducts($data,$request->input('pagePostModuleId'),$request->input('id'));
    $moduleDetails = \App\Models\PagePostModule::getDetails($request->input('pagePostModuleId'));
    \App\Models\Module::switchModuleIdInPreview($moduleDetails['entity_id'],$moduleDetails['entity_type'],$request->input('id'),$newProductOfferId);

    return response()->json($response);
  }
}
