<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Carbon\Carbon;

class DashboardController extends AdminController
{

  public function __construct(){
    parent::__construct();
    $this->setActiveMenu(['mainpage']);
    $this->setBreadcrumbs('admin');
  }

  public function index(){
    config([
      'laravel-analytics.siteId' => \App\Models\Settings::where('symbol','=','analytics_page_view_id')->first()->value
    ]);

    /**
     * deviceCategory
     */
    try{
      $answer = \Analytics::performQuery(Carbon::today()->subDays(7),Carbon::today(),'ga:visits',['dimensions' => 'ga:deviceCategory']);
      $deviceCategory = $answer->rows;
    }catch(\Exception $e){
      $deviceCategory = [];
    }

    $devices = new \stdClass();
    $devices->sum = 0;
    $devices->list = [];
    if(is_array($deviceCategory)){
      foreach($deviceCategory as $row){
        $devices->sum+= (int)$row[1];
        $devices->list[$row[0]] = (int)$row[1];
      }
    }
    arsort($devices->list);

    /**
     * geo
     */
    try{
      $answer = \Analytics::performQuery(Carbon::today()->subDays(7),Carbon::today(),'ga:visits',['dimensions' => 'ga:countryIsoCode,ga:country,ga:city']);
      $geo = $answer->rows;
      $analyticsResults = \Analytics::getVisitorsAndPageViews(7);
      $topPages = \Analytics::getMostVisitedPages(7,12);
      $active = \Analytics::getActiveUsers();
      $activeUsers = 0;
    }catch(\Exception $e){
       $geo = [];
       $analyticsResults = [];
       $activeUsers = 0;
       $topPages = [];
    }

    $analyticsResponse = [];

    $sumUsers = 0;
    if(count($analyticsResults) > 0){
      foreach($analyticsResults as $key=>$row){
        $analyticsResponse[] = [$row['date']->toDateString(),(int)$row['visitors'],"",(int)$row['pageViews'],""];
        $sumUsers+= (int)$row['visitors'] + (int)$row['pageViews'];
      }
    }

    foreach($geo as &$point){
      $point[1] = str_replace("'","`",$point[1]);
      $point[2] = str_replace("'","`",$point[2]);
    }

    return view('admin/dashboard/index')->with([
      'header' => trans('admin.admin_panel'),
      'analytics' => json_encode($analyticsResponse,JSON_UNESCAPED_SLASHES),
      'topPages' => $topPages,
      'activeUsers' => $activeUsers,
      'devices' => $devices,
      'geo' => $geo,
      'sumUsers' => $sumUsers
    ]);
  }

  public function admininfo(){
    if(\App\Models\User::isDeveloper()){
      return view('admin.dashboard.admininfo');
    }else{
      return redirect('/panel');
    }
  }

  public function changeLanguage($symbol){
    \App\Models\Language::change($symbol);
    return back();
  }

}
