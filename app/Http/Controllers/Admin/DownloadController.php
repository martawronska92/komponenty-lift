<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class DownloadController extends AdminController
{
  public function __construct(){
    parent::__construct();
    $this->model = new \App\Models\Download();
  }

  public function getCommonData(){
    return [
      'langs' => \App\Models\Language::getActive()
    ];
  }

  public function create(){
    $params = $this->getCommonData();
    return view('admin.download.add',$params)->render();
  }

  public function edit($pagePostModuleId,$id){

    if(!$this->commonModel->checkIfExists($pagePostModuleId,'page_post_module')){
      return $this->setMessageBoxAfterRedirect(0, route('dashboard'), 'admin.module.no_exists');
    }
    else{
      $params = $this->getCommonData();
      $moduleDetails = \App\Models\PagePostModule::getDetails($pagePostModuleId);
      $params['redirect_url'] = $moduleDetails['redirect_url'];

      $this->setActiveMenu($moduleDetails['from_module'] ? [$moduleDetails['from_module']->symbol] : ['page']);

      if(!$this->commonModel->checkIfExists($id,'download')){
        return $this->setMessageBoxAfterRedirect(0, $params['redirect_url'], 'admin.download.no_exists');
      }else{
        $params['info'] = \App\Models\Download::where('id','=',$id)->with(['language'])->first();
        $params['row'] = \App\Models\DownloadFile::where('download_id','=',$id)->with(['language' => function($query){
          $query->whereHas('language',function($query){
            $query->where('symbol','=',\App::getLocale());
          });
        }])
        ->orderBy('position','desc')
        ->get();
        $params['header'] = trans('admin.download.edit_header');
        $params['id'] = $id;
        $params['pagePostModuleId'] = $pagePostModuleId;

        $this->setBreadcrumbs('module_edit',[
          'entity_type' => $moduleDetails['entity_type'],
          'entity_id' => $moduleDetails['entity_id'],
          'entity_name' => 'download',
          'from_module' => $moduleDetails['from_module'],
          'pagePostModuleId' => $pagePostModuleId,
          'id' => $id,
          $moduleDetails['entity_type'].'_id' => $moduleDetails['entity_id']
        ]);
        return view('admin.download.add')->with($params);
      }
    }
  }


  public function saveDownload(Request $request,$pagePostModuleId,$id){
    $data = $request->all();

    if(array_key_exists('form',$data)){
      /**
       * zapisuje dodawany plik
       */
       $data = $request->all();
       $response = $this->model->storeFile($data,$id);
       return response()->json($response);
    }else{
      /**
       * zapisuje dane downloadu
       */
      $response = $this->model->storeData($data,$id);
      $moduleDetails = \App\Models\PagePostModule::getDetails($pagePostModuleId);
      return $this->setMessageBoxAfterRedirect($response->code,$moduleDetails['redirect_url'],'admin.update_'.($response->code ? 'success' : 'failure'));
    }

  }

  public function setposition(Request $request){
    $ids = $request->input('ids');
    $all = count($ids);
    foreach($ids as $key=>$id){
      $object = \App\Models\DownloadFile::find($id);
      $object->position = $all-$key;
      $object->save();
    }
  }

  /**
   * usunięcie całego downloadu
   */
  public function delete($ids){
    return response()->json($this->model->deleteMulti($ids));
  }

  /**
   * usunięcie poszczególnych plików z downloadu
   */
  public function deleteFiles($ids){
    $model = new \App\Models\DownloadFile();
    return response()->json($model->deleteMulti($ids));
  }

  public function getfile($id){
    $file = \App\Models\DownloadFile::with(['language'=>function($query){
      $query->whereHas('language',function($query){
        $query->where('symbol','=',\App::getLocale());
      });
    }])->where('id','=',$id)->first();

    if($file){
      $name = $file->language[0]->title !="" ? $file->language[0]->title : $file->filename_slug;
      $filePath = rtrim(public_path(),"/")."/file/download/".$file->filename_hashed.".".$file->ext;
      if(!\File::exists($filePath)){
        return $this->setMessageBoxAfterRedirect(0,\URL::previous(),'admin.download.no_exists');
      }else{
        return response()->download($filePath,$name.".".$file->ext);
      }
    }else{
      return $this->setMessageBoxAfterRedirect(0,\URL::previous(),'admin.download.no_exists');
    }
  }

  public function getdata($id = false){
    $response = new \stdClass();
    $response->code = 0;

    if($id == false){
      $response->code = 1;
      $response->view = view('admin.download.partial.form',[
        'langs' => \App\Models\Language::getActive(),
      ])->render();
    }else if(!$this->commonModel->checkIfExists($id,'download_file')){
      $response->msg = 'admin.download.object_no_exists';
    }else{
      $response->code = 1;
      $response->view = view('admin.download.partial.form',[
        'file' => \App\Models\DownloadFile::with('language')->where('id','=',$id)->first(),
        'langs' => \App\Models\Language::getActive(),
      ])->render();

    }

    return response()->json($response);
  }

  public function generatepreview(Request $request){
    $response = \App\Models\PagePostModule::duplicateEntity($request->input('pagePostModuleId'));

    $data = \App\Helpers\Parse::parseSerialize($request->input('form'));

    $newDownloadId = $this->model->createEmpty();
    $this->model->storeData($data,$newDownloadId);

    \App\Models\Module::switchModuleIdInPreview($response->entity_id,$response->entity_type,$request->input('id'),$newDownloadId);

    $files = \App\Models\DownloadFile::with(['language'])->where('download_id','=',$request->input('id'))->get();
    foreach($files as $file){
      $file = $file->toArray();

      $oldFileId = $file['id'];
      $languages = $file['language'];
      unset($file['language']);
      unset($file['id']);
      $file['download_id'] = $newDownloadId;

      $newFileDownloadId = \App\Models\DownloadFile::insertGetId($file);

      foreach($languages as $lang){
        unset($lang['id']);
        $lang['download_file_id'] = $newFileDownloadId;
        \App\Models\DownloadFileLanguage::create($lang);
      }
    }

    return response()->json($response);
  }
}
