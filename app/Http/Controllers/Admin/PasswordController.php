<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PasswordController extends AdminController
{

    public function change(){
      $this->setBreadcrumbs('password_reset');

      return view('admin.password.change',[
        'header' => trans('admin.password.reset_header')
      ]);
    }

    public function store(Request $request){
      $data = $request->input();

      $rules = [
        'old_password' => 'required',
        'new_password' => 'required|min:8|regex:"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$"',
        'new_password_confirm' => 'required|same:new_password|min:8|regex:"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$"'
      ];

      \Validator::make($data,$rules)->setAttributeNames([
        'old_password' => trans('admin.password.old_password'),
        'new_password' => trans('admin.password.new_password'),
        'new_password_confirm' => trans('admin.password.new_password_confirm')
        ])->validate();

      if(!\Hash::check($data['old_password'],\Auth::user()->password)){
        return $this->setMessageBoxAfterRedirect(0,route('admin_password_change'),trans('admin.password.wrong_old_password'));
      }

      \App\Models\User::where('id','=',\Auth::user()->id)->update(['password' => \Hash::make($data['new_password']) ]);

      return $this->setMessageBoxAfterRedirect(1,route('admin_password_change'),trans('admin.password.update_success'));
    }
}
