<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class ContentController extends AdminController
{
  var $model;

  public function __construct(){
    parent::__construct();
    $this->model = new \App\Models\Content();
  }

  protected function getCommonData(){
    return [
      'langs' => \App\Models\Language::getActive(),
      'settings' => \App\Models\Settings::getAll()
    ];
  }

  public function edit($pagePostModuleId,$id){
    if(!$this->commonModel->checkIfExists($pagePostModuleId,'page_post_module')){
      return $this->setMessageBoxAfterRedirect(0, route('dashboard'), 'admin.module.no_exists');
    }
    else{
      $params = $this->getCommonData();
      $moduleDetails = \App\Models\PagePostModule::getDetails($pagePostModuleId);
      $this->setActiveMenu($moduleDetails['from_module'] ? [$moduleDetails['from_module']->symbol] : ['page']);

      $params['redirect_url'] = $moduleDetails['redirect_url'];

      if(!$this->commonModel->checkIfExists($id,'content')){
        return $this->setMessageBoxAfterRedirect(0, $params['redirect_url'], 'admin.content.no_exists');
      }else{
        $params['header'] = trans('admin.content.header');
        $params['row'] = \App\Models\Content::with(['language'])->where('id','=',$id)->first();
        $params['pagePostModuleId'] = $pagePostModuleId;
        $params['id'] = $id;

        $this->setBreadcrumbs('module_edit',[
          'entity_type' => $moduleDetails['entity_type'],
          'entity_id' => $moduleDetails['entity_id'],
          'entity_name' => 'content',
          'pagePostModuleId' => $pagePostModuleId,
          'id' => $id,
          'from_module' => $moduleDetails['from_module'],
          $moduleDetails['entity_type'].'_id' => $moduleDetails['entity_id']
        ]);

        return view('admin.content.add',$params)->render();
      }
    }
  }

  public function store(Request $request,$pagePostModuleId,$id){
    $data = $request->input();

    $result = $this->model->saveContent($data,$id);

    $moduleDetails = \App\Models\PagePostModule::getDetails($pagePostModuleId);

    return $this->setMessageBoxAfterRedirect($result,$moduleDetails['redirect_url'], 'admin.update_'.($result ? 'success' : 'failure'));
  }

  public function uploadphoto(Request $request){
    $data = $request->all();

    $dir = 'img/content';
    $name = str_random(8)."_".snake_case($data['photo']->getClientOriginalName());

    $response = new \stdClass();
    $response->code = 0;

    try{
        $data['photo']->storeAs($dir, $name,'all');
        $response->code = 1;
        $response->file = "http://".$_SERVER['HTTP_HOST']."/".$dir."/".$name;
    }catch(Exception $e){
        $response->msg = "";
    }
    return response()->json($response);
  }

  public function generatepreview(Request $request){
    $response = \App\Models\PagePostModule::duplicateEntity($request->input('pagePostModuleId'));

    $data = \App\Helpers\Parse::parseSerialize($request->input('form'));

    $newContentId = $this->model->createEmpty();
    $this->model->saveContent($data,$newContentId);
    \App\Models\Module::switchModuleIdInPreview($response->entity_id,$response->entity_type,$request->input('id'),$newContentId);

    return response()->json($response);
  }

  public function getjslang(){
    return response()->json(trans('admin_js'));
  }
}
