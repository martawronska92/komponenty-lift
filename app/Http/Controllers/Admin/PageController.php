<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Module;

class PageController extends AdminController
{

    private $model;

    public function __construct(){
      parent::__construct();
      $this->middleware('privilege:page');
      $this->setActiveMenu(['page']);
      $this->model = new \App\Models\Page();
    }

    public function generateList($rows,$parentPageId = null){
      $list = new \App\Components\Clist\Clist();
      $list->sortable = "page";
      $list->header = trans('menu.page');

      if($parentPageId!=null){
        $allowAdd = \App\Models\Page::where('id','=',$parentPageId)->first()->level<2;
      }else{
        $allowAdd = true;
      }

      if($allowAdd){
        $list->buttons = [
            new \App\Components\Clist\ListButtonAdd('admin.page.add_button','','page/create'.($parentPageId ? '/'.$parentPageId : '')),
        ];
      }

      $list->actions = [
        new \App\Components\Clist\ListActionEdit('page'),
        new \App\Components\Clist\ListAction('admin.page.show_subpages','page/{id}',"button button--small button--blue button--withIcon button--iconList",false,""),
        new \App\Components\Clist\ListActionDelete('page',
            trans('admin.page.delete_page').\App\Helpers\Format::confirmDeleteName(' {title}')." ? ".trans('admin.page.delete_page_warning'))
      ];
      $list->columns = [
        new \App\Components\Clist\ListColumn('title','admin.list.title',false),
        new \App\Components\Clist\ListColumn('title','admin.page.subpage_counter',false,function($row){
          echo \App\Components\Clist\ListColumnFormat::showBadge($row[0]->subpage_counter);
        }),
        new \App\Components\Clist\ListColumn('active','admin.gallery.active',false,function($row){
          echo \App\Components\Clist\ListColumnFormat::switchValue($row[0]->active,'page',$row[0]->id,'active');
        })
      ];

      $list->rows = $rows;

      return $list->render();
    }

    public function index(Request $request){
        $parentPageId = $request->parentPageId;
        $this->setBreadcrumbs('page',['parentPageId'=>$request->parentPageId]);

        if($parentPageId && !\App\Models\User::hasPrivilege('page',$parentPageId)){
          return $this->setMessageBoxAfterRedirect(0, route('page'), 'admin.page.no_rights','list');
        }else{
          return view('admin.components.clist.index')->with([
            'list' => $this->generateList($this->model->getList($request->input(),$parentPageId),$parentPageId)
          ]);
        }
    }

    public function getCommonData(){
      $settings = \App\Models\Settings::getAll();

      return [
        'views' => $this->commonModel->getViews('page/view'),
        'langs' => \App\Models\Language::getActive(),
        'host' => $_SERVER['HTTP_HOST'],
        'modules' => \App\Models\Module::getFor('for_page'),
        'categories' => \App\Models\Category::getActive(),
        'settings' => $settings
      ];
    }

    public function create($parentPageId = false){
      $this->setBreadcrumbs('page_create',['parentPageId'=>$parentPageId]);
      $params = $this->getCommonData();

      $level = $parentPageId!=null ? \App\Models\Page::where('id','=',$parentPageId)->first()->level : 0;

      if($level>=2){
        $route = $parentPageId!=false ? route('page',['parentPageId'=>$parentPageId]) : route('page');
        return $this->setMessageBoxAfterRedirect(0, $route, 'admin.page.level_max','list');
      }else{
        $params['header'] = trans('admin.page.header_add');
        $params['pages'] = $this->model->getActive();
        $params['parentPageId'] = $parentPageId;
        $params['back_url'] = $parentPageId ? route('page',['parentPageId'=>$parentPageId]) : route('page');
        if($parentPageId!=false){
          $params['parentRow'] = \App\Models\Page::where('id','=',$parentPageId)->first();
        }
        return view('admin.page.add')->with($params);
      }
    }

    public function edit($id){
      if(!$this->commonModel->checkIfExists($id,'page')){
        return $this->setMessageBoxAfterRedirect(0, route('page'), 'admin.page.no_exists','list');
      }else if(!\App\Models\User::hasPrivilege('page',$id)){
        return $this->setMessageBoxAfterRedirect(0, route('page'), 'admin.page.no_rights','list');
      }else{
        $params = $this->getCommonData();
        $params['header'] = trans('admin.page.header_edit');
        $params['row'] = \App\Models\Page::where('id','=',$id)->with(['language','module' => function($query){
          $query->orderBy('position','asc');
        }])->first();

        if($params['row'] && ($params['row']->for_developer == 1 && !\App\Models\User::isDeveloper()))
          return redirect()->route('page');

        $params['parsed_modules'] = \App\Models\PagePostModule::parseModules($params['row']->module);
        $params['id'] = $id;
        $params['pages'] = $this->model->getActive($id);
        $params['back_url'] = $params['row']->parent_page_id!="" ? route('page',['parentPageId'=>$params['row']->parent_page_id]) : route('page');
        $this->setBreadcrumbs('page_edit',['page'=>$params['row'],'parentPageId' => $params['row']->parent_page_id]);
        return view('admin.page.add')->with($params);
      }
    }

    public function store(Request $request,$id = false){
      if($id!=false && !\App\Models\User::hasPrivilege('page',$id)){
        return $this->setMessageBoxAfterRedirect(0, route('page'), 'admin.page.no_rights','list');
      }else{
        $data = $request->input();
        $rules = [
          'title' => 'required|max:255',
          'url' => 'required|max:255',
          'meta_title' => 'required|max:255',
          'meta_description' => 'required|max:255'
        ];

        foreach($request->get('title') as $key => $val)
        {
          $rules['title.'.$key] = 'required|max:255';
          $rules['url.'.$key] = 'required|max:255';
          $rules['meta_title.'.$key] = 'required|max:255';
          $rules['meta_description.'.$key] = 'required|max:255';
        }

        \Validator::make($data,$rules)->setAttributeNames($this->model->getAttributesNames())->validate();

        $data = $request->input();

        if(strpos($request->path(),'create') !== false) $id = false;

        $result = $this->model->savePage($data,$id);
        $msg = 'admin.'.($id ? 'update' : 'page.add').'_';
        if(array_key_exists('save_and_stay',$data)){
          $route = route('page_edit',['id'=>$result->id]);
          $viewType = 'main';
        }else{
          $route = $data['parent_page_id'] != "" ? route('page',['parentPageId'=>$data['parent_page_id']]) : route('page');
          $viewType = 'list';
        }

        if($result->code){
          \App\Models\Sitemap::generate();
        }

        return $this->setMessageBoxAfterRedirect($result->code, $route, $msg.($result->code ? 'success' : 'failure'),$viewType);
      }
    }

    public function delete($id){
      if(!\App\Models\User::hasPrivilege('page',$id)){
        return $this->setMessageBoxAfterRedirect(0, route('page'), 'admin.page.no_rights','list');
      }else{
        $oldData = \App\Models\Page::where('id','=',$id)->first();

        $response = $this->model->deletePage($id);

        $route = $oldData->parent_page_id !="" ? route('page',['parentPageId'=>$oldData->parent_page_id]) : route('page');
        $msg = 'admin.page.delete_'.($response->code ? 'success' : 'failure').'|'.$response->itemCounter;

        if($response->code){
          \App\Models\Sitemap::generate();
        }

        return $this->setMessageBoxAfterRedirect($response->code,$route,$msg,'list');
      }
    }

    public function generatepreview(Request $request){
      $data = $request->input();

      parse_str($data['form'],$params);

      $params['draft'] = str_random(40);
      $params['active'] = 1;
      if(array_key_exists('from_id',$data))
        $params['from_id'] = $data['from_id'];

      $result = $this->model->savePage($params);

      $routeParams['hash'] = $params['draft'];
      if(\App\Helpers\Language::checkPrefix())
        $routeParams['frontlanguage'] = \App\Models\Language::checkFrontLanguage();

      $response = new \stdClass();
      $response->code = (int)$result->code;
      $response->msg = $response->code ? route('page_preview', $routeParams) : trans('admin.preview_failure');

      return response()->json($response);
    }

    public function showpreview($hash){
      $page = \App\Models\Page::where('draft','=',$hash)->first();

      if(!$page){
        return $this->setMessageBoxAfterRedirect(0, route('page') , 'admin.post.preview_failure','list');
      }else{
        $controller = new \App\Http\Controllers\Front\PageController();
        return $controller->generatePreview($page->id);
      }
    }

    public function waitforpreview(){
      $controller = new \App\Http\Controllers\Front\PageController();
      return $controller->waitForPreview();
    }

    public function setposition(Request $request){
      $ids = $request->input('ids');
      $all = count($ids);
      foreach($ids as $key=>$id){
        $object = \App\Models\Page::find($id);
        $object->position = $all-$key;
        $object->save();
      }
    }
}
