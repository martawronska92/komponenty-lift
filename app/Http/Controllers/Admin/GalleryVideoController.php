<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\GalleryShowType;

class GalleryVideoController extends AdminController
{
    private $model;

    public function __construct(){
      parent::__construct();
      $this->model = new \App\Models\GalleryVideo();
    }

    protected function getCommonData(){
      return [
        'types' => \App\Models\GalleryShowType::getSorted('video'),
        'langs' => \App\Models\Language::getActive()
      ];
    }

    public function edit($pagePostModuleId,$id){
      if(!$this->commonModel->checkIfExists($pagePostModuleId,'page_post_module')){
        return $this->setMessageBoxAfterRedirect(0, route('dashboard'), 'admin.module.no_exists');
      }else{
        $params = $this->getCommonData();
        $moduleDetails = \App\Models\PagePostModule::getDetails($pagePostModuleId);

        $this->setActiveMenu($moduleDetails['from_module'] ? [$moduleDetails['from_module']->symbol] : ['page']);

        $params['redirect_url'] = $moduleDetails['redirect_url'];

        if(!$this->commonModel->checkIfExists($id,'gallery')){
          return $this->setMessageBoxAfterRedirect(0, $params['redirect_url'], 'admin.gallery.object_no_exists');
        }else{
          $params['header'] = trans('admin.galleryvideo.header_edit');
          $params['row'] = \App\Models\Gallery::where('id','=',$id)->with('video')->first();
          $params['id'] = $id;
          $params['pagePostModuleId'] = $pagePostModuleId;

          $this->setBreadcrumbs('module_edit',[
            'entity_type' => $moduleDetails['entity_type'],
            'entity_id' => $moduleDetails['entity_id'],
            'entity_name' => 'galleryvideo',
            'pagePostModuleId' => $pagePostModuleId,
            'id' => $id,
            'from_module' => $moduleDetails['from_module'],
            $moduleDetails['entity_type'].'_id' => $moduleDetails['entity_id']
          ]);

          return view('admin.gallery_video.add')->with($params);
        }
      }
    }

    public function getdata($id = false){
      $response = new \stdClass();
      $response->code = 0;

      if($id == false){
        $response->code = 1;
        $response->view = view('admin.gallery_video.partial.video_form',[
          'langs' => \App\Models\Language::getActive()
        ])->render();
      }else if(!$this->commonModel->checkIfExists($id,'gallery_video')){
        $response->msg = 'admin.galleryvideo.object_no_exists';
      }else{
        $response->code = 1;
        $response->view = view('admin.gallery_video.partial.video_form',[
          'video' => \App\Models\GalleryVideo::with('language')->where('id','=',$id)->first(),
          'langs' => \App\Models\Language::getActive()
        ])->render();

      }

      return response()->json($response);
    }

    public function store(Request $request,$pagePostModuleId,$id){
      $rules = [
        'show_type_id' => 'required'
      ];

      $galleryModel = new \App\Models\Gallery();

      \Validator::make($request->all(),$rules)->setAttributeNames($galleryModel->getAttributesNames())->validate();

      $result = $this->model->storeGallery($request->input(),$id,$pagePostModuleId);

      return $this->setMessageBoxAfterRedirect($result->code,$result->redirect_url, $result->msg.($result->code ? 'success' : 'failure'));
    }

    public function setposition(Request $request){
      $ids = $request->input('ids');
      $all = count($ids);
      foreach($ids as $key=>$id){
        $object = \App\Models\GalleryVideo::find($id);
        $object->position = $all-$key;
        $object->save();
      }
    }

    public function addvideo(Request $request){
      return response()->json($this->model->addVideo($request->input()));
    }

    public function removevideo($id){
      $result = $this->commonModel->deleteMulti($id,'gallery_video');

      $response = new \stdClass();
      $response->code = (int)$result->code;
      $response->msg = trans_choice('admin.galleryvideo.removevideo_'.($result->code ? 'success' : 'failure'),$result->itemCounter);
      return response()->json($response);
    }

    public function generatepreview(Request $request){
      $response = \App\Models\PagePostModule::duplicateEntity($request->input('pagePostModuleId'));

      $data = \App\Helpers\Parse::parseSerialize($request->input('form'));

      $newGalleryId = $this->model->createEmpty();
      $this->model->storeGallery($data,$newGalleryId,$request->input('pagePostModuleId'));

      \App\Models\Module::switchModuleIdInPreview($response->entity_id,$response->entity_type,$request->input('id'),$newGalleryId);

      $videos = \App\Models\GalleryVideo::with(['language'])->where('gallery_id','=',$request->input('id'))->get();
      foreach($videos as $video){
        $video = $video->toArray();
        $languages = $video['language'];
        unset($video['id']);
        unset($video['language']);
        $video['gallery_id'] = $newGalleryId;
        $newVideoId = \App\Models\GalleryVideo::insertGetId($video);

        foreach($languages as $lang){
          unset($lang['id']);
          $lang['gallery_video_id'] = $newVideoId;
          \App\Models\GalleryVideoLanguage::create($lang);
        }
      }

      return response()->json($response);
    }
}
