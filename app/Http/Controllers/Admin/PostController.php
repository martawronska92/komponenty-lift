<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Carbon\Carbon;

set_time_limit(0);

class PostController extends AdminController
{

  private $model;
  private $name;

  public function __construct(){
    parent::__construct();
    $this->setActiveMenu(['post']);
    $this->model = new \App\Models\Post();
  }

  public function generateList($rows,$pagePostModuleId,$category){
    $this->model->clearOldDrafts();

    $list = new \App\Components\Clist\Clist();
    $list->header = $this->name;
    $list->sortable = "post";
    $list->buttons = [
      new \App\Components\Clist\ListButtonAdd('admin.post.add_button',false,'post/create/'.$pagePostModuleId.'/'.$category)
    ];
    $list->actions = [
      new \App\Components\Clist\ListActionEdit(false,'post/edit/'.$pagePostModuleId.'/{id}'),
      new \App\Components\Clist\ListActionDelete(false,
          trans('admin.post.delete_post').\App\Helpers\Format::confirmDeleteName(' {title}')." ?",'post/delete/'.$pagePostModuleId.'/{id}')
    ];
    $list->columns = [
      new \App\Components\Clist\ListColumn('title','admin.list.title',false),
      new \App\Components\Clist\ListColumn('public','admin.list.public',false,function($row){
        if($row[0]->public || ($row[0]->publish_date!=null && Carbon::now()>=$row[0]->publish_date))
          echo strtoupper(trans('admin.yes'));
        else {
          echo strtoupper(trans('admin.no'));
        }
      }),
      new \App\Components\Clist\ListColumn('active','admin.list.active',false,function($row){
        echo \App\Components\Clist\ListColumnFormat::switchValue($row[0]->active,'post',$row[0]->id,'active');
      }),
      new \App\Components\Clist\ListColumn('user_name','admin.list.added_by'),
      new \App\Components\Clist\ListColumn('created_at','admin.list.added_day',false,'formatDateCarbon')
    ];

    $list->rows = $rows;

    $list->pagination = count($rows) >0 ? new \App\Components\Clist\ListPagination($rows,route('post',['category'=>$rows[0]->category_id,'pagePostModuleId'=>$pagePostModuleId])) : false;
    return $list->render();
  }

  public function createBreadcrumbsObject($category_id,$pagePostModuleId,$post_id = false, $name=""){
    $params = [
      'category_id' => $category_id,
      'pagePostModuleId' => $pagePostModuleId
    ];

    if($name!="")
      $params['name'] = $name;
    if($post_id!=false)
      $params['post_id'] = $post_id;

    return $params;
  }

  public function index($pagePostModuleId,$category){
    if(!$this->commonModel->checkIfExists($pagePostModuleId,'page_post_module')){
      return $this->setMessageBoxAfterRedirect(0, route('dashboard'), 'admin.module.no_exists');
    }else{
      $categoryModel = new \App\Models\Category();

      if(!$this->commonModel->checkIfExists($category,'category')){
        return $this->setMessageBoxAfterRedirect(0, route('category'), 'admin.category.no_exists','list');
      }else{
          $this->name = $categoryModel->getNameById($category);
          $this->setBreadcrumbs('post',$this->createBreadcrumbsObject($category,$pagePostModuleId,false,$this->name));

          return view('admin.components.clist.index')->with([
            'list' => $this->generateList($this->model->getList($category),$pagePostModuleId,$category)
          ]);
      }
    }
  }

  public function getCommonData($type = ""){
    $params = [
      'langs' => \App\Models\Language::getActive(),
      'categories' => \App\Models\Category::getActive(),
      'host' => $_SERVER['HTTP_HOST'],
      'settings' => \App\Models\Settings::getAll(),
      'modules' => \App\Models\Module::getFor('for_post'),
      'header' => trans('admin.post.header_'.$type),
      'tags' => \App\Models\Tag::getInCurrentLang(),
      'canAccessTag' => \App\Models\User::canAccess('tag')
    ];
    return $params;
  }

  public function create($pagePostModuleId,$category){
    $categoryModel = new \App\Models\Category();

    if(!$this->commonModel->checkIfExists($category,'category')){
      return $this->setMessageBoxAfterRedirect(0, route('category'), 'admin.category.no_exists','list');
    }else{
      $this->setBreadcrumbs('post_create',$this->createBreadcrumbsObject($category,$pagePostModuleId));
      $params = $this->getCommonData('add');
      $params['category'] = $category;
      $category = \App\Models\Category::where('id','=',$params['category'])->with('field')->first();
      $params['fields'] = $category->field;
      $params['redirect_url'] = route('post',['pagePostModuleId'=>$pagePostModuleId,'category'=>$category]);
      return view('admin.post.add')->with($params);
    }
  }

  public function edit($pagePostModuleId,$id){
    if(!$this->commonModel->checkIfExists($id,'post')){
      return $this->setMessageBoxAfterRedirect(0, route('category'), 'admin.post.no_exists','list');
    }else{
      $params = $this->getCommonData('edit');
      $params['row'] = \App\Models\Post::where('id','=',$id)->with(['field','module' => function($query){
        $query->orderBy('position','asc');
      }])->first();
      if($params['row']->draft!=NULL){
        return $this->setMessageBoxAfterRedirect(0, route('post',['category'=>$params['row']['category_id']]), 'admin.post.no_exists','list');
      }else{
        $params['parsed_modules'] = \App\Models\PagePostModule::parseModules($params['row']->module);
        $params['category'] = $params['row']->category_id;
        $params['id'] = $id;
        $params['redirect_url'] = route('post',['pagePostModuleId'=>$pagePostModuleId,'category'=>$params['category']]);

        $fields = [];

        $category = \App\Models\Category::where('id','=',$params['category'])->with('field')->first();
        foreach($category->field as $categoryField){
            if(count($params['row']->field) > 0){
              $find = false;
              foreach($params['row']->field as $key=>$rowField){
                if($rowField->id == $categoryField->id)
                  $find = $key;
              }
              if($find!==FALSE)
                array_push($fields,$params['row']->field[$find]);
              else
                array_push($fields,$categoryField);
            }else{
              array_push($fields,$categoryField);
            }
        }

        $params['fields'] = $fields;
        $params['id'] = $id;

        $crumbParams = $this->createBreadcrumbsObject($category->id,$pagePostModuleId,$id);
        $crumbParams['post'] = $params['row'];
        $this->setBreadcrumbs('post_edit',$crumbParams);
        return view('admin.post.add')->with($params);
      }
    }
  }

  public function store(Request $request,$pagePostModuleId = false,$id = false){
    $rules = [
      'category_id' => 'required|numeric|min:1'
    ];
    if(!$request->has('public'))
      $rules['publish_date'] = 'required';

    foreach($request->get('title') as $key => $val)
    {
      $rules['title.'.$key] = 'required|max:255';
      $rules['url.'.$key] = 'required|max:255';
      if($request->has('meta_title.'.$key))
        $rules['meta_title.'.$key] = 'required|max:255';
      if($request->has('meta_description.'.$key))
        $rules['meta_description.'.$key] = 'required|max:255';
    }

    if($request->has('category_field_id')){
      foreach($request->get('category_field_id') as $key => $val)
      {
        $rules['category_field_id.'.$key] = 'required';
      }
    }


    \Validator::make($request->all(),$rules)->setAttributeNames($this->model->getAttributesNames())->validate();

    $data = $request->input();

    $id = array_key_exists('id', $data) ? $data['id'] : false;

    $result = $this->model->savePost($data,$id);

    if($request->has('from_module')){
      $msg = 'admin.'.($id ? 'update' :  $request->get('from_module').'.save_post').'_';
    }else{
      $msg = 'admin.'.($id ? 'update' : 'post.add').'_';
    }

    if(array_key_exists('save_and_stay',$data)){
      if($request->has('from_module')){
        $route = route($request->get('from_module').'_post_edit',['id' => $result->id]);
      }else
        $route = route('post_edit',['pagePostModuleId'=>$pagePostModuleId,'id'=>$result->id]);
    }else{
      if($request->has('from_module')){
        $route = route($request->get('from_module').'_category',['category'=>$data['category_id']]);
      }else
        $route = route('post',['pagePostModuleId'=>$pagePostModuleId,'id'=>$data['category_id']]);
    }

    if($result->code && $id == false){ //pusty moduł treści dla nowego wpisu
      $content = new \App\Models\Content();
      $module = new \App\Models\Module();
      \App\Models\PagePostModule::attachModule([
        'entity_id' => $result->id,
        'entity_type' => 'App\Models\Post',
        'module_id' => $module->getIdBySymbol('content'),
        'module_entity_id' => $content->createEmpty(),
        'position' => 0
      ]);

      if(\App\Models\Module::checkModelBySymbol($request->from_module)){
        $model = "\App\Models\\".ucfirst($request->from_module);
        if(method_exists($model,"addPostCallback")){
          $model::addPostCallback($result->id);
        }
      }
    }

    if($result->code){
      \App\Models\Sitemap::generate();
    }

    return $this->setMessageBoxAfterRedirect($result->code, $route, $msg.($result->code ? 'success' : 'failure'));
  }

  public function delete($pagePostModuleId,$id){
      $ids = is_array($id) ? $id[0] : explode(",",$id);
      $categoryId = $this->model->getCategoryId($ids[0]);

      $response = new \stdClass();
      $response->code = 0;
      $response->msg = trans('admin.post.delete_ failure'.'|'.count($ids));

      if(!$categoryId){
        $msg = 'admin.post.category_not_found';
        $route = route('dashboard');
      }else{
        $response = $this->model->deletePost($ids);

        $category = \App\Models\Category::where('id','=',$categoryId)->first();
        if($category->module_id!=NULL){
          $module = \App\Models\Module::where('id','=',$category->module_id)->first()->symbol;
          if(\Route::has($module.'_category',['category'=>$categoryId])){
            $route = route($module.'_category',['category'=>$categoryId]);
            $msg = 'admin.'.$module.'.delete_post_'.($response->code ? 'success' : 'failure').'|'.$response->itemCounter;
          }else{
            $route = route('post',['category'=>$categoryId,'pagePostModuleId'=>$pagePostModuleId]);
            $msg = 'admin.post.delete_'.($response->code ? 'success' : 'failure').'|'.$response->itemCounter;
          }
        }else{
          $route = route('post',['category'=>$categoryId,'pagePostModuleId'=>$pagePostModuleId]);
          $msg = 'admin.post.delete_'.($response->code ? 'success' : 'failure').'|'.$response->itemCounter;
        }

        if($response->code){
          \App\Models\Sitemap::generate();
        }
      }
      return $this->setMessageBoxAfterRedirect($response->code, $route , $msg,'list');
  }

  public function setposition(Request $request){
    $ids = $request->input('ids');
    $all = count($ids);
    foreach($ids as $key=>$id){
      $object = \App\Models\Post::find($id);
      $object->position = $all-$key;
      $object->save();
    }
  }

  public function generatepreview(Request $request){
    $data = $request->input();

    parse_str($data['form'],$params);

    $params['draft'] = str_random(40);
    $params['active'] = 1;
    if(array_key_exists('from_id',$data)){
      $params['from_id'] = $data['from_id'];
    }

    $result = $this->model->savePost($params);

    $routeParams['hash'] = $params['draft'];
    if(\App\Helpers\Language::checkPrefix())
      $routeParams['frontlanguage'] = \App\Models\Language::checkFrontLanguage();

    $response = new \stdClass();
    $response->code = (int)$result->code;
    $response->msg = $response->code ? route('post_preview', $routeParams) : trans('admin.preview_failure');

    return response()->json($response);
  }

  public function showpreview($hash){
    $post = \App\Models\Post::where('draft','=',$hash)->first();

    if(!$post){
      return $this->setMessageBoxAfterRedirect(0, route('page') , 'admin.post.preview_failure','list');
    }else{
      $controller = new \App\Http\Controllers\Front\PostController();
      return $controller->generatePreview($post->id);
    }
  }

  public function waitforpreview(){
    $controller = new \App\Http\Controllers\Front\PostController();
    return $controller->waitForPreview();
  }
}
