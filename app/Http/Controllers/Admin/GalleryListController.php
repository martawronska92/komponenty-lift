<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class GalleryListController extends AdminController
{
    private $model;

    public function __construct(){
      parent::__construct();
      $this->model = new \App\Models\GalleryList();
    }

    protected function getCommonData(){
      return [
        'langs' => \App\Models\Language::getActive()
      ];
    }

    public function edit($pagePostModuleId,$id){
      if(!$this->commonModel->checkIfExists($pagePostModuleId,'page_post_module')){
        return $this->setMessageBoxAfterRedirect(0, route('dashboard'), 'admin.module.no_exists');
      }
      else{
        $params = $this->getCommonData();
        $moduleDetails = \App\Models\PagePostModule::getDetails($pagePostModuleId);
        $params['redirect_url'] = $moduleDetails['redirect_url'];

        $this->setActiveMenu($moduleDetails['from_module'] ? [$moduleDetails['from_module']->symbol] : ['page']);

        if(!$this->commonModel->checkIfExists($id,'gallery_list')){
          return $this->setMessageBoxAfterRedirect(0,$params['redirect_url'], 'admin.gallerylist.object_no_exists');
        }else{
          $params['header'] = trans('admin.gallerylist.header_edit');
          $params['row'] = \App\Models\GalleryList::where('id','=',$id)->with('language')->first();
          $params['id'] = $id;
          $params['pagePostModuleId'] = $pagePostModuleId;
          $this->setBreadcrumbs('module_edit',[
            'entity_type' => $moduleDetails['entity_type'],
            'entity_id' => $moduleDetails['entity_id'],
            'entity_name' => 'galleryphotolist',
            'pagePostModuleId' => $pagePostModuleId,
            'id' => $id,
            'from_module' => $moduleDetails['from_module'],
            $moduleDetails['entity_type'].'_id' => $moduleDetails['entity_id']
          ]);
          return view('admin.gallery_list.add')->with($params);
        }
      }



    }

    public function store(Request $request,$pagePostModuleId,$id){
      $galleryModel = new \App\Models\GalleryList();

      $result = $this->model->storeList($request->input(),$id,$pagePostModuleId);

      return $this->setMessageBoxAfterRedirect($result->code,$result->redirect_url, $result->msg.($result->code ? 'success' : 'failure'));
    }
}
