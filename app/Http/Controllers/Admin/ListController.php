<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class ListController extends AdminController
{
    public function __construct(){
      parent::__construct();
    }

    public function checkfield(Request $request){
      $rules = [
        'item' => 'required|string',
        'itemId'=> 'required|integer|min:1',
        'field' => 'required|string',
        'value' => 'required|boolean'
      ];

      $data = $request->input();

      $validator = \Validator::make($request->all(),$rules);

      if (!$validator->fails() &&
          \Schema::hasTable($data['item']) &&
          \Schema::hasColumn($data['item'], $data['field']) &&
          \DB::table($data['item'])->where('id','=',$data['itemId'])->count()==1) {
        \DB::table($data['item'])->where('id','=',$data['itemId'])->update([
          $data['field'] => $data['value']
        ]);
      }
    }
}
