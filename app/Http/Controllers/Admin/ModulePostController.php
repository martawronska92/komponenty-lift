<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class ModulePostController extends AdminController
{
    var $model;
    var $module;
    var $postController;
    var $postLink;

    /**
     * kontroller modułu
     * @param array $active   active w menu
     * @param string $module   symbol modułu
     * @param string $postLink część linku dla postów (tworzenie,edycja,usuwanie)
     */
    public function __construct($active,$module,$postLink){
      parent::__construct();
      $this->middleware('permission:'.$module);
      $this->module = $module;
      $this->postLink = $postLink;
      $this->model = new \App\Models\Module();
      $this->postController = new \App\Http\Controllers\Admin\PostController();
      $this->setActiveMenu($active);
    }

    public function generateList($rows,$category){
      $list = new \App\Components\Clist\Clist();
      $list->header = trans('admin.'.$this->module.'.header');
      $list->sortable = "post";
      $list->buttons = [
        new \App\Components\Clist\ListButton('admin.'.$this->module.'.add_post_button',$this->module.'/'.$this->postLink.'/create/'.$category,'button button--small button--green button--withIcon button--iconAdd','')
      ];
      $list->actions = [
        new \App\Components\Clist\ListActionEdit($this->module.'/'.$this->postLink),
        new \App\Components\Clist\ListActionDelete($this->module.'/'.$this->postLink,
          trans('admin.'.$this->module.'.delete_post').\App\Helpers\Format::confirmDeleteName(' {title}')." ?")
      ];
      $list->columns = [
        new \App\Components\Clist\ListColumn('title','admin.list.title'),
        new \App\Components\Clist\ListColumn('active','admin.list.active',false,function($row){
          echo \App\Components\Clist\ListColumnFormat::switchValue($row[0]->active,'post',$row[0]->id,'active');
        })
      ];
      $list->rows = $rows;
      return $list->render();
    }

    public function index($id){
      if(!$this->commonModel->checkIfExists($id,'category')){
        return $this->setMessageBoxAfterRedirect(0, route($this->module), 'admin.'.$this->module.'.category_no_exists');
      }else{
        $categoryModel = new \App\Models\Category();
        $this->setBreadcrumbs($this->module.'_posts',[
          'category' => \App\Models\Category::getById($id)
        ]);
        $model = new \App\Models\Post();
        return view('admin.components.clist.index')->with([
          'list' => $this->generateList($model->getList($id),$id)
        ]);
      }
    }

    public function getCommonData($params){
      $moduleId = $this->model->getIdBySymbol($this->module);
      $categoryModel = new \App\Models\Category();
      $params['categories'] = $categoryModel->getListForModule($moduleId);
      $params['from_module'] = 'offer';
      return $params;
    }

    public function create($category){
      if(!$this->commonModel->checkIfExists($category,'category')){
        return $this->setMessageBoxAfterRedirect(0, route($this->module), 'admin.'.$this->module.'.category_no_exists');
      }else{
        $params = $this->postController->getCommonData();
        $params = $this->getCommonData($params);
        $params['header'] = trans('admin.'.$this->module.'.header_post_add');
        $params['category'] = $category;
        $params['redirect_url'] = route($this->module.'_category',['id'=>$category]);

        $categoryModel = \App\Models\Category::where('id','=',$category)->with('field')->first();
        $params['fields'] = $categoryModel->field;

        $categoryModel = new \App\Models\Category();
        $this->setBreadcrumbs($this->module.'_post_create',['category' => \App\Models\Category::getById($category)]);

        $view = 'admin.'.(\View::exists('admin.'.$this->module.'.addpost') ? $this->module : 'module').'.addpost';

        return view($view)->with($params);
      }
    }

    public function edit($id){
      if(!$this->commonModel->checkIfExists($id,'post')){
        return $this->setMessageBoxAfterRedirect(0, route('category'), 'admin.category.no_exists','list');
      }else{
        $params = $this->postController->getCommonData();
        $params = $this->getCommonData($params);
        $params['header'] = trans('admin.'.$this->module.'.header_post_edit');
        $params['row'] = \App\Models\Post::where('id','=',$id)->with(['field','module'])->first();
        $params['parsed_modules'] = \App\Models\PagePostModule::parseModules($params['row']->module);
        $params['category'] = $params['row']->category_id;
        $params['id'] = $id;
        $params['redirect_url'] = route($this->module.'_category',['id'=>$params['category']]);

        if(count($params['row']->field) > 0){
          $params['fields'] =  $params['row']->field;
        }else{
           $category = \App\Models\Category::find($params['category'])->with('field')->first();
           $params['fields'] = $category->field;
        }

        $params['id'] = $id;

        $categoryModel = new \App\Models\Category();
        $this->setBreadcrumbs($this->module.'_post_edit',[
          'category' => \App\Models\Category::getById($params['category']),
          'post' => $params['row']
        ]);

        $view = 'admin.'.(\View::exists('admin.'.$this->module.'.addpost') ? $this->module : 'module').'.addpost';

        return view($view)->with($params);
      }
    }

    public function delete($id){
        return $this->postController->delete(false,$id);
    }

    public function store(Request $request, $id = false){
      return $this->postController->store($request,$id);
    }
}
