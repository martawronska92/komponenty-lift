<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class UserController extends AdminController
{
    var $model;

    public function __construct(){
        parent::__construct();
        $this->middleware('permission:user');
        $this->setActiveMenu(['user']);
        $this->model = new \App\Models\User();
    }

    public function generateList($rows){
      $list = new \App\Components\Clist\Clist();
      $list->header = trans('menu.user');
      $list->buttons = [
        new \App\Components\Clist\ListButtonAdd('admin.user.add_button','user')
      ];
      $list->actions = [
        new \App\Components\Clist\ListActionEdit('user'),
        new \App\Components\Clist\ListActionDelete('user',
            trans('admin.user.delete').\App\Helpers\Format::confirmDeleteName(' {email}')." ?")
      ];
      $list->columns = [
        new \App\Components\Clist\ListColumn('email','admin.user.email',true),
        new \App\Components\Clist\ListColumn('first_name','admin.user.first_name',true),
        new \App\Components\Clist\ListColumn('last_name','admin.user.last_name',true),
        new \App\Components\Clist\ListColumn('last_login_history','admin.user.last_login',false,function($row){
          echo \App\Components\Clist\ListColumnFormat::formatDateCarbon($row[0]->last_login_history);
        }),
        new \App\Components\Clist\ListColumn('active','admin.list.active',false,function($row){
          echo \App\Components\Clist\ListColumnFormat::switchValue($row[0]->active,'users',$row[0]->id,'active');
        })
      ];
      $list->rows = $rows;
      return $list->render();
    }

    public function index(Request $request){
      $this->setBreadcrumbs('user');
      return view('admin.components.clist.index')->with([
        'list' => $this->generateList($this->model->getList($request->input()))
      ]);
    }

    public function getCommonData(){
      $params = [
        'modules' => \App\Models\Module::with(['language'=>function($query){
          $query->whereHas('language',function($query){
            $query->where('symbol','=',\App::getLocale());
          });
        }])->where('need_user_priviledge','=',1)->get(),
        'privileges' => \App\Models\Privilege::with(['language'=>function($query){
          $query->whereHas('language',function($query){
            $query->where('symbol','=',\App::getLocale());
          });
        }])->get(),
        'entityDetails' => []
      ];

      $response = [];
      foreach($params['modules'] as $module){
          $response[$module->language[0]->name] = [
            'type' => 'module',
            'id' => $module->id,
            'symbol' => $module->symbol,
            'entity_privilege' => 0
          ];
      }
      foreach($params['privileges'] as $privilege){
          $response[$privilege->language[0]->name] = [
            'type' => 'privilege',
            'id' => $privilege->id,
            'symbol' => $privilege->symbol,
            'entity_privilege' => $privilege->entity_privilege
          ];
          if($privilege->entity_privilege){
            $model = new $privilege->entity_model();
            $params['entityDetails'][$privilege->symbol] = $model->getMenu();
          }
      }

      ksort($response,SORT_STRING);

      return [
        'modules' => $response,
        'entityDetails' => $params['entityDetails'],
        'password' => $this->model->generatePassword()
      ];
    }

    public function create(){
      $this->setBreadcrumbs('user_create');
      $params = $this->getCommonData();
      $params['header'] = trans('admin.user.add_header');
      return view('admin.user.add')->with($params);
    }

    public function edit($id){
      if(!$this->commonModel->checkIfExists($id,'users')){
        return $this->setMessageBoxAfterRedirect(0, route('user'), 'admin.user.no_exists','list');
      }else{
        $params = $this->getCommonData();
        $params['header'] = trans('admin.user.edit_header');
        $params['row'] = \App\Models\User::with(['module','privilege'])->where('id','=',$id)->first();

        if($params['row'] && ($params['row']->role->symbol == 'developer' || ($params['row']->for_developer && !\App\Models\User::isDeveloper())))
          return redirect()->route('user');

        $params['id'] = $id;
        $this->setBreadcrumbs('user_edit',['user' => $params['row']]);
        return view('admin.user.add')->with($params);
      }
    }

    public function store(Request $request, $id = false){
        $data = $request->input();

        $rules = [
          'first_name' => 'required|max:255',
          'last_name' => 'required|max:255',
          'email' => 'required|email|max:255'
        ];

        if(array_key_exists('password',$data)){
          $rules['password'] = 'required|min:8|regex:"'.\App\Models\User::$passwordRegex.'"';
        }

        \Validator::make($data,$rules)->setAttributeNames($this->model->getAttributesNames())->validate();

        $data = $request->input();

        $response = $this->model->saveUser($data,$id);

        $viewType = 'main';
        if(isset($response->backToEdit))
          $route = \URL::previous();
        else if(array_key_exists('save_and_stay',$data))
          $route = route('user_edit',['id'=>$response->id]);
        else{
          $route = route('user');
          $viewType = 'list';
        }
        return $this->setMessageBoxAfterRedirect($response->code, $route, $response->msg,$viewType);
    }

    public function delete($ids){
        $ids = explode(',',$ids);

        $toDelete = [];
        foreach($ids as $id){
          $user = \App\Models\User::where('id','=',$id)->first();
          if(!\App\Models\User::isDeveloper()){
            if($user->role->symbol=="normal" && $user->for_developer == 0)
              array_push($toDelete,$id);
          }else{
            array_push($toDelete,$id);
          }
        }

        $response = $this->model->deleteUser($toDelete);
        return $this->setMessageBoxAfterRedirect($response->code,route('user'),$response->msg);
    }

    public function checkunique(Request $request){
        $response = new \stdClass();
        $response->code = 0;

        if($request->has('email') || $request->has('name')){
          $field = $request->has('email') ? 'email' : 'name';
          $query = \App\Models\User::where($field,'=',strtolower(trim($request->$field)));
          if($request->has('id') && $request->id!="")
              $query = $query->where('id','!=',$request->id);
          $response->code = (int) ($query->count() == 0);
        }

        return response()->json($response);
    }
}
