<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class MenuController extends AdminController
{

    public function __construct(){
      parent::__construct();
      $this->model = new \App\Models\Menu();
	  $this->setActiveMenu(['menu']);
    }

    public function generateList($rows){
      $list = new \App\Components\Clist\Clist();
      $list->header = trans('menu.menu');
      $list->buttons = [
        new \App\Components\Clist\ListButton('admin.menu.add_button','menu/create','button button--small button--green button--withIcon button--iconAdd','')
      ];
      $list->actions = [
        new \App\Components\Clist\ListActionEdit('menu'),
        new \App\Components\Clist\ListActionDelete('menu',
          trans('admin.menu.delete_item').\App\Helpers\Format::confirmDeleteName(' {name}')." ?")
      ];
      $list->columns = [
        new \App\Components\Clist\ListColumn('name','admin.list.name'),
        new \App\Components\Clist\ListColumn('symbol','admin.symbol')
      ];
      $list->rows = $rows;
      return $list->render();
    }

    public function index(){
      $this->setBreadcrumbs('menu');
      return view('admin.components.clist.index')->with([
        'list' => $this->generateList($this->model->getList())
      ]);
    }

    public function getCommonParams(){
      $pageModel = new \App\Models\Page();
      return [
        'header' => trans('menu.menu'),
        'langs' => \App\Models\Language::getActive(),
        'pages' => $pageModel->getForSelect(),
        'category' => \App\Models\Category::getActive()
      ];
    }

    public function create(){
      $this->setBreadcrumbs('menu_create');
      $params = $this->getCommonParams();
      $params['header'] = trans("admin.menu.add_header");
      return view('admin.menu.create')->with($params);
    }

    public function edit($id){
      if(!$this->commonModel->checkIfExists($id,'menu'))
        return $this->setMessageBoxAfterRedirect(0, route('menu'), 'admin.menu.no_exists','list');
      $params = $this->getCommonParams();
      $params["header"] = trans("admin.menu.edit_header");
      $params['menuRow'] = \App\Models\Menu::where('id','=',$id)->first();
      $params['elements'] = $this->model->getElements($id);
      $params['id'] = $id;
      $this->setBreadcrumbs('menu_edit',['menu' => $params['menuRow']]);
      return view('admin.menu.create')->with($params);
    }

    public function store(Request $request,$id = false){
      if($id!=false && !$this->commonModel->checkIfExists($id,'menu'))
        return $this->setMessageBoxAfterRedirect(0, route('menu'), 'admin.menu.no_exists','list');

      $rules = [
        'name' => 'required|max:255',
        'symbol' => 'required|max:100'
      ];
      $data = $request->input();
      \Validator::make($data,$rules)->setAttributeNames($this->model->getAttributesNames())->validate();

      $result = $this->model->storeMenu($data,$id);

      $route = !$id ? route('menu_edit',['id'=>$result->id]) : route('menu');
      return $this->setMessageBoxAfterRedirect($result->code, route('menu'), $result->msg,'list');
    }

    public function delete($id){
      if(!$this->commonModel->checkIfExists($id,'menu')){
        return $this->setMessageBoxAfterRedirect(0, route('menu'), 'admin.menu.no_exists','list');
      }else{
        $response = $this->commonModel->deleteMulti($id,'menu');
        return $this->setMessageBoxAfterRedirect($response->code,route('menu'), 'admin.menu.delete_'.($response->code ? 'success' : 'failure').'|'.$response->itemCounter,'list');
      }
    }
}
