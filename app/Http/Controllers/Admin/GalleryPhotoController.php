<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class GalleryPhotoController extends AdminController
{

    private $model;

    public function __construct(){
      parent::__construct();
      $this->model = new \App\Models\GalleryPhoto();
    }

    protected function getCommonData(){
      return [
        'types' => \App\Models\GalleryShowType::getSorted('photo'),
        'langs' => \App\Models\Language::getActive(),
        'settings' => \App\Models\Settings::getByGroup('images'),
      ];
    }

    public function edit($pagePostModuleId,$id){
      if(!$this->commonModel->checkIfExists($pagePostModuleId,'page_post_module')){
        return $this->setMessageBoxAfterRedirect(0, route('dashboard'), 'admin.module.no_exists');
      }
      else{
        $params = $this->getCommonData();
        $moduleDetails = \App\Models\PagePostModule::getDetails($pagePostModuleId);
        $params['redirect_url'] = $moduleDetails['redirect_url'];

        $this->setActiveMenu($moduleDetails['from_module'] ? [$moduleDetails['from_module']->symbol] : ['page']);

        if(!$this->commonModel->checkIfExists($id,'gallery')){
          return $this->setMessageBoxAfterRedirect(0,$params['redirect_url'], 'admin.gallery.object_no_exists');
        }else{
          $params['header'] = trans('admin.galleryphoto.header_edit');
          $params['row'] = \App\Models\Gallery::where('id','=',$id)->with('photo')->first();
          $params['id'] = $id;
          $params['pagePostModuleId'] = $pagePostModuleId;
          $this->setBreadcrumbs('module_edit',[
            'entity_type' => $moduleDetails['entity_type'],
            'entity_id' => $moduleDetails['entity_id'],
            'entity_name' => 'galleryphoto',
            'pagePostModuleId' => $pagePostModuleId,
            'id' => $id,
            'from_module' => $moduleDetails['from_module'],
            $moduleDetails['entity_type'].'_id' => $moduleDetails['entity_id']
          ]);
          return view('admin.gallery_photo.add')->with($params);
        }
      }
    }

    public function getdata($id = false){
      $response = new \stdClass();
      $response->code = 0;

      if($id == false){
        $response->code = 1;
        $response->view = view('admin.gallery_photo.partial.photo_form',[
          'langs' => \App\Models\Language::getActive(),
          'settings' => \App\Models\Settings::getByGroup('images')
        ])->render();
      }else if(!$this->commonModel->checkIfExists($id,'gallery_photo')){
        $response->msg = 'admin.galleryphoto.object_no_exists';
      }else{
        $response->code = 1;
        $response->view = view('admin.gallery_photo.partial.photo_form',[
          'photo' => \App\Models\GalleryPhoto::with('language')->where('id','=',$id)->first(),
          'langs' => \App\Models\Language::getActive(),
          'settings' => \App\Models\Settings::getByGroup('images')
        ])->render();

      }

      return response()->json($response);
    }

    public function store(Request $request,$pagePostModuleId,$id){
      $rules = [
        'show_type_id' => 'required'
      ];

      $galleryModel = new \App\Models\Gallery();

      \Validator::make($request->all(),$rules)->setAttributeNames($galleryModel->getAttributesNames())->validate();

      $result = $this->model->storeGallery($request->input(),$id,$pagePostModuleId);

      return $this->setMessageBoxAfterRedirect($result->code,$result->redirect_url, $result->msg.($result->code ? 'success' : 'failure'));
    }

    public function setposition(Request $request){
      $ids = $request->input('ids');
      $all = count($ids);
      foreach($ids as $key=>$id){
        $object = \App\Models\GalleryPhoto::find($id);
        $object->position = $all-$key;
        $object->save();
      }
    }

    public function addphoto(Request $request){
      return response()->json($this->model->addPhoto($request->all()));
    }

    public function removephoto($id){
      return response()->json($this->model->removePhoto(explode(",",$id)));
    }

    public function generatepreview(Request $request){
      $response = \App\Models\PagePostModule::duplicateEntity($request->input('pagePostModuleId'));

      $data = \App\Helpers\Parse::parseSerialize($request->input('form'));

      $newGalleryId = $this->model->createEmpty();
      $this->model->storeGallery($data,$newGalleryId,$request->input('pagePostModuleId'));

      \App\Models\Module::switchModuleIdInPreview($response->entity_id,$response->entity_type,$request->input('id'),$newGalleryId);

      $photos = \App\Models\GalleryPhoto::with(['language'])->where('gallery_id','=',$request->input('id'))->get();
      foreach($photos as $photo){
        $photo = $photo->toArray();
        $oldPhotoId = $photo['id'];
        $languages = $photo['language'];
        unset($photo['id']);
        unset($photo['language']);

        $photo['gallery_id'] = $newGalleryId;
        $newPhotoId = \App\Models\GalleryPhoto::insertGetId($photo);

        foreach($languages as $lang){
          unset($lang['id']);
          $lang['gallery_photo_id'] = $newPhotoId;
          \App\Models\GalleryPhotoLanguage::create($lang);
        }
      }

      return response()->json($response);
    }

    public function setmain(Request $request){
      $data = $request->input();
      \App\Models\GalleryPhoto::where('gallery_id','=',$data['galleryId'])->update(['main'=>0]);
      \App\Models\GalleryPhoto::where('id','=',$data['photoId'])->update(['main'=>1]);      
    }
}
