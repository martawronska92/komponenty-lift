<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\MenuElement;

class MenuElementController extends AdminController
{
    var $model;

    public function __construct(){
      parent::__construct();
      $this->model = new \App\Models\MenuElement();
    }    

    public function get($id){
      $response = new \stdClass();
      $response->code = 0;
      $response->msg = "";

      $exists = $this->commonModel->checkIfExists($id,'menu_element');

      if(!$exists){
        $response->msg = trans('admin.menuelement.no_exists');
      }else{
        $response->code = 1;
        $response->msg = $this->model->getById($id);
      }

      return response()->json($response);
    }

    public function store(Request $request){
      $data = $request->input();

      $rules = [
        "menu_element_type" => "required"
      ];
      if($data['menu_element_type']!=""){
        if($data['menu_element_type']=="link") $rules['url'] = 'required';
        if($data['menu_element_type']=="page") $rules['page'] = 'required';
        if($data['menu_element_type']=="category") $rules['category'] = 'required';
      }

      $validator = \Validator::make($request->all(),$rules);

      $response = new \stdClass();
      $response->code = 0;
      $response->msg = "";

      if ($validator->fails()) {
        $response->msg = trans('admin.menu.save_failure');
      }else{
        $model = new \App\Models\MenuElement();
        $response = $model->store($data);
      }

      return response()->json($response);
    }

    public function delete($id){
      $el = \App\Models\MenuElement::find($id);
      if($el){
        $result = (int)$el->delete();
      }else{
        $result = -1;
      }
      return response()->json($result);
    }

    private $positionToSet = 0;

    private function setChildrenPosition($level,$parentId,$data){
      foreach($data as $menuElement){
        $object = \App\Models\MenuElement::find($menuElement['key']);
        if($object){
          $object->position = $this->positionToSet;
          $object->level = $level;
          $object->parent_id = $parentId == 0 ? NULL : $parentId;
          $object->save();
          $this->positionToSet++;
        }

        if(array_key_exists('children',$menuElement) && count($menuElement['children'])>0)
          $this->setChildrenPosition($level+1, $menuElement["key"], $menuElement["children"]);
      }
    }

    public function setpositions(Request $request){
      $data = $request->input();
      $this->setChildrenPosition(0,0,$data['data']['children']);;
    }
}
