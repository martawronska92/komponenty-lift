<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class ModuleCategoryController extends AdminController
{
  var $model;
  var $module;
  var $categoryController;
  var $postLink;

  /**
   * kontroller modułu
   * @param array $active   active w menu
   * @param string $module   symbol modułu
   */
  public function __construct($active,$module){
    parent::__construct();
    //this->middleware('permission:'.$module);
    $this->module = $module;
    $this->model = new \App\Models\Module();
    $this->categoryController = new \App\Http\Controllers\Admin\CategoryController();
    $this->setActiveMenu($active);
  }

  public function generateList($rows,$categoryParentId = false){

    $list = new \App\Components\Clist\Clist();
    $list->header = trans('admin.'.$this->module.'.header');
    $list->sortable = "category";
    $list->buttons = [
      new \App\Components\Clist\ListButtonAdd('admin.'.$this->module.'.add_button',"",$this->module."/create".($categoryParentId ? "/".$categoryParentId : ""))
    ];
    $list->actions = [
      new \App\Components\Clist\ListActionEdit($this->module),
      new \App\Components\Clist\ListAction('admin.'.$this->module.'.showposts',$this->module.'/category/{id}','button button--small button--blue button--withIcon button--iconList',false,''),
      new \App\Components\Clist\ListAction('admin.'.$this->module.'.showsubcategory',$this->module.'/{id}','button button--small button--blue button--withIcon button--iconTree',false,''),
      new \App\Components\Clist\ListActionDelete($this->module,
        trans('admin.'.$this->module.'.delete_category').\App\Helpers\Format::confirmDeleteName(' {name}')." ? ".trans('admin.category.delete_category_warning'))
    ];
    $list->columns = [
      new \App\Components\Clist\ListColumn('name','admin.list.name'),
      new \App\Components\Clist\ListColumn('post_counter','admin.'.$this->module.'.post_counter',false,function($row){
        echo \App\Components\Clist\ListColumnFormat::showBadge($row[0]->post_counter);
      }),
      new \App\Components\Clist\ListColumn('subcategory_counter','admin.'.$this->module.'.subcategory_counter',false,function($row){
        echo \App\Components\Clist\ListColumnFormat::showBadge($row[0]->subcategory_counter);
      })
    ];
    $list->rows = $rows;
    return $list->render();
  }

  public function index(Request $request){
    $this->setBreadcrumbs($this->module,['categoryParentId' => $request->categoryParentId]);
    return view('admin.components.clist.index')->with([
      'list' => $this->generateList($this->model->getList($this->module,$request->categoryParentId),$request->categoryParentId)
    ]);
  }

  public function getCommonData($params,$type,$id = false){
      $params['header'] = trans('admin.'.$this->module.'.header_'.$type);
      $params['module_id'] = $this->model->getIdBySymbol('offer');
      $params['module'] = $this->module;
      $params['host'] = $_SERVER['HTTP_HOST'];
      $params['tabs'] = [
        'data' => trans('admin.post.tab_data'),
        'module' => trans('admin.post.tab_module')
      ];
      if(\App\Models\User::isDeveloper()){
        $params['tabs']['view'] = trans('admin.offer.tab_view');
      }
      $params['settings'] = \App\Models\Settings::getAll();
      $params['modules'] = \App\Models\Module::getFor('for_category');
      $model = new \App\Models\Category();
      $params['categories'] = $model->getListForModule($params['module_id'],$id);
      return $params;
  }

  public function create($categoryParentId = false){
    $this->setBreadcrumbs($this->module.'_create',['categoryParentId'=>$categoryParentId]);
    $params = $this->categoryController->getCommonData("modules/".$this->module."/category",["modules/".$this->module."/post","post/view"]);
    $params = $this->getCommonData($params,'add');
    $params['categoryParentId'] = $categoryParentId;

    if(\App\Models\Module::checkModelBySymbol($this->module)){
      $model = "\App\Models\\".ucfirst($this->module);
      if(method_exists($model,"getCategoryBackUrl")){
        $params['back_url'] = $model::getCategoryBackUrl($categoryParentId);
      }
    }

    return view('admin.module.addcategory')->with($params);
  }

  public function edit($id){
    if(!$this->commonModel->checkIfExists($id,'category')){
      $redirect_url = route($this->module);
      return $this->setMessageBoxAfterRedirect(0, $redirect_url, 'admin.category.no_exists');
    }else{
      $params = $this->categoryController->getCommonData("modules/".$this->module."/category",["modules/".$this->module."/post","post/view"]);
      $params = $this->getCommonData($params,'edit',$id);
      $params['row'] = \App\Models\Category::where('id','=',$id)->with(['language','field','subcategory','modules' => function($query){
        $query->orderBy('position','asc');
      }])->first();
      $params['id'] = $id;
      $params['parsed_modules'] = \App\Models\PagePostModule::parseModules($params['row']->modules);

      if(\App\Models\Module::checkModelBySymbol($this->module)){
        $model = "\App\Models\\".ucfirst($this->module);
        if(method_exists($model,"getCategoryBackUrl")){
          $params['back_url'] = $model::getCategoryBackUrl($params['row']);
        }
      }

      $this->setBreadcrumbs($this->module.'_edit',['category' => $params['row'], 'categoryParentId' => $params['row']->category_parent_id]);
      return view('admin.module.addcategory')->with($params);
    }
  }

  public function delete($id){
    if(!$this->commonModel->checkIfExists($id,'category')){
      $redirect_url = route($this->module);
      return $this->setMessageBoxAfterRedirect(0, $redirect_url, 'admin.category.no_exists');
    }else{
      $oldData = \App\Models\Category::where('id','=',$id)->first();
      $model = new \App\Models\Category();
      $result = $model->deleteCategory($id);

      $msg = 'admin.'.$this->module.'.delete_'.($result ? 'success' : 'failure').'|1';

      $route = route($this->module);
      if(\App\Models\Module::checkModelBySymbol($this->module)){
        $model = "\App\Models\\".ucfirst($this->module);
        if(method_exists($model,"getCategoryBackUrl")){
          $route = $model::getCategoryBackUrl($oldData['category_parent_id']);
        }
      }else{
        $route = route($this->module.'_edit',['id'=>$oldData['category_parent_id']]);
      }

      return $this->setMessageBoxAfterRedirect($result,$route,$msg);
    }
  }

  public function store(Request $request,$id = false){
    $rules = [
      'name' => 'required|max:255'
    ];
    foreach($request->get('name') as $key => $val)
    {
      $rules['name.'.$key] = 'required|max:255';
    }

    $data = $request->all();

    if($data['id'] == "") $id = false;

    $model = new \App\Models\Category();
    \Validator::make($data,$rules)->setAttributeNames($model->getAttributesNames())->validate();

    if(strpos($request->path(),'create') !== false) $id = false;

    $params = $this->getCommonData([],"");
    $fields = \App\Models\ModuleField::where('module_id','=',$params['module_id'])->get();
    foreach($fields as $key=>$field){
      foreach($field->language as $lang)
        $data['field'][$key][$lang->id] = $lang->pivot->name;
        if($id){
          $categoryFieldLang = \App\Models\CategoryField::where('category_id','=',$id)
            ->whereHas('language',function($query) use($lang){
              $query->where('language.id','=',$lang->id);
            })->first();
          if($categoryFieldLang){
            $data['fieldId'][$key] = $categoryFieldLang->id;
          }
        }
    }

    $result = $model->saveCategory($data,$id);

    $response = new \stdClass();
    $response->result = $result->code;
    $response->msg = 'admin.'.($id ? 'update' : $this->module.'.save'). '_'.($response->result ? 'success' : 'failure');
    if(array_key_exists('save_and_stay',$data))
      $response->route = route($this->module.'_edit',['id'=>$result->id]);
    else{
      if($data['category_parent_id']=="")
        $response->route = route($this->module);
      else if(\App\Models\Module::checkModelBySymbol($this->module)){
        $model = "\App\Models\\".ucfirst($this->module);
        $response->route = $model::getCategoryBackUrl($data['category_parent_id']);
      }else{
        $response->route = route($this->module.'_edit',['id'=>$data['category_parent_id']]);
      }
    }

    return $this->setMessageBoxAfterRedirect($response->result, $response->route, $response->msg);
  }
}
