<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class PagePostController extends AdminController
{
    var $model;

    public function __construct(){
      $this->model = new \App\Models\PagePostLanguage();
    }

    public function generatelink(Request $request){
      return response()->json($this->model->generateFriendlyLink($request->input()));
    }

    public function checkModule($moduleId,$method){
      $moduleData = \App\Models\Module::with(['language'=>function($query){
        $query->whereHas('language',function($query){
          $query->where('symbol','=',\App::getLocale());
        });
      },'categoryDefault'])->where('id','=',$moduleId)->first();
      return $moduleData ? $moduleData : false;
    }

    public function createmodule(Request $request){
      $response = new \stdClass();
      $response->code = 0;
      $response->msg = "";

      $params = $request->input();

      $moduleData = $this->checkModule($params['module_id'],'create');

      if($moduleData){
        $response->code = 1;

        $model = new $moduleData['module_entity_model']();
        $valid = true;

        if($moduleData['bind_entity'] == 1)
          $modelId = $params['module_entity_id'];
        else{
          if($moduleData->category_id!=NULL){
            $valid = true;
            $modelId = $params['category_id'] = $moduleData->category_id;
          }else if(!method_exists($model,'createEmpty')){
            $valid = false;
            $modelId = null;
          }else{
            $modelId = $model->createEmpty($moduleData);
          }
        }

        $pagePostModuleId = \App\Models\PagePostModule::attachModule([
          'entity_id' => $params['entity_id'],
          'entity_type' => 'App\Models\\'.ucfirst($params['entity_type']),
          'module_id' => $params['module_id'],
          'module_entity_id' => $modelId,
          'position' => \App\Models\PagePostModule::where('entity_id','=',$params['entity_id'])->where('entity_type','=','App\Models\\'.ucfirst($params['entity_type']))->max('position') + 1
        ]);

        $routeName = $moduleData['bind_entity'] == 1 ? $moduleData['symbol'] : $moduleData['symbol'].'_edit';
        if(\Route::has($routeName))
          $response->msg = route($routeName,['pagePostModuleId'=>$pagePostModuleId,'id'=>$modelId]);
        else{
          $params['active'] = 1;
          $params['module_name'] = $moduleData->language[0]->name;
          $params['module_entity_id'] = $modelId;
          $params['module_entity_model'] = $moduleData['module_entity_model'];
          $params['entity_name'] = $moduleData['bind_entity'] == 1 && method_exists($model,'getNameById') ? $model->getNameById($params['module_entity_id']) : "";
          $params['entity_details'] = method_exists($model,'getModuleDetails') ? $model->getModuleDetails($modelId) : "";
          $params['id'] = $params['module_id'] = $pagePostModuleId;
          $params['symbol'] = $moduleData->symbol;
          $params['category_id'] = $moduleData->category_id;

          $response->view = \View::make('admin.partial.module.choose_row',[
            'row' => (object)$params
          ])->render();
          $response->msg = "";
        }
      }

      return response()->json($response);
    }

    public function removemodule(Request $request){
      $response = new \stdClass();
      $response->code = 0;

      $id = $request->input('id');
      $moduleData = \App\Models\PagePostModule::with(['module'])->where('id','=',$id)->first();

      if($moduleData){
        $model = new $moduleData->module['module_entity_model']();
        $isObjectDelete = true;
        if(method_exists($model,'deleteObject')){
          $isObjectDelete = $model->deleteObject($moduleData['module_entity_id']);
        }

        if($isObjectDelete)
          \App\Models\PagePostModule::where('id','=',$id)->delete();

        $response->code = (int)$isObjectDelete;
      }

      return response()->json($response);
    }

    public function setposition(Request $request){
      $ids = $request->input('ids');

      //$all = count($ids);

      foreach($ids as $position => $id){
        \App\Models\PagePostModule::where('id','=',$id)->update([
          'position' => $position
        ]);
      }
    }
}
