<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModuleController extends Controller
{
    public function entity(Request $request){
      $data = $request->input();
      $result = [];

      if(array_key_exists('id',$data)){
       $module = \App\Models\Module::where('id','=',$data['id'])->first();
       if($module){         
         $result = call_user_func([$module->module_entity_model,'getDataForSelect']);
       }
     }
      return response()->json($result);
    }
}
