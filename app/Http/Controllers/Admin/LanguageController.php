<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Language;
use App\Components\Menu\Menu;
use App\Components\Clist\ListAction;

class LanguageController extends AdminController
{

  private $model;

  public function __construct(){
    parent::__construct();
    $this->middleware('permission:language');
    $this->setActiveMenu(['language']);
    $this->model = new Language();
  }

  public function generateList($rows){
    $list = new \App\Components\Clist\Clist();
    $list->header = trans('menu.language');
    $list->actions = [
      //new \App\Components\Clist\ListActionEdit('language'),
      new \App\Components\Clist\ListAction('admin.list.set_main',"language/setmain/{id}","button button--small button--blue button--withIcon button--iconGalka",false,"")
    ];
    $list->columns = [
      new \App\Components\Clist\ListColumn('name','admin.list.name'),
      new \App\Components\Clist\ListColumn('symbol','admin.list.symbol'),
      new \App\Components\Clist\ListColumn('active','admin.list.active',false,function($row){
        echo \App\Components\Clist\ListColumnFormat::switchValue($row[0]->active,'language',$row[0]->id,'active','language.switchValue');
      }),
      new \App\Components\Clist\ListColumn('main','admin.list.main',false,function($row){
        if($row[0]->main) echo "<span data-language-main='".$row[0]->id."'>".strtoupper(trans('admin.yes'))."</span>";
      }),
      new \App\Components\Clist\ListColumn('visible_on_front','admin.list.visible_on_front',false,function($row){
        echo \App\Components\Clist\ListColumnFormat::switchValue($row[0]->visible_on_front,'language',$row[0]->id,'visible_on_front','language.switchValue');
      })
      //new \App\Components\Clist\ListColumn('domain','admin.list.domain'),
    ];
    $list->rows = $rows;
    return $list->render();
  }

  public function index(){
    $this->setBreadcrumbs('language');
    return view('admin.components.clist.index')->with([
      'list' => $this->generateList($this->model->getList())
    ]);
  }

  /**
   * zmienia flagi języka
   * @param  Request $request
   */
  public function switchValue(Request $request){
    $response = $this->model->switchLangAttributes($request->input());
    return response()->json($response);
  }

  public function setMain($id){
    \App\Models\Language::query()->update(['main'=>0]);

    $oldData = \App\Models\Language::where('id','=',$id)->first();
    $path = public_path('sitemap_'.$oldData->symbol.'.xml');
    if(\File::exists($path)){
      \File::copy($path,public_path('sitemap.xml'));
    }

    return $this->saveLanguage([
      'main'=>1,
      'active'=>1,
      'visible_on_front' => 1
    ], $id);
  }

  public function edit($id){
    if(!$this->commonModel->checkIfExists($id,'language')){
      return $this->setMessageBoxAfterRedirect(0, route('language'), 'admin.language.no_exists','list');
    }else{
      $params['row'] = \App\Models\Language::where('id','=',$id)->first();
      $params['header'] = trans('admin.language.edit_header');
      $this->setBreadcrumbs('language_edit',['lang' => $params['row']]);

      return view('admin.language.edit')->with($params);
    }
  }

  public function saveLanguage($data,$id){
    try{
      if($this->commonModel->checkIfExists($id,'language')){
        if(array_key_exists('main',$data)){
          \App\Models\Language::query()->update(['main'=>0]);
        }

        $data['main'] = array_key_exists('main',$data) ? 1 : 0;
        $model = \App\Models\Language::find($id);
        $model->fill($data);
        $model->save();

        return $this->setMessageBoxAfterRedirect(1, route('language'), 'admin.update_success','list');
      }else{
        return $this->setMessageBoxAfterRedirect(0, route('language'), 'admin.update_failure','list');
      }
    }catch(Exception $e){
      return $this->setMessageBoxAfterRedirect(0, route('language'), 'admin.update_failure','list');
    }
  }

  public function store(Request $request,$id){
    return $this->saveLanguage($request->input(),$id);
  }

}
