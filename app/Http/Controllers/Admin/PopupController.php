<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

set_time_limit(0);

class PopupController extends AdminController
{

    var $model;

    public function __construct(){
      parent::__construct();
      $this->setActiveMenu(['module','popup']);
      $this->model = new \App\Models\Popup();
    }

    public function index(){
      $this->setBreadcrumbs('popup');
      $pageModel = new \App\Models\Page();
      return view('admin.popup.index',[
        'row'=>$this->model->getData(),
        'header'=> trans('menu.popup'),
        'langs'=>\App\Models\Language::getActive(),
        'settings' => \App\Models\Settings::getByGroup('images'),
        'pages' => $pageModel->getForSelect()
      ]);
    }

    public function store(Request $request){
      $data = $request->all();

      $oldPopup = \App\Models\Popup::with(['language'])->first();

      $langs = \App\Models\Language::getActive();

      $rules = [];
      if(!$request->has('visible')){
        $rules['visible_from'] = 'required|date_format:d-m-Y|before_equal:visible_to';
        $rules['visible_to'] = 'required|date_format:d-m-Y';
      }

      switch($data['content_type']){
        case 'youtube':
          $rules['youtube_content'] = 'required';
          break;
        case 'image':
          $imageExists = false;
          if($oldPopup && is_string($oldPopup->language[0]->content)){
            $file = public_path()."/img/popup/image.".substr($oldPopup->language[0]->content,0,strpos($oldPopup->language[0]->content,"?"));
            $imageExists = \File::exists($file);
          }

          if(!$oldPopup || !$imageExists){
            $rules['thumbnail_encoded'] = 'required';
          }
          break;
      }

      \Validator::make($data,$rules)->setAttributeNames($this->model->getAttributesNames())->validate();

      $result = $this->model->saveData($data);
      $msg = $result ? trans('admin.update_success') : trans('admin.update_failure');
      return $this->setMessageBoxAfterRedirect($result, route('popup'), $msg);
    }
}
