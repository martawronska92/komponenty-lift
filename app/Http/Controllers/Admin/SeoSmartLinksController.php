<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SeoSmartLinksController extends AdminController
{
  var $model;

  public function __construct(){
    parent::__construct();
    $this->middleware('privilege:seo');
    $this->setActiveMenu(['seo']);
    $this->model = new \App\Models\SeoSmartLinks();
  }

  public function index(Request $request){
    $rows = $this->model->getList($request->input());

    $list = new \App\Components\Clist\Clist();
    $list->header = trans('admin.seo.smart_links_header');
    $list->buttons = [
      new \App\Components\Clist\ListButtonAdd('admin.seo.add_smart_links_button','seo/smartlinks'),
      new \App\Components\Clist\ListButton('admin.seo.import_smart_links','seo/smartlinks/import','button button--small button--blue button--withIcon button--iconDownload',''),
      new \App\Components\Clist\ListButton('admin.seo.get_smart_links_sample','seo/smartlinks/samplefile','button button--small button--blue button--withIcon button--iconSample','')
    ];
    $list->actions = [
      new \App\Components\Clist\ListActionEdit('seo/smartlinks'),
      new \App\Components\Clist\ListActionDelete('seo/smartlinks',
          trans('admin.seo.delete_smart_link').\App\Helpers\Format::confirmDeleteName(' {word}')." ?")
    ];
    $list->columns = [
      new \App\Components\Clist\ListColumn('word','admin.seo.word',true),
      new \App\Components\Clist\ListColumn('link','admin.seo.link',true)
    ];
    $list->rows = $rows;

    return view('admin.components.clist.index')->with([
      'breadcrumbs' => $this->setBreadcrumbs('seo_smart_links'),
      'list' => $list->render()
    ]);
  }

  public function import(){
    return view('admin.seo.smartlinks.import',[
      'breadcrumbs' => $this->setBreadcrumbs('seo_smart_links_import'),
      'header' => trans('admin.seo.import_smart_links_header')
    ]);
  }

  public function add(){
    return view('admin.seo.smartlinks.add',[
      'header'=> trans('admin.seo.create_smart_link'),
      'breadcrumbs' => $this->setBreadcrumbs('seo_smart_links_add')
    ]);
  }

  public function edit($id){
    $params = [
      'header'=> trans('admin.seo.edit_smart_link'),
      'row' => \App\Models\SeoSmartLinks::where('id','=',$id)->first()
    ];
    $params['breadcrumbs'] = $this->setBreadcrumbs('seo_smart_links_edit',['link' => $params['row']]);
    return view('admin.seo.smartlinks.add',$params);
  }

  public function store(Request $request,$id = false){
    $response = $this->model->store($request->all(),$id);
    return $this->setMessageBoxAfterRedirect($response->code,route('seo_smart_links'),$response->msg,'list');
  }

  public function delete($id){
    $response = $this->commonModel->deleteMulti($id,'seo_smart_links');
    $msg = 'admin.seo.delete_links_'.($response->code ? 'success' : 'failure').'|'.$response->itemCounter;
    return $this->setMessageBoxAfterRedirect($response->code,route('seo_smart_links'),$msg,'list');
  }

  public function samplefile(){
    $file = public_path('file/seo_smartlinks_sample.csv');
    return response()->download($file);
  }
}
