<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $redirectTo = '/panel';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function reset(Request $request) {

        $valid = $this->validate($request,
          [
            'token' => 'required', 'email' => 'required|email',
            'password' => 'required|confirmed|min:8|regex:"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$"',
          ],
          [
            'regex' => trans('admin_js.validation.password'),
            'confirmed' => 'hasła muszą się zgadzać'
          ]
        );

        $inputs = $request->input();

        $response = $this->broker()->reset(
                $this->credentials($request), function ($user, $password) use ($request, $inputs) {
                    $this->resetPassword($user, $password);
                }
        );

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $response == \Password::PASSWORD_RESET ? $this->sendResetResponse($response) : $this->sendResetFailedResponse($request, $response);
    }

    protected function credentials(Request $request) {
        return $request->only('email', 'password', 'password_confirmation', 'token');
    }

    protected function resetPassword($user, $password) {
        $user->forceFill([
            'password' => bcrypt($password),
            'remember_token' => \Illuminate\Support\Str::random(60),
        ])->save();

        $this->guard()->login($user);
    }

    protected function sendResetResponse($response) {
        return redirect($this->redirectPath())
                        ->with('status', trans($response));
    }

    protected function sendResetFailedResponse(Request $request, $response) {
        return redirect()->back()
                        ->withInput($request->only('email'))
                        ->withErrors(['email' => trans($response)]);
    }

    public function broker() {
        return \Password::broker();
    }

    protected function guard() {
        return \Auth::guard();
    }
}
