<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

class HomeController extends FrontController
{
    public function __construct()
    {

    }

    public function index()
    {
        if(\Route::currentRouteName() == "homepage" && \App\Helpers\Language::checkPrefix()){
          return redirect()->route('homepage_language',['frontlanguage'=>\App::getLocale()]);
        }

        $langs = \App\Models\Language::getFrontLanguages();
        $params = [];
        foreach($langs as $lang){
          $routeParams = [];
          $routeName = 'homepage';
          if(\App\Helpers\Language::hasPrefix()){
            $routeParams['frontlanguage'] = $lang->symbol;
            $routeName.= '_language';
          }
          $params[$lang->symbol] = route($routeName,$routeParams);
        }

        return $this->assignCommonVariables(view('front/home'))->with([
          'lang_switcher_links' => $params
        ]);
    }
}
