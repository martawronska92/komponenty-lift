<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

class PagePostController extends FrontController
{
    public function show($param1,$param2 = null,$param3 = null, $param4 = null){
      $frontLanguage = "";
      if(\App\Helpers\Language::hasPrefix()){
        $frontLanguage = $param1;
      }

      for($i=4;$i>0;$i--){
        $variable = "param".$i;
        $param = $$variable;
        if($param!=null){
          break;
        }
      }

      if($param == null){
        echo "404";
      }else{

        $row = \App\Models\PagePostLanguage::where('url','=',$param)
          ->whereHas('language',function($query){
            $query->where('symbol','=',\App::getLocale());
          })
          ->first();

        if(!$row){
          echo "404"; //lub sprawdzanie kategorii
        }else{
          $entityType = ucfirst(\App\Helpers\Parse::parseEntity($row->entity_type));
          $controller = "\App\Http\Controllers\Front\\".$entityType."Controller";
          $controllerObject = new $controller();
          return $controllerObject->show($frontLanguage,$param);
        }
      }
    }
}
