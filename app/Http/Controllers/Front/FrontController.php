<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FrontController extends Controller
{

  public function __construct(){
    $this->middleware(function ($request, $next) {
      \App\Components\MessageBox::get();
      return $next($request);
    });
  }

  /**
   * pokazuje box z wiadomością po przekierowaniu
   * @param int $type  0 - error, 1 - success, 2 - info
   * @param string $url url na który przekierowanie
   * @param string $msg   wiadomość do tłumaczenia
   */
  public function setMessageBoxAfterRedirect($type,$url,$msg){
    \App\Components\MessageBox::set((int)$type,$msg,'main');
    return redirect($url);
  }

  /**
   * przypisuje wspólne zmienn dla wszystkich widoków
   * @param  boolean/string $view nazwa widoku
   * @return view        zwraca widok z przypisanymi zmiennymi
   */
  public function assignCommonVariables($view = false){
    $params = [
      'settings' => \App\Models\Settings::getAll(\App::getLocale()),
      'langPrefix' => \App\Helpers\Language::checkPrefix(),
      'segments' => \Request::segments()
    ];
    return $view ? $view->with($params) : $params;
  }

  /**
   * ustawia breadcrumbsy
   * @param string  $name  nazwa zarejestrowanego breadcrumbsa
   * @param boolean/object $object opcjonalny obiekt do pobierania danych do breadcrumbsa
   */
  public function setBreadcrumbs($name,$object = false){
    \View::share('breadcrumbsName',$name);
    if($object!=false){
      \View::share('breadcrumbsObject',$object);
    }
  }
}
