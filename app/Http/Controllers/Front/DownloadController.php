<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

class DownloadController extends FrontController
{

  public function getfile($filenameHashed){
    $file = \App\Models\DownloadFile::with('language')
      ->whereHas('language.language',function($query){
        $query->where('symbol','=',\App::getLocale());
      })
      ->where('filename_hashed','=',$filenameHashed)
      ->where('active','=',1);

    if($file->count() == 0){
      return redirect('/');
    }else{
      $row = $file->first();
      $file->update([
        'downloaded' => $row->downloaded+1
      ]);
      $pathToFile = public_path().'/file/download/'.$row->filename_hashed.'.'.$row->ext;

      if(!\File::exists($pathToFile)){
        return redirect(\URL::previous());
        exit();
      }

      $title = $row->language[0]->title;
      if($title == ""){
          $title = $row->id;
      }
      $title.= ".".$row->ext;

      $showFile = (int)\App\Models\Settings::getBySymbol('show_download_pdf');
      if(strtolower($row->ext) == "pdf" && $showFile){
        return \Response::make(file_get_contents($pathToFile), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$title.'"'
        ]);
      }else{
        return response()->download($pathToFile,$title);
      }
    }
  }

}
