<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RouterController extends FrontController
{
    public function __invoke($param1,$param2 = null,$param3 = null, $param4 = null,$param5 = null){
      $frontLanguage = "";

      $hasLanguagePrefix = \App\Helpers\Language::hasPrefix();

      if($hasLanguagePrefix){
        $frontLanguage = $param1;
        $callbackParams[] = $param1;
      }

      for($i=5; $i>0; $i--){
        $variable = "param".$i;
        $lastParam = $$variable;

        $lastParamNr = $i;
        if($lastParam!=null){
          break;
        }
      }

      if($lastParam == null){
        \Abort('404');
      }else{
        /**
         * czy kategoria
         */

        $category = \App\Helpers\Url::isCategory($lastParam);

        if($category && \Request::getPathInfo() == \App\Helpers\Url::getCategoryUrl($category->category_id,\App::getLocale()) ){
            $controller = "\App\Http\Controllers\Front\ModuleController";
            $controllerObject = new $controller();

            return call_user_func_array(array($controllerObject, "category"), [$lastParam]);
        }else{

          /**
           * czy strona lub wpis
           */

          $postPage = \App\Helpers\Url::isPostPage($lastParam);

          if($postPage){
            $entityType = \App\Helpers\Parse::parseEntity($postPage->entity_type);

            /**
             * sprawdzenie domyślnych linków dla posta/strony z requstem
             */
            if( ($entityType == "post" && in_array(\Request::getPathInfo(),\App\Helpers\Url::getPostUrl($postPage->entity_id,\App::getLocale(),true,true)))
             || ($entityType == "page" && \Request::getPathInfo()!=\App\Helpers\Url::getPageUrl($postPage->entity_id,\App::getLocale())) ){
               \Abort('404');
            }

            /**
             * zmienne url strony i wpisu
             */

            if($entityType == "post"){
              $pageUrl = "param".($hasLanguagePrefix ? "2" : "1");
              $callbackParams[] = $$pageUrl;
            }
            $callbackParams[] = $lastParam;
            if($entityType == "page")
              $callbackParams[] = $postPage->entity_id;

            /**
             * wywołanie akcji w kontrolerze
             */

            $controller = "\App\Http\Controllers\Front\\".ucfirst($entityType)."Controller";
            $controllerObject = new $controller();

            return call_user_func_array(array($controllerObject, "show"), $callbackParams);

          }else{
            \Abort('404');
          }
        }
      }
    }
}
