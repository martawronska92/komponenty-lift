<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends FrontController
{
    public function send(Request $request){
      $data = $request->input();

      $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'phone' => 'required|digits:9',
        'message' => 'required'
      ];

      $attributes = [
        'name' => trans('front.contact.name'),
        'email' => trans('front.contact.email'),
        'phone' => trans('front.contact.phone'),
        'message' => trans('front.contact.message')
      ];

      \Validator::make($data,$rules)->setAttributeNames($attributes)->validate();

      $emailAddr = \App\Models\Settings::getBySymbol('contact_notification_email');
      if($emailAddr!=""){
        \Mail::to($emailAddr)->send(new \App\Mail\Contact($data));
        $result = count(\Mail::failures()) ==0;
        $msg = trans('front.contact.message_send_'.($result ? 'success' : 'failure'));
        return $this->setMessageBoxAfterRedirect((int)$result,\URL::previous(),$msg);
      }else{
        return $this->setMessageBoxAfterRedirect(0,\URL::previous(),trans('front.contact.message_send_failure'));
      }
    }
}
