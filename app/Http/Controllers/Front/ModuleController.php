<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModuleController extends FrontController
{
    var $segments;
    var $pageController;

    public function __construct(){
      $this->pageController = new \App\Http\Controllers\Front\PageController();
      $this->postController = new \App\Http\Controllers\Front\PostController();
    }

    public function getCategoryData($slug){

       return \App\Models\Category::with([
        'language' => function($query){
            $query->whereHas('language',function($query){
              $query->where('symbol','=',\App::getLocale());
              $query->where('active','=',1);
              $query->where('visible_on_front','=',1);
            });
        },
        'post' => function($query){
            $query->whereHas('language',function($query){
                $query->whereHas('language',function($query){
                  $query->where('symbol','=',\App::getLocale());
                  $query->where('active','=',1);
                  $query->where('visible_on_front','=',1);
                });
            })
            ->where('active','=',1)
            ->orderBy('position','desc');
          },
          'subcategory' => function($query){
            $query->whereHas('language',function($query){
                $query->whereHas('language',function($query){
                  $query->where('symbol','=',\App::getLocale());
                  $query->where('active','=',1);
                  $query->where('visible_on_front','=',1);
                });
            })
            ->where('active','=',1)
            ->orderBy('position','desc');
          },
          'modules' => function($query){
            $query->where('active','=',1);
          }
        ])
        ->whereHas('language',function($query) use($slug){
          $query->where('url','=',$slug);
        })
        // ->whereHas('post',function($query){
        //     $query->whereHas('language.language',function($query){
        //       $query->where('symbol','=',\App::getLocale());
        //   });
        // })
        ->where('active','=',1)
        ->first();
    }

    public function generateLangLinks($categoryId,$postId = false){
      $languages = \App\Models\Language::getFrontLanguages();

      $resposne  = [];

      foreach($languages as $lang){
        if($postId){
          $response[$lang->symbol] = \App\Helpers\Url::getPostUrl($categoryId,$lang->symbol,true);
        }else if($categoryId){
          $response[$lang->symbol] = \App\Helpers\Url::getCategoryUrl($categoryId,$lang->symbol);
        }
      }

      return $response;
    }

    public function category($language,$slug = ""){
      $pageSlug = \Route::current()->hasParameter('frontlanguage') ? \Request::segment(2) : \Request::segment(1);

      if($slug == "") $slug = $language;
      $pageData = $this->pageController->getPageData($pageSlug);
      $categoryData = $this->getCategoryData($slug);

      foreach($categoryData->post as $key=>$post){
            $categoryData->post[$key]->language = $categoryData->post[$key]->language()->whereHas('language',function($query){
              $query->where('symbol','=',\App::getLocale());
            })->get();
      }

      $this->setBreadcrumbs('front_category',[
        'category' => $categoryData,
        'page' => $pageData
      ]);

      if($categoryData == null || $pageData == null){
        \Abort('404');
      }else{
        return $this->assignCommonVariables(view('front.'.$categoryData->category_view)->with([
          'category' => $categoryData,
          'page' => $pageData,
          'meta_title' => $categoryData->language[0]->meta_title,
          'meta_description' => $categoryData->language[0]->meta_description,
          'lang_switcher_links' => $this->generateLangLinks($categoryData->id)
        ]));
      }
    }

    public function postfromcategory(Request $request){
      $model = new \App\Models\Post();
      $posts = $model->getFrontList($request->input('category_id'),false,false,$request->input('page_nr'));

      $response = new \stdClass();
      $response->posts = [];

      foreach($posts as $post){
        $tempPost = new \stdClass();
        $tempPost->title = $post->language[0]->title;
        $tempPost->desc = $post->getContent();
        $tempPost->intro = $post->language[0]->content;
        $tempPost->date = ($post->created_at->day < 10 ? "0" : "").$post->created_at->day."/".($post->created_at->month < 10 ? "0" : "").$post->created_at->month;
        $tempPost->year = $post->created_at->year;
        $tempPost->link = \App\Helpers\Url::getFrontPostUrl($post->id);
        array_push($response->posts, $tempPost);
      }

      $response->page = $request->input('page_nr');
      return response()->json($response);
    }

    /*public function post($language="",$categorySlug,$postSlug=""){
      if($postSlug==""){
        $postSlug = $categorySlug;
        $categorySlug = $language;
        $language = "";
      }

      $pageSlug = \Route::current()->hasParameter('frontlanguage') ? \Request::segment(2) : \Request::segment(1);
      $pageData = $this->pageController->getPageData($pageSlug);
      $categoryData = $this->getCategoryData($categorySlug);
      $postData = $this->postController->getPostData($postSlug);

      if($categoryData == null || $pageData == null || $postData == null){
        \Abort('404');
      }else{
        return $this->assignCommonVariables(view('front.'.$categoryData->post_view)->with([
          'category' => $categoryData,
          'page' => $pageData,
          'post' => $postData,
          'meta_title' => $postData->language[0]->meta_title,
          'meta_description' => $postData->language[0]->meta_description,
          'lang_switcher_links' => $this->generateLangLinks($categoryData->id,$postData->id)
        ]));
      }
    }*/
}
