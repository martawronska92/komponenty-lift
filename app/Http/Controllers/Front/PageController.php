<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

class PageController extends FrontController
{
    var $model;

    public function __construct(){
      parent::__construct();
      $this->model = new \App\Models\Page();
    }

    /**
     * pobiera dane strony
     * @param  string $slug url - opcjonalnie z id
     * @param  integer $id  id strony - opcjonalnie z slugiem
     * @return \App\Models\Page
     */
    public function getPageData($slug = "" ,$id = ""){
      $data = \App\Models\Page::with(['language' => function($query){
        $query->whereHas('language',function($query){
            $query->where('symbol','=',\App::getLocale());
            $query->where('active','=',1);
            $query->where('visible_on_front','=',1);
        });
      },'user',
      'module' => function($query){
        $query->where('active','=',1)
               ->orderBy('position','asc');
      }
      ]);

      if($id!=""){
        $data = $data->where('id','=',$id);
      }
      else if($slug!=""){
        $data = $data->whereHas('language',function($query) use($slug){
          $query->where(\DB::raw('lower(url)'),'=',strtolower($slug));
          $query->whereHas('language',function($query){
              $query->where('symbol','=',\App::getLocale());
              $query->where('active','=',1);
              $query->where('visible_on_front','=',1);
          });
        })
        ->whereNull('draft');
      }

      return $data->where('active','=',1)->first();
    }

    /**
     * metoda wywoływana przez route`a
     * @param  string $language pl/en/...
     * @param  string $slug     url
     * @return view
     */
    public function show($language,$slug,$id = ""){
      if($id == ""){
        $id = $slug;
        $slug = $language;
        $language = "";
      }

      $pageData = $this->getPageData($slug,$id);

      $this->setBreadcrumbs('front_page',['id'=>$id]);

      if(!$pageData){
        \Abort('404');
      }else{
        return $this->showPreview($pageData);
      }
    }

    /**
     * metoda wywoływana z admina
     * @param  integer $pageId id strony
     * @return view
     */
    public function generatePreview($pageId){
      $pageData = $this->getPageData("",$pageId);
      return $this->showPreview($pageData);
    }

    /**
     * linki do przełącznika języków
     * @param  [type] $pageId [description]
     * @return [type]         [description]
     */
    public function generateLangLinks($pageData){
      $response = [];

      $langs = \App\Models\Language::getFrontLanguages();
      foreach($langs as $lang){
        $response[$lang->symbol] = \App\Helpers\Url::getPageUrl($pageData->id,$lang->symbol);
      }

      return $response;
    }

    public function waitForPreview(){
      return $this->assignCommonVariables(view('front.partial.preview.wait'));
    }

    /**
     * generuje widok z przekazanymi parametrami
     * @param  \App\Models\Page $pageData dane strony
     * @return view
     */
    public function showPreview($pageData){
      if(!\View::exists('front.'.$pageData->default_view)){
        return redirect('/');
      }else{
        $menu['parent_page_id'] = $this->model->getMainParentPageId($pageData->id);
        $menu['elements'] = $this->model->getAllChildrenList($menu['parent_page_id'],true);

        $params = [
          'page' => $pageData,
          'meta_title' => $pageData->language[0]->meta_title,
          'meta_description' => $pageData->language[0]->meta_description,
          'type' => 'page',
          'menu' => $menu
        ];

        $url = \Request::path();
        if(\App\Helpers\Language::hasPrefix()){
          $params['lang_switcher_links'] = $this->generateLangLinks($pageData);
          $url = substr($url,3,strlen($url));
        }

        /**
         * podmiana adresu url dla podglądu aby wyświetlać moduły
         */
        if(strpos($url,"showpreview")!==false){
          $url = $pageData->language[0]->url;
        }

        $params['modules'] = \App\Models\ModuleFrontLink::where('link','=',strtolower($url))
                      ->whereHas('language',function($query){
                        $query->where('symbol','=',\App::getLocale());
                      })->get();

        return $this->assignCommonVariables(view('front.'.$pageData->default_view)->with($params));
      }
    }
}
