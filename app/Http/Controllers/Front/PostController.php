<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

class PostController extends FrontController
{
  var $model;
  var $pageController;

  public function __construct(){
    parent::__construct();
    $this->model = new \App\Models\Post();
    $this->pageController = new \App\Http\Controllers\Front\PageController();
  }

  /**
   * pobiera dane wpisu
   * @param  string $slug url - opcjonalnie z id
   * @param  integer $id  id strony - opcjonalnie z slugiem
   * @return \App\Models\Post
   */
  public function getPostData($slug = "", $id = false){
    $data = \App\Models\Post::with([
      'language' => function($query){
        $query->whereHas('language',function($query){
            $query->where('symbol','=',\App::getLocale());
            $query->where('active','=',1);
            $query->where('visible_on_front','=',1);
        });
      },
      'user',
      'module' => function($query){
        $query->where('active','=',1)
               ->orderBy('position','asc');
      },
      'category',
      'field.language' => function($query){
        $query->where('symbol','=',\App::getLocale());
        $query->where('active','=',1);
        $query->where('visible_on_front','=',1);
      },
      'tag.language.language' => function($query){
        $query->where('symbol','=',\App::getLocale());
        $query->where('active','=',1);
        $query->where('visible_on_front','=',1);
      }
    ]);


    if($slug!=""){
      $data = $data->whereHas('language',function($query) use($slug){
        $query->where(\DB::raw('lower(url)'),'=',strtolower($slug));
        $query->whereHas('language',function($query){
            $query->where('symbol','=',\App::getLocale());
            $query->where('active','=',1);
            $query->where('visible_on_front','=',1);
        });
      });
    }else{
      $data = $data->where('id','=',$id);
    }


    return $data->where('active','=',1)->first();
  }

  /**
   * metoda wywoływana przez route`a
   * @param  string $language pl/en/...
   * @param  string $slug     url
   * @return view
   */
  public function show($language,$page,$post = ""){
    if($post == ""){
      $post = $page;
      $page = $language;
      $language = "";
    }

    $pageData = $this->pageController->getPageData($page);

    $postData = $this->getPostData($post);

    $this->setBreadcrumbs('front_post',[
      'post' => $postData,
      'page' => $pageData
    ]);

    if(!$postData || !$pageData){
      \Abort('404');
    }else{

      return $this->showPreview($postData,$pageData);
    }
  }

  public function waitForPreview(){
    return $this->assignCommonVariables(view('front.partial.preview.wait'));
  }

  /**
   * metoda wywoływana z admina
   * @param  integer $postId id wpisu
   * @return view
   */
  public function generatePreview($postId){
    $pageData = [];
    $postData = $this->getPostData("",$postId);
    return $this->showPreview($postData,$pageData);
  }

  /**
   * linki do przełącznika języków
   * @param  [type] $pageId [description]
   * @return [type]         [description]
   */
  public function generateLangLinks($postData,$pageData){
    $response  = [];

    $langs = \App\Models\Language::getFrontLanguages();
    foreach($langs as $lang){
      $response[$lang->symbol] = \App\Helpers\Url::getFrontPostUrl($postData->id,$lang->symbol);
    }

    return $response;
  }

  /**
   * [showPreview description]
   * @param  \App\Models\Post $postData dane wpisu
   * @param  \App\Models\Page $pageData dane strony
   * @return view
   */
  public function showPreview($postData,$pageData){
    if(!\View::exists('front.'.$postData->category->post_view)){
      return redirect('/');
    }else{
      $params = [
        'post' => $postData,
        'page' => $pageData,
        'meta_title' => $postData->language[0]->meta_title,
        'meta_description' => $postData->language[0]->meta_description,
        'type' => 'post'
      ];

      if(\App\Helpers\Language::hasPrefix() && count($pageData)>0 && count($postData)){
        $params['lang_switcher_links'] = $this->generateLangLinks($postData,$pageData);
      }

      return $this->assignCommonVariables(view('front.'.$postData->category->post_view)->with($params));
    }
  }

  public function getFromTag($frontlanguage,$tagName = "", $page = ""){
    if($page == ""){
      if(\App\Helpers\Language::hasPrefix()){
        $page = 1;
      }else{
        $page = $tagName;
        $tagName = $frontlanguage;
      }
    }

    $response = $this->model->getByTag($tagName,$page);

    $params = [
      'results' => $response->results,
      'paginator' => $response->paginator,
      'tag' => \App\Models\Tag::whereHas('language',function($query) use ($tagName){
        $query->where('friendly_name','=',$tagName);
        $query->whereHas('language',function($query){
          $query->where('symbol','=',\App::getLocale());
        });
      })->first(),
      'tagName' => $tagName
    ];

    $this->setBreadcrumbs('front_tag',[
      'name' => $params['tag'] && count($params['tag']->language) >0 ? $params['tag']->language[0]->name : $tagName,
      'friendly_name' => $params['tag'] && count($params['tag']->language) >0 ? $params['tag']->language[0]->friendly_name : $tagName
    ]);

   if(\App\Helpers\Language::hasPrefix()){
      if($params['tag']!=NULL){
        $langs = \App\Models\TagLanguage::with(['language'])->where('tag_id','=',$params['tag']->id)->get();
        foreach($langs as $lang){
             $params['lang_switcher_links'][$lang->language->symbol] = route('front_tag',['frontlanguage' => $lang->language->symbol, 'tagName' => $lang->friendly_name],false);
        }
      }else{
        $langs = \App\Models\Language::getFrontLanguages();
        foreach($langs as $lang){
             $params['lang_switcher_links'][$lang->symbol] = route('front_tag',['frontlanguage' => $lang->symbol, 'tagName' => $tagName],false);
        }
      }
    }

    return $this->assignCommonVariables(view('front.post.tag_posts',$params));
  }

}
