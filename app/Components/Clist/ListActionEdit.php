<?php

  namespace App\Components\Clist;

  class ListActionEdit extends ListAction{

    /**
     * tworzy akcję do edycji
     * @param  string  $url     url dla akcji edycji
     * @param  string  $customUrl     url nadpisujący domyślny
     * @return \App\Components\Clist\ListAction
     */
    public function __construct($url,$customUrl = false){
      $editActionUrl = $customUrl ? $customUrl : $url.'/edit/{id}';
      parent::__construct('admin.edit',$editActionUrl,'button button--small button--blue button--withIcon button--iconPencil',false,'');
    }

    public function render($row){
      return parent::render($row);
    }
  }
