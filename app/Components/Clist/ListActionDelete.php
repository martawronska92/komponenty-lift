<?php

  namespace App\Components\Clist;

  class ListActionDelete extends ListAction{

    /**
     * tworzy akcję do usunięcia obiektu
     * @param  string $url     podstawowy url
     * @param  string $confirm tekst w popupie na potwierdzenie
     * @param  string $customUrl url do nadpisania domyślnego
     * @return \App\Components\Clist\ListAction
     */
    public function __construct($url,$confirm,$customUrl = false){
      $deleteActionUrl = $customUrl ? $customUrl : $url."/delete/{id}";
      parent::__construct('admin.delete',$deleteActionUrl,'button button--small button--gray button--withIcon button--iconDel',$confirm,'');
    }

    public function render($row){
      return parent::render($row);
    }
  }
