<?php

  namespace App\Components\Clist;
  use App\Components\Clist\ListButton;

  class ListButtonAdd extends ListButton{

    /**
     * tworzy przycisk dodawania
     * @param  string $label etykieta do tłumaczenia
     * @param  string $url   adres url akcji
     * @param  string $customUrl własny adres do akcji dodawania
     * @return \App\Component\Clist\ListButton
     */
    public function __construct($label,$url,$customUrl = false){
      $addActionUrl = $customUrl ? $customUrl : $url.'/create/';
      parent::__construct($label,$addActionUrl,'button button--small button--green button--withIcon button--iconAdd','');
    }

    public function render(){
      return parent::render();
    }

  }
