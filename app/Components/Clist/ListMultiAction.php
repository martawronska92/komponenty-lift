<?php

namespace App\Components\Clist;

  class ListMultiAction{

      /**
       * akcje na wielu wierszach
       */

        private $url;
        private $label;
        private $confirm;

        /**
         * tworzy nową akcję
         * @param  string  $url     adres url akcji
         * @param  string  $label   etykieta do tłumaczenia
         * @param  string  $confirm  pytanie na potwierdzenie
         * @return AppComponentsClistListMultiAction
         */
        public function __construct($url,$label,$confirm){
          $this->url = $url;
          $this->label = trans($label);
          $this->confirm = trans($confirm);
        }

        public function getUrl(){
          return $this->url;
        }

        public function getLabel(){
          return $this->label;
        }

        public function getConfirm(){
          return $this->confirm;
        }

      }
