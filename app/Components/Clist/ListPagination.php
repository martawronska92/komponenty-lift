<?php

namespace App\Components\Clist;

  class ListPagination{

      /**
       * stronicowanie na liście
       */

        private $rows;
        private $url;

        /**
         * tworzy obiekt paginacji
         * @param  string  $rows   paginacja z eloqunet
         * @param  string  $url   url dla paginacji ajax
         */
        public function __construct($rows,$url=""){
          $this->rows = $rows;
          $this->url = $url;
        }

        public function getRows(){
          return $this->rows;
        }

        public function isAjax(){
          return $this->url != "";
        }

        public function getUrl(){
          return $this->url;
        }

      }
