<?php

namespace App\Components\Clist;

class ListButton{

  private $label;
  private $url;
  private $class;
  private $faclass;
  private $attributes;

  /**
   * tworzy przycisk nad listą
   * @param string $label etykieta do tłumaczenia
   * @param string $url   adres url akcji
   * @param string $class klasa dla przycisku
   * @param string $faclass klasa font awesome icon
   * @param array $attributes dodatkowe atrybuty przycisku
   */
  public function __construct($label,$url = "",$class,$faclass = "",$attributes = []){
    $this->class = $class;
    $this->label = $label;
    $this->url = $url!="" ? "/panel/".$url : "";
    $this->faclass = $faclass;
    $this->attributes = $attributes;
  }

  public function render(){
    return view('admin.components.clist.button')->with([
      'class'=>$this->class,
      'label'=>$this->label,
      'url'=>$this->url,
      'faclass'=>$this->faclass,
      'attributes' => $this->attributes
    ]);
  }

}
