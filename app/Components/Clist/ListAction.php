<?php

  namespace App\Components\Clist;

  /**
   * akcje na wierszu listy
   */
  class ListAction{

    private $url;
    private $confirm;
    private $class;
    private $label;
    private $faclass;

    /**
     * tworzy nową akcję
     * @param  string/callback  $label   etykieta do tłumaczenia / funkcja do wywołania
     * @param  string  $url     adres url akcji
     * @param  string  $class   klasa dla akcji
     * @param  boolean/string $confirm  czy wyświetlić potwierdzenie akcji, wtedy $confirm to wyświetlane pytanie
     * @param string $faclass klasa font awesome dla buttona akcji
     * @param array $attributes dodatkowe atrybuty przycisku
     * @return AppComponentsClistListAction
     */
    public function __construct($label,$url,$class,$confirm = false,$faclass="",$attributes = []){
      $this->label = $label;
      $this->url = $url!="" ? "/panel/".$url : "";
      $this->class = $class;
      $this->confirm = $confirm;
      $this->faclass = $faclass;
      $this->attributes = $attributes;
    }

    public function getUrl(){
      return $this->url;
    }

    public function getLabel(){
      return $this->label;
    }

    public function getClass(){
      return $this->class;
    }

    public function getConfirm(){
      return $this->confirm;
    }

    public function getFaClass(){
      return $this->faclass;
    }

    public function getAttributes(){
      return $this->attributes;
    }

    /**
     * parsuje url akcji i zamienia zmienne na ich wartości
     * @param  object/array $row pojedynczy wiersz listy
     * @param  string $url adres url akcji
     * @return string
     */
    public function parseUrl($row,$url){
      preg_match('/\{[a-zA-Z0-9\-\_\.]*\}/', $url, $matches);
      foreach($matches as $match){
        $field = str_replace(array('{','}'),"",$match);
        // if(strpos($field,".")!==FALSE){
        //   $parts = explode('.',$field);
        //   $value = $row[$parts[0]][0][$parts[1]];
        // }else{
          $value = $row->$field;
        //}
        $url = str_replace($match,$value,$url);
      }
      return $url;
    }

    public function render($row){
      return view('admin.components.clist.action')->with([
        'url'=>$this->parseUrl($row,$this->url),
        'confirm'=>$this->parseUrl($row,$this->confirm),
        'class'=>$this->class,
        'label'=>$this->label,
        'row'=>$row,
        'faclass'=>$this->faclass,
        'attributes' => $this->attributes
      ]);
    }

  }
