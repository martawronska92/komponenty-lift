<?php

  namespace App\Components\Clist;

  /**
   * filtrowanie wierszy na liście
   */
  class ListQuery{

    /**
     * główne zapytanie listy - eloquent
     * @var string
     */
    public $query;
    /**
     * pole według którego sortowanie
     * @var string
     */
    public $orderField;
    /**
     * porządek sortowania - asc/desc
     * @var string
     */
    public $order;
    /**
     * request zrzutowany do tablicy
     * @var array
     */
    public $request;
    /**
     * tablica nazw pól do pobrania z bazy
     * @var array
     */
    public $fields;
    /**
     * tablica nazw pól po których można wyszukiwać
     * @var array
     */
    public $searchable;
    /**
     * tablica pól po których można sortować
     * @var array
     */
    public $orderable;
    /**
     * czy stronicować wyniki zapytania
     * @var boolean
     */
    public $paginate;

    public function __construct(){
      $this->query = $this->orderField = $this->order = "";
      $this->request = $this->fields = $this->searchable = $this->orderable = [];
      $this->paginate = 1;
    }

    /**
     * foramtuje query do listy
     * @return  zwraca query z paginacją
     */
    public function format(){
      /**
       * FILTERS
       */

      $this->query = $this->query->select($this->fields);

      $filters = [];

      if(array_key_exists('search_value',$this->request)
            && array_key_exists('search_column',$this->request)
            && in_array($this->request['search_column'],$this->searchable)
            && $this->request['search_value']!=""){
        $this->query = $this->query->where($this->request['search_column'],'like',strtolower($this->request['search_value']).'%');
        $filters['search_column'] =  $this->request['search_column'];
        $filters['search_value'] = $this->request['search_value'];
      }

      /**
       * ORDER
       */

      if(array_key_exists('order',$this->request)
             && array_key_exists('order_field',$this->request)
             && $this->request['order']!=""
             && $this->request['order_field']!=""
           ){
         $this->order = $this->request['order'];
         $this->orderField = $this->request['order_field'];
      }

      if($this->orderField!="" && $this->order!="" && in_array($this->orderField,$this->orderable) && in_array($this->order,["asc","desc"])){
        $this->query = $this->query->orderBy($this->orderField,$this->order);
      }

      /**
       * PAGINATE
       */


      if(array_key_exists("rows_per_page",$this->request)){
        $perPage = $this->request['rows_per_page'];
        session(["rows_per_page" => $perPage]);
      }else if(session("rows_per_page")){
        $perPage = session("rows_per_page");
      }else{
        $perPage = \Config::get('cms.admin_per_page');
      }

      if(!$this->paginate) $perPage = 10000000;
      $this->query = $this->query->paginate($perPage);

      if(count($filters) > 0){
        $this->query = $this->query->appends($filters);
      }

      /**
       * RESULT
       */
      return $this->query;
    }
  }
