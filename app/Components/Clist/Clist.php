<?php

namespace App\Components\Clist;

class Clist{

  /**
   * $header nagłówek dla listy
   *
   * $buttons array tablica przycisków ListButton,które pojawiają się nad listą
   *
   * $columns array nazwy pól z bazy do wyświetlenia
   *
   * $multiActions array tablica ListMultiAction, gdy tablica ma elementy wówczas pojawiają się checkboxy na których można wykonać dane akcje
   *
   * $sortable boolean/string false lub nazwa controllera na którym bedzie wykonana metoda setposition
   *
   * $rows array tablica rekordów do akcji
   *
   * $actions array tablica ListAction z akcjami na danym wierszu
   *
   * $pagination object obiekt powstały w wyniku wywołania metody paginate/simplePaginate na rzecz query builder / Eloqunet
   *
   * $ajaxPagination boolean czy kolejne wiersze mają być doładowane ajaxem
   *
   * $search array tablica ListSearchColumn z kolumnami do wyszukiwania
   *
   * $form boolean czy opakować listę formularzem
   *
   * $view string własny widok listy
   *
   * $sumColumns array lista kolumn do sumowania
   *
   */
  public $header;
  public $buttons;
  public $columns;
  public $multiActions;
  public $sortable;
  public $rows;
  public $actions;
  public $pagination;
  public $subrows;
  public $form;
  public $view;
  public $sumColumns;

  public function __construct(){
    $this->buttons = $this->columns = $this->rows = $this->actions = $this->multiActions = $this->search = [];
    $this->subrows = $this->sumColumns = [];
    $this->pagination = $this->sortable = false;
    $this->view = $this->header = "";
    $this->form = 0;
  }

  public function render(){
    $viewParams = [
      'header' => $this->header,
      'buttons' => $this->buttons,
      'columns' => $this->columns,
      'multiActions'=> $this->multiActions,
      'sortable' => $this->sortable,
      'rows' => $this->rows,
      'actions' => $this->actions,
      'pagination' => $this->pagination,
      'search' => $this->search,
      'subrows' => $this->subrows,
      'form' => $this->form,
      'sumColumns' => $this->sumColumns
    ];
    if($this->view!=""){
      $viewParams = array_merge($viewParams,$this->view->getVariables());
    }
    return view($this->view!="" ? $this->view->getName() : 'admin.components.clist.list')->with($viewParams)->render();
  }

}
