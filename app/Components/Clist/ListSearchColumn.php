<?php

namespace App\Components\Clist;

  class ListSearchColumn{

      /**
       * wyszukiwanie na liście
       */

        private $field;
        private $label;

        /**
         * tworzy nowe pole dla wyszukiwarki
         * @param  string  $field     nazwa w pola w bazie
         * @param  string  $label   etykieta do tłumaczenia
         */
        public function __construct($field,$label,$searchType = 'string'){
          $this->field = $field;
          $this->label = trans($label);
        }

        public function getField(){
          return $this->field;
        }

        public function getLabel(){
          return $this->label;
        }

      }
