<?php

  namespace App\Components\Clist;

  use Carbon\Carbon;

  class ListColumnFormat{

    public static function switchValue($value,$item="",$itemId="",$itemField="",$callback=""){
      $value = (int)$value;

      $myStatus = "";
      if($value == 1)
        $myStatus = "checked='true'";

      $result = '<label class="toggle" data-value="'.$value.'" ';
      if($item!="" && $itemId!="" && $itemField!="")
        $result.= ' data-item="'.$item.'" data-item-id="'.$itemId.'" data-item-field="'.$itemField.'" ';
      if($callback!="")
        $result.= ' data-callback="'.$callback.'"';
      $result.= '><input data-item type="checkbox" '.$myStatus.' class="toggle_input"><div class="toggle-control"><span data-before="'.strtoupper(trans('admin.yes')).'" data-after="'.strtoupper(trans('admin.no')).'"></span></div></label>';

      return $result;
    }

    public static function onlyTrue($value){
      $value = (int)$value;
      return $value ? strtoupper(trans('admin.yes')) : "";
    }

    public static function createImage($img){

    }

    public static function formatDateCarbon($value){    
      return Carbon::parse($value)->format('d.m.Y');
    }

    public static function showBadge($value){
      return $value>0 ? "<span class='badge'>".$value."</span>" : "-";
    }

  }
