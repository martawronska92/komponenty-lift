<?php

namespace App\Components\Clist;

  class ListView{

      /**
       * własny widok list
       */

        private $path;
        private $variables;

        /**
         * tworzy nowy widok wyszukiwarki
         * @param  string  $field     nazwa w pola w bazie
         * @param  array  $label   etykieta do tłumaczenia
         */
        public function __construct($name,$variables = []){
          $this->name = $name;
          $this->variables = $variables;
        }

        public function getVariables(){
          return $this->variables;
        }

        public function getName(){
          return $this->name;
        }
  }
