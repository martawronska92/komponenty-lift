<?php

namespace App\Components\Clist;

/**
 * kolumna w wierszu na liście
 */

class ListColumn {

  private $field;
  private $label;
  private $callback;

  /**
   * tworzy nową kolumnę
   * @param string  $field    nazwa pola z bazy
   * @param string  $label    wyświetlana nazwa, to tłumaczenia
   * @param boolean $order    czy możliwe jest sortowanie po kolumnie
   * @param boolean/function $callback opcjonalna funkcja do wyświetlania pola
   */
  public function __construct($field,$label,$order = false,$callback = false){
    $this->field = $field;
    $this->label = $label;
    $this->order = $order;
    $this->callback = $callback;
  }

  public function getField(){
    return $this->field;
  }

  public function getLabel(){
    return $this->label;
  }

  public function getOrder(){
    return $this->order;
  }

  public function render($row){

    /**
     * callback do renderowania kolumny
     * w przypadku gdy wlaściwość callback to string wówczas bierze metodę predefiniowaną z ListColumnFormat
     * w przypadku gdy funkcja to ją wywołuje
     */

     $viewParams = [
         'row'=>$row,
         'field'=>$this->field,
         'callback'=>$this->callback,
         'label'=>$this->label,
         'field_after_format' => NULL
     ];

    $field = $this->field;
    $fieldAfterFormat = $field."_after_format";
    if($this->callback){
      $viewParams['field_after_format'] = $field."_after_format";
      if(gettype($this->callback) == "string"){
        $function = $this->callback;
        $row->$fieldAfterFormat = \App\Components\Clist\ListColumnFormat::$function($row->$field);
      }else{
        $row->$fieldAfterFormat = call_user_func($this->callback,array($row));
      }
    }

    return view('admin.components.clist.column')->with($viewParams);
  }
}
