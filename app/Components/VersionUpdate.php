<?php

  /**
   * klasa wykorzystywana do zapytań wersji
   */

  namespace App\Components;

  use Ixudra\Curl\Facades\Curl;

  class VersionUpdate{

    private $domain;

    public function __construct($domain){
      $this->domain = $domain;
    }

    /**
     * pobiera wersję systemu
     */
    public function getVersion(){
      return \Curl::to($this->domain.'/api/version_get')
        ->withData(['api_token'=>\Config::get('cms.api_token')])
        ->get();
    }


    /**
     *  pobiera wersję uaktualnień
     */
    public function getUpdateVersion(){
      return \Curl::to($this->domain.'/api/version_update_get')
        ->withData(['api_token'=>\Config::get('cms.api_token')])
        ->get();
    }

    /**
     *  dodaje rekord uaktualnień i ustawia wersję systemu
     */
    public function setUpdateVersion($version){
      return \Curl::to($this->domain.'/api/version_update_set?api_token='.\Config::get('cms.api_token'))
        ->withData($version)
        ->post();
    }

    /**
     *  wykonuje przesłane migracje z uaktualnień
     */
    public function migrate(){
      return \Curl::to($this->domain.'/api/version_migrate?api_token='.\Config::get('cms.api_token'))
        ->get();
    }

    /**
     *  wykonuje przesłane seedery z uaktualnień
     */
    public function seed($seedClass){
      return \Curl::to($this->domain.'/api/version_seed?api_token='.\Config::get('cms.api_token'))
        ->withData([
          'seedClass' => $seedClass
        ])
        ->post();
    }

    /**
     *  czyści storage views i cache route
     */
    public function clearCache(){
      return \Curl::to($this->domain.'/api/version_cache?api_token='.\Config::get('cms.api_token'))
              ->get();
    }

  }
