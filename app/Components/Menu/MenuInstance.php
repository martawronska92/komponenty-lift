<?php

namespace App\Components\Menu;

class MenuInstance {

    public static function get(){

        $menu = new \App\Components\Menu\Menu();

        $elements = [
          'mainpage' => ['name' => trans('menu.mainpage'), 'url' => route('dashboard'), 'privileges' => []],
          'page' => ['name' => trans('menu.page'),'url'=>'/panel/page','privileges'=>['page']],
          'offer' => ['name' => trans('menu.offer'),'url'=>'/panel/offer','privileges'=>['offer']],
          'popup' => ['name' => trans('menu.popup'),'url'=>'/panel/popup','privileges'=>['popup']],
          'slider' => ['name' => trans('menu.slider'),'url'=>'/panel/slider','privileges'=>['slider']],
          'tag' => ['name' => trans('menu.tag'),'url'=>'/panel/tag','privileges'=>['tag']],
          'settings' => ['name' => trans('menu.settings'),'url'=>'/panel/settings','privileges'=>['settings']],
          'user' => ['name' => trans('menu.user'),'url'=>'/panel/user','privileges'=>['user']],
          'language' => ['name' => trans('menu.language'),'url'=>'/panel/language','privileges'=>['language']],
          'seo' => ['name' => trans('menu.seo'),'url'=>'/panel/seo/smartlinks','privileges'=>['seo']]
        ];

        if(\Auth::user() && \App\Models\User::isDeveloper()){
          $elements['menu'] = ['name'=>trans('menu.menu'),'url'=>'/panel/menu','privileges'=>[]];
        }

        $names = [];
        foreach($elements as $symbol => $element){
          $names[$symbol] = $element['name'];
        }
        //array_multisort($names, SORT_ASC, SORT_STRING, $elements);


        foreach($elements as $symbol => $element){
          $el = new \App\Components\Menu\MenuItem($symbol,$element['name'],$element['url'],$element['privileges']);
          $menu->addElement($el);
        }

        return $menu->getMenu();
    }
}
