<?php

namespace App\Components\Menu;

/**
 * generowanie menu
 */

class Menu{

    private $root;
    private $priviledges;
    private $requiredModules;
    private $userModules;

    public function __construct(){
        $this->root = new \App\Components\Menu\MenuItem('root','root','root');
        $this->requiredModules = \App\Models\Module::where('need_user_priviledge','=',1)->get()->pluck('symbol')->toArray();
        $this->defaultPrivileges = \App\Models\Privilege::get()->pluck('symbol')->toArray();
        $this->userModules = \Auth::user() ? \Auth::user()->module->pluck('symbol')->toArray() : [];
        $this->userPrivileges = \Auth::user() ? \Auth::user()->privilege->pluck('symbol')->toArray() : [];
    }

    /**
     * dodaje element do menu
     * @param AppComponentsMenuMenuItem $item
     */
    public function addElement(\App\Components\Menu\MenuItem $item){
        $this->root->addSubElement($item);
    }

    /**
     * usuwa wywbrany element z menu
     * @param  AppComponentsMenuMenuItem $item
     */
    public function removeElement(\App\Components\Menu\MenuItem $item){
        $this->root->removeElement($item);
    }

    /**
     * zwraca utworzone menu
     * @return AppComponentsMenuMenu
     */
    public function getMenu(){
        $this->checkPriviledges();
        return $this->root->getElements();
    }

    public static function render($menu,$activeMenu){
      return \View::make('admin.components.menu.menu',['menu'=>$menu,'activeMenu'=>$activeMenu])->render();
    }

    /**
     * sprawdza dostępność elementów menu według uprawnień użytkownika
     */
    public function checkPriviledges(){

        foreach($this->root->getElements() as $root){
            $has = true;
            foreach($root->getPriviledges() as $p){
              if(
                (in_array($p,$this->requiredModules) && !in_array($p,$this->userModules))
                ||
                (in_array($p,$this->defaultPrivileges) && !in_array($p,$this->userPrivileges))
              ){
                $has = false;
              }
            }

            if($has == false){
                $this->root->removeElement($root);
            }

            foreach($root->getElements() as $sub){
                $has = true;

                foreach($sub->getPriviledges() as $p){
                  if(
                    (in_array($p,$this->requiredModules) && !in_array($p,$this->userModules))
                    ||
                    (in_array($p,$this->defaultPrivileges) && !in_array($p,$this->userPrivileges))
                  ){
                    $has = false;
                  }
                }

                if($has == false){
                    $root->removeElement($sub);
                }
            }
        }
    }
}
