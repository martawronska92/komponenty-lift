<?php

namespace App\Components\Menu;

/**
 * element menu
 */

class MenuItem{

    private $url;
    private $name;
    private $alias;
    private $priviledges;
    private $elements;
    private $check;

    /**
     * utworzenie elementu menu
     * @param string $alias       alias w menu
     * @param string $name        wyświetlana nazwa elementu
     * @param string $url         url na który przekierowuje element
     * @param array  $priviledges uprawnienia
     */

    public function __construct($alias,$name,$url,$priviledges = array()){      
        $this->alias = $alias;
        $this->name = $name;
        $this->url = $url;
        $this->priviledges = $priviledges;
        $this->elements = [];
    }

    /**
     * dodaje elementu menu
     * @param AppComponentsMenuMenuMenuItem $el
     */
    public function addSubElement(\App\Components\Menu\MenuItem $el){
        $this->elements[$el->alias] = $el;
    }

    /**
     * zwraca elementy-dzieci danego elementu
     * @return array AppComponentsMenuMenuMenuItem
     */
    public function getElements(){
        return $this->elements;
    }

    /**
     * usuwa dany element
     * @param  AppComponentsMenuMenuMenuItem $el
     */
    public function removeElement(\App\Components\Menu\MenuItem $el){
        unset($this->elements[$el->alias]);
    }

    /**
     * zwraca nazwę elementu
     */
    public function getName(){
        return $this->name;
    }

    /**
     * zwraca nazwę elementu
     */
    public function getUrl(){
        return $this->url;
    }

    /**
     * zwraca uprawnienia
     * @return array
     */
    public function getPriviledges(){
        return $this->priviledges;
    }

    /**
     * zwraca alias elementu
     */
    public function getAlias(){
        return $this->alias;
    }
}
