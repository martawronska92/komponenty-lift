<?php

  namespace App\Components\ShortTag;

  class Tag{

    private $tagName;
    private $attributes;
    private $destinationModel;

    /**
     * tworzy tag do short codue
     * @param string $tagName    nazwa tagu np.: [gallery ...]
     * @param array $attributes  lista wymaganych attrybutów, sprawdzanych przy parsowaniu
     * @param string $destinationModel model z którego będzie wywołana statyczna metoda parseShortTag z tablicą attrybutów jako argument
     */
    public function __construct($tagName,$attributes,$destinationModel){
      $this->tagName = $tagName;
      $this->attributes = $attributes;
      $this->destinationModel = $destinationModel;
    }

    public function getName(){
      return $this->tagName;
    }

    public function getAttributes(){
      return $this->attributes;
    }

    public function getModel(){
      return $this->destinationModel;
    }

  }
