<?php

  namespace App\Components\ShortTag;

  use \App\Components\ShortTag\Tag;

  class Parser{

    /**
     * dostępne tagi do parsowania
     * klucz - wpisywany tytuł tagu
     * wartość - list wymaganych attrybutów dla danego tagu
     */
    public static $availableTags = [];

    public static function init(){
      self::$availableTags = [
        'galleryphoto' => new Tag('galleryphoto',['id'],'App\Models\GalleryPhoto'),
        'galleryvideo' => new Tag('galleryvideo',['id'],'App\Models\GalleryVideo')
      ];
    }

    /**
     * parsuje shorttagi w treści
     * @param  string $content treść do parsowania
     * @return string treść po parsowaniu
     */
    public static function run($content){
      self::init();

      foreach(self::$availableTags as $tag){
        $pattern = "/\[".$tag->getName()."[a-zA-Z0-9\_\-\"\'\=\s]*\]/";
        $model = $tag->getModel();
        $content = preg_replace_callback($pattern,function($matches) use($tag){
          foreach($matches as $match){
            $removeChars = ["'",'"',"]","["];
            $match = str_replace($removeChars,'',$match);
            $attributes = self::parseAttributes($match,$tag->getName());
            if(self::checkValid($tag->getName(),$attributes)){
              return call_user_func([$tag->getModel(),'parseShortTag'],$attributes);
            }else{
              return "";
            }

          }
        },$content);
      }
      return $content;
    }

    /**
     * parsuje atrybuty w znalezionym shorttagu
     * @param  string $text znaleziony shorttag
     * @return array  lista atrbutów w wartościami na zasadzie klucz/wartość
     */
    public static function parseAttributes($text,$tagName){
      $parts = explode(" ",$text);

      $attributes = [];
      foreach($parts as $arg){
        if(trim($arg)!="" && $arg!=$tagName){
          $arg = trim($arg);
          if(strpos($arg,"=")!==FALSE){
            $attributes[strtolower(substr($arg,0,strpos($arg,"=")))] =  substr($arg,strpos($arg,"=")+1);
          }else{
            $attributes[$arg] = "";
          }
        }
      }
      return $attributes;
    }

    /**
     * sprawdza wymagane atrybuty dla tagu
     * @param  string $tagNameToCheck    nazwa tagu
     * @param  array $attributesToCheck lista wymaganych attrybutów
     * @return boolean     czy atrybut poprawny
     */
    public static function checkValid($tagNameToCheck,$attributesToCheck){
      $valid = true;

      foreach(self::$availableTags[strtolower($tagNameToCheck)] as $attribute){
            if(!array_key_exists($attribute,$attributesToCheck)){
              $valid = false;
            }
      }

      return $valid;
    }

  }
