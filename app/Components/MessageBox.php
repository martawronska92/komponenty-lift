<?php

  namespace App\Components;

  class MessageBox{

    public static function set($msgType,$msg,$viewType){
      switch($msgType){
        case 0:
          $msgType = 'error';
          break;
        case 1:
          $msgType = 'success';
          break;
        default:
          $msgType = 'info';
          break;
      }

      if(strpos($msg,"|")!==FALSE){
          $parts = explode("|",$msg);
          $msg = trans_choice($parts[0],(int)$parts[1]);
      }else{
          $msg = trans($msg);
      }

      if(strtolower($viewType)!='main' && strtolower($viewType)!='list')
        $viewType = 'main';

      $variableName = 'MessageBox'.ucfirst($viewType);

      session()->flash($variableName,
        [
          'type' => $msgType,
          'msg' => $msg
        ]
      );
    }

    public static function get(){
       if(session()->has('MessageBoxMain')){
         \View::share('MessageBoxMain',session('MessageBoxMain'));
       }
       if(session()->has('MessageBoxList')){
         \View::share('MessageBoxList',session('MessageBoxList'));
       }
    }

  }
