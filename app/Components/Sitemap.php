<?php

  /**
   * klasa wykorzystywana do generowania sitemapy dla modułów na froncie
   */

  namespace App\Components;

  class Sitemap{

    private $url;
    private $lastUpdatedAt;
    private $priority;
    private $frequency;
    private $images;
    private $title;
    private $language;

    /**
     * [__construct description]
     * @param string $url          [description]
     * @param string/date $updatedAt    [description]
     * @param string $priority     [description]
     * @param string $frequency    [description]
     * @param array $images       [description]
     * @param string $title        [description]
     * @param string $language [description]
     */
    public function __construct($url,$updatedAt,$priority,$frequency = 'daily',$images = [], $title = "", $language = ""){
      $this->url = $url;
      $this->lastUpdatedAt = $updatedAt;
      $this->priority = $priority;
      $this->frequency = $frequency;
      $this->images = $images;
      $this->title = $title;
      $this->language = $language;
    }

    public function getUrl(){
      return $this->url;
    }

    public function getUpdatedAt(){
      return $this->lastUpdatedAt;
    }

    public function getPriority(){
      return $this->priority;
    }

    public function getFrequency(){
      return $this->frequency;
    }

    public function getImages(){
      return $this->images;
    }

    public function getTitle(){
      return $this->title;
    }

    public function getLanguage(){
      return $this->language;
    }

  }
