<?php

  /**
   * klasa wykorzystywana do generowania routingu dla modułów na froncie
   */

  namespace App\Components;

  class Route{

    private $type;
    private $method;
    private $url;
    private $action;
    private $name;
    private $params;

    /**
     * tworzenie nowego route`a
     * @param string $type page/module typ rouet`a czy moduł przeznaczony dla strony (page) czy oddzielny (module)
     * @param string $method metoda requesta np.: get, post itp
     * @param string $url url w route
     * @param string $action akcja wykorzystywana w route według wzoru controller@action
     * @param string $name   nazwa route
     * @param array $params tablica parametrów wykorzystywanych w route z ich walidacją np ['id'=>'[0-9]+']
     */
    public function __construct($type,$method,$url,$action,$name = "",$params = []){
      $this->type = $type;
      $this->method = strtolower($method);
      $this->url = $url;
      $this->action = $action;
      $this->name = $name;
      $this->params = $params;
    }

    public function getType(){
      return $this->type;
    }

    public function getMethod(){
      return $this->method;
    }

    public function getUrl(){
      return $this->url;
    }

    public function getAction(){
      return $this->action;
    }

    public function getName(){
      return $this->name;
    }

    public function getParams(){
      return $this->params;
    }

    public static function parseRoutes($module,$type){
      if(method_exists($module->module_entity_model,'getRouting')){
        $routes = call_user_func([$module->module_entity_model,'getRouting'],$module);

        if(count($routes)>0){
          foreach($routes as $route){
            if($route->getType()==$type){
              $method = $route->getMethod();
              $r = \Route::$method($route->getUrl(),$route->getAction());
              if($route->getName()!="")
                $r = $r->name($route->getName());
              if(count($route->getParams())>0){
                $r = $r->where($route->getParams());
              }
            }
          }
        }
      }
    }

  }
