<?php

namespace App\Helpers;

class Format {

    /**
     * formatuje nazwę własną obiektu w dialogu potwierdzenia
     * @param  string $name nazwa obiektu
     * @return string
     */

    public static function confirmDeleteName($name) {
        return "<b><i>".$name."</i></b>";
    }

    /**
     * konwertuje z YYYY-MM-DD do DD-MM-YYYY
     * @param  string $date data wejściowa
     * @return string    data wyjściowa
     */
    public static function formatDate($date){
      return $date!="" ? date("d-m-Y", strtotime($date)) : $date;
    }

    /**
     * konwertuje z YYYY-MM-DD do DD.MM.YYYY
     * @param  string $date data wejściowa
     * @return string    data wyjściowa
     */
    public static function formatDateWithDots($date){
      return $date!="" ? date("d.m.Y", strtotime($date)) : $date;
    }

    /**
     * konwertuje z YYYY-MM-DD HH::II do H::I, DD.MM.YYYY
     * @param  string $dateTime data wejściowe
     * @return string    data wyjściowa
     */
    public static function formatDateTime($dateTime){
      return $dateTime!="" ? date("H:i, d.m.Y ", strtotime($dateTime)) : $dateTime;
    }

    /**
     * ucina stringi do żądanej długości sprawdzając ostatni znak aby był literą lub cyfrą
     * @param  string  $string tekst wejściowy
     * @param  integer $limit  żądana długość
     * @return string          tekst wyjściowy po obcięciu
     */
    public static function cropString($string,$limit = 100){
      $string = trim($string);
      $string = strip_tags($string);
      $string = str_limit($string,$limit);

      return $string;
    }

    public static function moduleDetailsTitle($title){
      return "<b style='color:#0F5ECF;'>".$title."</b>";
    }

    /**
     * zwraca html dla ikonki podpowiedzi z akcją hover
     * @param  string $text tekst podpowiedzi
     * @return string       html podpowiedzi
     */
    public static function iconHint($text){
      return '<div class="popupTooltip js-popupTooltip" title="'.$text.'"></div>';
    }

    /**
     * zwraca html opakowujący element w podpowiedź z akcją hover
     * @param  string $text    tekst podpowiedzi
     * @param  string $element tekst/element do opakowania
     * @return           [description]
     */
    public static function elementHint($text,$element){
      return '<div class="js-popupTooltip" title="'.$text.'" aria-describedby="tippy-2">'.$element.'</div>';
    }
}
