<?php

namespace App\Helpers;

class Html {

  public static function getBodyClass($page,$post){
    $response = "";

    if (\Route::getCurrentRoute() && (\Route::getCurrentRoute()->uri() == '/' || \Route::getCurrentRoute()->uri()=="/".\App::getLocale())){
      $response = 'home';
    }else if($page && isset($page->default_view)){
      $parts = explode('.',$page->default_view);
      $response = array_pop($parts);
    }

    return $response;
  }

}
