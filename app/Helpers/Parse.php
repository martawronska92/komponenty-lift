<?php

namespace App\Helpers;

class Parse {

  /**
   * zwraca ostatni fragment ścieżki
   * @param  string $entity_type ścieżka do modelu
   * @return string  ostatni fragment ścieżki
   */
  public static function parseEntity($entity_type){
    $parts = explode('\\',$entity_type);
    return strtolower(end($parts));
  }

  /**
   * parsuje formularz przekazany przez jquery serialize do tablicy
   */
  public static function parseSerialize($form){
    $data = array();
    parse_str($form, $data);
    return $data;
  }

}
