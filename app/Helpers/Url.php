<?php

namespace App\Helpers;

class Url {

  /**
   * zwraca link do strony z wyszukiwarką
   */
  public static function getSearchUrl(){
      $url = \DB::table('module_front_link')
                ->join('module','module.id','=','module_front_link.module_id')
                ->join('language','language.id','=','module_front_link.language_id')
                ->where('language.symbol','=',\App::getLocale())
                ->where('module.symbol','=','search')
                ->first();

      if($url){
        $page = \DB::table('page')
          ->select('page.id')
          ->join('page_post_language','page_post_language.entity_id','=','page.id')
          ->join('language','language.id','=','page_post_language.language_id')
          ->where('language.symbol','=',\App::getLocale())
          ->where('page_post_language.entity_type','=','App\\Models\\Page')
          ->where('page_post_language.url','=',$url->link)
          ->where('page.active','=',1)
          ->first();
        if($page)
          return self::getPageUrl($page->id,\App::getLocale());
        else
          return "";
      }else{
        return "";
      }

  }

  public static function getPageUrl($pageID,$language = false){
      $url = "";
      $language = $language == false ? \App::getLocale() : $language;

      do{
        $urlPart = \DB::table('page')
          ->join('page_post_language','page.id','=','page_post_language.entity_id')
          ->join('language','page_post_language.language_id','=','language.id')
          ->where('page_post_language.entity_type','=','App\\Models\\Page')
          ->where('language.symbol','=',$language)
          ->where('page.id','=',$pageID)
          ->first();

        if($urlPart){
          if($pageID == $urlPart->parent_page_id){
            $parentID = NULL;
          }else{
            $pageID = $parentID = $urlPart->parent_page_id;
            $url = $urlPart->url."/".$url;
          }
        }else{
          $parentID = NULL;
        }

      }while($parentID!=NULL);

      $url = "/".(\App\Helpers\Language::hasPrefix() ? $language."/" : "").$url;

      return rtrim($url,"/");
  }

  public static function getFrontPostUrl($postID,$language = false){
    $language = $language==false ? \App::getLocale() : $language;
    return self::getPostUrl($postID,$language,true);
  }

  public static function findPageForCategoryQuery($language){
    return \DB::table('page_post_language as page_data')
       ->select('page_data.url as page_url','page_post_module.id as page_post_module_id','page_data.entity_type',
                  'page_data.entity_id','module.module_entity_model','page_post_module.module_entity_id')
       ->distinct()

       ->join('language as page_language','page_language.id','=','page_data.language_id')
       ->join('page','page.id','=','page_data.entity_id')
       ->leftJoin('page_post_module',function($join){
         $join->on('page_post_module.entity_id','=','page_data.entity_id');
         //$join->on('page_post_module.entity_type','=',\DB::raw('"App\\Models\\Page"'));
       })
       ->leftJoin('module','page_post_module.module_id','=','module.id')
       ->leftJoin('module_front_link','module_front_link.link','=','page_data.url')
       ->leftJoin('language as module_front_link_language','module_front_link.language_id','=','module_front_link_language.id')
       ->where('page_language.symbol','=',$language)
       ->where('page_data.entity_type','=','App\\Models\\Page')
       ->where(function($query) use ($language){
         $query->whereNull('module_front_link_language.symbol')
            ->orWhere('module_front_link_language.symbol','=',$language);
       });
  }

  public static function getPostUrl($postID,$language,$front = false,$returnMultiple = false){
    /**
     * sprawdzanie categorii: czy z modułu, czy ma rodzica itp ...
     */

    $postRow = \DB::table('page_post_language as post_data')
       ->select('post_data.entity_id as id','post_data.title as post_title',
         'post_data.url as post_url','category.module_id','category.id as category',
         'category.category_parent_id','category_language.url as category_url',
         'module.symbol','module.module_entity_model')
       ->distinct()

       ->join('language as post_language','post_language.id','=','post_data.language_id')
       ->join('post','post.id','=','post_data.entity_id')
       ->join('category','category.id','=','post.category_id')
       ->join('category_language','category_language.category_id','=','category.id')
       ->join('language as category_language_data', 'category_language_data.id','=', 'category_language.language_id')
       ->leftJoin('module','module.id','=','category.module_id')
       ->leftJoin('module_front_link','module_front_link.module_id','=','category.module_id')
       ->leftJoin('language as module_front_link_language','module_front_link.language_id','=','module_front_link_language.id')

       ->where('post_language.symbol','=',$language)
       ->where('category_language_data.symbol','=',$language)
       ->where('post_data.entity_type','=','App\\Models\\Post')
       ->where('post.id','=',$postID);

    $postRow = $postRow->where(function($query) use ($language){
         $query->whereNull('module_front_link_language.symbol')
            ->orWhere('module_front_link_language.symbol','=',$language);
       });

    $postRow = $postRow->first();

    $hasParentCategory = $postRow->category_parent_id != NULL;
    $category = $postRow;

    /**
     * przeszukanie kategorii nadrzędnych
     */
    $categoryUrl = "";
    if($category->module_id!=NULL){
      $categoryUrl = $postRow->category_url."/";
      while($category->category_parent_id!=NULL){
          $category = \App\Models\Category::with(['module','language'=>function($query) use ($language){
            $query->whereHas('language',function($query) use ($language){
              $query->where('symbol','=',$language);
            });
          }])->where('id','=',$category->category_parent_id)->first();
          $categoryUrl = $category->language[0]->url."/".$categoryUrl;
      }
      $categoryUrl = trim($categoryUrl,"/");
    }

    /**
     * znalezienie kategorii lub modułu przypisanego do kategorii a właściwego dla posta
     */



    $rows = self::findPageForCategoryQuery($language);

    if($category->module_id != NULL){
      $rows = $rows->where(function($query) use ($category){
          $query->where(function($query) use ($category){
                  if($category->module_entity_model == "App\Models\Category"){
                    $query->where('module.module_entity_model','=','App\\Models\\Category');
                    $query->where('page_post_module.module_entity_id','=',$category->category);
                  }else{
                    $query->where('module.id','=',$category->module_id);
                  }
               })
              ->orWhere('module_front_link.module_id','=',$category->module_id);

      });
    }else{
      // $rows = $rows->where(function($query){
      //                   $query->where('module.symbol','=','category')->orWhereNotNull('module.category_id');
      //               })
      //              ->where('page_post_module.module_entity_id','=',$hasParentCategory ? $category->id : $postRow->category);
    }


    if($front){
      if($returnMultiple){
        $response = [];
        $pageRows = $rows->get();
        foreach($pageRows as $pageRow){
            $resposne[] = (\App\Helpers\Language::hasPrefix() ? "/".$language : "")."/".$pageRow->page_url.($categoryUrl!="" ? "/".$categoryUrl : "")."/".$postRow->post_url;
        }
        return $response;
      }else{
        $pageRow = $rows->first();
        if(!$pageRow)
          return "";
        else
          return (\App\Helpers\Language::hasPrefix() ? "/".$language : "")."/".$pageRow->page_url.($categoryUrl!="" ? "/".$categoryUrl : "")."/".$postRow->post_url;
      }
    }else{
      $pageRow = $rows->first();
      if($hasParentCategory && $category->module_id != NULL)
        return route($category->module->symbol.'_post_edit',['id'=>$postID]);
      else if($postRow->module_id!=NULL){
        return route($postRow->symbol.'_post_edit',['id'=>$postID]);
      }else{
        return route('post_edit',['id' => $postID,'pagePostModuleId' => $pageRow->page_post_module_id]);
      }
    }
  }

  public static function getCategoryUrl($categoryId,$language){
    /**
     * przeszukanie kategorii nadrzędnych
     */

    $categoryUrl = "";

    $categoryParentId = NULL;

    do{
        $category = \App\Models\Category::with(['module','language'=>function($query) use ($language){
          $query->whereHas('language',function($query) use ($language){
            $query->where('symbol','=',$language);
          });
        }])->where('id','=', ($categoryParentId ? $categoryParentId : $categoryId))->first();

        $categoryParentId = $category->category_parent_id;
        $categoryUrl.= $category->language[0]->url."/".$categoryUrl;

    }while($category->category_parent_id!=NULL);
    /**
     * strona dla kategorii
     */

    $pageRow = self::findPageForCategoryQuery($language)
              ->where(function($query) use ($category){
                  $query->where('module.id','=',$category->module_id)
                    ->orWhere('module_front_link.module_id','=',$category->module_id);
              })
              ->first();
    if($pageRow)
      return (\App\Helpers\Language::hasPrefix() ? "/".$language : "")."/".$pageRow->page_url."/".trim($categoryUrl,"/");
    else
      return "";
  }

  public static function isCategory($lastUrlParam){
    $results = \App\Models\CategoryLanguage::where('url','=',$lastUrlParam)
      ->whereHas('category',function($query){
        $query->where('active','=',1);
      })
      ->whereHas('language',function($query){
        $query->where('symbol','=',\App::getLocale());
      })->get();
    if(count($results) == 0){
      return false;
    }else{
      foreach($results as $result){
        $url = self::getCategoryUrl($result->category_id,\App::getLocale());
        if($url == "/".\Request::path()){
          return $result;
          exit();
        }
      }
      return false;
    }
  }

  public static function isPostPage($lastUrlParam){
    $results = \App\Models\PagePostLanguage::where('url','=',$lastUrlParam)
      ->whereHas('language',function($query){
        $query->where('symbol','=',\App::getLocale());
      })->get();

      if(count($results) == 0){
        return false;
      }else{
        foreach($results as $result){
          if($result->entity_type == "App\Models\Page")
            $url = self::getPageUrl($result->entity_id);
          else
            $url = self::getFrontPostUrl($result->entity_id);
          if($url == "/".\Request::path()){
            return $result;
            exit();
          }
        }
        return false;
      }
  }

  public static function getPagePostUrlFromModule($moduleSymbol){
    $response = "";
    $module = \App\Models\Module::where('symbol','=',$moduleSymbol)->first();
    if($module){
      $firstConnection = \App\Models\PagePostModule::where('module_id','=',$module->id)->first();
      if($firstConnection){
        if($firstConnection->entity_type == 'App\Models\Page')
          $response = self::getPageUrl($firstConnection->entity_id);
        else
          $response = self::getFrontPostUrl($firstConnection->entity_id);
      }
    }
    return $response;
  }
}
