<?php

namespace App\Helpers;

class Youtube {

    /**
     * formatuje link YT do formatu embed
     * @param  string $link standardowy link do filmu YT
     * @return string
     */

    public static function embed($link) {
        $link = str_replace('watch?v=','embed/',$link);
        if(strpos('rel=',$link)===FALSE){
          $link.= strpos($link,'?') === FALSE ? '?' : '&';
          $link.= 'rel=0';
        }
        return $link;
    }

    public static function getIdFromUrl($link){
      preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $link, $matches);
      return is_array($matches) && count($matches)>1 ? $matches[1] : "";
    }
}
