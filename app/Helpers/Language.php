<?php

namespace App\Helpers;

class Language {

    /**
     * sprawdza czy dodawać prefix przed url
     * @return boolean
     */
    public static function checkPrefix() {
      $langs = \App\Models\Language::where('active','=',1)->where('visible_on_front','=',1)->count() > 1;
      if($langs && \Config::get('lang_per_domain') == 1)
        $langs = false;
      return $langs;
    }

    public static function hasPrefix(){
      return \Route::current() && \Route::current()->hasParameter('frontlanguage');
    }

    public static function getPrefix(){
      return \Request::segment(1);
    }

    public static function setCurrent(){
      if(\Route::current() && \Route::current()->hasParameter('frontlanguage')){
          $frontlanguage = \App\Models\Language::checkFrontLanguage(\Route::current()->getParameter('frontlanguage'));
          //var_dump($frontlanguage);
          app()->setLocale($frontlanguage);
          //var_dump($frontlanguage);
          session('lang_domain',$frontlanguage);
      // }else if(session()->exists('lang_symbol')){
         //echo "else if";
      //   var_dump(session('lang_symbol'));
      //     app()->setLocale(session('lang_symbol'));
      }else if(\Config::get('cms.lang_per_domain') == 1){
         //echo "else if 2";
          $lang = \App\Models\Language::searchForDomain();
          //var_dump($lang);
          if($lang){
            app()->setLocale($lang->symbol);
            session('lang_domain',$lang->symbol);
          }
      }else{
        // var_dump(\App::getLocale());
          if(\Request::segment(1)=="panel"){
            $language = self::getAdminLocale();
          }else{
            $language = \App\Models\Language::checkFrontLanguage('');
          }
          app()->setLocale($language);
      }
    }

    public static function getAdminLocale(){
      if(!session()->has('admin_lang_symbol')){
        $language = \App\Models\Language::checkAdminLanguage();
        session([
          'admin_lang_id' => $language->id,
          'admin_lang_symbol' => $language->symbol
        ]);
      }
      return session('admin_lang_symbol');
    }
}
