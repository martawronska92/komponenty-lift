<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{

    public function boot()
    {
        View::composer('front.page.view.contact',function($view){
          $view->with('openinghours',\App\Models\OpeningHours::get());
        });

        View::composer(['errors/403','errors/404','errors/500','auth/login','auth/passwords/email','auth/passwords/reset'], function($view){
          $view->with([
            'settings' => \App\Models\Settings::getAll(\App::getLocale()),
            'langPrefix' => \App\Helpers\Language::checkPrefix()
          ]);
        });

        include_once(base_path().'/resources/views/front/variables.php');
    }

    public function register()
    {

    }
}
