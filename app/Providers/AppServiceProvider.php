<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      \Validator::extend('before_equal', function($attribute, $value, $parameters, $validator) {
          return strtotime($validator->getData()[$parameters[0]]) >= strtotime($value);
      });

      \App\Models\Category::observe(\App\Observers\CategoryObserver::class);        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
