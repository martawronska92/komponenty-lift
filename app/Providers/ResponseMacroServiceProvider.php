<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Register the application's response macros.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('format', function ($value) {
            return Response::json($value);
        });

        Response::macro('default', function ($code,$value) {
            $resp = new \stdClass();
            $resp->code = $code;
            $resp->msg = $value;
            return Response::json($resp);
        });

        Response::macro('object', function ($code,$value,$parameters = []) {
            $resp = new \stdClass();
            $resp->code = $code;
            $resp->msg = $value;
            foreach($parameters as $name=>$value){
              $resp->{$key} = $value;
            }
            return $resp;
        });
    }
}
