<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPrivilege extends Model
{
    protected $table = "user_privilege";

    protected $fillable = ['entity_id','user_id','privilege_id'];
}
