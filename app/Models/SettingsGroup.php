<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingsGroup extends Model {

	protected $table = 'settings_group';
	public $timestamps = false;

	public function settings()
	{
		return $this->hasMany('App\Models\Settings');
	}

	public function getAll(){
		return self::with(['settings' => function($query){
				$query->orderBy('settings_group_id','asc')
						  ->orderBy('position','asc')
							->orderBy('settings_subgroup_id','asc');
			}])
			->where('subgroup','=',0)
			->orderBy('position','asc')
			->get();
	}

}
