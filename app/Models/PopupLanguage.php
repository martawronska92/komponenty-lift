<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PopupLanguage extends Model
{
    protected $table = 'popup_language';

    protected $fillable = ['content','popup_id','language_id'];

    public function language(){
      return $this->belongsTo('App\Models\Language');
    }

    public function getContentAttribute($value){
      return json_decode($value);
    }

    public function setContentAttribute($value){
        $this->attributes['content'] = json_encode($value);
    }
}
