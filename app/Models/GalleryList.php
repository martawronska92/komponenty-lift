<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryList extends Model
{
  protected $table = 'gallery_list';

  public function language(){
    return $this->hasMany('App\Models\GalleryListLanguage');
  }

  public static function renderOnFront($module){
    $model = new self();
    $galleries = $model->getGalleries($module->module_entity_id);

    if(count($galleries)>0){
      /**
       * Disttinct
       */
      $pageIds = [];
      foreach($galleries as $key=>$gallery){
        if(in_array($gallery->entity_id,$pageIds)){
          $galleries->forget($key);
        }
        array_push($pageIds,$gallery->entity_id);
      }

      return view('front.modules.gallerylist.photolist')->with([
        'galleries' => $galleries,
        'info' => \App\Models\GalleryList::whereHas('language.language',function($query){
          $query->where('symbol','=',\App::getLocale());
        })->where('id','=',$module->module_entity_id)->first()
      ])->render();
    }else {
      return "";
    }
  }

  public function createEmpty(){
    return self::create()->id;
  }

  public function deleteObject($id, $preview = false){
      $ids = explode(",",$id);

      $commonModel = new \App\Models\Base();
      $response = $commonModel->deleteMulti($ids,'gallery_list');

      return $response->code;
  }

  private function getPageIdFromList($listId){
    $moduleId = \App\Models\Module::where('symbol','=','galleryphotolist')->first()->id;

    return \App\Models\PagePostModule::where('module_id','=',$moduleId)
                  ->where('entity_type','=','App\\Models\\Page')
                  ->where('module_entity_id','=',$listId)
                  ->pluck('entity_id')
                  ->first();
  }

  public function getChildrenPagesIds($listId){
    return \App\Models\Page::where('parent_page_id','=',$this->getPageIdFromList($listId))->orderBy('position','DESC')->get()->pluck('id')->toArray();
  }

  public function getGalleries($listId){
    return \App\Models\PagePostModule::whereIn('page_post_module.entity_id',$this->getChildrenPagesIds($listId))
                          ->join('page','page.id','=','page_post_module.entity_id')
                          ->join('page_post_language','page_post_module.entity_id','=','page_post_language.entity_id')
                          ->join('language','language.id','=','page_post_language.language_id')
                          ->leftJoin('gallery_photo','gallery_photo.gallery_id','=','page_post_module.module_entity_id')
                          ->leftJoin('gallery_photo_language','gallery_photo.id','=','gallery_photo_language.gallery_photo_id')
                          ->leftJoin('language as lang_photo','lang_photo.id','=','gallery_photo_language.language_id')						  
                          ->where('page_post_language.entity_type','=','App\\Models\\Page')
                          ->where('page_post_module.entity_type','=','App\\Models\\Page')
                          ->where('language.symbol','=',\App::getLocale())
                          ->where('module_id','=',\DB::table('module')->where('symbol','=','galleryphoto')->first()->id)
                          ->where('page_post_module.active','=',1)
                          ->where(function($query){
                            $query->whereNull('gallery_photo.main');
                            $query->orWhere(function($query){
                                $query->where('gallery_photo.main','=',1);
                                $query->where('gallery_photo.active','=',1);
                                $query->where('gallery_photo.draft','=',0);
                            });
                            $query->orWhere('gallery_photo.id','=',\DB::raw('(select id from gallery_photo where gallery_id = page_post_module.module_entity_id and active=1 and draft=0 and (select count(id) from gallery_photo where main=1 and gallery_id = page_post_module.module_entity_id)=0 order by position desc limit 1)'));
                          })
                          ->where(function($query){
                            $query->whereNull('lang_photo.symbol');
                            $query->orWhere('lang_photo.symbol','=',\App::getLocale());
                          })
                          ->orderBy('page.position','desc')
                          ->orderBy('page_post_module.position','asc')
                          ->get(['page_post_language.title','page_post_language.entity_id','gallery_photo_language.title as photo_title',
                                'page_post_module.module_entity_id as gallery_id',
                                'gallery_photo.id as photo_id','gallery_photo.ext as photo_ext']);
  }

  public function getPhotos(){

  }

  public function getModuleDetails($listId){
    $response = "";

    $info = \App\Models\GalleryList::where('id','=',$listId)->with(['language' => function($query){
      $query->whereHas('language',function($query){
        $query->where('symbol','=',\App::getLocale());
      });
    }])->first();


    if($info && count($info->language)>0 && $info->language[0]->title!="")
      $response = \App\Helpers\Format::moduleDetailsTitle(\App\Helpers\Format::cropString($info->language[0]->title,150)).", ".lcfirst(trans('admin.gallerylist.gallery_counter'));
    else
      $response = trans('admin.gallerylist.gallery_counter');


    $galleriesCounter = count($this->getGalleries($listId));

    return $response.": ".$galleriesCounter;
  }

  public function storeList($data,$id = false, $pagePostModuleId = false){
    $galleryModel = $id ? \App\Models\GalleryList::find($id) : new \App\Models\GalleryList();
    $galleryModel->save();

    $galleryModel->language()->delete();
    if(array_key_exists('title',$data)){
      foreach($data['title'] as $langId=>$value){
        $galleryModel->language()->create([
          'language_id' => $langId,
          'title' => $value,
          'description' => $data['description'][$langId]
        ]);
      }
    }

    $response = new \stdClass();
    $response->code = $galleryModel->id ? 1 : 0;
    $response->msg = ($id ? 'admin.update' : 'admin.gallery.add').'_';

    if($pagePostModuleId){
      $moduleDetails = \App\Models\PagePostModule::getDetails($pagePostModuleId);
      $response->redirect_url = $moduleDetails['redirect_url'];
    }

    return $response;
  }

  public function deletePreviewObject($id){
    \App\Models\GalleryList::where('id','=',$id)->delete();
  }
}
