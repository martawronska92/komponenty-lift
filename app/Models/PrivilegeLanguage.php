<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrivilegeLanguage extends Model
{
    protected $table = "privilege_language";

    public function language(){
      return $this->belongsTo('App\Models\Language');
    }
}
