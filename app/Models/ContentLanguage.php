<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentLanguage extends Model
{
    protected $table = "content_language";

    protected $fillable = ['title','content','language_id','content_id'];

    public function language(){
      return $this->belongsTo('App\Models\Language');
    }
}
