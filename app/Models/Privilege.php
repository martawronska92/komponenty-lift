<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Privilege extends Model
{
    protected $table = "privilege";

    public function language(){
      return $this->hasMany('App\Models\PrivilegeLanguage','privilege_id');
    }
}
