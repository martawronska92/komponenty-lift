<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\GalleryVideo;

class GalleryVideo extends Model
{

    protected $table = 'gallery_video';

    protected $fillable = ['url','title','thumbnail','position','gallery_id','active','draft','time'];

    public function gallery(){
      return $this->belongsTo('App\Models\Gallery');
    }

    public function language(){
      return $this->hasMany('App\Models\GalleryVideoLanguage');
    }

    public function getPosition($galleryId){
      return self::where('gallery_id','=',$galleryId)->max('position') + 1;
    }

    public function addVideo($data){
      $data['type'] = 'video';

      $ytVideoId = \App\Helpers\Youtube::getIdFromUrl($data['url']);
      $ytInfo = json_decode(file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=contentDetails&id=".$ytVideoId."&key=".\Config::get('cms.youtube_api_token')), true);

      if(is_array($ytInfo)){
        $duration = "";
        foreach ($ytInfo['items'] as $info)
        {
           $duration = $info['contentDetails']['duration'];
        }
        if($duration!=""){
          $durationFormat = new \DateInterval($duration);
          $data['time'] = $durationFormat->format('%H:%I:%S');
        }
      }

      if(array_key_exists('video_id',$data) && $data['video_id']!=""){
        $video = self::find($data['video_id']);
        $video->fill($data);
        $video->save();
        $data['draft'] = $video->draft;
      }else{
        $data['position'] = $this->getPosition($data['gallery_id']);
        $data['draft'] = 0;
        $video = \App\Models\Gallery::find($data['gallery_id'])->video()->create($data);
      }

      $title = "";
      if(array_key_exists('language',$data)){
        $video->language()->delete();
        $currentLangId = \App\Models\Language::where('symbol','=',\App::getLocale())->pluck('id')->first();
        parse_str($data['language'],$langParams);
        foreach($langParams['title'] as $langId=>$value){
          \App\Models\GalleryVideo::find($video->id)->language()->create([
              'language_id' => $langId,
              'title'=> $value
          ]);
          if($langId == $currentLangId)
            $title = $value;
        }
      }

      $response = new \stdClass();
      $response->code = $video->id ? 1 : 0;
      if($response->code){
        $data['id'] = $video->id;
        $data['active'] = 1;
        $data['title'] = $title;
        $data['thumbnail'] = $video->thumbnail;
        $response->view = (String) \View::make('admin.gallery_video.partial.videobox',['video'=>$data])->render();
      }

      return $response;
    }

    public static function getDataForSelect(){
      return \App\Models\Gallery::getDataForSelect('video');
    }

    public static function renderOnFront($module){
      $videos = self::with(['language'=>function($query){
            $query->whereHas('language',function($query){
              $query->where('symbol','=',\App::getLocale());
            });
          },'gallery'])
          ->where('active','=',1)
          ->where('gallery_id','=',$module->module_entity_id);

      if(\Route::currentRouteName()!="page_preview" && \Route::currentRouteName()!="post_preview")
        $videos = $videos->where('draft','=',0);

      $videos = $videos->orderBy('position','desc')->get();

      $info = \App\Models\Gallery::where('id','=',$module->module_entity_id)->with(['language' => function($query){
            $query->whereHas('language',function($query){
              $query->where('symbol','=',\App::getLocale());
            });
      }])->first();

      if(count($videos)>0)
        return view('front.modules.gallery.video_'.$videos[0]->gallery->show_type->symbol)->with([
          'videos'=>$videos,
          'settings' => \App\Models\Settings::getAll(),
          'info' => $info
        ]);
      else
        return  "";
    }

    public function createEmpty(){
      return \App\Models\Gallery::create([
        'show_type_id' => \App\Models\GalleryShowType::where('gallery_type','=','video')->pluck('id')->first(),
        'type' => 'video'
      ])->id;
    }

    public function deleteObject($id,$preview = false){
      $ids = explode(",",$id);

      if(count($ids) == 1 && \App\Models\Gallery::where('id','=',$ids[0])->count() == 0){
        return true;
      }else{
        $commonModel = new \App\Models\Base();
        $response = $commonModel->deleteMulti($id,'gallery');
        return $response->code;
      }
    }

    public function getModuleDetails($galleryId){
      $response = "";

      $info = \App\Models\Gallery::where('id','=',$galleryId)->with(['language' => function($query){
        $query->whereHas('language',function($query){
          $query->where('symbol','=',\App::getLocale());
        });
      }])->first();

      if($info && count($info->language)>0 && $info->language[0]->title!="")
        $response = \App\Helpers\Format::moduleDetailsTitle(\App\Helpers\Format::cropString($info->language[0]->title,150)).", ".lcfirst(trans('admin.galleryvideo.video_counter'));
      else
        $response = trans('admin.galleryvideo.video_counter');

      return $response.": ".self::where('gallery_id','=',$galleryId)->count();
    }

    public function storeGallery($data,$id,$pagePostModuleId = 0){
      if($id)
        $galleryModel = \App\Models\Gallery::find($id);
      else
        $galleryModel = new \App\Models\Gallery();

      $data['type'] = 'video';
      $data['active'] = !array_key_exists('active',$data) ? 0 : 1;
      $galleryModel->fill($data);
      $galleryModel->save();

      $galleryModel->language()->delete();
      if(array_key_exists('title',$data)){
        foreach($data['title'] as $langId=>$value){
          $galleryModel->language()->create([
            'language_id' => $langId,
            'title' => $value,
            'description' => $data['description'][$langId]
          ]);
        }
      }

      $moduleDetails = \App\Models\PagePostModule::getDetails($pagePostModuleId);

      $response = new \stdClass();
      $response->code = $galleryModel->id ? 1 : 0;
      $response->msg = 'admin.'.($id ? 'update' : 'gallery.add').'_';

      \App\Models\GalleryVideo::where('gallery_id','=',$galleryModel->id)->update([
        'draft' => 0
      ]);

      if($pagePostModuleId){
        $moduleDetails = \App\Models\PagePostModule::getDetails($pagePostModuleId);
        $response->redirect_url = $moduleDetails['redirect_url'];
      }


      return $response;
    }

    public function deletePreviewObject($id){
      \App\Models\GalleryVideo::where('gallery_id','=',$id)->delete();
      \App\Models\Gallery::where('id','=',$id)->delete();
    }
}
