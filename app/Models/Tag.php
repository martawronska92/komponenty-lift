<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'tag';

    public function language(){
      return $this->hasMany('App\Models\TagLanguage','tag_id');
    }

    public function post(){
      return $this->belongsToMany('App\Models\Post','post_tag','tag_id','post_id');
    }

    public function getList($data){
      $query = new \App\Components\Clist\ListQuery();
      $query->query = \DB::table('tag')
        ->select('tag_language.*','tag.id')
        ->join('tag_language','tag.id','=','tag_language.tag_id')
        ->join('language','language.id','=','tag_language.language_id')
        ->where('language.symbol','=',\App::getLocale());
      $query->orderField = "tag_language.name";
      $query->order = "asc";
      $query->request = $data;
      $query->searchable = ['tag_language.name'];
      $query->orderable = ['tag_language.name'];
      $query->fields = ['tag_language.name','tag.id'];
      $query->paginate = 0;
      return $query->format();
    }

    public function getAttributesNames(){
  		$langs = \App\Models\Language::getActive();
  		foreach($langs as $lang){
  			$attr['name.'.$lang->id] = trans('admin.list.name')." (".$lang->name.")";
  		}
      return $attr;
    }

    public static function getInCurrentLang(){
      return \DB::table('tag')
        ->join('tag_language','tag.id','=','tag_language.tag_id')
        ->join('language','language.id','=','tag_language.language_id')
        ->where('language.symbol','=',\App::getLocale())
        ->orderBy('tag_language.name','asc')
        ->get(['tag.id','tag_language.name as text']);
    }

    public function storeTag($data,$id = false){
      $response = new \stdClass();
      $response->code = 0;
      $response->msg = trans($id ? 'admin.update_failure' : 'admin.tag.add_failure');

      try{

        \DB::beginTransaction();

        $tag = $id ? self::where('id','=',$id)->first() : new self();
        $tag->save();

        $tag->language()->delete();
        $langs = 0;

        $valid = true;

        foreach($data['name'] as $key=>$value){
          $exists = \App\Models\TagLanguage::where('friendly_name','=',str_slug($value))->where('language_id','=',$key)->count();
          if(!$exists){
            $lang = $tag->language()->create([
              'language_id' => $key,
              'name' => $value,
              'friendly_name' => str_slug($value)
            ]);
            if($lang) $langs++;
          }else{
            $valid = false;
          }
        }

        if($langs == count($data['name'])){
          \DB::commit();
          $response->code = 1;
          $response->id = $tag->id;
          $response->msg = trans($id ? 'admin.update_success' : 'admin.tag.add_success');
        }else{
          if(!$valid){
            $response->msg = trans('admin.tag.already_exists');
          }
          \DB::rollback();
        }

      }catch(Exception $e){
        \DB::rollback();
      }

      return $response;
    }

    public static function getSitemap($module,$langPrefix){
      $sitemap = [];
      $postsPerPage = \Config::get('cms.tag_per_page');

      $tags = \App\Models\Tag::with([
        'post',
        'language'
      ])
      ->whereHas('post')
      ->get();

      foreach($tags as $tag){
        if($tag->post->count() > 0){

          $links = $sitemaps = [];

          foreach($tag->language as $lang){
            $url = ($langPrefix ? "/".$lang->language->symbol.'/' : '')."tag/".$lang->friendly_name;
            $item = new \App\Components\Sitemap(\URL::to($url),
                   $lang->updated_at,
                   \App\Models\Sitemap::getPriority($url,$langPrefix),
                   'daily',
                   array(),
                   $lang->name,
                   $lang->language->symbol);
            array_push($sitemap,$item);
          }

          if(ceil($tag->post->count()/$postsPerPage) > 1){
            for($p=1;$p<ceil($tag->post->count()/$postsPerPage);$p++){
              $links = [];

              foreach($tag->language as $lang){
                $url = ($langPrefix ? "/".$lang->language->symbol.'/' : '')."tag/".$lang->friendly_name."/".($p+1);
                $item = new \App\Components\Sitemap(\URL::to($url),
                       $tag->language[0]['updated_at'],
                       \App\Models\Sitemap::getPriority($url,$langPrefix),
                       'daily',
                       array(),
                       $tag->language[0]->name,
                       $lang->language->symbol);
                 array_push($sitemap,$item);
              }
            }
          }

        }

      }

      return $sitemap;
    }
}
