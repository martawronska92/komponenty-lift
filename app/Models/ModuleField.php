<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModuleField extends Model
{
    protected $table = "module_field";

    public function language(){
      return $this->belongsToMany('App\Models\Language','module_field_language','module_field_id','language_id')->withPivot(['name']);
    }
}
