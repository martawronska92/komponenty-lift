<?php

namespace App\Models;
use App\Models\Categorylang;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class Post extends Base {

  protected $table = 'post';

	public $timestamps = true;

  protected $fillable = ['category_id','public','publish_date','active','thumbnail','user_id',
        'position','draft','thumbnail_options'];

  public function setPublishDateAttribute($value)
  {
      $this->attributes['publish_date'] = $value!="" ? date("Y-m-d H:i", strtotime($value)) : NULL;
  }

  public function language()
	{
		$relation = $this->morphMany('App\Models\PagePostLanguage','entity');
    // if(\Request::segment(1)!="admin"){
			// $relation = $relation->whereHas('language',function($query){
			// 	$query->where('symbol','=',\App::getLocale());
			// 	$query->where('active','=',1);
			// 	$query->where('visible_on_front','=',1);
			// });
		// }
    return $relation;
	}

  public function category(){
    return $this->belongsTo('App\Models\Category');
  }

  public function field(){
      return $this->belongsToMany('App\Models\CategoryField','category_field_value','post_id','category_field_id')
        ->withPivot(['value']);
  }

  public function module(){
		return $this->morphMany('App\Models\PagePostModule','entity')->with(['module']);//->withPivot('module_entity_id');
	}

  public function user(){
    return $this->belongsTo('App\Models\User','user_id');
  }

  public function tag(){
    return $this->belongsToMany('App\Models\Tag','post_tag','post_id','tag_id');
  }

  public function getImage($size = 'thumbnail'){
    if($this->thumnail!=""){
      return "/img/post/".$this->id."/".$size.".".$this->thumbnail;
    }else{
      return "";
    }
  }

  public function getTitle(){
    $title = "";
    if($this->language && count($this->language) > 0){
      $title = $this->language[0]->title;
    }
    return $title;
  }

  public function getContent($length = 0){
    $responseText = "";
    foreach($this->module as $module){
      if($module->module->symbol == 'content'){
        $content= \App\Models\Content::with(['language' => function($query){
          $query->whereHas('language',function($query){
            $query->where('symbol','=',\App::getLocale());
          });
        }])->where('id','=',$module->module_entity_id)->first();
        if($content && $content->language->count()>0){
          $responseText.= strip_tags($content->language[0]->content)." ";
        }
      }
    }

    $responseText = trim($responseText);

    return (string)($length>0 ? str_limit($responseText,$length) : $responseText);
  }

  public function getList($categoryId, $front = false){
    $query = new \App\Components\Clist\ListQuery();

		$query->query =\DB::table('post')
      ->select()
      ->join('page_post_language','entity_id','=','post.id')
      ->join('language','language.id','=','page_post_language.language_id')
      ->leftJoin('users','users.id','=','post.user_id')
      ->where('post.category_id','=',$categoryId)
      ->where('page_post_language.entity_type','=','App\Models\Post')
      ->where('language.symbol','=',\App::getLocale())
      ->whereNull('post.draft');

		$query->fields = ['post.*','page_post_language.title',\DB::raw('concat(users.first_name," ",users.last_name) as user_name')];
		//$query->searchable = ['category_language.name'];
		$query->orderable = ['position'];
    $query->request = [];
    $query->paginate = 0;
    $query->orderField = 'position';
    $query->order = 'desc';
    return $query->format();
  }

  public function getFrontList($categoryId,$paginate = true,$allLanguage = false,$pageNr = false){
    if($allLanguage)
      $query = self::with(['language']);
    else{
        $query = self::with(['language' => function($query){
            $query->whereHas('language',function($query){
              $query->where('symbol','=',\App::getLocale());
            });
          },'user']);
    }

    if(!$allLanguage){
      $query = $query->whereHas('language.language',function($query){
                $query->where('symbol','=',\App::getLocale());
            });
    }

    if(is_array($categoryId)){
      $query = $query->whereIn('category_id',$categoryId);
    }else{
      $query = $query->where('category_id','=',$categoryId);
    }

    $query = $query->whereNull('draft')
            ->where('active','=',1)
             ->where(function($query){
                 $query->where('public','=',1);
                 $query->orWhere(function($query){
                     $query->where('public','=',0);
                     $query->where('publish_date','<=',Carbon::now());
                 });
       })
      ->orderBy('position','desc');

      if($paginate)
        return $query->paginate(\Config::get('cms.admin_per_page'));
      else{
        if($pageNr!=false){
          $category = \App\Models\Category::where('id','=',$categoryId)->first();
          return $query->skip($category->per_page*($pageNr-1))->take($category->per_page)->get(['post.*']);
        }else{
          return $query->get(['post.*']);
        }
      }
  }

  public function getAttributesNames(){
		$attr = [
			'category_id' => trans('admin.post.category')
		];

		$langs = \App\Models\Language::getActive();
		foreach($langs as $lang){
			$attr['title.'.$lang->id] = trans('admin.list.title')." (".$lang->name.")";
      $attr['content.'.$lang->id] = trans('admin.list.content')." (".$lang->name.")";
      $attr['url.'.$lang->id] = trans('admin.post.url')." (".$lang->name.")";
      $attr['meta_title.'.$lang->id] = trans('admin.settings.meta_title')." (".$lang->name.")";
			$attr['meta_description.'.$lang->id] = trans('admin.settings.meta_description')." (".$lang->name.")";
		}

    /**
     * FIELDS
     */
    $fields = \App\Models\CategoryField::get();
    foreach($fields as $field){
      foreach($field->language as $lang){
        if($lang->symbol == \App::getLocale()){
          $attr['category_field_id.'.$field->id] = $lang->pivot->name;
        }
      }
    }

		return $attr;
  }

  public function getLastPosition($categoryId){
    return self::where('category_id','=',$categoryId)->count();
  }

  public function savePost($data,$id = false,$customTransaction = false){
    $response = new \stdClass();
    $response->code = 0;

    try{

      if(!$customTransaction)
			   \DB::beginTransaction();

      if(array_key_exists('thumbnail_encoded',$data) && $data['thumbnail_encoded']!=""){
        $data['thumbnail'] = "png?v=".mt_rand(10000000, 99999999);
      }

      $data['active'] = array_key_exists('active', $data) ? 1 : 0;
      $data['public'] = array_key_exists('public', $data) ? 1 : 0;
      $data['publish_date'] = $data['public'] ? null : (array_key_exists('publish_date',$data) ? $data['publish_date'] : null);

			if(!$id){
        $data['position'] = $this->getLastPosition($data['category_id']) + 1;
				$data['user_id'] = \Auth::user()->id;
				$post = self::create($data);
			}else{
				//$category = true;
				$post = self::find($id);
				$post->fill($data);
				$post->save();
			}

			$langs = 0;
			if($post){
        $thumbDir = public_path().'/img/post/'.$post->id;
        if(!is_dir($thumbDir))
          mkdir($thumbDir);

				//$id = $category->id;

        $pagePostLanguageModel = new \App\Models\PagePostLanguage();

        /**
         * Langs
         */
				$post->language()->delete();
				foreach($data['title'] as $key=>$value){
          $params = [
						'title' => $value,
            'url' => $pagePostLanguageModel->generateFriendlyLink([
								'title' => $data['url'][$key],
								'type' => 'post',
								'lang' => $key,
								'id' => $post->id,
                'category_id' => $data['category_id']
						 ]),
						'language_id' => $key
					];
          if(array_key_exists('meta_title',$data))
            $params['meta_title'] = $data['meta_title'][$key];
          if(array_key_exists('meta_description',$data))
            $params['meta_description'] = trim($data['meta_description'][$key]);
          if(array_key_exists('content',$data))
            $params['content'] = trim($data['content'][$key]);
					$lang = $post->language()->create($params);

					if($lang) $langs++;
				}

        /**
         * Fields
         */

        \App\Models\CategoryFieldValue::where('post_id','=',$post->id)->delete();
        if(array_key_exists('category_field_id',$data)){
          foreach($data['category_field_id'] as $fieldId=>$fieldValue){
            $post->field()->attach($fieldId,['value'=>$fieldValue]);
          }
        }


        /**
         * MODULES FOR PREVIEW
         */
         if(array_key_exists('from_id',$data) && $data['from_id']!=""){
 					$oldPost = \App\Models\Post::with(['module' => function($query){
 						$query->orderBy('position','asc');
 					}])->where('id','=',$data['from_id'])->first();

 					foreach($oldPost->module as $module){
 						$params = $module->toArray();
 						$params['entity_id'] = $post->id;
 						\App\Models\PagePostModule::create($params);
 					}
 				}

			}

      /**
       * TAGS
       */
      $post->tag()->detach();
      if(array_key_exists('tag',$data)){
        foreach($data['tag'] as $tagId){
          $post->tag()->attach($tagId);
        }
      }

			if($post && count($data['title'])==$langs){
        if(array_key_exists('thumbnail_encoded',$data)){
          if($data['thumbnail_encoded']!=""){
            $content = explode(',',$data['thumbnail_encoded']);
            file_put_contents($thumbDir.'/'.'thumbnail.png',base64_decode($content[1]));
            if($data['thumbnail_original']!=""){
              $original = explode(',',$data['thumbnail_original']);
              file_put_contents($thumbDir.'/'.'original.png',base64_decode($original[1]));
            }
          }else{
            if(\File::exists($thumbDir.'/thumbnail.png')) @unlink($thumbDir.'/thumbnail.png');
            if(\File::exists($thumbDir.'/original.png')) @unlink($thumbDir.'/original.png');
          }
        }

        if(!$customTransaction)
				    \DB::commit();
				$response->code = 1;
        $response->id = $post->id;
			}else{
        if(!$customTransaction)
				    \DB::rollback();
			}

      return $response;
		}catch(Exception $e){
			return $response;
		}
  }

  public function getCategoryId($postId){
    $post = \App\Models\Post::where('id','=',$postId)->first();
    return $post ? $post->category_id : false;
  }

  public function getModulesForPost(){
    return \App\Models\Module::where('for_post','=',1)->where('active','=',1)->get();
  }

  public static function getSearchResults($phrase){
      return \DB::table('page_post_language as post_data')
        ->select('post_data.entity_id as id','post_data.title as post_title','page_data.url as page_url',
          'post_data.content',
          'post_data.url as post_url','category.module_id','category.id as category')
        ->distinct()

        /**
         * posty
         */
        ->join('language as post_language','post_language.id','=','post_data.language_id')
        ->join('post','post.id','=','post_data.entity_id')

        /**
         * kategorie
         */
        ->join('category','category.id','=','post.category_id')
        ->join('category_language','category_language.category_id','=','category.id')
        ->join('language as category_language_data', 'category_language_data.id','=', 'category_language.language_id')

        /**
         * strony
         */
        ->join('page_post_module','page_post_module.module_entity_id','=','post.category_id')
        ->join('module','page_post_module.module_id','=','module.id')
        ->join('page_post_language as page_data','page_data.entity_id','=','page_post_module.entity_id')
        ->join('language as page_language','page_language.id','=','page_data.language_id')
        ->join('page','page.id','=','page_post_module.entity_id')

        ->join('users','users.id','=','post.user_id')

        /**
         * content
         */
        ->leftJoin('page_post_module as page_post_module_content','page_post_module_content.entity_id','=','post.id')
        ->leftJoin('module as module_content','page_post_module_content.module_id','=','module_content.id')
        ->leftJoin('content','content.id','=','page_post_module_content.module_entity_id')
        ->leftJoin('content_language','content.id','=','content_language.content_id')
        ->leftJoin('language as language_content','content_language.language_id','=','language_content.id')

        /**
         * tagi
         */
        //->leftJoin('post_tag','post_tag.post_id','=','post.id')
        //->leftJoin('tag_language','post_tag.tag_id','tag_language.tag_id')
        //->leftJoin('language as language_tag','language_tag.id','=','tag_language.language_id')

        ->where('post_language.symbol','=',\App::getLocale())
        ->where('page_language.symbol','=',\App::getLocale())
        ->where('category_language_data.symbol','=',\App::getLocale())

        ->where('post_data.entity_type','=','App\\Models\\Post')
        ->where('page_data.entity_type','=','App\\Models\\Page')
        ->where('page_post_module.entity_type','=','App\\Models\\Page')

        ->where(function($query){
            $query->where('module.symbol','=','category')->orWhereNotNull('module.category_id');
        })

        ->where(function($query){
          $query->whereNull('module_content.symbol');
          $query->orWhere('module_content.symbol','!=','content');
          $query->orWhere(function($query){
              $query->whereNotNull('module_content.symbol')
                  ->where('module_content.symbol','=','content')
                  ->where('language_content.symbol','=',\App::getLocale())
                  ->where('page_post_module_content.entity_type','=','App\\Models\\Post');
          });
        })

        // ->where(function($query){
        //   $query->whereNull('tag_language.name');
        //   $query->orWhere(function($query){
        //       $query->whereNotNull('tag_language.name')
        //             ->where('language_tag.symbol','=',\App::getLocale());
        //   });
        // })

        ->where(function($query) use ($phrase){
            $query->where('post_data.title','like',"%".trim($phrase)."%");
            $query->orWhere('post_data.url','like',"%".trim($phrase)."%");
            $query->orWhere('category_language.name','like',"%".trim($phrase)."%");
            $query->orWhere('post_data.content','like',"%".trim($phrase)."%");
            $query->orWhere(function($query) use($phrase){
  						$query->where('module_content.symbol','=','content');
  						$query->where('content_language.content','like',"%".$phrase."%");
  					});
        })
        ->where('post.active','=',1)
        ->where('page.active','=',1)
        ->whereNull('page.draft')
        ->whereNull('post.draft')

        ->whereNull('category.module_id')

        ->where(function($query){
            $query->where('public','=',1);
            $query->orWhere(function($query){
                $query->where('public','=',0);
                $query->where('publish_date','<=',Carbon::now());
            });
        });
// var_dump($results->toSql());
//         exit();
    }

  public function clearOldDrafts(){
      self::whereNotNull('draft')
            ->whereDate('created_at','<=',Carbon::now()->subDays(2)->toDateString())
            ->delete();
  }

  public function getByTag($tagFriendlyName,$page){
      $perPage = \Config::get('cms.tag_per_page');
      //$page = \Request::has('page') ? (int)\Request::get('page')-1 : 0;
      //$currentPage = \Request::has('page') ? \Request::get('page') : 1;
      $currentPage = $page;
      $page = $page-1;

      $posts = \DB::table('post')
        ->select('post_data.entity_id as id','post_data.title as post_title','page_data.url as page_url',
          'post_data.url as post_url','category.module_id','category.id as category')
        ->distinct()
        /**
         * posty
         */
        ->join('page_post_language as post_data','post_data.entity_id','=','post.id')
        ->join('language as post_language','post_data.language_id','=','post_language.id')

        /**
         * kategorie
         */
        ->join('category','category.id','=','post.category_id')
        ->join('category_language','category_language.category_id','=','category.id')
        ->join('language as category_language_data', 'category_language_data.id','=', 'category_language.language_id')

        /**
         * strony
         */
        ->join('page_post_module','page_post_module.module_entity_id','=','post.category_id')
        ->join('module','page_post_module.module_id','=','module.id')
        ->join('page_post_language as page_data','page_data.entity_id','=','page_post_module.entity_id')
        ->join('language as page_language','page_language.id','=','page_data.language_id')
        ->join('page','page.id','=','page_post_module.entity_id')

        /**
         * tagi
         */
        ->join('post_tag','post_tag.post_id','=','post.id')
        ->join('tag_language','tag_language.tag_id','=','post_tag.tag_id')
        ->join('language as tag_lang','tag_language.language_id','=','tag_lang.id')

        ->where('post_language.symbol','=',\App::getLocale())
        ->where('page_language.symbol','=',\App::getLocale())
        ->where('tag_lang.symbol','=',\App::getLocale())
        ->where('tag_language.friendly_name','=',$tagFriendlyName)
        ->where('category_language_data.symbol','=',\App::getLocale())
        ->where('post_data.entity_type','=','App\Models\Post')
	->where('page_data.entity_type','=','App\Models\Page')
        ->where('page_post_module.entity_type','=','App\\Models\\Page')
        ->where(function($query){
          $query->where('module.symbol','=','category')
                ->orWhereNotNull('module.category_id')
                ->orWhere('module.module_entity_model','=','App\\Models\\Category');
        })
        ->where('post.active','=',1)
        ->where('page.active','=',1)
        ->where('page_post_module.active','=',1)
        ->whereNull('page.draft')
        ->whereNull('post.draft')

        //->whereNull('category.module_id')

        ->where(function($query){
            $query->where('public','=',1);
            $query->orWhere(function($query){
                $query->where('public','=',0);
                $query->where('publish_date','<=',Carbon::now());
            });
        })
        ->orderBy('post_data.title','asc');

        $postsCopy = clone $posts;
        $counter = $postsCopy->get()->count();

        $results = $posts->take($perPage)->skip($perPage*$page)->get();

        $links = [];
        $langPrefix = \App\Helpers\Language::checkPrefix();
        if(count($results)>0){
          foreach($results as $key=>$post){
            $links[] = ["/".($langPrefix ? \App::getLocale() ."/": "").$post->page_url."/".$post->post_url,$post->post_title];
          }
        }



        $response = new \stdClass();
        $response->results = $links;
        $paginator = new Paginator($links,$counter, $perPage,$currentPage,['pageName'=>'p']);

        $routeParams = ['tagName' => $tagFriendlyName];
        if(\App\Helpers\Language::hasPrefix()) $routeParams['frontlanguage'] = \App::getLocale();
        $route = route('front_tag',$routeParams,false); //\Route::current()->url

        $response->paginator = $paginator->setPath($route);//, Paginator::resolveCurrentPage());

        return $response;
    }

  public static function checkFromModule($postID){
    return \DB::table('post')
      ->select('module.*')
      ->join('category','post.category_id','=','category.id')
      ->join('module','category.module_id','=','module.id')
      ->where('post.id','=',$postID)
      ->first();
  }

  public function deletePost($postId){

    if(!is_array($postId))
      $postId = [$postId];

    $directoriesToDelete = $postId;

    $toDelete = count($postId);
    $deleted = 0;

    $modules = [];

    \DB::beginTransaction();

    foreach($postId as $id){
      $modules = array_merge($modules,\App\Models\PagePostModule::where('entity_id','=',$id)->where('entity_type','=','App\Models\Post')->get()->pluck('id')->toArray());
      $isDeleted = (int)\App\Models\Post::where('id','=',$id)->delete();
      $deleted+= $isDeleted;
      if($isDeleted){
        \App\Models\PagePostLanguage::where('entity_id','=',$id)->where('entity_type','App\Models\Post')->delete();
        \App\Models\PagePostModule::where('entity_id','=',$id)->where('entity_type','App\Models\Post')->delete();
      }
    }

    $response = new \stdClass();
    $response->code = 0;
    $response->itemCounter = $toDelete;

    if($deleted == $toDelete){
        $response->code = 1;

        \App\Models\PagePostModule::removeModule(array_unique($modules));

        $path = public_path().'/img/post';
        foreach($directoriesToDelete as $id){
          \File::deleteDirectory($path.'/'.$id);
        }

        \DB::commit();
    }else{
      \DB::rollback();
    }

    return $response;
  }

  public static function duplicate($postId,$draft = true, $setActive = true){
		$oldPost = \App\Models\Post::with(['module' => function($query){
      //$query->where('active','=',1);
			$query->orderBy('position','asc');
		},'language','category'])->where('id','=',$postId)->first();

		$hash = str_random(40);
		if($draft){
			$oldPost->draft = $hash;
		}
		if($setActive)
			$oldPost->active = 1;


		$newPost = \App\Models\Post::create($oldPost->toArray());

		$response = new \stdClass();
		$response->code = 0;
		$response->msg = trans('admin.preview_failure');

		if($newPost){

			foreach($oldPost->language as $language){
				$params = $language->toArray();
				$params['entity_id'] = $newPost->id;
				\App\Models\PagePostLanguage::create($params);
			}

			foreach($oldPost->module as $module){
				$params = $module->toArray();
				$params['entity_id'] = $newPost->id;
				\App\Models\PagePostModule::create($params);
			}

      foreach($oldPost->category->field->toArray() as $field){
        unset($field['value']['id']);
        $field['value']['post_id'] = $newPost->id;
        \App\Models\CategoryFieldValue::create($field['value']);
      }

			$routeParams['hash'] = $hash;
      if(\App\Helpers\Language::checkPrefix())
        $routeParams['frontlanguage'] = \App\Models\Language::checkFrontLanguage();

      $response->code = 1;
      $response->msg = route('post_preview', $routeParams);
			$response->entity_id = $newPost->id;
			$response->hash = $hash;
		}

    return $response;
  }

  public function getPostsFromModule($moduleID){
    $categories = \App\Models\Category::where('module_id','=',$moduleID)->pluck('id')->toArray();
    return $this->getFrontList($categories,false);
  }

}
