<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Sitemap extends Model
{
    public static function generate(){

      $sitemap = [];

      $langPrefix = \App\Helpers\Language::checkPrefix();

      //$sitemap->setCache('laravel.sitemap', 60);

      //if (!$sitemap->isCached()){


      $clientLangs = \App\Models\Language::all();

      $frontLangs = \App\Models\Language::getFrontLanguages();

        if(count($frontLangs) == 1){
          $lang = $clientLangs[0];
          $sitemap[$lang->symbol] = \App::make("sitemap");
          $sitemap[$lang->symbol]->add(\URL::to('/'), Carbon::now(), '1.0', 'daily',[],'');
        }else{
          foreach($clientLangs as $key=>$lang){
            $sitemap[$lang->symbol] = \App::make("sitemap");
            $sitemap[$lang->symbol]->add(\URL::to('/'.$lang->symbol), Carbon::now(), '1.0', 'daily',[],'');
          }
        }


        $pages = \App\Models\Page::with(['module','language'])->where('active','=',1)->get();

        $postModel = new \App\Models\Post();

        $moduleData = [];

        /**
         * iteracja po wszystkich stronach
         */

        foreach($pages as $page){

          /**
           * wersje językowe dla strona
           */

          $langs = [];

          foreach($page->language as $key => $lang){
              $url = ($langPrefix ? "/".$lang->language->symbol.'/' : '').trim(\App\Helpers\Url::getPageUrl($page->id,$lang->language->symbol),'/');
              if(array_key_exists($lang->language->symbol,$sitemap)){
                $sitemap[$lang->language->symbol]->add(\URL::to($url),
                              $page->updated_at,
                              self::getPriority($url,$langPrefix),
                              'daily',
                              [],
                              $lang->title
                );
              }
          }

          /**
           * sprawdzanie sitemapy modułów podłączanych do stron
           */

          foreach($page->module as $module){
            /**
             * moduł ma podłączaną kategorię do modułu
             */
            if($module->module->category_id != ""){
              $posts = $postModel->getFrontList($module->module->category_id,false,true);

              foreach($posts as $post){
                foreach($post->language as $key => $lang){
                  $url = ($langPrefix ? $lang->language->symbol : '').'/'.trim($lang->url.'/'.$lang->url,'/');
                  if(array_key_exists($lang->language->symbol,$sitemap)){
                    $sitemap[$lang->language->symbol]->add(\URL::to($url), //url
                        $post->updated_at, //date
                        self::getPriority($url,$langPrefix),             //priority
                        'daily',            //freq,
                        [],               //images - optional
                        $lang->title  //title - optional
                    );
                  }
                }
              }
            }else{

              /**
               * inne modułu które były podłączane do stron które mają metodę odpowiedzialną za sitemapę
               */

              if(!array_key_exists($module->module_id,$moduleData))
                $moduleData[$module->module_id] = \App\Models\Module::where('id','=',$module->module_id)->first();

              if($moduleData[$module->module_id]->has_sitemap && method_exists($module->module_entity_model,'getSitemap')){
                $moduleSitemap = call_user_func([$module->module_entity_model,'getSitemap'],$module->module,$langPrefix);
                foreach($moduleSitemap as $route){
                  if(array_key_exists($route->getLanguage(),$sitemap)){
                    $sitemap[$route->getLanguage()]->add($route->getUrl(), //url
                        Carbon::now(), //date
                        $route->getPriority(),             //priority
                        $route->getFrequency(),            //freq,
                        $route->getImages(),               //images - optional
                        $route->getTitle()  //title - optional
                    );
                  }
                }
              }
            }
          }
        }

        /**
         * moduły nie podłączone posiadające sitemapę
         */
        $modules = \App\Models\Module::where('has_sitemap','=',1)->where('active','=',1)->where('for_post','=',0)->where('for_page','=',0)->get();
        foreach($modules as $module){
          if(!array_key_exists($module->id,$moduleData))
            $moduleData[$module->id] = $module;

          if($moduleData[$module->id]->has_sitemap && method_exists($module->module_entity_model,'getSitemap')){
            $moduleSitemap = call_user_func([$module->module_entity_model,'getSitemap'],$module,$langPrefix);
            foreach($moduleSitemap as $route){
              if(array_key_exists($route->getLanguage(),$sitemap)){
                $sitemap[$route->getLanguage()]->add($route->getUrl(), //url
                    Carbon::now(), //date
                    $route->getPriority(),             //priority
                    $route->getFrequency(),            //freq,
                    $route->getImages(),               //images - optional
                    $route->getTitle()  //title - optional
                );
              }
            }
          }
        }

        foreach($sitemap as $lang=>$singleSitemap){
          $singleSitemap->store('xml','sitemap_'.$lang);
        }

        $defaultLanguage = \App\Models\Language::where('main','=',1)->first();

        return $sitemap[$defaultLanguage->symbol]->store('xml', 'sitemap');
    }

    public static function getPriority($url,$langPrefix){
      $parts = count(explode("/",trim($url,"/")));
      $priority = [1 => '0.85', 2 => '0.69', 3 => '0.56', 4 => '0.46', 5 => '0.38', 6 => '0.31', 7 => '0.25'];

      if($langPrefix)
        $parts--;

      return array_key_exists($parts,$priority) ? $priority[$parts] : '0.21';
    }
}
