<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryFieldLanguage extends Model
{
    protected $table = "category_field_language";

    protected $fillable = ['category_field_id','language_id'];

    public function language(){
      return $this->belongsTo('App\Models\Language');
    }
}
