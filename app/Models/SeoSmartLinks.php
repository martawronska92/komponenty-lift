<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeoSmartLinks extends Base
{
    protected $table = "seo_smart_links";
    protected $fillable = ['word','link'];

    public function getList($data){
      $query = new \App\Components\Clist\ListQuery();
      $query->query = \DB::table('seo_smart_links');
      $query->orderField = "word";
      $query->order = "asc";
      $query->request = $data;
      $query->searchable = ['word','link'];
      $query->orderable = ['word','link'];
      $query->fields = ['seo_smart_links.*'];
      $query->paginate = 0;
      return $query->format();
    }

    public function store($data,$id = false){
      $response = new \stdClass();
      $response->code = 0;
      $response->msg = trans('admin.seo.import_failure');

      if(array_key_exists('file',$data) && $data['file']->getSize()>0){
          if(strtolower($data['file']->getClientOriginalExtension())!="csv"){
            $response->msg = trans('admin.seo.wrong_file');
          }else{
            $fileName = str_random(20).".csv";
            $data['file']->storeAs('',$fileName,'all');
            $path = public_path()."/".$fileName;
            if(\File::exists($path)){
              if (($handle = fopen($path, 'r')) !== false)
              {
                  while (($row = fgetcsv($handle, 2000, ";")) !== false)
                  {
                      if(count($row) == 2){
                        self::create([
                          'word' => $row[0],
                          'link' => $row[1]
                        ]);
                      }
                  }
                  fclose($handle);

                  $response->code = 1;
                  $response->msg = trans('admin.seo.import_success');
              }
              @unlink($path);
          }
        }
      }else if(array_key_exists('word',$data)){
          $object = $id ? self::find($id) : new self();
          $object->fill($data);
          $object->save();

          $msg = ($id ? "admin.update" : "admin.seo.add_link")."_";
          if($object->id){
            $response->code = 1;
            $response->msg = $msg.'success';
          }else{
            $response->msg = $msg.'failure';
          }
      }

      return $response;
    }

    public static function searchAndReplace($text){
      $rows = self::all();
      if(count($rows)>0){
        $prefix = \App\Helpers\Language::checkPrefix();

        foreach($rows as $row){
          $words = explode(",",$row->word);

          foreach($words as $word){
            $link = explode('/',$row->link);
            $link = end($link);

            $pageOrPost = \App\Models\PagePostLanguage::with('language')->where('url','like',strtolower($link))->first();
            $category = \App\Models\CategoryLanguage::with('language')->where('url','like',strtolower($link))->first();

            if($pageOrPost || $category){
              $language = "/";
              if($prefix){
                  $language = ($pageOrPost ? $pageOrPost->language->symbol :  $category->language->symbol);
              }

              $pos = strrpos(strtolower($text), strtolower($word));

              if($pos !== false)
              {
                  $word = substr($text,$pos,strlen($word));
                  $url = str_replace("//","/",$language.$row->link);
                  $replacement = "<a href='".$url."'>".$word."</a>";
                  $text = substr_replace($text, $replacement, $pos, strlen($word));
              }

              //$text = str_ireplace($word,"<a href='".$language.$row->link."'>".$word."</a>",$text);
            }
          }
        }
      }

      return $text;
    }
}
