<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SliderItemLanguage extends Model
{
    protected $table = 'slider_item_language';

    protected $fillable = ['title_1','title_2','content','language_id','slider_item_id'];

    public function language(){
      return $this->belongsTo('App\Models\Language');
    }
}
