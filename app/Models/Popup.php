<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Popup extends Base
{

    protected $table = 'popup';

    public function setVisibleFromAttribute($value)
    {
        $this->attributes['visible_from'] = $value!="" ? date("Y-m-d", strtotime($value)) : NULL;
    }

    public function setVisibleToAttribute($value)
    {
        $this->attributes['visible_to'] = $value!="" ? date("Y-m-d", strtotime($value)) : NULL;
    }


    public function language(){
      return $this->hasMany('App\Models\PopupLanguage');
    }

    public function getData(){
      return self::with('language')->first();
    }

    public function getAttributesNames(){
      $attr = [
        'visible_from' => trans('admin.popup.visible_from'),
        'visible_to' => trans('admin.popup.visible_to'),
        'youtube_content' => trans('admin.popup.yt_link'),
        'thumbnail_encoded' => trans('admin.popup.image'),
        'validation.before_equal' => trans('admin.popup.compoare_date_invalid'),
        'link_type' => trans('admin'),
        'link_value' => trans('admin')
      ];

      $langs = \App\Models\Language::getActive();
      foreach($langs as $lang){
        $attr['html_content.'.$lang->id] = trans('admin.popup.html')." (".$lang->name.")";
      }

      return $attr;
    }

    public function saveData($data){
      try{

        \DB::beginTransaction();

        $row = \App\Models\Popup::first();
        $id = $row ? $row->id : false;
        $popup = $id ? \App\Models\Popup::find($id) : new \App\Models\Popup();
        $popup->content_type = $data['content_type'];
        $popup->active = array_key_exists('active',$data) ? 1 : 0;
        $popup->visible_from = !array_key_exists('visible',$data) && $data['visible_from']!="" ? $data['visible_from'] : null;
        $popup->visible_to = !array_key_exists('visible',$data) && $data['visible_to']!="" ? $data['visible_to'] : null;
        $popup->thumbnail_options = $data['thumbnail_options'];
        $popup->link_type = $data['link_type'];
        $popup->link_value = "";
        if($data['link_type']!=""){
          $popup->link_value =  $data['link_type']=='page' ? $data['page'] : $data['url'];
        }
        $popup->link_new_window = $data['link_new_window'];
        $popup->save();

        $langs = \App\Models\Language::getActive();
        $inserts = 0;

        if($popup){
            $popup->language()->delete();
            $rand = mt_rand(10000000, 99999999);

            foreach($langs as $lang){
              $params = [
                'language_id' => $lang->id
              ];

              switch($data['content_type']){
                case 'html':
                  $params['content'] = trim($data['html_content'][$lang->id]);
                  break;
                case 'image':
                  //if($data['thumbnail_encoded']!=""){
                    $params['content'] = "png?v=".$rand;
                  //}
                  break;
                case 'youtube':
                  $params['content'] = [
                    'title' => $data['youtube_title'],
                    'url' => $data['youtube_content'],
                    'thumbnail' => $data['youtube_thumbnail']
                  ];
                  break;
              }
              $popupLang = $popup->language()->create($params);
              if($popupLang)
                $inserts++;
            }
        }

        if($popup && $inserts == count($langs)){
          if($data['content_type']=="image"){
            if($data['thumbnail_encoded']!=""){
              if(\File::exists(public_path().'/img/popup/image_thumbnail.png'))
                @unlink(public_path().'/img/popup/image_thumbnail.png');
              $content = explode(',',$data['thumbnail_encoded']);
              file_put_contents(public_path().'/img/popup/image_thumbnail.png',base64_decode($content[1]));
            }
            if($data['thumbnail_original']!=""){
              if(\File::exists(public_path().'/img/popup/image_original.png'))
                  @unlink(public_path().'/img/popup/image_original.png');
              $content = explode(',',$data['thumbnail_original']);
              file_put_contents(public_path().'/img/popup/image_original.png',base64_decode($content[1]));
            }
          }
          \DB::commit();
          return true;
        }else{
          \DB::rollback();
          return false;
        }

      }catch(Exception $e){
        return false;
      }
    }

    public static function renderOnFront(){
      $row = self::with(['language'])->where('active','=',1)
        ->where(function($query){
          $query->where(function($query){
            $query->whereNull('visible_from');
            $query->whereNull('visible_to');
          });
          $query->orWhere(function($query){
            $query->where('visible_from','<=',Carbon::now());
            $query->where('visible_to','>=',Carbon::now());
          });
        })
        ->whereHas('language.language',function($query){
          $query->where('symbol','=',\App::getLocale());
        })
        ->first();

      $settings = \App\Models\Settings::getAll();

      $show = \Request::path() == "/" || \Request::path()=="/".\App::getLocale() || \Request::path() == "" || \Request::path()==\App::getLocale();

      return view('front.modules.popup.window')->with([
        'row' => $row,
        'width' => $row['content_type']=='image' ? $settings['popup_thumb_width'] : $settings['video_popup_width'],
        'height' => $row['content_type']=='image' ? $settings['popup_thumb_height'] : $settings['video_popup_height'],
        'show' => $show
      ]);
    }
}
