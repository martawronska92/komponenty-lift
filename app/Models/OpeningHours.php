<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OpeningHours extends Model
{
    protected $table = "opening_hours";

    protected $fillable = ['from','to','open'];

    public function language(){
      return $this->hasMany('App\Models\OpeningHoursLanguage');
    }

    public static function get(){
      return self::with(['language' => function($query){
        $query->whereHas('language',function($query){
          $query->where('symbol','=',\App::getLocale());
        });
      }])->get();
    }

    public function store($data){
      try{
        \DB::beginTransaction();

        self::getQuery()->delete();

        if(array_key_exists('openinghours_name',$data)){
          $keys = array_keys($data['openinghours_name']);


          foreach($keys as $key){
            $object = new self();
            if(array_key_exists('openinghours_from',$data)){
              $object->from = array_key_exists($key,$data['openinghours_from']) ? $data['openinghours_from'][$key] : "";
              $object->to = array_key_exists($key,$data['openinghours_to']) ? $data['openinghours_to'][$key] : "";
              $object->open = (int)array_key_exists($key,$data['openinghours_open']);
              $object->save();
            }else{
              $object = new self();
              $object->from = "";
              $object->to = "";
              $object->open = 0;
              $object->save();
            }

            foreach($data['openinghours_name'][$key] as $langId=>$value){
              $object->language()->create([
                'language_id' => $langId,
                'value' => $value
              ]);
            }

          }
        }

        \DB::commit();

        return true;
      }catch(Exception $e){
        \DB::rollback();
        return false;
      }
    }
}
