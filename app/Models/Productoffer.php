<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Productoffer extends Model
{
    protected $table = "product_offer";

    protected $fillable = ['post_id','page_post_module_id'];

    public function product(){
      return $this->hasMany('App\Models\ProductofferProduct','product_offer_id');
    }

    public function language(){
      return $this->hasMany('App\Models\ProductofferLanguage','product_offer_id');
    }

    public function getProducts($data){
      $moduleId = \App\Models\Module::where('symbol','=','offer')->pluck('id')->first();

      $query = new \App\Components\Clist\ListQuery();
  		$query->query = \DB::table('post')
        ->join('page_post_language','post.id','=','page_post_language.entity_id')
        ->join('language','page_post_language.language_id','=','language.id')
        ->join('category','category.id','=','post.category_id')
        ->join('category_language','category_language.category_id','=','category.id')
        ->join('language as language_category','language_category.id','=','category_language.language_id')
        ->where('language.symbol','=',\App::getLocale())
        ->where('language_category.symbol','=',\App::getLocale())
        ->where('page_post_language.entity_type','=','App\\Models\\Post')
        ->where('category.module_id','=',$moduleId);

  		 $query->orderField = 'title';
  		 $query->order = 'asc';
  		 $query->fields = ['page_post_language.title','post.id','category.id as category_id','category_language.name as category_name'];
  	   $query->request = $data;
  		 $query->orderable = ['title','category_name'];
  		 $query->searchable = ['title','category_language.name'];
  		 $query->paginate = 0;
  	   return $query->format();
    }

    public function saveProducts($data,$pagePostModuleId,$id){
       \DB::beginTransaction();

       $productoffer = self::find($id);

       $productoffer->product()->delete();
       $productoffer->language()->delete();

       $toInsert = $inserted = 0;

       foreach($data['title'] as $langId => $title){
         $productoffer->language()->create([
           'language_id' => $langId,
           'title' => $title
         ]);
       }

       if(array_key_exists('product',$data)){
         $toInsert = count($data['product']);
         foreach($data['product'] as $postId=>$value){
           $object = $productoffer->product()->create([
             'post_id' => $postId
           ]);
           if($object) $inserted++;
         }
       }

       if($toInsert == $inserted){
         \DB::commit();
         return true;
       }else{
         \DB::rollback();
         return false;
       }
    }

    public function createEmpty(){
      return self::create()->id;
    }

    public static function renderOnFront($pagePostModule){
      $offer = \App\Models\Productoffer::with(['language' => function($query){
        $query->whereHas('language',function($query){
          $query->where('symbol','=',\App::getLocale());
        });
      },'product'])->where('id','=',$pagePostModule->module_entity_id)->first();

      $products = \App\Models\Post::with(['language' => function($query){
        $query->whereHas('language',function($query){
          $query->where('symbol','=',\App::getLocale());
        })
        ->orderBy('title','asc');
      }])
      ->join('page_post_language','post.id','=','page_post_language.entity_id')
      ->join('language','page_post_language.language_id','=','language.id')
      ->whereIn('post.id',$offer->product->pluck('post_id')->toArray())
      ->where('page_post_language.entity_type','=','App\\Models\\Post')
      ->where('language.symbol','=',\App::getLocale())
      ->orderBy('page_post_language.title','asc')
      ->get(['post.id','page_post_language.title','post.thumbnail']);

      // echo "<pre>";
      // var_dump($offer->toArray());
      // echo "</pre>";
      // exit();

      return view('front.modules.productoffer.list',[
        'offer' => $offer,
        'products' => $products
      ])->render();
    }

    public function getModuleDetails($productofferId){
      $offer = \App\Models\Productoffer::with(['language' => function($query){
        $query->whereHas('language',function($query){
          $query->where('symbol','=',\App::getLocale());
        });
      },'product'])->where('id','=',$productofferId)->first();

      $response = $offer->language && count($offer->language) ? \App\Helpers\Format::moduleDetailsTitle(\App\Helpers\Format::cropString($offer->language[0]->title,150)) : "";
      $response.= $response!="" ? ", ".lcfirst(trans('admin.productoffer.among')) : trans('admin.productoffer.among');
      $response.= ": ".$offer->product->count();
      return $response;
    }
}
