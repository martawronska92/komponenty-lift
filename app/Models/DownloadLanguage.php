<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DownloadLanguage extends Model
{
    protected $table = "download_language";

    protected $fillable = ["download_id","language_id","title","description"];

    public function language(){
      return $this->belongsTo('App\Models\Language');
    }
}
