<?php

namespace App\Models;
use App\Models\Categorylang;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class Search extends Base {

  public static function getResults($phrase){

    $perPage = \Config::get('cms.search_per_page');
    $currentPage = \Request::has('p') ? \Request::get('p') : 1;
    $pageNr = \Request::has('p') ? (int)\Request::get('p')-1 : 0;

    $searchMore = true;
    $links = [];
    $langPrefix = \App\Helpers\Language::hasPrefix();

    //PAGE
    $pages = \App\Models\Page::getSearchResults($phrase)->get();
    if($pages && count($pages)>0){
      foreach($pages as $page){
        $links[] = [\App\Helpers\Url::getPageUrl($page->id,\App::getLocale()),$page->title];
      }
    }

    //POST
    $posts =  \App\Models\Post::getSearchResults($phrase)->get();
    // echo "<pre>";
    // var_dump($posts);
    // echo "</pre>";
    // exit();
    if(count($posts)>0){
      foreach($posts as $post){
        $links[] = ["/".($langPrefix ? \App::getLocale() : "")."/".$post->page_url."/".$post->post_url,$post->post_title];
      }
    }

    //MODULES
    /**
     * przeszukanie modułów do szukania
     */
    $modulesForSearch = \App\Models\Module::where('has_search','=',1)->get();

    if(count($modulesForSearch)>0){
      foreach($modulesForSearch as $module){
        if(method_exists($module->module_entity_model,'getSearch')){
          $results = call_user_func([$module->module_entity_model,'getSearch'],$phrase,$module);
          if(is_array($results) && count($results)>0){
            $links = array_merge($links,$results);
          }
        }
      }
    }



    $response = new \stdClass();
    $response->results = array_slice($links,$pageNr*$perPage,$perPage);

    $paginator = new Paginator($links,count($links), $perPage,$currentPage,['pageName'=>'p']);
    $response->paginator = $paginator->setPath(\Route::current()->url);//, Paginator::resolveCurrentPage());

    return $response;
  }

  public static function renderOnFront(){
    $phrase = \Request::get('s');
    $results = [];
    $paginator = false;

    if($phrase!=""){
      $response = self::getResults($phrase);
      $results = $response->results;
      $paginator = $response->paginator;
    }

    return view('front.modules.search.results')->with([
      'results' => $results,
      'paginator' => $paginator
    ]);
  }

}
