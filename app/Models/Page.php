<?php

namespace App\Models;
use App\Models\Categorylang;
use App\Models\PagePostLanguage;
use Illuminate\Database\Eloquent\Model;

class Page extends Base {

	protected $table = 'page';

	//protected $dateFormat = 'Y-m-d';

	public $timestamps = true;

	protected $fillable = ['active','user_id','default_view','draft','parent_page_id',
							'level','position','default_children_view'];

	public function language()
	{
		$relation = $this->morphMany('App\Models\PagePostLanguage','entity');
		// if(\Request::segment(1)!="admin"){
		// 	$relation = $relation->whereHas('language',function($query){
		// 		$query->where('symbol','=',\App::getLocale());
		// 		$query->where('active','=',1);
		// 		$query->where('visible_on_front','=',1);
		// 	});
		// }
		return $relation;
	}

	public function subpage(){
		return $this->hasMany('App\Models\Page','parent_page_id');
	}

	public function module(){
		return $this->morphMany('App\Models\PagePostModule','entity')->with(['module']);//->withPivot('module_entity_id');
	}

	public function user(){
    return $this->belongsTo('App\Models\User','user_id');
  }

	public function menu_element()
	{
		return $this->morphMany('App\Models\MenuElement', 'entity');
	}

	public function getPageQuery(){
		 	$query = \DB::table('page')
			->join('page_post_language','page.id','=','page_post_language.entity_id')
			->join('language','language.id','=','page_post_language.language_id')
			->leftJoin('users','page.user_id','=','users.id')
			->where('page_post_language.entity_type','=','App\\Models\\Page')
			->where('language.symbol','=',\App::getLocale());

			if(\Auth::user() && \Request::segment(1)=='panel')
				$query = $query->whereIn('page.id',$this->getAllowedPages());

			if(!\App\Models\User::isDeveloper()){
 		 		$query = $query->where(function($query){
 					$query->whereNull('page.for_developer')->orWhere('page.for_developer','!=',1);
 				});
 		 	}

			return $query;
	}

	public function getList($data = [],$parentPageId = null){
		 $query = new \App\Components\Clist\ListQuery();
		 $query->query = $this->getPageQuery();

		 if($parentPageId!=NULL && !is_array($parentPageId))
		 	$parentPageId = [$parentPageId];

		 $query->query = $parentPageId!=NULL ? $query->query->whereIn('parent_page_id',$parentPageId) : $query->query->whereNull('page.parent_page_id');

		 $query->orderField = 'position';
		 $query->order = 'desc';
		 $query->fields = ['page.*','page_post_language.title',\DB::raw('concat(users.first_name," ",users.last_name) as user_name'),
		 			\DB::raw('(select count(id) from page as p where p.parent_page_id = page.id) as subpage_counter')];
	   $query->request = $data;
		 $query->orderable = ['title','id','user_name','created_at','position'];
		 $query->searchable = ['title','user_name'];
		 $query->paginate = 0;
	   return $query->format();
	}

	public function getAllowedPages(){
		return \Auth::user()->privilege()->where('symbol','=','page')->pluck('user_privilege.entity_id')->toArray();
	}

	public function getActive($skipPageId = false, $order = ['page_post_language.title','asc']){
		$response = $this->getPageQuery()->where('page.active','=',1);
		if($skipPageId!=false)
			$response = $response->where('page.id','!=',$skipPageId);
		$response = $response->whereNull('draft');
		return $response->orderBy($order[0],$order[1])
			->get(['page.id','page_post_language.title','page.level','page.parent_page_id']);
	}

	public function getLastPosition(){
		return self::max('position') + 1;
	}

	public function getForSelect(){
		$response = new \stdClass();

		$response->pages = $this->getActive();
		$response->parent = $response->pages->filter(function($page){
			return $page->parent_page_id != NULL;
		})->groupBy('parent_page_id')->toArray();

		return $response;
	}

	public static function getById($id){
		return self::with('language')
						->whereHas('language.language',function($query){
							$query->where('symbol','=',\App::getLocale());
						})
						->where('id','=',$id)
						->first();
	}

	public function getAttributesNames(){
		$langs = \App\Models\Language::getActive();
		foreach($langs as $lang){
			$attr['title.'.$lang->id] = trans('admin.list.title')." (".$lang->name.")";
      $attr['content.'.$lang->id] = trans('admin.list.content')." (".$lang->name.")";
      $attr['url.'.$lang->id] = trans('admin.post.url')." (".$lang->name.")";
			$attr['meta_title.'.$lang->id] = trans('admin.settings.meta_title')." (".$lang->name.")";
			$attr['meta_description.'.$lang->id] = trans('admin.settings.meta_description')." (".$lang->name.")";
		}

		return $attr;
	}

  public static function duplicate($pageId,$draft = true, $setActive = true){
		$oldPage = \App\Models\Page::with(['module' => function($query){
			//$query->where('active','=',1);
			$query->orderBy('position','asc');
		},'language'])->where('id','=',$pageId)->first();

		$hash = str_random(40);
		if($draft){
			$oldPage->draft = $hash;
		}
		if($setActive)
			$oldPage->active = 1;


		$newPage = \App\Models\Page::create($oldPage->toArray());

		$response = new \stdClass();
		$response->code = 0;
		$response->msg = trans('admin.post.preview_failure');

		if($newPage){

			foreach($oldPage->language as $language){
				$params = $language->toArray();
				$params['entity_id'] = $newPage->id;
				\App\Models\PagePostLanguage::create($params);
			}

			foreach($oldPage->module as $module){
				$params = $module->toArray();
				$params['entity_id'] = $newPage->id;
				\App\Models\PagePostModule::create($params);
			}

			$routeParams['hash'] = $hash;
      if(\App\Helpers\Language::checkPrefix())
        $routeParams['frontlanguage'] = \App\Models\Language::checkFrontLanguage();

      $response->code = 1;
      $response->msg = route('page_preview', $routeParams);
			$response->entity_id = $newPage->id;
			$response->hash = $hash;
		}

		return $response;
	}

	public function savePage($data,$id = false){
		try{
			\DB::beginTransaction();

      $data['active'] = array_key_exists('active', $data) ? 1 : 0;
			if($data['parent_page_id']==""){
				$data['parent_page_id'] = null;
				$data['level'] = 0;
			}else{
				$data['level'] = \App\Models\Page::where('id','=',$data['parent_page_id'])->pluck('level')->first() + 1;
			}

			if(!$id){
				$data['user_id'] = \Auth::user()->id;
				$data['position'] = $this->getLastPosition();
				$page = self::create($data);
			}else{
				$page = self::find($id);
				$page->fill($data);
				$page->save();
			}

			$langs = 0;
			if($page){

				$pagePostLanguageModel = new \App\Models\PagePostLanguage();

				/* LANGUAGE */

				$page->language()->delete();
				foreach($data['title'] as $key=>$value){
					$lang = $page->language()->create([
						'title' => $value,
            'url' => $pagePostLanguageModel->generateFriendlyLink([
								'title' => $data['url'][$key],
								'type' => 'page',
								'lang' => $key,
								'id' => $page->id,
								'parent_page_id' => $data['parent_page_id']
						 ]),
						'language_id' => $key,
            'meta_title' => trim($data['meta_title'][$key]),
            'meta_description' => trim($data['meta_description'][$key]),
					]);
					if($lang) $langs++;
				}

				/* MODULES FOR PREVIEW */
				if(array_key_exists('from_id',$data) && $data['from_id']!=""){
					$oldPage = \App\Models\Page::with(['module' => function($query){
						$query->where('active','=',1);
						$query->orderBy('position','asc');
					}])->where('id','=',$data['from_id'])->first();

					foreach($oldPage->module as $module){
						$params = $module->toArray();
						$params['entity_id'] = $page->id;
						\App\Models\PagePostModule::create($params);
					}
				}
			}

			$response = new \stdClass();
			$response->code = 1;

			if($page && count($data['title'])==$langs){
				\DB::commit();
				$response->code = 1;
				$response->id = $page->id;

				if(!array_key_exists('draft',$data) && $id == false){
					$privilegeId = \App\Models\Privilege::where('symbol','=','page')->pluck('id')->first();
					\Auth::user()->privilege()->attach($privilegeId,['entity_id' => $page->id]);

					$developers = \App\Models\User::whereHas('role',function($query){
						$query->where('symbol','=','developer');
					})->get();

					foreach($developers as $developer){
						if($developer->id!=\Auth::user()->id){
							$developer->privilege()->attach($privilegeId,['entity_id' => $page->id]);
						}
					}
				}
			}else{
				\DB::rollback();
			}

			return $response;
		}catch(Exception $e){
			return $response;
		}
	}

	public static function getSearchResults($phrase){
		return \DB::table('page_post_language')
      ->select('page_post_language.entity_id as id','page_post_language.title','page_post_language.url')
      ->distinct()

			/**
			 * page
			 */
      ->join('language','language.id','=','page_post_language.language_id')
      ->join('page','page.id','=','page_post_language.entity_id')

			/**
			 * users
			 */
      ->join('users','users.id','=','page.user_id')

			/**
			 * content
			 */
			->leftJoin('page_post_module as page_post_module_content','page_post_module_content.entity_id','=','page.id')
			->leftJoin('module as module_content','page_post_module_content.module_id','=','module_content.id')
			->leftJoin('content','content.id','=','page_post_module_content.module_entity_id')
			->leftJoin('content_language','content.id','=','content_language.content_id')
			->leftJoin('language as language_content','content_language.language_id','=','language_content.id')

      ->where('language.symbol','=',\App::getLocale())
			->where('page_post_language.entity_type','=','App\\Models\\Page')
			//->where('page_post_module_content.entity_type','=','App\\Models\\Page')

			->where(function($query){
				$query->whereNull('module_content.symbol');
				$query->orWhere('module_content.symbol','!=','content');
				$query->orWhere(function($query){
						$query->whereNotNull('module_content.symbol')
								->where('module_content.symbol','=','content')
								->where('language_content.symbol','=',\App::getLocale())
								->where('page_post_module_content.entity_type','=','App\\Models\\Page');
				});
			})

      ->where(function($query) use($phrase){
          $query->where('page_post_language.title','like',"%".$phrase."%");
          $query->orWhere('page_post_language.url','like',"%".$phrase."%");
					$query->orWhere(function($query) use($phrase){
						$query->where('module_content.symbol','=','content');
						$query->where('content_language.content','like',"%".$phrase."%");
					});
      })
      ->where('page.active','=',1);
	}

	public function getChildren($pageId){
		$children = \App\Models\Page::where('parent_page_id','=',$pageId)->get()->pluck('id')->toArray();
		if(count($children)>0){
			foreach($children as $pageId){
				$children = array_unique(array_merge($this->getChildren($pageId),$children));
			}
			return $children;
		}else{
			return array_unique($children);
		}
	}

	public function getMainParentPageId($pageId){
		$tempParentPageId = $parentPageId = $pageId;
		do{
			if($tempParentPageId!=NULL)
				$parentPageId = $tempParentPageId;
			$tempParentPageId = \App\Models\Page::where('id','=',$parentPageId)->pluck('parent_page_id')->first();
	  }while($tempParentPageId!=NULL);
		return $parentPageId;
	}

	public function getAllChildrenList($pageId,$checkActive = false){
		$childrenIds = $this->getChildren($pageId);
		$pages = self::with('language')
						->whereHas('language.language',function($query){
							$query->where('symbol','=',\App::getLocale());
						})
						->whereIn('id',$childrenIds);

		if($checkActive)
			$pages = $pages->where('page.active','=',1);

		$pages = $pages->orderBy('position','desc')->get();

		return \App\Models\Menu::parseElements($pages);
	}

	public function deleteCallback($pageId,$withModule = false){
		\App\Models\PagePostLanguage::where('entity_id','=',$pageId)->where('entity_type','App\Models\Page')->delete();
		if($withModule)
			\App\Models\PagePostModule::where('entity_id','=',$pageId)->where('entity_type','App\Models\Page')->delete();
		\App\Models\MenuElement::where('entity_id','=',$pageId)->where('entity_type','App\Models\Page')->delete();

		\DB::table('user_privilege')
			->join('privilege','privilege.id','=','user_privilege.privilege_id')
			->where('privilege.symbol','=','page')
			->where('user_privilege.entity_id','=',$pageId)
			->delete();

		$popup = \App\Models\Popup::first();
		if($popup && $popup->link_type == "page" && $popup->link_value==$pageId){
			\App\Models\Popup::where('id','=',$popup->id)->update([
				'link_value' => '',
				'link_type' => ''
			]);
		}

		if(\App\Models\Page::where('id','=',$pageId)->first())
			return \App\Models\Page::where('id','=',$pageId)->delete();
		else
			return 1;
	}

	public function deletePage($pageId){
		if(!is_array($pageId))
			$pageId = [$pageId];

		$modules = [];

		$response = new \stdClass();
		$response->itemCounter = count($pageId);

		\DB::beginTransaction();

		$toDelete = 1;
		$deleted = 0;

		foreach($pageId as $id){
			$children = $this->getChildren($id);
			$toDelete+= count($children);
			$modules = array_merge($modules,\App\Models\PagePostModule::whereIn('entity_id',array_merge($children,[$id]))->where('entity_type','=','App\Models\Page')->get()->pluck('id')->toArray());
			$isDeleted = $this->deleteCallback($id);

			if($isDeleted){
				foreach($children as $pageId){
					$deleted+= $this->deleteCallback($pageId);
				}
			}
			$deleted+= (int)$isDeleted;
		}

		if($deleted == $toDelete){
			\DB::commit();
			\App\Models\PagePostModule::removeModule(array_unique($modules));
			$response->code = 1;
		}else{
			\DB::rollback();
			$response->code = 0;
		}

		return $response;
	}

	public function getMenu(){
		$result = $this->getPageQuery()->orderBy('title','asc')->get(['page.id','page_post_language.title','page.level','parent_page_id']);
		return \App\Models\Menu::parseElements($result);
	}
}
