<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuElementLanguage extends Model
{
    protected $table = "menu_element_language";

    protected $fillable = ['title','language_id'];

    public function language(){
      return $this->belongsTo('App\Models\Language');
    }
}
