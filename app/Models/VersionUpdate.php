<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VersionUpdate extends Model
{
    protected $table = "version_update";

    protected $fillable = ['version_number','description'];

    /**
     * pobiera uaktualnienia począwszy od numeru wersji
     */
    public function getVersions($fromVersion){
      return self::where('version_number','>',$fromVersion)->orderBy('version_number','asc')->get();
    }

    /**
     * sprawdza czy dana aktualizacja jest już zainstalowana
     */
    public function hasVersion($versionNumber){
      return self::where('version_number','=',$versionNumber)->count() > 0;
    }

    /**
     * ustawia numer i informacje o nowo dodanym uaktualnieniu
     */
    public function setVersion($version){
      $response = new \stdClass();
      $response->code  = 1;
      $response->version = $version['version_number'];
      $response->domain = $_SERVER['HTTP_HOST'];
      $response->msg = "";

      if(!$this->hasVersion($version['version_number'])){
         $result = self::create([
          'version_number' => $version['version_number'],
          'description' => $version['description']
        ]);

        \App\Models\Version::first()->update([
          'version_number' => $version['version_number']
        ]);

        $response->code = $result ? 1 : 0;
        $response->msg = trans('admin.version.install_'.($result ? 'success' : 'failure'));
      }else{
        $response->code = 2;
        $response->msg = "Żądana wersja jest już zainstalowana";
      }

      return $response;
    }

    /**
     * rozsyła uaktualnienia do wszystkich FTP
     */
    public function updateSystem(){
      $ftpAccounts = \App\Models\VersionFtpAccount::all();
      $currentVersion = \App\Models\Version::first()->version_number;

      $response = [];
      foreach($ftpAccounts as $account){  //wszystkie konta FTP
        $component = new \App\Components\VersionUpdate($account->domain);
        $accountVersion = $component->getVersion();
        $currentVersion = $currentVersion ? $currentVersion->version_number : 0;
        if($currentVersion > $accountVersion){ //jeśli bieżąca wersja > od tej na FTP
          $versions = $this->getVersions($accountVersion); //różnica wersji
          $installedVersions = 0;
          foreach($versions as $version){
            $result = $this->install($version,$account); //instaluj wersję
            if(!$result->code){
              $response[$account->host] = [$result];
            }else{
              $tempResult = json_decode($component->setUpdateVersion($version->toArray()));
              if($tempResult->code == 2) //sprawdzamy ile wersji jest już zainstalowanych
                $installedVersions++;
              else
                $response[$account->host][] = $tempResult; //aktualizuj nr wersji
            }
          }//foreach

          if(count($versions) == $installedVersions){
            $tempResponse = new \stdClass();
            $tempResponse->code = 2;
            $tempResponse->msg = trans('admin.version.uptodate');
            $response[$account->host] = [$tempResponse];
          }

        }//if
      }//foreach

      // echo "<pre>";
      // var_dump($response);
      // echo "</pre>";
      // exit();

      return $response;
    }

    /**
     * instaluje uaktualnienie na danym FTP
     */
    public function install($version,$ftpAccount){
      $response = new \stdClass();
      $response->code = 1;
      $response->msg = "";

      $account = $ftpAccount->toArray();

      try{
          $account['username'] = decrypt($account['username']);
          $account['password'] = decrypt($account['password']);

          \Config::set('ftp.connections.connection1', $account);

          $response = \App\Models\VersionFtpAccount::checkConnection($account);

          if($response->code){
            $path = storage_path('updates/'.$version->version_number);

            $component = new \App\Components\VersionUpdate($ftpAccount->domain);

            if(!\File::isDirectory($path)){
              $response->code = 0;
              $response->msg = trans('admin.version.no_update_directory');
            }else if(!\File::exists($path.'/update.xml')){
              $response->code = 0;
              $response->msg = trans('admin.version.no_xml_file');
            }else{
              $data = simplexml_load_string(file_get_contents($path.'/update.xml'));
              if(property_exists($data,'changes')){
                if(property_exists($data->changes,'change') && count($data->changes->change)>0){
                  $baseFtpDirectory = "/public_html";
                  foreach($data->changes->change as $change){
                    $change->item = ltrim((string)$change->item,"/");
                    if(!property_exists($change,'type')) $change->type = '';

                    switch((string)$change->type){
                      case 'new':
                        if(\File::isDirectory($path.'/'.$change->item))
                          \FTP::connection()->makeDir($baseFtpDirectory."/".$change->item);
                        else
                          \FTP::connection()->uploadFile($path.'/'.$change->item, $baseFtpDirectory."/".$change->item,FTP_BINARY);
                        break;
                      case 'delete':
                        if(\File::isDirectory($path.'/'.$change->item))
                          \FTP::connection()->removeDir($baseFtpDirectory."/".$change->item, 1);
                        else
                          \FTP::connection()->delete($baseFtpDirectory."/".$change->item);
                        break;
                      case 'rename':
                        \FTP::connection()->rename($baseFtpDirectory."/".$change->old_item, $baseFtpDirectory."/".$change->new_item);
                        break;
                      case 'truncate':
                        if(\File::isDirectory())
                          \FTP::connection()->truncateDir($baseFtpDirectory."/".$change->item);
                        break;
                      default:
                        \FTP::connection()->uploadFile($path."/".$change->item, $baseFtpDirectory."/".$change->item,FTP_BINARY);
                        break;
                    }
                  }
                }
              }

              //assetsy ?


              /**
               * migracje
               */
              $component->migrate();

              /**
               * seedy
               */
              if(property_exists($data,'seeds')){
                foreach($data->seeds->seedClass as $seedClass){
                  $component->seed((string)$seedClass);
                }
              }

              /**
               * cache
               */
              $component->clearCache();

              \FTP::disconnect();

              $response->msg = trans('admin.version.install_success');
            }
          }
        }catch(Exception $e){
          $response->code = 0;
          $response->msg = trans('admin.version.ftp_connection_error');
        }

        return $response;

      /**
      * 1) pobranie listy uaktualnien
      * 2) sprawdzenie czy istnieją pliki
      * 3) sprawdzenie czy paczki zawierają update.xml
      * 4)
      *        - czy backup ?
      *        - czy sa migracje ?
      *        - czy są seedy ?
      *        - czy assetsy ?
      *        - kopiowanie
      *  5) php artisan view:clear
      *  6) php artisan route:clear
      *  7) zmiana wersji
      */
    }

    public function migrate(){
      \Artisan::call('migrate');
    }

    public function seed($seedClass){
      \Artisan::call('db:seed',['--class' => $seedClass ]);
    }

    public function clearCache(){
      \Artisan::call('view:clear');
      \Artisan::call('route:clear');
      \File::cleanDirectory(storage_path('framework/views'));
    }
}
