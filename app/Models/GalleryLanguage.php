<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryLanguage extends Model
{
    protected $table = 'gallery_language';

    protected $fillable = ['title','description','language_id','gallery_id'];

    public function language(){
      return $this->belongsTo('App\Models\Language');
    }
}
