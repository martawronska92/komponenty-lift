<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductofferProduct extends Model
{
    protected $table = "product_offer_product";

    protected $fillable = ['post_id'];

    public function info(){
      return $this->belongsTo('App\Models\Post','post_id');
    }
}
