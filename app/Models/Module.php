<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Module extends Model
{
    protected $table = 'module';

    protected $fillable = ['symbol','url','for_page','active'];

    public function category(){
      return $this->hasMany('App\Models\Category','module_id');
    }

    public function categoryDefault(){
      return $this->hasOne('App\Models\ModuleCategoryDefault','module_id');
    }

    public function language(){
      return $this->hasMany('App\Models\ModuleLanguage');
    }

    public function field(){
      return $this->hasMany('App\Models\ModuleField')->with('language');
    }

    public function frontlink(){
      return $this->hasMany('App\Models\ModuleFrontLink','module_id');
    }

    public function getFrontList($moduleSymbol,$category_parent_id = NULL){
      $module = self::where('symbol','=',$moduleSymbol)->first();
      return \App\Models\Category::with(['language' => function($query){
        $query->whereHas('language',function($query){
          $query->where('symbol','=',\App::getLocale());
        });
      }])
      ->where('category_parent_id','=',$category_parent_id)
      ->where('active','=',1)
      ->where('module_id','=',$module->id)
      ->orderBy('position','desc')
      ->get();
    }

    public function getList($moduleSymbol,$categoryParentId = null){
      $module = self::where('symbol','=',$moduleSymbol)->first();
      if($module){
        $query = \DB::table('category')
          ->select(['category.*','category_language.name',
              \DB::raw('(select count(id) from post where post.category_id = category.id) as post_counter'),
              \DB::raw('(select count(c.id) from category c where c.category_parent_id = category.id) as subcategory_counter')]
          )
          ->join('category_language','category_language.category_id','=','category.id')
          ->join('language','language.id','=','category_language.language_id')
          ->where('language.symbol','=',\App::getLocale())
          ->where('category.active','=',1)
          ->where('module_id','=',$module->id);
        $query = $categoryParentId!=NULL ? $query->where('category_parent_id','=',$categoryParentId) : $query->whereNull('category_parent_id');
        return $query->orderBy('position','desc')->get();
      }else{
        return [];
      }
    }

    /**
     * pobiera moduły dla postów/stron/kategorii
     * @param  string $forItem 'for_post' / 'for_page' / 'for_category'
     * @return array      lista modułów
     */
    public static function getFor($forItem = 'for_post'){
      $query = \DB::table('module')
              ->select(['module.*','module_language.name'])
              ->join('module_language','module_language.module_id','=','module.id')
              ->join('language','module_language.language_id','=','language.id')
              ->where('module.active','=',1)
              ->where($forItem,'=',1)
              ->where('language.symbol','=',\App::getLocale());

      if(!\App\Models\User::isDeveloper()){
        $query = $query->where(function($query){
          $query->whereNull('module.for_developer');
          $query->orWhere('module.for_developer','!=',1);
        });
      }

      $query = $query->orderBy('module_language.name','ASC');

      return $query->get();
    }

    public function getIdBySymbol($symbol){
      return self::where('symbol','=',$symbol)->pluck('id')->first();
    }

    public static function checkModelBySymbol($symbol){
      return \File::exists(app_path().'/Models/'.ucfirst($symbol).'.php');
    }

    public static function renderOnFront($module){
      $model = new \App\Models\Module();
      $categories = $model->getFrontList($module->module->symbol);

      if(get_class($module) == "App\Models\ModuleFrontLink"){
        $page = \App\Models\Page::with(['language' => function($query)  use ($module){
              $query->where('url','=',$module->link);
              $query->whereHas('language',function($query){
                $query->where('symbol','=',\App::getLocale());
              });
            }])
            ->whereHas('language',function($query) use ($module){
              $query->where('url','=',$module->link);
              $query->whereHas('language',function($query){
                $query->where('symbol','=',\App::getLocale());
              });
            })
            ->first();
      }else{
        $page = \App\Models\Page::with(['language' => function($query){
              $query->whereHas('language',function($query){
                $query->where('symbol','=',\App::getLocale());
              });
            }])
            ->where('id','=',$module->entity_id)
            ->whereHas('language.language',function($query){
              $query->where('symbol','=',\App::getLocale());
            })
            ->first();
      }

      return view('front.modules.'.$module->module->symbol.'.list')->with([
        'langPrefix' => \App\Helpers\Language::checkPrefix(),
        'page' => $page,
        'categories' => $categories,
        'module' => $module
      ])->render();
    }

    /**
     * [getSitemap description]
     * @param  \App\Models\Module $module dane modułu
     * @param  boolean $langPrefix  czy prefix do jezyków
     * @return array   tablica obiektów \Components\Sitemap
     */
    public static function getSitemap($module,$langPrefix){

      $sitemaps = [];

      $categories = \App\Models\Category::with([
        'post' => function($query){
            $query->where('active','=',1);
            $query->where(function($query){
        		         $query->where('public','=',1);
        		         $query->orWhere(function($query){
        		           $query->where('public','=',0);
        		           $query->where('publish_date','<=',Carbon::now());
        		});
          });
        },
        'language'
      ])
      ->where('active','=',1)
      ->where('module_id','=',$module->id)
      ->get();

      foreach($categories as $category){
          $links = [];

          foreach($category->language as $key=>$language){
            $url = ($langPrefix ? $language->language->symbol : "")."/".trim(\App\Helpers\Url::getCategoryUrl($category->id,$language->language->symbol),"/");
            $item = new \App\Components\Sitemap(\URL::to($url),
               $language->updated_at,
               \App\Models\Sitemap::getPriority($url,$langPrefix),
               'daily',
               array(),
               $language->name,
               $language->language->symbol);
             array_push($sitemaps,$item);
          }



           foreach($category->post as $post){
             $links = [];

             foreach($post->language as $key=>$language){
                $url = ($langPrefix ? $language->language->symbol : "")."/".trim(\App\Helpers\Url::getFrontPostUrl($post->id,$language->language->symbol),"/");
                $item = new \App\Components\Sitemap(\URL::to($url),
                   $language->updated_at,
                   \App\Models\Sitemap::getPriority($url,$langPrefix),
                   'daily',
                   array(),
                   $language->title,
                   $language->language->symbol);
                 array_push($sitemaps,$item);
             }
           }
      }

      return $sitemaps;
    }

    public static function getSearch($phrase,$module){

      $langPrefix = \App\Helpers\Language::hasPrefix();

      $results = \DB::table('page_post_language as post_data')
        ->select('post_data.title as post_title','page_data.url as page_url','post_data.url as post_url',
          'category_language.url as category_url','category_language.name as category_name','category.id as category_id')
        ->distinct()

        /**
         * posty
         */
        ->join('language as post_language','post_language.id','=','post_data.language_id')
        ->join('post','post.id','=','post_data.entity_id')

        /**
         * kategorie
         */
        ->join('category','category.id','=','post.category_id')
        ->join('category_language','category_language.category_id','=','category.id')
        ->join('language as category_language_data', 'category_language_data.id','=', 'category_language.language_id')

        /**
         * strony
         */
        ->join('module','module.id','=','category.module_id')
        ->join('page_post_module','page_post_module.module_id','=','module.id')
        ->join('page_post_language as page_data','page_data.entity_id','=','page_post_module.entity_id')
        ->join('language as page_language','page_language.id','=','page_data.language_id')
        ->join('page','page.id','=','page_post_module.entity_id')

        ->join('users','users.id','=','post.user_id')

        ->where('post_language.symbol','=',\App::getLocale())
        ->where('page_language.symbol','=',\App::getLocale())
        ->where('category_language_data.symbol','=',\App::getLocale())

        ->where('post_data.entity_type','=','App\\Models\\Post')
        ->where('page_post_module.entity_type','=','App\\Models\\Page')

        ->where('module.symbol','=',$module->symbol)

        ->where(function($query) use ($phrase){
            $query->where('post_data.title','like',$phrase."%");
            $query->orWhere('post_data.content','like',"%".$phrase."%");
            $query->orWhere('post_data.url','like',$phrase."%");
            $query->orWhere('category_language.name','like',$phrase."%");
        })
        ->where('post.active','=',1)
        ->where('page.active','=',1)
        ->whereNull('page.draft')
        ->whereNull('post.draft')

        ->where('category.module_id','=',$module->id)

        ->where(function($query){
            $query->where('public','=',1);
            $query->orWhere(function($query){
                $query->where('public','=',0);
                $query->where('publish_date','<=',Carbon::now());
            });
        })
        ->get();

      $links = [];
      if(count($results)>0){
        $categoryId = 0;
        foreach($results as $result){
          if($result->category_id != $categoryId){
            $links[] = ["/".($langPrefix ? \App::getLocale() : "")."/".$result->page_url."/".$result->category_url,$result->category_name];
            $categoryId = $result->category_id;
          }
          $links[] = ["/".($langPrefix ? \App::getLocale() : "")."/".$result->page_url."/".$result->category_url."/".$result->post_url,$result->post_title];
        }
      }
      return $links;
    }

    public static function switchModuleIdInPreview($entityId,$entityType,$oldModuleEntityId,$newModuleEntityId){
      \App\Models\PagePostModule::where('entity_id','=',$entityId)
        ->where('entity_type','=',$entityType)
        ->where('module_entity_id','=',$oldModuleEntityId)
        ->update([
          'module_entity_id' => $newModuleEntityId,
          'draft' => 1
        ]);
    }
}
