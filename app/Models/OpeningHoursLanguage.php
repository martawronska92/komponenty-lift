<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OpeningHoursLanguage extends Model
{
    protected $table = "opening_hours_language";

    protected $fillable = ['language_id','value','opening_hours_id'];

    public function language(){
      return $this->belongsTo('App\Models\Language');
    }
}
