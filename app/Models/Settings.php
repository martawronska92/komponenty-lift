<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model {

	protected $table = 'settings';

	public $timestamps = false;

	public function group(){
		return $this->belongsTo('App\Models\SettingsGroup','settings_group_id');
	}

	public function subgroup(){
		return $this->belongsTo('App\Models\SettingsGroup','settings_subgroup_id');
	}

	public function language(){
		return $this->hasMany('App\Models\SettingsLanguage','settings_id');
	}

	public static function getByGroup($symbol){
		$rows = self::whereHas('group',function($query) use($symbol){
			$query->where('symbol','=',$symbol);
		})->get();
		return self::parse($rows);
	}

	public static function getBySymbol($symbol){
		if(self::checkIfExists($symbol)){
			$row = self::where('symbol','=',strtolower($symbol))->first();
			return $row->value;
		}else{
			return "";
		}
	}

	public static function checkIfExists($symbol){
		return self::where('symbol','=',strtolower($symbol))->count() > 0;
	}

	public function getOptionsAttribute($value)
  {
        return json_decode($value);
  }

	public static function getAll($languageSymbol = false){
		$rows = self::with('language')
						->orderBy('settings_group_id','ASC')
						->orderBy('position','ASC')
						->get();
		return self::parse($rows,$languageSymbol);
	}

	public static function parse($rows,$languageSymbol = false){
		$response = [];
		foreach($rows as $row){
			if(count($row->language)>0){
				if($languageSymbol != false){
					foreach($row->language as $l){
						if($l->language->symbol == $languageSymbol)
							$response[$row->symbol] = $l->value;
					}
				}else{
					$response[$row->symbol] = $row;
				}
			}else
				$response[$row->symbol] = $row->value;
		}
		return $response;
	}

	public function saveSettings($data){
			try{
				$symbols = self::pluck('symbol');

				foreach($symbols as $symbol){
					if(array_key_exists($symbol,$data)){
						$value = $data[$symbol];

						if(is_array($value)){
								$languageSettings = self::with('language')->where('symbol','=',$symbol)->first();
								if($languageSettings->language->count()>0){
									\App\Models\SettingsLanguage::where('settings_id','=',$languageSettings->id)->delete();
								}
								foreach($value as $languageId => $set){
										\App\Models\SettingsLanguage::insert([
											'language_id' => $languageId,
											'value' => $set,
											'settings_id' => $languageSettings->id
										]);
								}
						}else{
							self::where('symbol','=',$symbol)->update(['value'=>$value]);
						}
					}
				}

				if(array_key_exists('contact_name',$data)){
					$model = new \App\Models\OpeningHours();
					$model->store($data);
				}

				return true;
			}catch(Exception $e){
				return false;
			}
	}

}
