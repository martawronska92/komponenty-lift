<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryPhotoLanguage extends Model
{
    protected $table = "gallery_photo_language";

    protected $fillable = ['language_id','title','gallery_photo_id','filename'];

    public function language(){
      return $this->belongsTo('App\Models\Language');
    }
}
