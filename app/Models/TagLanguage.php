<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TagLanguage extends Model
{
    protected $table = "tag_language";

    protected $fillable = ['name','language_id','friendly_name','tag_id'];

    public function language(){
      return $this->belongsTo('App\Models\Language');
    }
}
