<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModuleFrontLink extends Model
{
    protected $table = "module_front_link";

    public function module(){
      return $this->belongsTo('App\Models\Module','module_id');
    }

    public function language(){
      return $this->belongsTo('App\Models\Language','language_id');
    }
}
