<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryVideoLanguage extends Model
{
  protected $table = "gallery_video_language";

  protected $fillable = ['language_id','title','gallery_video_id'];

  public function language(){
    return $this->belongsTo('App\Models\Language');
  }
}
