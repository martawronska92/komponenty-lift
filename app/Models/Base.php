<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Base extends Model{

  public function checkIfExists($id,$table){
    return \DB::table($table)->where('id','=',$id)->count() > 0;
  }

  public function deleteMulti($ids,$table){
      $ids = is_array($ids) ? $ids : explode(",",$ids);

      $response = new \stdClass();

      \DB::beginTransaction();

      $deleted =  0;
      $callbackResults = [];

      foreach($ids as $id){
        $row = \DB::table($table)->where('id','=',$id);
        $deleted+= $row->count() ? $row->delete() : 0;
      }

      $response = new \stdClass();
      $response->itemCounter = count($ids);      
      $response->ids = $ids;

      if($deleted  == count($ids)){
        \DB::commit();
        $response->code = 1;
      }else{
        \DB::rollback();
        $response->code = 0;
      }

      return $response;
  }

  public function getViews($dirs){
    if(!is_array($dirs)){
      $dirs = [$dirs];
    }

    $response = [];

    foreach($dirs as $dir){
  		$files = \File::allFiles(base_path()."/resources/views/front/".$dir);

  		foreach($files as $file){
        $content = file_get_contents(base_path()."/resources/views/front/".$dir."/".$file->getFilename());
        preg_match_all("/View name:[0-9a-zA-Z\s]*/i",$content,$matches);
        $viewSymbol = str_replace("/",".",$dir)."." . explode('.',$file->getFilename())[0];
        $viewName = (count($matches[0]) > 0) ? trim(explode(":",$matches[0][0])[1]) : $viewSymbol;
        $response[$viewSymbol] =$viewName;
  		}
    }

		return $response;
	}
}
