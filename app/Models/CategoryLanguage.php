<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class CategoryLanguage extends Model {

	protected $table = 'category_language';

	public $timestamps = true;

	protected $fillable = ['name','description','language_id','category_id','url','meta_title','meta_description'];

	public function language()
	{
		return $this->belongsTo('App\Models\Language');
	}

	public function category()
	{
		return $this->belongsTo('App\Models\Category');
	}

	/**
	 * sprawdza czy bieżący przyjazny link jest poprawny
	 * @param  string $slug bieżący slug linku
	 * @param  integer $lang id langu sluga
	 * @param  integer/string $id   id sprawdzanego bytu aby nie był brany pod uwagę
	 * @return $query
	 */
	public function checkFriendlyLink($slug,$lang,$id){
		$query = self::where('url','=',trim($slug))
							->where('language_id','=',$lang);
	  if($id!=""){
				$query = $query->where('category_id','!=',$id);
	  }
		return $query;
	}
}
