<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DownloadFileLanguage extends Model
{
    protected $table = "download_file_language";

    protected $fillable = ['title','language_id','download_file_id'];  

    public function language(){
      return $this->belongsTo('App\Models\Language');
    }
}
