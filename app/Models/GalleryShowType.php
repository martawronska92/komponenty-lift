<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryShowType extends Model
{
    protected $table = 'gallery_show_type';

    /**
     * zwraca rodzaje wyświetleń dla danego symbolu
     * @param  string $symbol photo/video
     * @return array         tablica rodzajów wyświetleń
     */
    public static function getTypes($symbol){
      return self::where('gallery_type','=',$symbol)->get();
    }

    public static function getSorted($symbol){
      $types = self::getTypes($symbol);

      $response = [];
      foreach($types as $type){
        $response[$type->id] = ucfirst(trans('admin.gallery.show_type_'.$type->symbol));
      }
      asort($response);

      return $response;
    }
}
