<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Download extends Base
{
    protected $table = "download";

    public function file(){
      return $this->hasMany('App\Models\DownloadFile');
    }

    public function language(){
      return $this->hasMany('App\Models\DownloadLanguage');
    }

    public function getList($data = []){
      $query = new \App\Components\Clist\ListQuery();
      //$query->query = \App\Models\Gallery::with(['photo'])->where('type','=','photo');
      $query->query = \DB::table('download')
        ->join('download_file','download_file.download_id','=','download.id')
        ->join('download_file_language','download_file_language.download_file_id','=','download_file.id')
        ->join('language','language.id','=','download_file_language.language_id')
        ->where('language.symbol','=',\App::getLocale());

      $query->orderField = "position";
      $query->order = "desc";
      $query->request = $data;
      $query->orderable = ['position'];
      $query->fields = ['download_file.*','download_file_language.title'];
      return $query->format();

    }

    public function getFrontList(){
      return self::with(['language','file' => function($query){
        $query->where('active','=',1);
      }])
        ->whereHas('language.language',function($query){
            $query->where('symbol','=',\App::getLocale());
        })
       ->orderBy('position','desc')
       ->get();
    }

    public function getAttributesNames(){
  		$attr = [
  			'file' => trans('admin.download.file')
  		];

  		$langs = \App\Models\Language::getActive();
  		foreach($langs as $lang){
  			$attr['title.'.$lang->id] = trans('admin.post.title')." (".$lang->name.")";
        $attr['content.'.$lang->id] = trans('admin.post.content')." (".$lang->name.")";
        $attr['url.'.$lang->id] = trans('admin.post.url')." (".$lang->name.")";
  		}

  		return $attr;
    }

    public function getPosition($downloadId){
      return \App\Models\DownloadFile::where('download_id','=',$downloadId)->max('position') + 1;
    }

    public function storeFile($data,$id = false){
      \DB::beginTransaction();

      $response = new \stdClass();

      $params = array();


      if(array_key_exists('form',$data))
        parse_str($data['form'], $params);

      if($id && array_key_exists('Filedata',$data) && $data['Filedata']->getSize()>\App\Helpers\File::getMaxSize()){
        $response->code = 0;
        $response->msg = trans('admin.download.add_maxsize_failure');
      }else{

        $download = $id ? \App\Models\Download::find($id) : new \App\Models\Download();

        $fileParams = [];

        if(array_key_exists('Filedata',$data) && $data['Filedata']->getSize()>0){
          $fileName = pathinfo($data['Filedata']->getClientOriginalName(), PATHINFO_FILENAME);
          $fileParams['ext'] = $data['Filedata']->getClientOriginalExtension();
          $fileParams['filesize'] = $data['Filedata']->getSize();
          $fileParams['filename_slug'] = $fileName;

          if(!array_key_exists('file_id',$params)){
            $fileParams['filename_hashed'] = str_random(32);
            $fileParams['position'] = $this->getPosition($id);
            $fileParams['downloaded'] = 0;
            $fileParams['draft'] = 0;
          }
        }

        $fileParams['active'] = array_key_exists('active',$params) ? $params['active'] : 1;


        if(array_key_exists('file_id',$params)){
          $file = \App\Models\DownloadFile::where('id','=',$params['file_id'])->first();
          $file->update($fileParams);
          $file->language()->delete();
        }else{
          $file = $download->file()->create($fileParams);
        }

        $insertedLangs = 0;
        if(array_key_exists('title',$params)){
          foreach($params['title'] as $lang=>$title){
            $result = $file->language()->create([
              'title' => $title,
              'language_id' => $lang
            ]);
            if($result) $insertedLangs++;
          }
        }else{
          $params['title'] = [];
        }

        if($download && $insertedLangs == count($params['title'])){
          if(array_key_exists('Filedata',$data))
            $upload = $data['Filedata']->storeAs('file/download', $file->filename_hashed.'.'.$file->ext,'all');
          else
            $upload = true;

          if($upload){
            \DB::commit();
            $response->code = 1;
            $data = \App\Models\DownloadFile::where('id','=',$file->id)
            ->with(['language' => function($query){
              $query->whereHas('language',function($query){
                $query->where('symbol','=',\App::getLocale());
              });
            }])->first();
            $response->id = $file->id;
            $response->view = \View::make('admin.download.partial.filebox',['file'=>$data])->render();
            $response->left_view = \View::make('admin.download.partial.form',[
                'langs' => \App\Models\Language::getActive()
              ])->render();
          }else{
            \DB::rollback();
            $response->code = 0;
            $response->msg = trans('admin.update_failure');
          }
        }else{
          \DB::rollback();
          $response->code = 0;
          $response->msg = trans('admin.update_failure');
        }
      }

      return $response;
    }

    public function storeData($data,$id = false){
      \DB::beginTransaction();

      $download = $id ? \App\Models\Download::find($id) : new \App\Models\Download();

      $toInsert = count($data['title']);
      $inserted = 0;

      $download->language()->delete();
      foreach($data['title'] as $langId=>$value){
        $lang = $download->language()->create([
          'language_id' => $langId,
          'title' => $value,
          'description' => $data['description'][$langId]
        ]);
        if($lang)
          $inserted++;
      }

      \App\Models\DownloadFile::where('download_id','=',$download->id)->update([
        'draft' => 0
      ]);

      $response = new \stdClass();
      $response->code = $toInsert==$inserted;
      if($response->code){
        $response->msg = "";
        \DB::commit();
      }else{
        $response->msg = "";
        \DB::rollback();
      }
      return $response;
    }

    public function deleteMulti($ids,$table = ''){
      \DB::beginTransaction();

      $ids = is_array($ids) ? $ids : explode(",",$ids);

      $deleted =  0;
      $filesToDelete = [];

      foreach($ids as $id){
        $downloadFiles = \App\Models\DownloadFile::where('download_id','=',$id)->get();
        if(count($downloadFiles)>0){
          foreach($downloadFiles as $file)
            array_push($filesToDelete,$file->filename_hashed.'.'.$file->ext);
        }
        $deleted+= (int)\App\Models\Download::where('id','=',$id)->delete();
      }

      $response = new \stdClass();

      if($deleted  == count($ids)){
        foreach($filesToDelete as $file){
          $file = public_path('file/download/').$file;
          if(\File::exists($file))
            \File::delete($file);
        }
        \DB::commit();
        $response->code = 1;
      }else{
        \DB::rollback();
        $response->code = 0;
      }

      $response->msg = trans_choice('admin.download.delete_'.($response->code ? 'success' : 'failure'),count($ids));

      return $response;
    }

    public static function renderOnFront($module){
        $list = \App\Models\DownloadFile::where('download_id','=',$module->module_entity_id)
        ->with(['language' => function($query){
            $query->whereHas('language',function($query){
              $query->where('symbol','=',\App::getLocale());
            })->orderBy('title','asc');
        }])
        ->where('active','=',1);
        if(\Route::currentRouteName()!="page_preview" && \Route::currentRouteName()!="post_preview")
          $list = $list->where('draft','=',0);
        $list = $list->orderBy('position','desc')->get();

        $info = \App\Models\Download::where('id','=',$module->module_entity_id)
        ->with(['language' => function($query){
            $query->whereHas('language',function($query){
              $query->where('symbol','=',\App::getLocale());
            });
        }])->first();
      return view('front.modules.download.list')->with(['list'=>$list,'info'=>$info]);
    }

    public static function getRouting(){
      return [
        new \App\Components\Route('module','get',trans('admin.download.get_route').'/{hash}/{filename}','Front\DownloadController@getfile','download_getfile')
      ];
    }

    public function createEmpty(){
      return self::create()->id;
    }

    public function deleteObject($id, $preview = false){
      if(self::where('id','=',$id)->count() == 0)
        return true;
      else{
        $response = $this->deleteMulti($id);
        return $response->code;
      }
    }

    public function getModuleDetails($downloadId){
      $response = "";
      $download = \App\Models\Download::where('id','=',$downloadId)->with(['language' => function($query){
        $query->whereHas('language',function($query){
          $query->where('symbol','=',\App::getLocale());
        });
      }])->first();
      if($download && count($download->language)>0 && $download->language[0]->title!="")
        $response.= \App\Helpers\Format::moduleDetailsTitle(\App\Helpers\Format::cropString($download->language[0]->title,150)).", ".lcfirst(trans('admin.download.file_counter'));
      else
        $response.= trans('admin.download.file_counter');
      $response.= ": ".\App\Models\DownloadFile::where('download_id','=',$downloadId)->count();
      return $response;
    }
}
