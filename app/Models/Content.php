<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = "content";

    public function language(){
      return $this->hasMany('App\Models\ContentLanguage');
    }

    public function createEmpty(){
      return self::create()->id;
    }

    public function saveContent($data,$id){
      \DB::beginTransaction();

      \App\Models\ContentLanguage::where('content_id','=',$id)->delete();

      $inserts = 0;
      foreach($data['content'] as $langId=>$value){
        $result = \App\Models\Content::find($id)->language()->create([
          'language_id' => $langId,
          'content' => $value,
          'title' => $data['title'][$langId]
        ]);
        if($result->id)
          $inserts++;
      }

      if($inserts == count($data['content'])){
        \DB::commit();
        return true;
      }else{
        \DB::rollback();
        return false;
      }
    }

    public static function renderOnFront($module){
      $content = self::where('id','=',$module->module_entity_id)
        ->with(['language' => function($query){
            $query->whereHas('language',function($query){
              $query->where('symbol','=',\App::getLocale());
            });
        }])->first();

      if($content && count($content->language) > 0){
        $content->language[0]->content = \App\Models\SeoSmartLinks::searchAndReplace($content->language[0]->content);
      }

      return view('front.modules.content.content',['content'=>$content])->render();
    }

    public function deleteObject($id, $preview = false){
      if(\App\Models\Content::where('id','=',$id)->count() == 0)
        return true;
      else
        return \App\Models\Content::where('id','=',$id)->delete();
    }

    public function getModuleDetails($contentId){
      $object = self::where('id','=',$contentId)->with(['language' => function($query){
        $query->whereHas('language',function($query){
          $query->where('symbol','=',\App::getLocale());
        });
      }])->first();

      $content = "";
      if($object && count($object->language)>0){
        if($object->language[0]->title!="")
          $content = \App\Helpers\Format::cropString($object->language[0]->title,150);
        else
          $content = str_limit(strip_tags($object->language[0]->content),70);
      }

      return \App\Helpers\Format::moduleDetailsTitle($content);
    }
}
