<?php

namespace App\Models;
use App\Models\Categorylang;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Category extends Base {

	protected $table = 'category';
	public $timestamps = true;

	protected $fillable = ['category_view','per_page','loading_type','post_view','user_id','module_id','thumbnail',
			'category_parent_id','thumbnail_options','level','show_modules'];

	public function language()
	{
		return $this->hasMany('App\Models\CategoryLanguage');
	}

	public function post(){
			return $this->hasMany('App\Models\Post','category_id');
	}

	public function module(){
		return $this->belongsTo('App\Models\Module');
	}

	public function modules(){
		return $this->morphMany('App\Models\PagePostModule','entity')->with(['module']);//->withPivot('module_entity_id');
	}

	public function field(){
		return $this->hasMany('App\Models\CategoryField')->with(['language','value']);
	}

	public function subcategory(){
		return $this->hasMany('App\Models\Category','category_parent_id')->with(['language']);
	}

	public function getList($data){
		$query = new \App\Components\Clist\ListQuery();

		$query->query = \DB::table('category')
					->join('category_language','category.id','=','category_language.category_id')
					->join('language','language.id','=','category_language.language_id')
					->where('language.symbol','=',\App::getLocale())
					->whereNull('category.module_id');
		$query->fields = ['category.id','category_language.name',\DB::raw('(select count(post.id) from post where post.category_id = category.id and post.draft is null) as post_counter')];
		$query->searchable = ['category_language.name'];
		$query->orderable = ['id','name','post_counter'];
    $query->request = $data;
    return $query->format();
	}

	public static function getActive(){
		return self::with(['language' => function($query){
						$query->whereHas('language',function($query){
							$query->where('symbol','=',\App::getLocale());
						});
					}])
					->whereNull('module_id')
					->where('active','=',1)
					->get();
	}

	/**
	 * listuje kategorie z modułu
	 * @param  int $moduleId id modułu
	 * @param  int $skipCategoryId id kategorii do pominięcia
	 * @return collection  kolekcja modułów
	 */
	public function getListForModule($moduleId,$skipCategoryId = false){
		$query = self::with(['language'=> function($query){
							$query->whereHas('language',function($query){
								$query->where('symbol','=',\App::getLocale());
							})
							->orderBy('name','desc');
						}])
						->where('module_id','=',$moduleId)
						->where('active','=',1);
		 if($skipCategoryId)
		 		$query = $query->where('id','!=',$skipCategoryId);

		 $query = $query->orderBy('position','desc');

		 return	$query->get();
	}

	public function getAttributesNames(){
		$attr = [
			'per_page' => trans('admin.category.per_page')
		];

		$langs = \App\Models\Language::getActive();
		foreach($langs as $lang){
			$attr['name.'.$lang->id] = trans('admin.name')." (".$lang->name.")";
		}

		return $attr;
	}

	public function getPosition($moduleId = null){
		return self::where('module_id','=',$moduleId)->max('position') + 1;
	}

	public function saveCategory($data,$id = false){
		$response = new \stdClass();
		$response->code = 0;

		try{
			\DB::beginTransaction();

			if(array_key_exists('thumbnail_encoded',$data) && $data['thumbnail_encoded']!=""){
        $data['thumbnail'] = "png?v=".mt_rand(10000000, 99999999);
      }

			if(array_key_exists('category_parent_id',$data)){
				if($data['category_parent_id']==""){
					$data['category_parent_id'] = null;
					$data['level'] = 0;
				}else{
					$data['level'] = \App\Models\Category::where('id','=',$data['category_parent_id'])->pluck('level')->first() + 1;
				}
			}

			if(!$id){
				$data['user_id'] = \Auth::user()->id;
				$data['position'] = $this->getPosition(array_key_exists('module_id',$data) ? $data['module_id'] : NULL) + 1;
				$category = self::create($data);
			}else{
				//$category = true;
				$category = self::find($id);
				$category->fill($data);
				$category->save();
			}

			$insertedLangs = $langsToInsert = 0;
			if($category){
				$id = $category->id;

				$thumbDir = public_path().'/img/category/'.$category->id;
        if(!is_dir($thumbDir) && array_key_exists('thumbnail_encoded',$data))
          mkdir($thumbDir);

				/**
				 * langs
				 */
				if(array_key_exists('name',$data)){
					$langsToInsert = count($data['name']);
					\App\Models\CategoryLanguage::where('category_id','=',$id)->delete();
					foreach($data['name'] as $key=>$value){
						$lang = \App\Models\CategoryLanguage::create([
							'name' => $value,
							'description' => array_key_exists('description',$data) ? $data['description'][$key] : NULL,
							'language_id' => $key,
							'category_id' => $id,
							'url' => array_key_exists('url',$data) ? $data['url'][$key] : NULL,
							'meta_title' => array_key_exists('meta_title',$data) ? $data['meta_title'][$key] : NULL,
							'meta_description' => array_key_exists('meta_description',$data) ? $data['meta_description'][$key] : NULL
						]);
						if($lang) $insertedLangs++;
					}
				}

				/**
				 * Fields
				 */
				$deletedFields = 0;
				$fieldsToDelete = [];

				if(array_key_exists('deleted_fields',$data)){
					$fieldsToDelete = $data['deleted_fields']!="" ? explode(",",$data['deleted_fields']) : [];
					foreach($fieldsToDelete as $fieldId){
						$field = \App\Models\CategoryField::find($fieldId);
						$deletedFields+= $field ? (int)$field->delete() : 0;
					}
				}

				if(array_key_exists('field',$data)){

						foreach($data['field'] as $key=>$field){

							if(!array_key_exists('fieldId',$data) || !array_key_exists($key,$data['fieldId'])){
								$baseField = $category->field()->create([]);
							}else{
								\App\Models\CategoryFieldLanguage::where('category_field_id','=',$data['fieldId'][$key])->delete();
								$baseField = \App\Models\CategoryField::find($data['fieldId'][$key]);
							}
							foreach($field as $langId => $name){
								$baseField->language()->attach($langId,['name'=>$name]);
							}
						}
					}

			}

			if($category && $langsToInsert==$insertedLangs && $deletedFields==count($fieldsToDelete)){
				if(array_key_exists('thumbnail_encoded',$data)){
					if($data['thumbnail_encoded']!=""){
          	$content = explode(',',$data['thumbnail_encoded']);
          	file_put_contents($thumbDir.'/'.'thumbnail.png',base64_decode($content[1]));
						if($data['thumbnail_original']!=""){
							$content = explode(',',$data['thumbnail_original']);
							file_put_contents($thumbDir.'/'.'original.png',base64_decode($content[1]));
						}
					}else{
						if(\File::exists($thumbDir.'/'.'thumbnail.png')) @unlink($thumbDir.'/'.'thumbnail.png');
						if(\File::exists($thumbDir.'/'.'original.png')) @unlink($thumbDir.'/'.'original.png');
					}
				}
				\DB::commit();
				$response->id = $category->id;
				$response->code = 1;
			}else{
				\DB::rollback();
			}
			return $response;

		}catch(Exception $e){
			return $response;
		}
	}

	public static function getNameById($id){
		 $category = self::getById($id);
		 return $category ? $category->language[0]->name : "";
	}

	public static function renderOnFront($module){
		$category = \App\Models\Category::where('id','=',$module->module_entity_id)->first();

		$query = \App\Models\Post::with([
			'language' => function($query){
				$query->whereHas('language',function($query){
					$query->where('symbol','=',\App::getLocale());
				});
			}
		])
		->where('category_id','=',$module->module_entity_id)
		->where('active','=',1)
		->where(function($query){
		         $query->where('public','=',1);
		         $query->orWhere(function($query){
		           $query->where('public','=',0);
		           $query->where('publish_date','<=',Carbon::now());
		         });
		})
		->whereNull('draft');

	  if($category == false)
			return "";
		else
	  	return view('front.'.$category->category_view)->with([
				'category'=>$category,
				'counter'=>$query->count(),
				'posts'=>$query->orderBy('position','desc')->skip(0)->take($category->per_page)->get(),
				'langPrefix' => \App\Helpers\Language::checkPrefix(),
				'segments' => \Request::segments()
			]);
	}

	public static function getById($id){
		return self::where('id','=',$id)
						 ->with('language')
						 ->whereHas('language.language',function($query){
							 $query->where('symbol','=',\App::getLocale());
						 })->first();
	}

	public static function getDataForSelect(){
		return self::join('category_language','category.id','=','category_language.category_id')
		->join('language','category_language.language_id','=','language.id')
		->select('category.id','category_language.name')
		->where('category.active','=',1)
		->where('language.symbol','=',\App::getLocale())
		->whereNull('module_id')
		->get()->toArray();
	}

	public function createEmpty($moduleData){

    $categoryId = \App\Models\Category::create([
      'category_view' => ($moduleData && $moduleData->categoryDefault && $moduleData->categoryDefault->category_view!="") ? $moduleData->categoryDefault->category_view : 'category.view.default',
			'post_view' => ($moduleData && $moduleData->categoryDefault && $moduleData->categoryDefault->post_view!="") ? $moduleData->categoryDefault->post_view : 'post.view.default',
			'active' => 1,
			'per_page' => ($moduleData && $moduleData->categoryDefault && $moduleData->categoryDefault->per_page!="") ? $moduleData->categoryDefault->per_page : 9,
			'user_id' => \Auth::user()->id,
			'module_id' => $moduleData['id']
    ])->id;

		$moduleLanguage = \App\Models\ModuleLanguage::where('module_id','=',$moduleData['id'])->get();
		foreach($moduleLanguage as $lang){
				\App\Models\CategoryLanguage::create([
					'name' => $lang->name,
					'language_id' => $lang->language_id,
					'category_id' => $categoryId
				]);
		}

		return $categoryId;
  }

	public function deleteCategory($id){
		return (int)\App\Models\Category::where('id','=',$id)->first()->delete();
	}

	/**
	 * wywoływane przy usuwaniu modułu
	 */
	public function deleteObject($categoryId,$preview = false){
		$result = new \stdClass();
		$result->code = 1;

		$moduleId = \App\Models\Module::where('category_id','=',$categoryId)->get()->pluck('id')->toArray();

		// if(\App\Models\PagePostModule::where('module_id','=',$moduleId)->count()==1){
			$posts = \App\Models\Post::where('category_id','=',$categoryId)->get()->pluck('id')->toArray();
			if(count($posts)>0){
				$postModel = new \App\Models\Post();
				$result = $postModel->deletePost($posts);
			}
		// }

		$this->deleteCategory($categoryId);

		return $result->code;
	}

	/**
	 * zwraca szczegóły modułu do list modułów w widoku strony/wpisu
	 * @param  int $categoryId id kategorii
	 * @return string	szczegóły
	 */
	public function getModuleDetails($categoryId){
		$object = self::where('id','=',$categoryId)->with(['language'=>function($query){
			$query->whereHas('language',function($query){
				$query->where('symbol','=',\App::getLocale());
			});
		}])->first();

		$name = count($object->language)>0 ? $object->language[0]->name."," : "";

		return trans('admin.category.post_counter').": ".$object->post()->count();
	}

	public function getModuleMenu($moduleId){
		return \App\Models\Menu::parseElements($this->getListForModule($moduleId));
	}
}
