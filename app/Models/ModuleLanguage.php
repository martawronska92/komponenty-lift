<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModuleLanguage extends Model
{
    protected $table = "module_language";

    public function language(){
      return $this->belongsTo('App\Models\Language');
    }    
}
