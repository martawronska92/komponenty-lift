<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Base
{
    protected $table = 'gallery';

    protected $fillable = ['name','show_type_id','type'];

    public function show_type(){
      return $this->belongsTo('App\Models\GalleryShowType');
    }

    public function video(){
      return $this->hasMany('App\Models\GalleryVideo','gallery_id')->orderBy('position','desc');
    }

    public function photo(){
      return $this->hasMany('App\Models\GalleryPhoto','gallery_id')
        // ->whereHas('language.language',function($query){
        //   $query->where('symbol','=',\App::getLocale());
        // })
        ->orderBy('position','desc');
    }

    public function language(){
      return $this->hasMany('App\Models\GalleryLanguage');
    }

    public function getAttributesNames(){
      return [
        'name' => trans('admin.name'),
        'show_type_id' => trans('admin.gallery.show_type')
      ];
    }
}
