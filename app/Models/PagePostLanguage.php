<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class PagePostLanguage extends Model {

	protected $table = 'page_post_language';

	public $timestamps = true;

	protected $fillable = ['title','content','language_id','entity_id','entity_type','url','meta_title',
					'meta_description'];

	public function page(){
		return $this->morphedByMany('App\Models\Page','entity','page_post_language','id','entity_id');
	}

	public function post(){
		return $this->morphedByMany('App\Models\Post', 'entity');
	}

	public function language()
	{
		return $this->belongsTo('App\Models\Language');
	}

	public function checkCategoryFriendlyLink($data){
		$slug = str_slug(array_key_exists('title',$data) ? $data['title'] : $data['name'], '-');
		$categoriesSlug = \App\Models\CategoryLanguage::where('language_id','=',$data['lang'])
								->whereHas('category',function($query) use ($data){
									if((int)$data['parent_category_id']){
										$query->where('category_parent_id','=',(int)$data['parent_category_id']);
									}else{
										$query->whereNull('category_parent_id');
									}
									if((int)$data['module_id']){
										$query->where('module_id','=',(int)$data['module_id']);
									}else{
										$query->whereNull('module_id');
									}
								});
		if((int)$data['id']!=0)
			$categoriesSlug = $categoriesSlug->where('category_id','!=',(int)$data['id']);
		$categoriesSlug = $categoriesSlug->get()
								->pluck('url')
								->toArray();

		return !in_array($slug,$categoriesSlug);
	}

	public function checkPagePostFriendlyLink($data){
		$categoryId = array_key_exists('category_id',$data) ? $data['category_id'] : false;
		$slug = str_slug(array_key_exists('title',$data) ? $data['title'] : $data['name'], '-');

		$category = \App\Models\Category::with(['module','language'=> function($query) use ($data){
			$query->where('language_id','=',$data['lang']);
		}])->where('id','=',$categoryId)->first();

		/**
		 * pobranie id strony dla wpisu
		 */
		if($data['type'] == "post"){
			$page = false;
			if($category->module->module_entity_model == "App\Models\Category"){
				$query = "SELECT entity_id FROM page_post_module
					LEFT JOIN module ON page_post_module.module_id = module.id
					LEFT JOIN category ON category.module_id = module.id
					WHERE page_post_module.entity_type = 'App\\\\Models\\\\Page'
					AND  module.module_entity_model = 'App\\\\Models\\\\Category'
					AND page_post_module.module_entity_id = ".$categoryId." LIMIT 1";

				$page = \DB::select($query);
			}

			if($page){
				$pageId = $page[0]->entity_id;
				$parentPageId = \App\Models\Page::where('id','=',$pageId)->first()->parent_page_id;
			}else{
				$pageId = $parentPageId = false;
			}
		}else if($data['type'] == "page"){
			$pageId = $data['id'];
			$parentPageId = $data['parent_page_id'];
		}

		/**
		 * pobranie wszystkich children
		 */
		$childrenSlug = [];
		$rootPagesIds = \App\Models\Page::whereNull('parent_page_id')->whereNull('draft')->get()->pluck('id')->toArray();

		if((int)$parentPageId){
				$childrenSlug = self::where('language_id','=',$data['lang'])->where('entity_type','=','App\\Models\\Page');
				if($data['type'] == "page" && $data['id']!=""){
					$childrenSlug = $childrenSlug->where('entity_id','!=',$data['id']);
				}

				$childrenIds = $parentPageId!="" ? \App\Models\Page::where('parent_page_id','=',$parentPageId)->whereNull('draft')->get()->pluck('id')->toArray() : [];

				if($parentPageId){
					$childrenSlug = $childrenSlug->whereIn('entity_id',$childrenIds);
				}else{
					$childrenSlug = $childrenSlug->whereIn('entity_id',$rootPagesIds);
				}
				$childrenSlug = $childrenSlug->get()->pluck('url')->toArray();
		}else if($data['type'] == "page"){
				$childrenSlug = self::where('language_id','=',$data['lang'])->where('entity_type','=','App\\Models\\Page');
				if($data['type'] == "page" && $data['id']!=""){
					$childrenSlug = $childrenSlug->where('entity_id','!=',$data['id']);
				}
				$childrenSlug = $childrenSlug->whereIn('entity_id',$rootPagesIds)->get()->pluck('url')->toArray();
		}else if($data['type'] == "post" && $category && $category->module->module_entity_model == "App\\Models\\Category"){
				$pageId = \App\Models\PagePostModule::where('entity_type','=','App\\Models\\Page')->where('module_id','=',$category->module->id)
									->where('module_entity_id','=',$category->id)->first()->entity_id;
			  $childrenIds = \App\Models\Page::where('parent_page_id','=',$pageId)->whereNull('draft')->get()->pluck('id')->toArray();
				$childrenSlug = self::where('language_id','=',$data['lang'])
														->where('entity_type','=','App\\Models\\Page')
														->whereIn('entity_id',$childrenIds)->get()->pluck('url')->toArray();
		}

		/**
		 * pobranie wszystkich wpisów przypisanych do kategorii strony
		 */
		$query = "SELECT DISTINCT page_post_module.module_entity_id FROM page_post_module
				 LEFT JOIN module ON page_post_module.module_id = module.id
				 LEFT JOIN category ON category.module_id = module.id
				 WHERE module.module_entity_model = 'App\\\\Models\\\\Category'";
	  if($data['type']=="page"){
				if((int)$parentPageId){
				 	$query.= " AND page_post_module.entity_id = ".$parentPageId;
				}else if(count($rootPagesIds) > 0){
					$query.= " AND page_post_module.entity_id IN (".implode(',',$rootPagesIds).")";
				}
				$categoryIds = \DB::select($query);
		}else if($data['type']=='post'){
				 if($pageId){
						$query.= " AND page_post_module.entity_id = (".$pageId.")";
						$categoryIds = \DB::select($query);
					}else{
						$categoryIds = [$categoryId];
					}
		}else{
				$categoryIds = [];
		}

		$postSlug = [];

		if(count($categoryIds)){
			$ids = [];
			if(!$category || $category->module->module_entity_model == "App\Models\Category"){
				foreach($categoryIds as $row){
					array_push($ids,$row->module_entity_id);
				}
			}else{
				$ids = $categoryIds;
			}

			$postIds = \App\Models\Post::whereIn('category_id',$ids)->get()->pluck('id')->toArray();

			$postSlug = self::whereIn('entity_id',$postIds)
										->where('language_id','=',$data['lang'])
										->where('entity_type','=','App\\Models\\Post')
										->get()
										->pluck('url')
										->toArray();
		}

		return !in_array($slug,$childrenSlug) && !in_array($slug,$postSlug);
	}

  /**
   * sprawdza czy bieżący przyjazny link jest poprawny
   * @param array $data
   * title - tytuł strony wpisu
   * id - id stron wpisu
   * category_id - id kategorii dla wpisu
   * type - typ post/page
   * lang - id langa
   * @return boolean       true/false czy link poprawny
   */
	public function checkFriendlyLink($data){
		if($data['type'] == 'category'){
			return $this->checkCategoryFriendlyLink($data);
		}else{
			return $this->checkPagePostFriendlyLink($data);
		}
	}

	/**
	 * sprawdza a w razie konieczności generuje nowy przyjazny link
	 * @param  array $data
	 * @return string       zwraca prawidłowego linku
	 */
	public function generateFriendlyLink($data){
		$slug = str_slug(array_key_exists('title',$data) ? $data['title'] : $data['name'], '-');
		$valid = $this->checkFriendlyLink($data);

		if($valid){
			return $slug;
		}else{
			$index = 0;
			do{
				$index++;
				$data['title'] = $slug."-".$index;
				$valid = $this->checkFriendlyLink($data);
			}while(!$valid);
			return $data['title'];
		}

	}

}
