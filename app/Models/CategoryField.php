<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryField extends Model
{
    protected $table = "category_field";

    protected $fillable = ['category_id'];

    public function value(){
      return $this->hasOne('App\Models\CategoryFieldValue');
    }

    public function language(){
      return $this->belongsToMany('App\Models\Language','category_field_language','category_field_id','language_id')
                  ->withPivot(['name','language_id','id']);
    }

}
