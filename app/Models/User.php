<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'email', 'password','first_name','last_name','active','role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static $passwordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d].*$";

    public function module(){
        return $this->belongsToMany('App\Models\Module','user_module')->withPivot('user_id');
    }

    public function privilege(){
        return $this->belongsToMany('App\Models\Privilege','user_privilege')->withPivot(['user_id','entity_id']);
    }

    public function role(){
        return $this->belongsTo('App\Models\Role','role_id');
    }

    public function getList($data = []){
        $query = new \App\Components\Clist\ListQuery();
        //$query->query = self::where('id','>',0);
        $normalRole = \App\Models\Role::where('symbol','=','normal')->pluck('id')->first();
        $query->query = \DB::table('users')->where('role_id','=',$normalRole);
        if(!\App\Models\User::isDeveloper())
          $query->query = $query->query->where('for_developer','=',0);
        $query->orderField = "email";
        $query->order = "asc";
        $query->request = $data;
        $query->orderable = ['id','email','first_name','last_name','last_login_history'];
        $query->fields = ['id','email','first_name','last_name','last_login_history','active'];
        $query->paginate = 0;
        return $query->format();
    }

    public function getAttributesNames(){
      return [
        'name' => trans('admin.user.name'),
        'first_name' => trans('admin.user.first_name'),
        'last_name' => trans('admin.user.last_name'),
        'email' => trans('admin.user.email'),
        'password' => trans('auth.password')
      ];
    }

    public static function isDeveloper(){
      return \Auth::user() && \Auth::user()->role && \Auth::user()->role->symbol == 'developer';
    }

    public function saveUser($data,$id = false){
        \DB::beginTransaction();

        $response = new \stdClass();
        $response->code = 0;
        $response->msg = "";

        // $uniqueName = self::where(\DB::raw('lower(name)'),'=',trim(strtolower($data['name'])));
        // if($id)
        //   $uniqueName = $uniqueName->where('id','!=',$id);

        $uniqueEmail = self::where(\DB::raw('lower(email)'),'=',trim(strtolower($data['email'])));
        if($id)
          $uniqueEmail = $uniqueEmail->where('id','!=',$id);

        $password = "";
        if(array_key_exists('password',$data) && $data['password']!=""){
          $password = $data['password'];
        }else if(!$id){
          $password =  str_random(7).rand(0,9);
        }
        if($password!=""){
          $data['password'] = \Hash::make($password);
        }

        // if($uniqueName->count() > 0){
        //   $response->msg = trans('admin.user.name_exists',['name'=>$data['name']]);
        //   $response->backToEdit = 1;
        // }else
        if($uniqueEmail->count() > 0){
          $response->msg = trans('admin.user.email_exists',['email'=>$data['email']]);
          $response->backToEdit = 1;
        }else{
          $data['role_id'] = \App\Models\Role::where('symbol','=','normal')->pluck('id')->first();
          $user = $id ? self::find($id) : new self();
          $user->fill($data);
          $user->save();
          $response->code = $user->id ? 1 : 0;
          $response->id = $user->id;
          $response->msg = 'admin.'.($id ? 'update_' : 'user.save_').($response->code ? 'success' : 'failure');

          if($id != \Auth::user()->id && array_key_exists('module',$data))
            $user->module()->sync($data['module']);

          if($id != \Auth::user()->id && array_key_exists('privilege',$data)){
            $user->privilege()->detach();
            foreach($data['privilege'] as $privilegeId){
              if(array_key_exists('privilege_details',$data) && array_key_exists($privilegeId,$data['privilege_details'])){
                foreach($data['privilege_details'][$privilegeId] as $entityId=>$value){
                  $user->privilege()->attach($privilegeId,['entity_id'=>$entityId]);
                }
              }else{
                $user->privilege()->attach($privilegeId);
              }
            }
          }

          if($response->code && $password!=""){
              $data['original_password'] = $password;
              \Mail::to($data['email'])->send(new \App\Mail\User($data));
              $response->msg = 'admin.'.($id ? 'user.update_withmail_' : 'user.save_withmail_').'success';
          }
        }

        $response->code ? \DB::commit() : \DB::rollback();

        return $response;
    }

    public function deleteUser($ids){
        $response = new \stdClass();
        $response->code = 0;
        $response->msg = "";

        \DB::beginTransaction();

        $deleted = 0;
        foreach($ids as $id){
          $user = self::find($id);
          $deleted+= $user ? (int)$user->delete() : 0;
        }

        /*
            sprawdzic czy zostal jakis admin
         */
        if($deleted == count($ids)){
          $response->code = 1;
          $response->msg = trans('admin.user.delete_success');
          \DB::commit();
        }else{
          $response->msg = trans('admin.user.delete_failure');
          \DB::rollback();
        }

        return $response;
    }

    /**
     * sprawdza czy użytkownik ma dostęp do danego modułu
     * @param  string $moduleSymbol symbol modułu
     * @return boolean    dostęp do modułu
     */
    public static function canAccess($moduleSymbol){
      $requiredModules = \App\Models\Module::where('need_user_priviledge','=',1)->get()->pluck('symbol')->toArray();
      $userModules = \Auth::user()->module->pluck('symbol')->toArray();

      return in_array($moduleSymbol,$requiredModules) && !in_array($moduleSymbol,$userModules) ? false : true;
    }

    /**
     * sprawdza czy użytkownik ma dane uprawnienie
     * @param  string  $priviledgeSymbol symbol uprawnienia
     * @return boolean   posiadanie uprawnienia
     */
    public static function hasPrivilege($privilegeSymbol,$entityId = null){
      $defaultPrivileges = \App\Models\Privilege::get()->pluck('symbol')->toArray();
      $userPrivileges = \Auth::user()->privilege()->get();;

      $userPrivilegeSymbol = [];
      $userPrivilegeEntity = [];
      foreach($userPrivileges as $privilege){
        array_push($userPrivilegeSymbol,$privilege->symbol);
        if($privilegeSymbol == $privilege->symbol)
          array_push($userPrivilegeEntity,$privilege->pivot->entity_id);
      }

      return in_array($privilegeSymbol,$defaultPrivileges)
            && in_array($privilegeSymbol,$userPrivilegeSymbol)
            && ($entityId == NULL || ($entityId!=NULL && in_array($entityId,$userPrivilegeEntity)));
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new \App\Notifications\ResetPassword($token));
    }

    public function generatePassword(){
      do{
        $password = str_random(7).rand(0,9);
      }while(!preg_match("/".self::$passwordRegex."/",$password));
      return $password;
    }
}
