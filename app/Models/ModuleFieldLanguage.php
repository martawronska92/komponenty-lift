<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModuleFieldLanguage extends Model
{
    protected $table = "module_field_language";

    public function language(){
      return $this->belongsTo('App\Models\Language');
    }
}
