<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\SliderItem;

class Slider extends Base
{

    protected $table = 'slider';

    protected $fillable = ['name','symbol','active'];

    public function slide(){
      return $this->hasMany('App\Models\SliderItem');
    }

    public function getList(){
		$query = new \App\Components\Clist\ListQuery();
		$query->query = \DB::table('slider');
		$query->fields = ['slider.*',\DB::raw('(select count(id) from slider_item where slider_item.slider_id=slider.id) as slides_counter')];
    $query->paginate = 0;
	  $query->request = [];
		return $query->format();

    }

    public static function getActive(){
      return self::with('slide')->where('active','=',1)->get();
    }

    public function getAttributesNames(){
      return [
        'internal_content' => trans('admin.slider.internal_link'),
        'external_content' => trans('admin.slider.external_content'),
        'thumbnail_encoded' => trans('admin.slider.image')
      ];
    }

    protected function getPosition($sliderId){
      return \App\Models\SliderItem::where('slider_id','=',$sliderId)->max('position') + 1;
    }

    public function getSlidesFromSlider($sliderId){
      $query = new \App\Components\Clist\ListQuery();

      $query->query = \DB::table('slider_item')
        ->join('slider','slider_item.slider_id','=','slider.id')
        ->join('slider_item_language','slider_item.id','=','slider_item_language.slider_item_id')
        ->join('language','language.id','=','slider_item_language.language_id')
        ->where('language.symbol','=',\App::getLocale())
        ->where('slider.id','=',$sliderId);

      $query->orderField = "position";
      $query->order = "asc";
      $query->fields = ['slider_item.*','slider_item_language.title_1','slider_item_language.title_2'];
      $query->orderable = ['position'];
      $query->paginate = 0;
      return $query->format();
    }

    public function store($data,$id = false){
        \DB::beginTransaction();

        $slideParams = [
          'external' => (int)$data['external'],
          'active' => (int)array_key_exists('active', $data)
        ];

        if($data['external']==-1)
          $slideParams['link'] = NULL;
        else
          $slideParams['link'] = $data['external'] ? $data['external_content'] : $data['internal_content'];

        if($data['thumbnail_encoded']!=""){
          $slideParams['ext'] = "png?v=".mt_rand(10000000, 99999999);
          $slideParams['thumbnail_options'] = $data['thumbnail_options'];
        }

        if(array_key_exists('slider_id',$data)){
          $slideParams['position'] = $this->getPosition($data['slider_id']);
          $slider = \App\Models\Slider::find($data['slider_id']);
          $slide = $slider->slide()->create($slideParams);
        }else {
          $slide = \App\Models\SliderItem::find($id);
          $slideParams['position'] = $this->getPosition($slide->slider_id);
          $slide->fill($slideParams);
          $slide->save();
          $slide->language()->delete();
        }

        $thumbDir = public_path().'/img/slider/'.$slide->id;
        if(!is_dir($thumbDir))
          mkdir($thumbDir);

        $insertedLangs = 0;
        foreach($data['title_1'] as $lang=>$value){
          $newSlide = $slide->language()->create([
            'title_1'=>$value,
            'title_2'=>$data['title_2'][$lang],
            'language_id'=>$lang
          ]);
          if($newSlide) $insertedLangs++;
        }

        $response = new \stdClass();

        if($slide && $insertedLangs == count($data['title_1'])){
          if($data['thumbnail_encoded']!=""){
            $content = explode(',',$data['thumbnail_encoded']);
            file_put_contents($thumbDir.'/thumbnail.png',base64_decode($content[1]));
            if($data['thumbnail_original']!=""){
              $original = explode(',',$data['thumbnail_original']);
              file_put_contents($thumbDir.'/'.'original.png',base64_decode($original[1]));
            }
          }

          \DB::commit();

          $response->code = 1;
          $response->sliderId = $slide->slider_id;
        }else{
          \DB::rollback();
          $response->code = 0;
          $response->sliderId = array_key_exists('slider_id',$data) ? $data['slider_id'] : $slide->slider_id;
        }

        return $response;
    }

    public function getSliderId($id){
      return \App\Models\SliderItem::where('id','=',$id)->first()->slider_id;
    }

    public static function renderOnFront(){
      $symbol = in_array(\Route::currentRouteName(),['homepage','homepage_language']) ? 'homepage' : 'subpage';

      $slides = \App\Models\SliderItem::with([
        'language' => function($query){
          $query->whereHas('language',function($query){
            $query->where('symbol','=',\App::getLocale());
          });
        }
      ])
      ->whereHas('slider',function($query) use ($symbol){
        $query->where('symbol','=',$symbol);
      })
      ->where('active','=',1)
      ->orderBy('position','asc')
      ->get();

      return view('front.modules.slider.slides')->with(['slides' => $slides]);
    }
}
