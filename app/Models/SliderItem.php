<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SliderItem extends Base
{
    protected $table = 'slider_item';

    protected $fillable = ['slider_id','ext','active','position','external','link','thumbnail_options'];

    public function slider(){
      return $this->belongsTo('App\Models\Slider');
    }

    public function language(){
      return $this->hasMany('App\Models\SliderItemLanguage');
    }
}
