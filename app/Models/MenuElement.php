<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuElement extends Base
{
    protected $table = "menu_element";

    public function menu(){
      return $this->belongsTo('App\Models\Menu');
    }

    public function language(){
      return $this->hasMany('App\Models\MenuElementLanguage','menu_element_id');
    }

    public function page(){
      //var_dump($this->morphedByMany('App\Models\Page','entity','menu_element','id','entity_id')
      //          ->leftJoin('page_post_language','page_post_language.entity_id','=','page.id')
                // ->wherePivot('language_id','=',1)
                //->withPivot(['id'])
                // ->toSql());
        //exit();


      return $this->morphedByMany('App\Models\Page','entity','menu_element','id','entity_id');
      // ->with([
      //   'language.language' => function($query){
      //     $query->where('symbol','=',\App::getLocale());
      //   }
      // ])->whereHas('language.language',function($query){
      //   $query->where('symbol','=',\App::getLocale());
      // })->distinct();
                //->leftJoin('page_post_language','page_post_language.entity_id','=','page.id')
                //->where('page_post_language.language_id','=',1);
                //->where('page_post_language.entity_type','=','App\\Models\\Page')
                //->withPivot(['id']);

      //return $this->belongsToMany('App\Models\Page','menu_element','entity_id','id')
      //          ->where('entity_type','=','App\\Models\\Page');

    }

    public function getById($id){
      return self::where('id','=',$id)->with('language')->first()->toArray();
    }

    public function getPosition($menuId){
      return self::where('menu_id','=',$menuId)->max('position') + 1;
    }

    public function store($data){
      \DB::beginTransaction();

      $response = new \stdClass();
      $response->code = 0;
      $response->msg = "";

      $menuElement = array_key_exists("id",$data) && $data['id']!="" ? \App\Models\MenuElement::find($data['id']) : new \App\Models\MenuElement();

      $menuELementText = "";
      $currentLangId = \App\Models\Language::where('symbol','=',\App::getLocale())->first()->id;

      if($menuElement){
        $menuElement->external = (int)($data['menu_element_type'] == "link");
        $menuElement->active = $data['active'];
        $menuElement->external_url = $data['url'];
        $menuElement->menu_id = $data['menu_id'];
        $menuElement->mainpage = (int)($data['menu_element_type']=="mainpage");

        if(array_key_exists("id",$data) && $data['id']==""){
          $menuElement->level = $data['level'];
          $menuElement->position = $this->getPosition($data['menu_id']) + 1;
          $menuElement->parent_id = $data['parent_id'] == 0 ? NULL : (int)$data['parent_id'];
        }

        if($data['menu_element_type']=="page" || $data['menu_element_type']=="category"){
          $menuElement->entity_id = $data['menu_element_type']=="page" ? $data['page'] : $data['category'];
          $menuElement->entity_type = "App\Models\\".ucfirst($data['menu_element_type']);
        }
        $menuElement->save();

        $insertedLangs = 0;
        $menuElement->language()->delete();
        foreach($data['title'] as $lang=>$title){
          $elLang = $menuElement->language()->create([
            'title' => $title,
            'language_id' => $lang
          ]);
          $insertedLangs+= $elLang ? 1 : 0;
          if($currentLangId == $lang){
            $menuElementText = $title;
          }
        }

        if($insertedLangs == count($data['title'])){
          \DB::commit();
          $response->code = 1;
          $response->msg = trans('admin.menu.edit_success');
          $response->text = $menuElementText;
          $response->id = $menuElement->id;
        }else{
          \DB::rollback();
          $response->code = 0;
          $response->msg = trans('admin.menu.edit_failure');
        }
      }else{
        $response->code = 0;
        $response->msg = trans('admin.menu.save_failure');
      }

      return $response;
    }
}
