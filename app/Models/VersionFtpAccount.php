<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VersionFtpAccount extends Model
{
    protected $table = "version_ftp_account";

    public static function checkConnection($account){
      $response = new \stdClass();
      $response->code = 1;
      $response->msg = "";

      if(!checkdnsrr($account['host'],'ANY')){
        $response->code = 0;
        $response->msg = trans('admin.version.ftp_host_no_exists',['host' => $account['host']]);
      }

      if($response->code){
          $con = ftp_connect($account['host'],21,90);

          if($con == false){
            $response->code = 0;
            $response->msg = trans('admin.version.ftp_connection_failure');
          }else{
            $loggedIn = @ftp_login($con, $account['username'], $account['password']);

            if($loggedIn == false) {
              $response->code = 0;
              $response->msg = trans('admin.version.ftp_login_failure');
            }
          }

          ftp_close($con);
      }

      return $response;
    }
}
