<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingsLanguage extends Model
{
    protected $table = 'settings_language';

    public function language(){
      return $this->belongsTo('App\Models\Language');
    }
}
