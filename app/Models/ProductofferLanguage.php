<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductofferLanguage extends Model
{
    protected $table = "product_offer_language";

    protected $fillable = ['title','language_id'];

    public function language(){
      return $this->belongsTo('App\Models\Language');
    }
}
