<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Language extends Base {

	protected $table = 'language';

	protected $fillable = ['active','main','domain'];

	public $timestamps = false;

	public static function getActive(){
		return self::where('active','=',1)->get();
	}

	public function getList($data = []){
		$query = new \App\Components\Clist\ListQuery();
		$query->query = \DB::table('language');

		 $query->orderField = 'name';
		 $query->order = 'asc';
		 $query->fields = ['language.*'];
		 $query->orderable = ['name'];
	   $query->request = $data;

	   return $query->format();
	}


	/**
	 * języki możliwe do zmiany w przełączniku
	 * @return [type] [description]
	 */
	public static function getFrontLanguages(){
		return self::where('active','=',1)->where('visible_on_front','=',1)->get();
	}

	public static function searchForDomain(){
		return self::where('domain','like','%'.$_SERVER['HTTP_HOST'].'%')->where('active','=',1)->where('visible_on_front','=',1)->first();
	}

	/**
     * obsługa zmiany języka
     * @param string $symbol symbol języka
     * @return boolean
     */
    public static function change($symbol){
        $lang = self::where('symbol','=',$symbol)
                ->where('active','=',1)
                ->first();

        if($lang){
            session([
                'admin_lang_id'=> $lang->id,
                'admin_lang_symbol' => $lang->symbol
            ]);

            return true;
        }else{
            return false;
        }
    }

		public static function checkFrontLanguage($langSymbol = ""){
				$exists = self::where('active','=',1)->where('visible_on_front','=',1);
				if($langSymbol!=""){
					$exists = $exists->where('symbol','=',strtolower($langSymbol));
				}else {
					$exists = $exists->where('main','=',1);
				}

				$exists = $exists->first();

				if($exists == null){
					$exists = self::where('active','=',1)->where('visible_on_front','=',1)->first();
				}

				$response = $exists ? $exists->symbol : \App::getLocale();

				return $response;
		}

		public static function checkAdminLanguage(){
			 return self::where('active','=',1)->first();
		}

		public function switchLangAttributes($data){
			 $response = new \stdClass();
			 $response->code = 1;

			 $params[$data['field']] = $data['value'];
			 $response->$data['field'] = (int)$data['value'];

	     //sparawdzic ile widocznych dla klienta
	     $frontLanguages = \App\Models\Language::where('visible_on_front','=',1)->where('id','!=',$data['itemId'])->count();
			 $langData = \App\Models\Language::where('id','=',$data['itemId'])->first();

			 /**
			  * visible_on_front
			  */
	     if($data['field']=='visible_on_front'){
	       if((int)$data['value'] == 0){
	         if($frontLanguages == 0){ //nie ma innych jezyków na froncie
	           $response->code = 0;
	           $response->msg = trans('admin.language.last_visible_on_front');
						 $response->visible_on_front = $response->active = $params['active'] = $params['visible_on_front'] = 1;
	         }
	       }else{//jezyk dla frontu musi być aktywny
					 $params['active'] = $response->active = 1;
				 }
			 /**
			  * active
			  */
	     }else if($data['field']=='active'){
	       if((int)$data['value'] == 0){
	         $activeLanguages = \App\Models\Language::where('active','=',1)->where('id','!=',$data['itemId'])->count();
	         if($activeLanguages == 0){ //nie ma innych języków aktywnych, musi być >0
	           $response->code = 0;
	           $response->msg = trans('admin.language.last_active');
						 $response->active = 1;
					 }else if($frontLanguages == 0){ //ostatni język musi być aktywny i widoczny na froncie
						 $response->msg = trans('admin.language.last_active_and_visible');
						 $params['active'] = $params['visible_on_front'] = $response->active = $response->visible_on_front = 1;
		       }else{
		         $params['visible_on_front'] = $response->visible_on_front = 0;
		       }
	     	}
			}

	    if($response->code == 1){
				 if($frontLanguages==0){
					 $params['main'] = 1;
					 $response->main = 1;
				 }else if($frontLanguages==1){

				 }
	       \App\Models\Language::where('id','=',$data['itemId'])->update($params);
	    }

			return $response;
		}

}
