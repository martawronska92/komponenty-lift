<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModuleCategoryDefault extends Model
{
  protected $table = 'module_category_default';

  protected $fillable = ['category_view','post_view','per_page','loading_type'];
}
