<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DownloadFile extends Model
{
    protected $table = "download_file";

    protected $fillable = ['active','filename_hashed','filename_slug','filesize','position','downloaded','ext',
        'download_id','draft'];

    public function language(){
      return $this->hasMany('App\Models\DownloadFileLanguage');
    }

    public function deleteMulti($ids){
      \DB::beginTransaction();

      $ids = is_array($ids) ? $ids : explode(",",$ids);

      $deleted =  0;
      $filesToDelete = [];

      foreach($ids as $id){
        $file = \App\Models\DownloadFile::where('id','=',$id)->first();
        if($file){
            array_push($filesToDelete,$file->filename_hashed.'.'.$file->ext);
            $deleted+= (int)\App\Models\DownloadFile::where('id','=',$id)->delete();
        }
      }

      $response = new \stdClass();

      if($deleted  == count($ids)){
        foreach($filesToDelete as $file){
          $file = public_path('file/download/').$file;
          if(\File::exists($file))
            \File::delete($file);
        }
        \DB::commit();
        $response->code = 1;
      }else{
        \DB::rollback();
        $response->code = 0;
      }

      $response->msg = trans_choice('admin.download.delete_'.($response->code ? 'success' : 'failure'),count($ids));

      return $response;
    }
}
