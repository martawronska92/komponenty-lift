<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{

  public static function getCategoryBackUrl($param){
    if(is_object($param))
      return route('offer',['categoryParentId' => $param->category_parent_id]);
    else{
      return $param ? route('offer',['categoryParentId' => $param]) : route('offer');
    }
  }

  public static function addPostCallback($postId){
    $module = new \App\Models\Module();

    $galleryPhoto = new \App\Models\GalleryPhoto();

    \App\Models\PagePostModule::attachModule([
      'entity_id' => $postId,
      'entity_type' => 'App\Models\Post',
      'module_id' => $module->getIdBySymbol('galleryphoto'),
      'module_entity_id' => $galleryPhoto->createEmpty(),
      'position' => 0
    ]);

    $download = new \App\Models\Content();

    \App\Models\PagePostModule::attachModule([
      'entity_id' => $postId,
      'entity_type' => 'App\Models\Post',
      'module_id' => $module->getIdBySymbol('download'),
      'module_entity_id' => $download->createEmpty(),
      'position' => 0
    ]);
  }

  public function export(){
    $moduleID = \App\Models\Module::where('symbol','=','offer')->first()->id;
    $categories = \App\Models\Category::where('module_id','=',$moduleID)->pluck('id')->toArray();
    $model = new \App\Models\PostData();
    return $model->export($categories);
  }
}
