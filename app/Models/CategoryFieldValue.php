<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryFieldValue extends Model
{
    protected $table = "category_field_value";

    protected $fillable = ['category_field_id','post_id'];    
}
