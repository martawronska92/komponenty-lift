<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PagePostModule extends Model
{
    protected $table = "page_post_module";

    protected $fillable = ['module_id','module_entity_id','position','entity_id','entity_type'];

    public function module()
    {
       return $this->belongsTo('App\Models\Module','module_id');
    }

    /**
     * parsuje moduły podłączone do strony/wpisu
     * @param  collection $modules kolekcja modułów
     * @return array    z parsowane moduły
     */
    public static function parseModules($modules){
      $response = [];
  		foreach($modules as $module){
  			$temp = new \stdClass();
  			$temp->module_name = "";
  			foreach($module->module->language as $moduleLang){
  				if($moduleLang->language->symbol == \App::getLocale()){
  					$temp->module_name = $moduleLang->name;
  				}
  			}

        $temp->id = $module->id;
  			$temp->symbol = $module->module->symbol;
  			$temp->module_id = $module->id;
  			$temp->entity_name = "";

        $moduleModel = new $module->module->module_entity_model();
        if(method_exists($moduleModel,'getModuleDetails'))
          $temp->entity_details = call_user_func(array($moduleModel, 'getModuleDetails'),$module->module_entity_id);

  			$temp->module_entity_id = $module->module_entity_id;
        $temp->module_entity_model = $module->module->module_entity_model;
        $temp->bind_entity = $module->module->bind_entity;
        $temp->entity_id = $module->entity_id;
        $temp->active = $module->active;
        $temp->category_id = $module->module->category_id;

  			if($module->module->bind_entity){
  				$temp->entity_name = call_user_func(array($module->module->module_entity_model, 'getNameById'),$module->module_entity_id);
  			}

  			$response[$module->id] = $temp;
  		}

  		return $response;
    }

    /**
     * zwraca adres powrotu z modułu podłączonego do strony
     * @param  int $pagePostModuleId id z tabeli page_post_module
     * @return string       adres powrotu
     */
    public static function getRedirectUrl($pagePostModuleId){
      return self::getDetails($pagePostModuleId)['redirect_url'];
    }

    /**
     * zwraca dane o module podłączonym do strony/wpisu
     * @param  int $pagePostModuleId id z tabeli page_post_module
     * @return stdClass  dane modułu
     */
    public static function getDetails($pagePostModuleId){
      $response = [];

      $module = \App\Models\PagePostModule::where('id','=',$pagePostModuleId)->first();
      $response['entity_type'] = \App\Helpers\Parse::parseEntity($module->entity_type);
      $response['entity_id'] = $module->entity_id;
      //'pagePostModuleId'=>$pagePostModuleId
      $routeParams = [
        'id' => $response['entity_id']
      ];

      if($response['entity_type']=="post")
        $routeParams['pagePostModuleId'] = $pagePostModuleId;

      if($response['entity_type'] == "category"){
        $category = \App\Models\Category::where('id','=',$routeParams['id'])->with(['module'])->first();
        if($category->module){
          $response['redirect_url'] = route($category->module->symbol.'_edit',$routeParams);
        }else{
          $routeParams['pagePostModuleId'] = $pagePostModuleId;
          $response['redirect_url'] = route($response['entity_type'].'_edit',$routeParams);
        }
      }else
        $response['redirect_url'] = route($response['entity_type'].'_edit',$routeParams);

      $fromModule = false;
      if($response['entity_type'] == "post"){
          $fromModule = \App\Models\Post::checkFromModule($response['entity_id']);
          unset($routeParams['pagePostModuleId']);
          if($fromModule!=false && \Route::has($fromModule->symbol.'_post_edit'))
              $response['redirect_url'] = route($fromModule->symbol.'_post_edit',$routeParams);
      }

      $response['from_module'] = $fromModule;

      return $response;
    }

    /**
     * usuwa moduły przypisane do stron/wpisów
     * @param  int/array $ids id lub idki z tabeli page_post_module
     */
    public static function removeModule($ids){
      if(!is_array($ids)){
        $ids = [$ids];
      }

      $toDelete = count($ids);
      $deleted = 0;

      foreach($ids as $pagePostModuleId){
        $moduleData = \App\Models\PagePostModule::where('id','=',$pagePostModuleId)->first();
        if($moduleData){
          $model = new $moduleData->module->module_entity_model();
          if(method_exists($model,'deleteObject')){
              $isObjectDelete = $model->deleteObject($moduleData['module_entity_id']);
            }
          }
          $deleted+= (int)\App\Models\PagePostModule::where('id','=',$pagePostModuleId)->delete();
      }

      return $toDelete == $deleted;
   }

   public static function attachModule($params){
     return self::create($params)->id;
   }

   public static function duplicateEntity($pagePostModuleId){
     $row = self::where('id','=',$pagePostModuleId)->first();

     if($row->entity_type == "App\Models\Post"){
       $response = \App\Models\Post::duplicate($row->entity_id);
     }else{
       $response = \App\Models\Page::duplicate($row->entity_id);
     }

     $response->entity_type = $row->entity_type;

     return $response;
   }
}
