<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryListLanguage extends Model
{
  protected $table = "gallery_list_language";

  protected $fillable = ['language_id','title','gallery_list_id','description'];

  public function language(){
    return $this->belongsTo('App\Models\Language');
  }
}
