<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\MenuElement;

class Menu extends Base
{
    protected $table = "menu";

    protected $fillable = ['name','symbol'];

    public function element(){
      return $this->hasMany('App\Models\MenuElement')->orderBy('position','asc');
    }

    public function getList(){
      return self::orderBy('name','DESC')->get();
    }

    public function getAttributesNames(){
      return [
        'name' => trans('admin.name'),
        'symbol' => trans('admin.symbol'),
      ];
    }

    public static function getELements($menuId){
      $elements = \App\Models\MenuElement::with(['language' => function($query){
        $query->whereHas('language',function($query){
            $query->where('symbol','=',\App::getLocale());
        });
      }])
      ->whereHas('menu',function($query) use($menuId){
        $query->where('id','=',$menuId);
      })
      ->orderBy('level','ASC')
      ->orderBy('position','ASC')
      ->get();

      return self::parseElements($elements);

    }

    public static function getFrontElements($menuSymbol){
      $elements = self::where('menu.symbol','=',$menuSymbol)
        ->join('menu_element','menu_element.menu_id','=','menu.id')
        ->join('menu_element_language','menu_element.id','=','menu_element_language.menu_element_id')
        ->join('language','menu_element_language.language_id','=','language.id')
        ->leftJoin('page','menu_element.entity_id','=','page.id')
        ->leftJoin('page_post_language','page_post_language.entity_id','=','page.id')
        ->leftJoin('language as page_language','page_post_language.language_id','=','page_language.id')
        ->where('language.symbol','=',\App::getLocale())
        ->where('menu_element.active','=',1)
        ->where(function($query){
            $query->whereNull('menu_element.entity_id');
            $query->orWhere(function($query){
              $query->whereNotNull('menu_element.entity_id');
              $query->where('page_language.symbol','=',\App::getLocale());
              $query->where('menu_element.entity_type','=','App\\Models\\Page');
              $query->where('page_post_language.entity_type','=','App\\Models\\Page');
              $query->where('page.active','=',1);
            });
        })
        ->orderBy('level','ASC')
        ->orderBy('menu_element.position','ASC')
        ->get(['menu_element.id','menu_element.parent_id','menu_element.level','menu_element.external',
              'menu_element.entity_id','menu_element.mainpage',
              'menu_element.external_url','menu_element_language.title as menu_title','page_post_language.url']);

      $elements = collect($elements);

      return self::parseElements($elements);
    }

    public static function parseElements($elements){
      $response = new \stdClass();
      $response->hierarchy = [];
      $response->data = [];

      foreach($elements as $element){
        $response->data[$element->id] = $element;

        if(!array_key_exists($element->level,$response->hierarchy)){
          $response->hierarchy[$element->level] = [];
        }

        if($element->level == 0)
          $response->hierarchy[$element->level][0][] = $element->id;
        else{
          $parent = isset($element->parent_id) ? $element->parent_id : $element->parent_page_id;
          $response->hierarchy[$element->level][$parent][] = $element->id;
        }

      }

      return $response;
    }

    public static function render($level,$parentId,$elements,$langPrefix,$view = ""){
      if($view == "")
        $view = 'admin.menu.partial.submenu';

      return \View::make($view)->with([
        'level' => $level,
        'elements' => $elements,
        'parentId' => $parentId,
        'langPrefix' => $langPrefix
      ])->render();
    }

    public function storeMenu($data,$id = false){
      $response = new \stdClass();
      $response->code = 0;
      $response->msg = "";

      try{
        $menu = $id ? self::find($id) : new self();
        $menu->fill($data);
        $menu->save();

        $response->code = $menu->id ? 1 : 0;
        $response->msg = trans('admin.menu.'.($id ? 'edit' : 'add').'_'.($response->code ? 'success' : 'failure'));
        $response->id = $menu->id;
      }catch(Exception $e){
        $response->msg = trans('admin.menu.'.($id ? 'edit' : 'add').'_failure');
      }

      return $response;
    }

    /**
     *  renderuje menu na podstawie komponentu menu
     */
    public static function renderOnFront($symbol,$langPrefix){
      $elements = self::getFrontElements($symbol);
      return self::render(0,0,$elements,$langPrefix,"front.modules.menu.main");
    }

    /**
     * renderuje menu na podstawie stron
     */
    public static function renderFromPages($view = ''){
      $model = new \App\Models\Page();
      $elements = self::parseElements($model->getActive(false,['page.position','desc']));
      $template = $view == "" ? "page" : $view;
      return self::render(0,0,$elements,\App::getLocale(),"front.modules.menu.".$template);
    }
}
