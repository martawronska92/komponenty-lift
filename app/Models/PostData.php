<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;

class PostData extends Model
{
  public function export($categories){
    $categories = is_array($categories) ? $categories : [$categories];
    $posts = \App\Models\Post::with(['language','field','category','tag'])->whereIn('category_id',$categories)->get();

    $file = storage_path('offer_export_'.str_random(20).'.csv');
    $fh = fopen($file, 'w+');

    $languages = \App\Models\Language::getActive();
    $fields = ['id'];

    /**
     * pola językowane
     */

    $langFields = ['tytuł','opis','meta tytuł','meta opis'];
    foreach($langFields as $field){
      foreach($languages as $lang){
        $fields[] = $field.' '.$lang->symbol;
      }
    }

    $fields[] = 'id kategorii';
    $fields[] = 'nazwa kategorii';


    /**
     * atrybuty dodatkowe
     */
    if(count($posts) > 0){
      $category = \App\Models\Category::with(['field','language'=> function($query){
        $query->whereHas('language',function($query){
          $query->where('symbol','=','pl');
        });
      }])->where('id','=',$posts[0]->category_id)->first();
      foreach($category->field as $field){
        foreach($field->language as $lang){
          if($lang->symbol == "pl")
            $fields[] = mb_strtolower($lang->pivot->name);
        }
      }
    }

    $fields[] = 'obrazek';
    $fields[] = 'aktywny';

    fputcsv($fh, $fields,';');

    if(count($posts) > 0){

      /**
       * pola językowane
       */
      $breaks = array("\r\n", "\n", "\r");
      $langFields = ['title','content','meta_title','meta_description'];
      foreach($posts as $post){
        $values = [];
        $values[] = $post->id;
        foreach($langFields as $field){
          $find = false;
          foreach($languages as $lang){
            foreach($post->language as $postLang){
              if($postLang->language->symbol == $lang->symbol){
                $values[] = str_ireplace($breaks, " ", $postLang[$field]);
                $find = true;
              }
            }
          }
          if(!$find)
            $values[] = "";
        }

        /**
         * id kategorii
         */
        $values[] = $post->category_id;


        /**
         * nazwa kategorii
         */
        $values[] = $category->language[0]->name;

        /**
         * attrybuty
         */
        foreach($post->field as $field){
          $values[] = $field->value->value;
        }

        /**
         * obrazek
         */
        $values[] = "";

        /**
         * aktywny
         */
        $values[] = $post->active;

        fputcsv($fh, $values,';');
      }
    }

    return $file;
  }

  public function import($data){
    $response = new \stdClass;
    $response->code = 0;

    if(strtolower($data['file']->getClientOriginalExtension())!="csv"){
      $response->msg = trans('admin.seo.wrong_file');
    }else{
      try{
        $fileName = str_random(20).".csv";
        $data['file']->storeAs('',$fileName,'all');
        $path = public_path()."/".$fileName;
        if(\File::exists($path)){
          if (($handle = fopen($path, 'r')) !== false)
          {
              $key = 0;
              $header;
              $postModel = new \App\Models\Post();
              $postLanguageModel = new \App\Models\PagePostLanguage();
              $images = [];

              $productCounter = 0;
              $success = 0;

              \DB::beginTransaction();

              while (($row = fgetcsv($handle, 2000, ";")) !== false)
              {

                  $postInfo = [];
                  $postLanguage = [];

                  if($key==0){
                    $header = $row;
                    $valid = $this->validateImportFields($row);
                  }else if($valid){
                    /**
                     * wstawienie produktu
                     */

                    $productCounter++;
                    $index = 1;
                    $langs = \App\Models\Language::get()->keyBy('symbol')->toArray();
                    $validProduct = true;

                    while(array_key_exists($index,$header) && $header[$index]!="id kategorii" && $validProduct){
                      $langSymbol = strtolower(substr($header[$index],-2));

                      if(array_key_exists($langSymbol,$langs))
                        $langId = $langs[$langSymbol]['id'];
                      else{
                        $validProduct = false;
                        $langId = false;
                      }

                      if($langId){
                        $field = trim(substr($header[$index],0,mb_strlen($header[$index])-2));
                        switch($field){
                          case 'tytuł':
                            $postData['title'][$langId] = $row[$index];
                            $postData['url'][$langId] = $row[$index];

                            // $postLanguage['title'] = $row[$index];
                            // $postLanguage['url'] = $row[$index];

                            break;
                          case 'opis':
                            $postData['content'][$langId] = $row[$index];
                            // $postLanguage['content'] = $row[$index];
                            break;
                          case 'meta tytuł':
                            $postData['meta_title'][$langId] = $row[$index];
                            // $postLanguage['meta_title'] = $row[$index];
                            break;
                          case 'meta opis':
                            $postData['meta_description'][$langId] = $row[$index];
                            //$postLanguage['meta_description'] = $row[$index];
                            break;
                        }
                        $postLanguage['language_id'] = $langId;
                      }
                      $index++;
                    }

                    $postData['category_id'] = $row[$index];

                    $index++; //nazwa kategorii
                    $index++; //fields

                    /**
                     * FIELDS
                     */
                     $fields = \App\Models\CategoryField::where('category_id','=',$postData['category_id'])->count();
                     if($fields > 0){
                       for($i=0; $i<$fields; $i++){
                         $fieldId = \DB::table('category_field')
                            ->join('category_field_language','category_field.id','=','category_field_language.category_field_id')
                            ->where('category_field.category_id','=',$postData['category_id'])
                            ->where(\DB::raw('lower(category_field_language.name)'),'=',mb_strtolower($header[$index]))
                            ->pluck('category_field.id')
                            ->first();

                         if($fieldId){
                          $postData['category_field_id'][$fieldId] = $row[$index];
                          // $postFields = [
                          //   'category_id'
                          // ];
                         }

                         $index++;
                       }
                     }

                    /**
                     * OBRAZEK
                     */
                    if($header[$index] == "obrazek"){
                      $image = $row[$index];
                      $index++;
                    }

                    if($header[$index] == "aktywny")
                      $postData['active'] = (int)$row[$index];

                    // if($row[0]!=""){
                    //   $postId = (int)$row[0];
                    //   \DB::table('post')->where('id','=',$postId)->update($postData);
                    // }else{
                    //   $postData['position'] = 1;
                    //   $postData['public'] = 1;
                    //   $postId = \DB::table('post')->insertGetId($postData);
                    // }
                    //
                    // \DB::table('page_post_language')->where('entity_type','=','App\\Models\\Post')->where('entity_id','=',$postId)->delete();
                    // $postLanguage['entity_id'] = $postId;
                    // $postLanguage['entity_type'] = "App\Models\Post";
                    // \DB::table('page_post_language')->insert($postLanguage);

                    $result = $postModel->savePost($postData,$row[0]!="" ? (int)$row[0] : false,true);
                    if($result->code == 1){
                      $success++;
                      $images[$result->id] = $image;
                    }
                    // if($postId){
                    //   $success++;
                    //   $images[$postId] = $image;
                    // }
                  }
                  $key++;
              }
              fclose($handle);
          }

          if(!$valid){
            $response->msg = "Plik jest nieprawidłowy";
          }else if($key==0 || $key==1){
            $response->msg = trans('admin.offer.import_no_products');
          }else{
            if($productCounter == $success){
              \DB::commit();
              $response->code = 1;
              $response->msg = trans('admin.offer.import_success');

              $postImageWidth = \App\Models\Settings::getBySymbol('post_thumb_width');
              $postImageHeight = \App\Models\Settings::getBySymbol('post_thumb_height');

              @unlink($path);

              foreach($images as $postId=>$image){
                if($image!=""){
                  $info = get_headers($image,1);

                  if(is_array($info) && count($info)>0){
                    $oldExt = \App\Models\Post::where('id','=',$postId)->pluck('thumbnail')->first();
                    $ext = \App\Helpers\File::mimeToExt($info['Content-Type']);
                    $path = public_path('img/post/'.$postId);

                    if(!is_dir($path))
                      mkdir($path,0777);

                    $fileOriginalName = $path.'/original.';
                    if(file_exists($fileOriginalName.$oldExt))
                      @unlink($fileOriginalName.$oldExt);
                    Image::make($image)->save($fileOriginalName.$ext);

                    $fileThumbName = $path.'/thumbnail.'.$ext;
                    if(file_exists($fileThumbName.$oldExt))
                      @unlink($fileThumbName.$oldExt);
                    Image::make($fileOriginalName.$ext)->fit($postImageWidth,$postImageHeight,function ($constraint) {
                      $constraint->upsize();
                    })->save($fileThumbName);

                    \App\Models\Post::where('id','=',$postId)->update([
                      'thumbnail' => $ext."?v=".mt_rand(10000000, 99999999)
                    ]);
                  }
                }
              }
            }else{
              \DB::rollback();
              $response->code = 0;
              $response->msg = trans('admin.offer.import_failure');
            }
          }

          @unlink($path);
        }
      }catch(Exception $e){
        $response->msg = trans('admin.offer.import_failure');
      }
    }

    return $response;
  }

  public function validateImportFields($row){
    $valid = true;



    return $valid;
  }
}
