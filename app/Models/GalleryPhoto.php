<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;

ini_set('memory_limit', '512M');

class GalleryPhoto extends Model
{
  protected $table = 'gallery_photo';

  protected $fillable = ['url','title','ext','position','gallery_id','active','thumbnail_options','draft'];

  public function gallery(){
    return $this->belongsTo('App\Models\Gallery')->with(['show_type']);
  }

  public function getList($data = []){
    $query = new \App\Components\Clist\ListQuery();
    //$query->query = \App\Models\Gallery::with(['photo'])->where('type','=','photo');
    $query->query = \DB::table('gallery')
      ->where('type','=','photo');

    $query->request = $data;
    $query->searchable = ['name'];
    $query->orderable = ['id','name','photo_counter'];
    $query->fields = ['gallery.*',\DB::raw('(select count(id) from gallery_photo where gallery_photo.gallery_id = gallery.id) as photo_counter')];
    return $query->format();
  }

  public function getPosition($galleryId){
    return self::where('gallery_id','=',$galleryId)->max('position') + 1;
  }

  public function language(){
    return $this->hasMany('App\Models\GalleryPhotoLanguage');
  }

  public function addPhoto($data){
    $response = new \stdClass();
    $response->code = 0;

    $maxSize = \App\Helpers\File::getMaxSize();

    /**
     * sprawdzanie czy z multiple czy z kadrownika
     */
    $thumbnailFieldName = array_key_exists('thumbnail',$data) ? 'thumbnail' : 'multiple';
    $originalFieldName = array_key_exists('original',$data) ? 'original' : 'multiple';

    // var_dump(ini_get('memory_limit'));
    // exit();

    $oldPhotoFileNames = [];
    $oldPhotoFileExtension = "";

    if(!array_key_exists('photo_id',$data) && ($data[$originalFieldName]->getSize() > $maxSize || $data[$thumbnailFieldName]->getSize() > $maxSize)){
      $response->code = 0;
      $response->msg = trans('admin.galleryphoto.upload_size_failure');
    }else{
      \DB::beginTransaction();

      $data['ext'] = 'png';

      if(array_key_exists('photo_id',$data)){
        $photo = self::find($data['photo_id']);

        $oldPhotoFileExtension = $photo->ext;

        $photo->fill($data);
        $photo->save();
        $data['main'] = $photo->main;
        $data['draft'] = $photo->draft;

        $oldPhotoFileNames = $photo->language;

      }else{
        $data['position'] = $this->getPosition($data['galleryId']);
        $data['draft'] = 0;
        $data['main'] = \App\Models\GalleryPhoto::where('gallery_id','=',$data['galleryId'])->count()==0 ? 1 : 0;
        $photo = \App\Models\Gallery::find($data['galleryId'])->photo()->create($data);
      }

      $photo->touch();
      $data['dirname'] = $photo->id;
      self::where('id','=',$photo->id)->update(['dirname'=>$photo->id]);
      $response->code = $photo->id ? 1 : 0;
    }

    if($response->code){
      $dir = 'img/gallery_photo/'.$photo->id;
      $path = public_path().'/'.$dir;

      if(!array_key_exists('photo_id',$data) && !is_dir($path))
        mkdir($path);

      $settings = \App\Models\Settings::getByGroup('images');

      if(array_key_exists($thumbnailFieldName,$data)){
        $data[$thumbnailFieldName]->storeAs($dir, 'thumbnail.'.$data['ext'],'all');
      }
      if(array_key_exists($originalFieldName,$data))
        $data[$originalFieldName]->storeAs($dir, 'original.'.$data['ext'],'all');

      $normalPhoto = public_path().'/'.$dir.'/normal.'.$data['ext'];
      $originalPhoto = public_path().'/'.$dir.'/original.'.$data['ext'];
      $thumbnailPhoto = public_path().'/'.$dir.'/thumbnail.'.$data['ext'];

      // Image::make($originalPhoto)->fit((int)$settings['galleryphoto_normal_width'],(int)$settings['galleryphoto_normal_height'], function ($constraint) {
      //       $constraint->upsize();
      // })->save($normalPhoto);

      $img = Image::make($originalPhoto);
      $img->fit((int)$settings['galleryphoto_normal_width'],(int)$settings['galleryphoto_normal_height'],function ($constraint) {
          $constraint->upsize();
      });
      $img->save($normalPhoto);
      $img->destroy();
      unset($img);

      //if($thumbnailFieldName == 'multiple'){ // dla zdjęcia z kadrownika
        $img = Image::make($thumbnailPhoto);
        $img->fit((int)$settings['galleryphoto_thumb_width'],(int)$settings['galleryphoto_thumb_height'],function ($constraint) {
            $constraint->upsize();
        });
        $img->save($thumbnailPhoto);
        $img->destroy();
        unset($img);
      //}


      $data['id'] = $photo->id;
      $data['active'] = 1;

      $currentLangId = \App\Models\Language::where('symbol','=',\App::getLocale())->pluck('id')->first();

      $title = "";
      if(array_key_exists('language',$data)){
        $photo->language()->delete();

        parse_str($data['language'],$langParams);
        foreach($langParams['title'] as $langId=>$value){
          \App\Models\GalleryPhoto::find($photo->id)->language()->create([
              'language_id' => $langId,
              'title'=> $value,
              'filename'=> $value!='' ? str_slug($value) : ''
          ]);

          /**
           * kopiowanie plików dla nazw
           */

           if(count($oldPhotoFileNames)>0){
             foreach($oldPhotoFileNames as $row){
               $slug = str_slug($row->title);
               $path = public_path().'/'.$dir.'/'.$slug;
               if(\File::exists($path.'_original.'.$oldPhotoFileExtension))
                @unlink($path.'_original.'.$oldPhotoFileExtension);
               if(\File::exists($path.'_normal.'.$oldPhotoFileExtension))
                @unlink($path.'_normal.'.$oldPhotoFileExtension);
               if(\File::exists($path.'_thumbnail.'.$oldPhotoFileExtension))
                @unlink($path.'_thumbnail.'.$oldPhotoFileExtension);
             }
           }

          if($value != ""){
            $slug = str_slug($value);
            $normalPhotoLang = public_path().'/'.$dir.'/'.$slug.'_normal.'.$data['ext'];
            $originalPhotoLang = public_path().'/'.$dir.'/'.$slug.'_original.'.$data['ext'];
            $thumbnailPhotoLang = public_path().'/'.$dir.'/'.$slug.'_thumbnail.'.$data['ext'];

            if(\File::exists($normalPhoto))
              \File::copy($normalPhoto,$normalPhotoLang);
            if(\File::exists($originalPhoto))
              \File::copy($originalPhoto,$originalPhotoLang);
            if(\File::exists($thumbnailPhoto))
              \File::copy($thumbnailPhoto,$thumbnailPhotoLang);
          }

          if($langId == $currentLangId)
            $title = $value;
        }
      }

      $data['updated_at'] = date("Y-m-d H:i:s");
      $settings = \App\Models\Settings::getAll();
      $response->view = (String) \View::make('admin.gallery_photo.partial.photobox',[
          'photo'=>$data,
          'title' => $title,
          'settings' => $settings
        ])->render();
      $response->form_view = (String) \View::make('admin.gallery_photo.partial.photo_form',[
        'langs' => \App\Models\Language::getActive(),
        'settings' => $settings
      ])->render();

        \DB::commit();
    }else{
      \DB::rollback();
    }

    return $response;
  }

  public function removePhoto($ids){
    $response = new \stdClass();
    $response->code = 0;
    $response->msg = "";

    $deleted = [];
    \DB::beginTransaction();

    foreach($ids as $id){
      $photo = \App\Models\GalleryPhoto::where('id','=',$id);
      $toDeleted = $photo->pluck('dirname')->first();
      if($photo->count()>0 && (int)$photo->delete())
        $deleted[] = $toDeleted;
    }

    $response->code = (int)(count($deleted) == count($ids));

    if($response->code)
      \DB::commit();
    else {
      \DB::rollback();
    }

    $response->msg = trans_choice('admin.galleryphoto.removephoto_'.($response->code ? 'success' : 'failure'),count($ids));
    if($response->code){
        foreach($deleted as $dirname){
          $dir = public_path("img/gallery_photo/".$dirname);
          if(is_dir($dir)){
            \File::deleteDirectory($dir);
          }
        }
    }

    return $response;
  }

  public function getPhotos($galleryId){
    return self::whereHas('gallery',function($query) use($galleryId){
      $query->where('id','=',$galleryId);
    })->orderBy('position','DESC')->get();
  }

  public static function renderOnFront($module, $customView = ""){
    $photos = self::with(['language'=>function($query){
        $query->whereHas('language',function($query){
          $query->where('symbol','=',\App::getLocale());
        });
      },'gallery'])
      ->where('active','=',1)
      ->where('gallery_id','=',$module->module_entity_id);

    if(\Route::currentRouteName()!="page_preview" && \Route::currentRouteName()!="post_preview")
      $photos = $photos->where('draft','=',0);

    $photos = $photos->orderBy('position','desc')->get();

    $info = \App\Models\Gallery::where('id','=',$module->module_entity_id)->with(['language' => function($query){
      $query->whereHas('language',function($query){
        $query->where('symbol','=',\App::getLocale());
      });
    }])->first();


    if(count($photos)>0){
      $view = 'front.modules.gallery.photo_'.$photos[0]->gallery->show_type->symbol;
      if($customView!=""){
        $view = $customView;
      }
      return view($view)->with([
        'photos' => $photos,
        'info' => $info
      ])->render();
    }else {
      return "";
    }
  }

  public function createEmpty(){
    return \App\Models\Gallery::create([
      'show_type_id' => \App\Models\GalleryShowType::where('gallery_type','=','photo')->pluck('id')->first(),
      'type' => 'photo'
    ])->id;
  }

  public function deleteObject($id, $preview = false){
      $ids = explode(",",$id);

      if(count($ids) == 1 && \App\Models\Gallery::where('id','=',$ids[0])->count() == 0){
        return true;
      }else{

        $directoriesToDelete = [];
        foreach($ids as $id){
          $directoriesToDelete[] = self::where('gallery_id','=',$id)->get()->pluck('dirname');
        }

        $commonModel = new \App\Models\Base();
        $response = $commonModel->deleteMulti($id,'gallery');

        if($response->code && !$preview){
            $path = public_path().'/img/gallery_photo';
            foreach($directoriesToDelete as $dirs){
              foreach($dirs as $dir){
                \File::deleteDirectory($path.'/'.$dir);
              }
            }
        }

        return $response->code;
      }
  }

  public function getModuleDetails($galleryId){
    $response = "";

    $info = \App\Models\Gallery::where('id','=',$galleryId)->with(['language' => function($query){
      $query->whereHas('language',function($query){
        $query->where('symbol','=',\App::getLocale());
      });
    }])->first();


    if($info && count($info->language)>0 && $info->language[0]->title!=""){
      $response = \App\Helpers\Format::moduleDetailsTitle(\App\Helpers\Format::cropString($info->language[0]->title,150)).", ".lcfirst(trans('admin.galleryphoto.photo_counter'));
    }else
      $response = trans('admin.galleryphoto.photo_counter');

    return $response.": ".self::where('gallery_id','=',$galleryId)->count();
  }

  public function storeGallery($data,$id = false, $pagePostModuleId = false){
    if($id)
      $galleryModel = \App\Models\Gallery::find($id);
    else
      $galleryModel = new \App\Models\Gallery();

    $data['type'] = 'photo';
    $data['active'] = !array_key_exists('active',$data) ? 0 : 1;
    $galleryModel->fill($data);
    $galleryModel->save();

    $galleryModel->language()->delete();
    if(array_key_exists('title',$data)){
      foreach($data['title'] as $langId=>$value){
        $galleryModel->language()->create([
          'language_id' => $langId,
          'title' => $value,
          'description' => $data['description'][$langId]
        ]);
      }
    }

    $response = new \stdClass();
    $response->code = $galleryModel->id ? 1 : 0;
    $response->msg = ($id ? 'admin.update' : 'admin.gallery.add').'_';

    \App\Models\GalleryPhoto::where('gallery_id','=',$galleryModel->id)->update([
      'draft' => 0
    ]);


    if($pagePostModuleId){
      $moduleDetails = \App\Models\PagePostModule::getDetails($pagePostModuleId);
      $response->redirect_url = $moduleDetails['redirect_url'];
    }

    return $response;
  }

  public function deletePreviewObject($id){
    \App\Models\GalleryPhoto::where('gallery_id','=',$id)->delete();
    \App\Models\Gallery::where('id','=',$id)->delete();
  }

  public function getPhotosFromPage($pageId,$among){
    if(!is_array($pageId))
      $pageId = [$pageId];
    return \DB::table('page_post_module')
      ->join('module','page_post_module.module_id','=','module_id')
      ->join('gallery_photo','gallery_photo.gallery_id','=','page_post_module.module_entity_id')
      ->whereIn('entity_id',$pageId)
      ->where('entity_type','=','App\\Models\\Page')
      ->where('module.symbol','=','galleryphoto')
      ->limit($among)
      ->inRandomOrder()
      ->get(['gallery_photo.*']);

  }
}
