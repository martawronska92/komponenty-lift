<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class User extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $params;
    public $settings;
    public $portalUrl;
    public $loginUrl;

    public function __construct($params)
    {
        $this->params = $params;
        $this->settings = \App\Models\Settings::getByGroup('contact');
        $this->portalUrl = \Config::get('app.url');
        $this->loginUrl = route('login');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $result = $this->view('emails.user_password')
          ->subject(trans('admin.user.user_password_subject'));
        if($this->settings['contact_email']!="" && $this->settings['contact_name']!=""){
          $result = $result->from([
            'address' => $this->settings['contact_email'],
            'name' => $this->settings['contact_name']
          ]);
        }
        return $result;
    }
}
