<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    public $params;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.contact')
              //->text('admin.newsletter.template.'.$this->newsletter['template']['symbol'].'_plain')
              //->with($params)
              //->to($this->recipient['email'],$this->recipient['name']." ".$this->recipient['surname'])
              ->subject(trans('front.contact.subject'))
              ->from($this->params['email'],$this->params['name']);
    }
}
