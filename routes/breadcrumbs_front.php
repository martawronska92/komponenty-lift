<?php

//---------------------- STRONA GŁÓwna -----------------------------------------

Breadcrumbs::register('front_mainpage', function($breadcrumbs)
{
    $breadcrumbs->push(trans('front.homepage'), \App\Helpers\Language::hasPrefix() ? "/".\App\Helpers\Language::getPrefix() : "/");
});

//----------------------- STRONA -----------------------------------------------

Breadcrumbs::register('front_page', function($breadcrumbs,$params)
{
    $id = $params['id'];
    $ids = [$id];
    do{
      $parentPageId = \App\Models\Page::where('id','=',$id)->first()->parent_page_id;
      if($parentPageId!=NULL){
        array_unshift($ids,$parentPageId);
        $id = $parentPageId;
      }
    }while($parentPageId!=NULL);


    $parts = \App\Models\PagePostLanguage::whereHas('language',function($query){
          $query->where('symbol','=',\App::getLocale());
        })
       ->whereIn('entity_id',$ids)
       ->where('entity_type','=','App\Models\Page')
       //->orderByRaw("field(url,{$placeholders})", $parsed->requestParts)
       ->get();


    $breadcrumbs->parent('front_mainpage');

    /**
    * dodawanie stron
    */

    $responseUrl = \URL::to("/");
    foreach($parts as $part){
      $responseUrl.= "/".$part->url;
      $breadcrumbs->push($part->title,$responseUrl);
    }
});

//---------------------- WPIS --------------------------------------------------

Breadcrumbs::register('front_post', function($breadcrumbs,$params)
{
    $parsed = prepareRequestForBreadcrumbs();
    $breadcrumbs->parent('front_mainpage');

    /**
     * dodanie strony
     */
    $parsed->responseUrl.= "/".$params['page']->language[0]->url;
    $breadcrumbs->push($params['page']->language[0]->title,$parsed->responseUrl);

    /**
     * usunięcie początku odpowiedzialnego za stronę bo już dodany
     */
    array_shift($parsed->requestParts);

    /**
     * usunięcie końca odpowiedzialnego za wpis
     */
    array_pop($parsed->requestParts);

    if(count($parsed->requestParts) > 0){
      /**
       * dodanie kategorii
       */

      $placeholders = implode(',',array_fill(0, count($parsed->requestParts), '?'));

      $parsed->responseParts = \App\Models\CategoryLanguage::whereIn('url',$parsed->requestParts)
            ->whereHas('language',function($query){
              $query->where('symbol','=',\App::getLocale());
            })
	    ->orderByRaw("field(url,{$placeholders})", $parsed->requestParts)
	    ->get();

      foreach($parsed->responseParts as $part){
        $parsed->responseUrl.= "/".$part->url;
        $breadcrumbs->push($part->name,$parsed->responseUrl);
      }
    }

    $breadcrumbs->push($params['post']->language[0]->title,$parsed->responseUrl."/".$params['post']->language[0]->url);
});

//---------------------- KATEGORIA ---------------------------------------------

Breadcrumbs::register('front_category', function($breadcrumbs,$params)
{
    $parsed = prepareRequestForBreadcrumbs();
    $breadcrumbs->parent('front_mainpage');

    /**
     * dodanie strony
     */
    $parsed->responseUrl.= "/".$params['page']->language[0]->url;
    $breadcrumbs->push($params['page']->language[0]->title,$parsed->responseUrl);

    /**
     * usunięcie początku odpowiedzialnego za stronę bo już dodany
     */
    array_shift($parsed->requestParts);

    /**
     * dodanie kategorii
     */

    $placeholders = implode(',',array_fill(0, count($parsed->requestParts), '?'));

    $parsed->responseParts = \App\Models\CategoryLanguage::whereIn('url',$parsed->requestParts)
          ->whereHas('language',function($query){
            $query->where('symbol','=',\App::getLocale());
          })
	  ->orderByRaw("field(url,{$placeholders})", $parsed->requestParts)
	  ->get();

    foreach($parsed->responseParts as $part){
      $parsed->responseUrl.= "/".$part->url;
      $breadcrumbs->push($part->name,$parsed->responseUrl);
    }
});

//----------------------- TAGI -------------------------------------------------

Breadcrumbs::register('front_tag', function($breadcrumbs,$params)
{
    $breadcrumbs->parent('front_mainpage');
    $routeParams = ['tagName'=>$params['friendly_name']];
    if(\App\Helpers\Language::hasPrefix())
      $routeParams['frontlanguage'] = \App\Helpers\Language::getPrefix();

    $breadcrumbs->push(trans('front.tag.breadcrumbs'),route('front_tag',$routeParams,false));
    $breadcrumbs->push($params['name'],"/");
});

// -----------------------------------------------------------------------------

function prepareRequestForBreadcrumbs(){
    $response = new \stdClass();
    $response->requestParts = explode('/',trim(\Request::path(),'/'));
    $response->responseUrl = "";

    if(\App\Helpers\Language::hasPrefix()){
      unset($response->requestParts[0]);
      $response->responseUrl = "/".\App\Helpers\Language::getPrefix();
    }

    return $response;
}
