<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('/version_get',function(Request $request){
    $version = \App\Models\Version::first();
    return $version ? $version->version_number : '1.0';
})->middleware('auth:api');

Route::get('/version_update_get',function(Request $request){
    $version = \App\Models\VersionUpdate::first();
    return $version ? $version->version_number : 0;
})->middleware('auth:api');

Route::post('/version_update_set', function (Request $request) {
    $data = $request->input();
    $model = new \App\Models\VersionUpdate();
    return response()->json($model->setVersion($data));
})->middleware('auth:api');

Route::get('/version_migrate', function(Request $request){
  $model = new \App\Models\VersionUpdate();
  $model->migrate();
})->middleware('auth:api');

Route::post('/version_seed', function(Request $request){
  $model = new \App\Models\VersionUpdate();
  $model->seed($request->input('seedClass'));
})->middleware('auth:api');

Route::get('/version_cache', function(Request $request){
  $model = new \App\Models\VersionUpdate();
  $model->clearCache();
})->middleware('auth:api');
