<?php

use Illuminate\Http\Request;

Route::group(['middleware' => ['lang','nonwww']], function($router){

  if(\Schema::hasTable('language')){
    $langPrefix = \App\Helpers\Language::checkPrefix();
    if($langPrefix && strlen(\Request::segment(1))==2){
      \App::setLocale(\Request::segment(1));
    }
  }else{
    $langPrefix = "";
  }

  /**
   * strona główna
   */
  Route::get('/', 'Front\HomeController@index')->name('homepage');
  Route::get('/{frontlanguage}', 'Front\HomeController@index')->name('homepage_language');

  /**
   * sitemapa
   */

  Route::get('/{frontlanguage}/sitemap.xml',function($frontlanguage){
    $path = public_path('sitemap_'.$frontlanguage.'.xml');
    if(\File::exists($path)){
      return response()->file($path);
    }else{
      return response()->file(public_path('sitemap.xml'));
    }
  });

  /**
   * panel do logowania
   */
  Route::get('panel', 'Auth\LoginController@showLoginForm')->name('login');
  Route::post('panel', 'Auth\LoginController@login');
  Route::post('logout', 'Auth\LoginController@logout');

  /**
   * tagi
   */
  $tagRoute = ($langPrefix ? "/{frontlanguage}/" : "/")."tag/{tagName}/{page?}";
  Route::get($tagRoute,'Front\PostController@getFromTag')->name('front_tag');

  /**
   * kontakt
   */
  Route::post(trans('front.contact.route'),'Front\ContactController@send')->name('contact');

  /**
   * moduły niepodłączane do stron z routingiem
   */

   if(\Schema::hasTable("module")){
    $modules = \App\Models\Module::where('has_routing','=',1)->get();

    Route::group(['prefix' => 'module'], function () use ($modules){
      foreach($modules as $module){
        \App\Components\Route::parseRoutes($module,'module');
      }

      /**
      * posty z kategorii
      */
      Route::post("postfromcategory",'Front\ModuleController@postfromcategory');

    });
      foreach($modules as $module){
        \App\Components\Route::parseRoutes($module,'module');
      }
    }

    /**
     * moduły podłączane do stron
     */

    // if(\Schema::hasTable('page')){
    //   $pages = \App\Models\Page::with(['module','language'])
    //       ->whereHas('language.language',function($query){
    //         $query->where('symbol','=',\App::getLocale());
    //       })
    //       ->get();
    //
    //   foreach($pages as $page){
    //     foreach($page->module as $module){
    //       if($module->module->has_routing){
    //         $routeUrl = ($langPrefix ? '{frontlanguage}/' : '').$page->url;
    //         Route::group(['prefix' => $routeUrl], function () use ($module){
    //           \App\Components\Route::parseRoutes($module->module,'page');
    //         });
    //       }
    //     }
    //   }
    // }

    $routeUrl = '/'.($langPrefix ? '{frontlanguage}/' : '').'{param1}/{param2?}/{param3?}/{param4?}';

    Route::get($routeUrl,'Front\RouterController')
       ->where([
         'param1' => '[a-zA-Z0-9\-\_\?\=\&]+',
         'param2' => '[a-zA-Z0-9\-\_\?\=\&]+',
         'param3' => '[a-zA-Z0-9\-\_\?\=\&]+',
         'param4' => '[a-zA-Z0-9\-\_\?\=\&]+',
         'param5' => '[a-zA-Z0-9\-\_\?\=\&]+'
       ])
       ->name('front_page');
});
