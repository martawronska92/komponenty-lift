<?php

  Route::get('panel/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password_form');
  Route::post('panel/reset', 'Auth\ResetPasswordController@reset')->name('password_reset');
  Route::post('panel/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password_email');
  Route::get('panel/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password_token');

  Route::post('panel/content/uploadphoto','Admin\ContentController@uploadphoto');

  Route::group(['middleware' => ['auth','web','lang','session_timeout','developer'],'prefix' => 'panel'], function () {
    Route::get('menu','Admin\MenuController@index')->name('menu');
    Route::get('menu/create','Admin\MenuController@create')->name('menu_create');
    Route::get('menu/edit/{id}','Admin\MenuController@edit')->name('menu_edit');
    Route::post('menu/create','Admin\MenuController@store');
    Route::post('menu/edit/{id}','Admin\MenuController@store');
    Route::get('menu/delete/{id}','Admin\MenuController@delete')->name('menu_delete');

    Route::get('menuelement/get/{id}','Admin\MenuElementController@get')->name('menuelement');
    Route::post('menuelement/store','Admin\MenuElementController@store');
    Route::post('menuelement/setpositions','Admin\MenuElementController@setpositions');
    Route::get('menuelement/delete/{id}','Admin\MenuElementController@delete')->name('menuelement_delete');

    Route::get('/admininfo','Admin\DashboardController@admininfo');
  });

  Route::group(['middleware' => ['auth','web','lang','session_timeout'],'prefix' => 'panel'], function () {

    Route::get('/',function(){
      if(\Auth::user()){
        return redirect()->route('dashboard');
      }
    });

    Route::get('dashboard','Admin\DashboardController@index')->name('dashboard');
    Route::get('changelanguage/{symbol}','Admin\DashboardController@changeLanguage')->name('language_change')->where('symbol','[a-zA-Z]{2}');

    Route::post('list/checkfield','Admin\ListController@checkfield');

    Route::get('page/{parentPageId?}','Admin\PageController@index')->name('page');
    Route::get('page/create/{parentPageId?}','Admin\PageController@create')->name('page_create');
    Route::post('page/create/{parentPageId?}','Admin\PageController@store')->name('page_store');
    Route::get('page/edit/{id}','Admin\PageController@edit')->name('page_edit');
    Route::post('page/edit/{id}','Admin\PageController@store');
    Route::get('page/delete/{id?}','Admin\PageController@delete')->name('page_delete');
    Route::post('page/generatepreview','Admin\PageController@generatepreview');
    Route::get('page/showpreview/{hash}/{frontlanguage?}','Admin\PageController@showpreview')->name('page_preview')->middleware('page_post_preview');
    Route::get('page/waitforpreview','Admin\PageController@waitforpreview')->name('page_wait_for_preview');
    Route::post('page/setposition','Admin\PageController@setposition');

    //Route::get('category','Admin\CategoryController@index')->name('category');
    // Route::get('category/create/{pagePostModuleId}','Admin\CategoryController@create')->name('category_create');
    // Route::post('category/create/{pagePostModuleId}','Admin\CategoryController@store');
    Route::get('category/edit/{pagePostModuleId}/{id}','Admin\CategoryController@edit')->name('category_edit');
    Route::post('category/edit/{pagePostModuleId}/{id}','Admin\CategoryController@store');
    Route::get('category/delete/{pagePostModuleId}/{id}','Admin\CategoryController@delete')->name('category_delete');
    Route::post('category/setposition','Admin\CategoryController@setposition');
    Route::post('category/generatelink','Admin\CategoryController@generatelink');

    Route::get('post/category/{pagePostModuleId}/{category}','Admin\PostController@index')->name('post')->where('category', '[0-9]+');
    Route::get('post/create/{pagePostModuleId}/{category}','Admin\PostController@create')->name('post_create')->where('category', '[0-9]+');
    Route::post('post/create/{pagePostModuleId}/{category}','Admin\PostController@store')->where('category', '[0-9]+')->name('post_store');
    Route::get('post/edit/{pagePostModuleId}/{id}','Admin\PostController@edit')->name('post_edit');
    Route::post('post/edit/{pagePostModuleId}/{id}','Admin\PostController@store');
    Route::get('post/delete/{pagePostModuleId}/{id?}','Admin\PostController@delete')->name('post_delete')->where('id', '[0-9\,]+');
    Route::post('post/setposition','Admin\PostController@setposition');
    Route::post('post/generatepreview','Admin\PostController@generatepreview');
    Route::get('post/showpreview/{hash}/{frontlanguage?}','Admin\PostController@showpreview')->name('post_preview')->middleware('page_post_preview');
    Route::get('post/waitforpreview','Admin\PostController@waitforpreview')->name('post_wait_for_preview');

    Route::post('pagepost/generatelink','Admin\PagePostController@generatelink');
    Route::post('pagepost/setposition','Admin\PagePostController@setposition');
    Route::post('pagepost/createmodule','Admin\PagePostController@createmodule');
    Route::post('pagepost/removemodule','Admin\PagePostController@removemodule');

    Route::get('galleryvideo/edit/{pagePostModuleId}/{id}','Admin\GalleryVideoController@edit')->name('galleryvideo_edit');
    Route::post('galleryvideo/edit/{pagePostModuleId}/{id}','Admin\GalleryVideoController@store');
    Route::get('galleryvideo/delete/{pagePostModuleId}/{id?}','Admin\GalleryVideoController@delete')->name('galleryvideo_delete')->where('id', '[0-9\,]+');
    Route::post('galleryvideo/setposition','Admin\GalleryVideoController@setposition');
    Route::post('galleryvideo/addvideo','Admin\GalleryVideoController@addvideo');
    Route::get('galleryvideo/removevideo/{id}','Admin\GalleryVideoController@removevideo')->where('id', '[0-9\,]+');
    Route::get('galleryvideo/getdata/{id?}','Admin\GalleryVideoController@getdata');
    Route::post('galleryvideo/generatepreview','Admin\GalleryVideoController@generatepreview');

    Route::get('galleryphoto','Admin\GalleryPhotoController@index')->name('galleryphoto');
    Route::get('galleryphoto/edit/{pagePostModuleId}/{id}','Admin\GalleryPhotoController@edit')->name('galleryphoto_edit');
    Route::post('galleryphoto/edit/{pagePostModuleId}/{id}','Admin\GalleryPhotoController@store');
    Route::get('galleryphoto/delete/{pagePostModuleId}/{id?}','Admin\GalleryPhotoController@delete')->name('galleryphoto_delete')->where('id', '[0-9\,]+');
    Route::post('galleryphoto/setposition','Admin\GalleryPhotoController@setposition');
    Route::post('galleryphoto/addphoto','Admin\GalleryPhotoController@addphoto');
    Route::get('galleryphoto/removephoto/{id}','Admin\GalleryPhotoController@removephoto')->where('id', '[0-9\,]+');
    Route::get('galleryphoto/getdata/{id?}','Admin\GalleryPhotoController@getdata');
    Route::post('galleryphoto/generatepreview','Admin\GalleryPhotoController@generatepreview');
    Route::post('galleryphoto/setmain','Admin\GalleryPhotoController@setmain');

    Route::get('galleryphotolist/edit/{pagePostModuleId}/{id}','Admin\GalleryListController@edit')->name('galleryphotolist_edit');
    Route::post('galleryphotolist/edit/{pagePostModuleId}/{id}','Admin\GalleryListController@store');

    Route::get('download/edit/{pagePostModuleId}/{id}','Admin\DownloadController@edit')->name('download_edit');
    Route::post('download/edit/{pagePostModuleId}/{id}','Admin\DownloadController@saveDownload');
    Route::get('download/delete/{pageId}/{id?}','Admin\DownloadController@delete')->name('download_delete')->where('id', '[0-9\,]+');
    Route::get('download/deletefiles/{id?}','Admin\DownloadController@deleteFiles')->name('download_delete_files')->where('id', '[0-9\,]+');
    Route::post('download/setposition','Admin\DownloadController@setposition');
    Route::get('download/getdata/{id?}','Admin\DownloadController@getdata');
    Route::get('download/getfile/{id}','Admin\DownloadController@getfile')->name('download_get');
    Route::any('download/generatepreview','Admin\DownloadController@generatepreview');

    Route::get('content/edit/{pagePostModuleId}/{id}','Admin\ContentController@edit')->name('content_edit');
    Route::post('content/edit/{pagePostModuleId}/{id}','Admin\ContentController@store');
    Route::get('content/delete/{pagePostModuleId}/{id?}','Admin\ContentController@delete')->name('content_delete')->where('id', '[0-9\,]+');
    Route::any('content/generatepreview','Admin\ContentController@generatepreview');
    Route::post('content/getjslang','Admin\ContentController@getjslang');

    Route::get('popup','Admin\PopupController@index')->name('popup');
    Route::post('popup','Admin\PopupController@store')->name('popup_store');

    Route::get('slider','Admin\SliderController@index')->name('slider');
    Route::get('slider/slides/{sliderId}','Admin\SliderController@slides')->name('slides')->where('sliderId', '[0-9]+');
    Route::get('slider/create/{sliderId}','Admin\SliderController@create')->name('slider_create')->where('sliderId','[0-9]+');
    Route::post('slider/create/{sliderId?}','Admin\SliderController@store')->name('slider_store')->where('sliderId','[0-9]+');
    Route::get('slider/edit/{slideId}','Admin\SliderController@edit')->name('slider_edit')->where('slideId','[0-9]+');
    Route::post('slider/edit/{slideId}','Admin\SliderController@store')->where('slideId','[0-9]+');
    Route::get('slider/delete/{slideId?}','Admin\SliderController@delete')->name('slider_delete')->where('slideId', '[0-9\,]+');
    Route::post('slider/setposition','Admin\SliderController@setposition');


    Route::get('offer/{categoryParentId?}','Admin\OfferController@index')->name('offer')->where('categoryParentId','[0-9]+');
    Route::get('offer/create/{categoryParentId?}','Admin\OfferController@create')->name('offer_create')->where('categoryParentId','[0-9]+');
    Route::get('offer/edit/{id}','Admin\OfferController@edit')->name('offer_edit');
    Route::post('offer/create/{categoryParentId?}','Admin\OfferController@store');
    Route::post('offer/edit/{id}','Admin\OfferController@store');
    Route::get('offer/delete/{id}','Admin\OfferController@delete')->name('offer_delete');
    Route::get('offer/category/{id}','Admin\OfferController@category')->name('offer_category');
    Route::get('offer/product/create/{category}','Admin\OfferController@productcreate')->name('offer_post_create');
    Route::post('offer/product/create/{category}','Admin\OfferController@productstore');
    Route::get('offer/product/edit/{id}','Admin\OfferController@productedit')->name('offer_post_edit');
    Route::post('offer/product/edit/{id}','Admin\OfferController@productstore');
    Route::get('offer/product/delete/{id}','Admin\OfferController@productdelete')->name('offer_post_delete')->where('id','[0-9\,]+');
    Route::get('offer/import','Admin\OfferController@import')->name('offer_import');
    Route::post('offer/import','Admin\OfferController@saveImport');
    Route::get('offer/export','Admin\OfferController@export')->name('offer_export');

    Route::get('language', 'Admin\LanguageController@index')->name('language');
    Route::get('language/setmain/{id}','Admin\LanguageController@setMain')->name('language_setmain');
    Route::get('language/edit/{id}','Admin\LanguageController@edit')->name('language_edit');
    //Route::post('language/edit/{id}','Admin\LanguageController@store');
    Route::post('language/switchvalue','Admin\LanguageController@switchValue');

    Route::get('user','Admin\UserController@index')->name('user');
    Route::get('user/create','Admin\UserController@create')->name('user_create');
    Route::get('user/edit/{id}','Admin\UserController@edit')->name('user_edit');
    Route::post('user/create','Admin\UserController@store');
    Route::post('user/edit/{id}','Admin\UserController@store');
    Route::get('user/delete/{id}','Admin\UserController@delete')->name('user_delete');
    Route::post('user/checkunique','Admin\UserController@checkunique');

    Route::get('settings','Admin\SettingsController@index')->name('settings');
    Route::post('settings','Admin\SettingsController@save');

    Route::post('module/entity','Admin\ModuleController@entity');

    Route::get('password/change','Admin\PasswordController@change')->name('admin_password_change');
    Route::post('password/change','Admin\PasswordController@store');

    Route::get('tag','Admin\TagController@index')->name('tag');
    Route::get('tag/create','Admin\TagController@create')->name('tag_create');
    Route::get('tag/edit/{id}','Admin\TagController@edit')->name('tag_edit');
    Route::post('tag/create','Admin\TagController@store');
    Route::post('tag/edit/{id}','Admin\TagController@store');
    Route::get('tag/delete/{id?}','Admin\TagController@delete')->name('tag_delete')->where('id','[0-9\,]+');
    Route::post('tag/store','Admin\TagController@storeFromPost');

    Route::get('seo/smartlinks','Admin\SeoSmartLinksController@index')->name('seo_smart_links');
    Route::get('seo/smartlinks/import','Admin\SeoSmartLinksController@import');
    Route::post('seo/smartlinks/import','Admin\SeoSmartLinksController@store');
    Route::get('seo/smartlinks/edit/{id}','Admin\SeoSmartLinksController@edit')->name('seo_smart_links_edit');
    Route::get('seo/smartlinks/create','Admin\SeoSmartLinksController@add')->name('seo_smart_links_create');
    Route::post('seo/smartlinks/edit/{id}','Admin\SeoSmartLinksController@store');
    Route::post('seo/smartlinks/create','Admin\SeoSmartLinksController@store');
    Route::get('seo/smartlinks/delete/{id?}','Admin\SeoSmartLinksController@delete')->where('id','[0-9\,]+')->name('seo_smart_links_delete');
    Route::get('seo/smartlinks/samplefile','Admin\SeoSmartLinksController@samplefile');

    Route::get('productoffer/edit/{pagePostModuleId}/{id}','Admin\ProductofferController@edit')->name('productoffer_edit');
    Route::post('productoffer/edit/{pagePostModuleId}/{id}','Admin\ProductofferController@store');
    Route::get('productoffer/delete/{pagePostModuleId}/{id}','Admin\ProductofferController@delete')->name('productoffer_delete');
    Route::post('productoffer/generatepreview','Admin\ProductofferController@generatepreview');

    Route::get('version/update','Admin\VersionController@update')->name('version_update');
    Route::get('version/migrate','Admin\VersionController@migrate')->name('version_migrate');

    if(\File::exists(__DIR__.'/../app/Http/Controllers/Admin/VersionFtpController.php')){
      Route::get('versionftp','Admin\VersionFtpController');
    }

    Route::get('artisan','Admin\ArtisanController');

    Route::get('{slug?}', function(){
        \Abort('404');
    })->where('slug', '.+');

});
