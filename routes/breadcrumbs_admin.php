<?php

//--------------------------- ADMIN --------------------------------------------

Breadcrumbs::register('admin', function($breadcrumbs)
{
    $breadcrumbs->push(trans('admin.breadcrumbs.panel'), route('dashboard'));
});

//----------------------------- PAGE -------------------------------------------

Breadcrumbs::register('page', function($breadcrumbs,$params)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push(trans('menu.page'), route('page'));
    $tempBreadcrumbs = [];

    if(array_key_exists('parentPageId',$params) && $params['parentPageId']){
      $parentPageId = $params['parentPageId'];

      do{

        $page = \App\Models\Page::with(['language' => function($query){
            $query->whereHas('language',function($query){
              $query->where('symbol','=',\App::getLocale());
            });
          }])
          ->where('id','=',$parentPageId)
          ->first();

          $parentPageId = "";
          if($page){
            $tempBreadcrumbs[] = [
              'id' => $page-> id,
              'name' => ucfirst($page->language[0]->title)
            ];

            $parentPageId = $page->parent_page_id;
          }

      }while($parentPageId!="");

      $tempBreadcrumbs = array_reverse($tempBreadcrumbs);
      foreach($tempBreadcrumbs as $crumb){
        $breadcrumbs->push($crumb['name'], route('page',['parentPageId' => $crumb['id']]));
      }
    }
});

Breadcrumbs::register('page_create', function($breadcrumbs,$params)
{
    $breadcrumbs->parent('page',$params);
    $breadcrumbs->push(trans('admin.page.header_add'), route('page'));
});

Breadcrumbs::register('page_edit', function($breadcrumbs,$params)
{
    $breadcrumbs->parent('page',$params);

    $breadcrumbs->push(ucfirst($params['page']->language[0]->title), route('page_edit',['id' => $params['page']->id]));
});

//------------------------- POST CATEGORY --------------------------------------

Breadcrumbs::register('category', function($breadcrumbs,$params = array())
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push(trans('menu.page'), route('page'));

    if(count($params)>0){
      // if(array_key_exists('pagePostModuleId',$params)){
      //   $module = \App\Models\PagePostModule::where('id','=',$params['pagePostModuleId'])->first();
      // }else{

        $moduleId = \App\Models\Module::where('category_id','=',$params['category_id'])->pluck('id')->first();

        if(!$moduleId){
          $moduleId = \App\Models\Category::where('id','=',$params['category_id'])->pluck('module_id')->first();
        }

        $module = \App\Models\PagePostModule::where('module_id','=',$moduleId)->where('module_entity_id','=',$params['category_id'])->first();

      //}

      $page = $basicPage = \App\Models\Page::with(['language' => function($query){
            $query->whereHas('language',function($query){
              $query->where('symbol','=',\App::getLocale());
            });
        }])
        ->where('id','=',$module->entity_id)
        ->first();

      $breadcrumbsParts = [];

      while($page->parent_page_id!=NULL){
        $parentId = $page->parent_page_id;
        $page = \App\Models\Page::with(['language' => function($query){
              $query->whereHas('language',function($query){
                $query->where('symbol','=',\App::getLocale());
              });
          }])
          ->where('id','=',$page->parent_page_id)
          ->first();
        array_push($breadcrumbsParts,$page);
      }

      $breadcrumbsParts = array_reverse($breadcrumbsParts);
      foreach($breadcrumbsParts as $page){
        $breadcrumbs->push(ucfirst($page->language[0]->title), route('page',['parentPageId' => $parentId]));
      }

      $breadcrumbs->push(ucfirst($basicPage->language[0]->title), route('page_edit',['id'=>$basicPage->id]));
    }
});

Breadcrumbs::register('category_edit', function($breadcrumbs,$params)
{
    $breadcrumbs->parent('category',$params);
    $category = \App\Models\Category::with(['language' => function($query){
      $query->whereHas('language',function($query){
        $query->where('symbol','=',\App::getLocale());
      });
    }])->where('id','=',$params['category_id'])->first();

    $breadcrumbs->push($category->language[0]->name, route('category_edit',['pagePostModuleId'=>$params['pagePostModuleId'],'id'=>$params['category_id']]));
});

//------------------------------- POST -----------------------------------------

Breadcrumbs::register('post', function($breadcrumbs,$params)
{
    $breadcrumbs->parent('category',$params);
    $breadcrumbs->push($params['name'], route('post',['category'=>$params['category_id'],'pagePostModuleId'=>$params['pagePostModuleId']]));
});

Breadcrumbs::register('posts', function($breadcrumbs,$params)
{
    if(array_key_exists('name',$params)){
      $categoryName = $params['name'];
    }else{
      $categoryModel = new \App\Models\Category();
      $categoryName = $categoryModel->getNameById($params['category_id']);
    }
    $pagePostModuleId = array_key_exists('pagePostModuleId',$params) ? $params['pagePostModuleId'] : $params['pagePostModuleId'];
    $breadcrumbs->parent('category',$params);
    $breadcrumbs->push($categoryName, route('post',['category'=>$params['category_id'],'pagePostModuleId'=>$pagePostModuleId]));
});

Breadcrumbs::register('post_create', function($breadcrumbs,$params)
{
    $breadcrumbs->parent('posts',$params);
    $breadcrumbs->push(trans('admin.post.header_add'), route('post',['category'=>$params['category_id'],'pagePostModuleId'=>$params['pagePostModuleId']]));
});

Breadcrumbs::register('post_edit', function($breadcrumbs,$params)
{
    $title = trans('admin.post.header_edit');
    if(array_key_exists('post',$params)){
      foreach($params['post']->language as $lang){
        if($lang->language->symbol == \App::getLocale()){
          $title = $lang->title;
        }
      }
    }
    $breadcrumbs->parent('posts',$params);
    $breadcrumbs->push(ucfirst($title), route('post_edit',['id'=>$params['post_id'],'pagePostModuleId'=>$params['pagePostModuleId']]));
});

//--------- MODULE: GALLERY VIDEO, GALLERY PHOTO, DOWNLOAD, CONTENT ------------
Breadcrumbs::register('module_edit', function($breadcrumbs,$params)
{
  if(array_key_exists('from_module',$params) && $params['from_module']!=false && $params['entity_type'] == 'post' && Breadcrumbs::exists($params['from_module']->symbol.'_post_edit')){
    $object = \App\Models\Post::with(['language','category'])->where('id','=',$params['entity_id'])->first();

    $breadcrumbs->parent($params['from_module']->symbol.'_post_edit',[
      'category' => $object->category,
      'post' => $object
    ]);
  }else if($params['entity_type'] == 'post'){
    $params['post'] = \App\Models\Post::with(['language','category'])->where('id','=',$params['entity_id'])->first();
    $params['category'] = \App\Models\Category::getById($params['post']->category_id);
    $params['category_id'] = $params['post']->category_id;
    $breadcrumbs->parent('post_edit',$params);
  }else if($params['entity_type'] == "category"){
    $params['category'] = \App\Models\Category::where('id','=',$params['entity_id'])->with(['module'])->first();
    if($params['category']->module){
      $params['categoryParentId'] = $params['category']->category_parent_id;
      $breadcrumbs->parent($params['category']->module->symbol.'_edit',$params);
    }else{
      $breadcrumbs->parent('category_edit',$params);
    }
  }else{
    $page = \App\Models\Page::with(['language'])->where('id','=',$params['entity_id'])->first();
    $breadcrumbs->parent('page_edit',[
      'page' => $page,
      'parentPageId' => $page->parent_page_id!="" ? $page->parent_page_id : NULL
    ]);
  }
  $breadcrumbs->push(trans('admin.breadcrumbs.'.$params['entity_name']), route($params['entity_name'].'_edit',['pagePostModuleId'=>$params['pagePostModuleId'],'id'=>$params['id']]));
});

//--------------------------------- POPUP --------------------------------------

Breadcrumbs::register('popup', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push(trans('menu.popup'), route('popup'));
});


//------------------------------- SLIDER----------------------------------------
Breadcrumbs::register('slider', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push(trans('menu.slider'), route('slider'));
});

Breadcrumbs::register('slides', function($breadcrumbs,$params)
{
    $breadcrumbs->parent('slider');
    $breadcrumbs->push(ucfirst(trans($params['slider']->name)), route('slides',['sliderId'=>$params['slider']->id]));
});

Breadcrumbs::register('addslider', function($breadcrumbs,$params)
{
    $breadcrumbs->parent('slides',$params);
    $breadcrumbs->push(trans('admin.slider.header_add'), route('slider_create',['sliderId'=>$params['slider']->id]));
});

Breadcrumbs::register('editslider', function($breadcrumbs,$params)
{
    $breadcrumbs->parent('slides',$params);
    $current = \App::getLocale();
    $title = "";
    foreach($params['slide']->language as $lang){
      if($lang->language->symbol == $current){
        if($lang->title_1!="")
          $title = $lang->title_1;
        else if($lang->title_2!="")
            $title = $lang->title_2;
      }
    }
    if(trim($title) == "") $title = trans('admin.slider.header_edit');
    $breadcrumbs->push($title, route('slider_edit',['id'=>$params['slider']->id]));
});

//------------------------------- OFFER ----------------------------------------

Breadcrumbs::register('offer', function($breadcrumbs,$params)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push(trans('menu.offer'), route('offer'));
    if(array_key_exists('categoryParentId',$params) && $params['categoryParentId']){
      $categoryParentId = $params['categoryParentId'];

      do{

        $category = \App\Models\Category::with(['language' => function($query){
            $query->whereHas('language',function($query){
              $query->where('symbol','=',\App::getLocale());
            });
          }])
          ->where('id','=',$categoryParentId)
          ->first();

          $tempBreadcrumbs[] = [
            'id' => $category-> id,
            'name' => ucfirst($category->language[0]->name)
          ];

          $categoryParentId = $category->category_parent_id;

      }while($categoryParentId!="");

      $tempBreadcrumbs = array_reverse($tempBreadcrumbs);
      foreach($tempBreadcrumbs as $crumb){
        $breadcrumbs->push($crumb['name'], route('offer',['categoryParentId' => $crumb['id']]));
      }
    }
});

Breadcrumbs::register('offer_create', function($breadcrumbs,$params)
{
    $breadcrumbs->parent('offer',$params);
    $breadcrumbs->push(trans('admin.offer.creating'), route('offer'));
});

Breadcrumbs::register('offer_edit', function($breadcrumbs,$params)
{
    $breadcrumbs->parent('offer',$params);
    $breadcrumbs->push($params['category']->language[0]->name, route('offer_edit',['id' => $params['category']->id]));
});

Breadcrumbs::register('offer_posts', function($breadcrumbs,$params)
{
    $params['categoryParentId'] = $params['category']->id;
    $breadcrumbs->parent('offer',$params);
    $breadcrumbs->push(trans('admin.offer.product_list'),route('offer_category',['id'=>$params['category']->id]));
});

Breadcrumbs::register('offer_post_create', function($breadcrumbs,$params)
{
    $breadcrumbs->parent('offer_posts',$params);
    $breadcrumbs->push(trans('admin.offer.header_post_add'), route('offer'));
});

Breadcrumbs::register('offer_post_edit', function($breadcrumbs,$params)
{
    $breadcrumbs->parent('offer_posts',$params);
    $title = "";
    foreach($params['post']->language as $lang){
      if($lang->language->symbol == \App::getLocale())
        $title = $lang->title;
    }
    $breadcrumbs->push($title, route('offer_post_edit',['id'=>$params['post']->id]));
});

Breadcrumbs::register('offer_import', function($breadcrumbs){
    $breadcrumbs->parent('offer',[]);
    $breadcrumbs->push(trans('admin.offer.import'),route('offer_import'));
});


//------------------------------- MENU -----------------------------------------
Breadcrumbs::register('menu', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push(trans('menu.menu'), route('menu'));
});

Breadcrumbs::register('menu_create', function($breadcrumbs)
{
    $breadcrumbs->parent('menu');
    $breadcrumbs->push(trans('admin.menu.add_header'), route('menu_create'));
});

Breadcrumbs::register('menu_edit', function($breadcrumbs,$params)
{
    $breadcrumbs->parent('menu');
    $breadcrumbs->push($params['menu']->name, route('menu_edit',['id'=>$params['menu']->id]));
});

//------------------------------- LANGUAGE--------------------------------------

Breadcrumbs::register('language', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push(trans('menu.language'), route('language'));
});

Breadcrumbs::register('language_edit', function($breadcrumbs,$params)
{
    $breadcrumbs->parent('language');
    $breadcrumbs->push($params['lang']->name, route('language'));
});

//------------------------------- USER -----------------------------------------

Breadcrumbs::register('user', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push(trans('menu.user'), route('user'));
});

Breadcrumbs::register('user_create', function($breadcrumbs)
{
    $breadcrumbs->parent('user');
    $breadcrumbs->push(trans('admin.user.add_header'), route('user_create'));
});

Breadcrumbs::register('user_edit', function($breadcrumbs,$params)
{
    $breadcrumbs->parent('user');
    $title = $params['user']->first_name." ".$params['user']->last_name;
    if(trim($title) == "")
      $title = trans('admin.user.edit_header');
    $breadcrumbs->push($title, route('user_edit',['id'=>$params['user']->id]));
});

//----------------------------- PAGE -------------------------------------------

Breadcrumbs::register('tag', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push(trans('menu.tag'), route('tag'));
});

Breadcrumbs::register('tag_create', function($breadcrumbs)
{
    $breadcrumbs->parent('tag');
    $breadcrumbs->push(trans('admin.tag.header_add'), route('tag_create'));
});

Breadcrumbs::register('tag_edit', function($breadcrumbs,$params)
{
    $breadcrumbs->parent('tag');

    $current = \App::getLocale();
    $name = "";
    foreach($params['tag']->language as $lang){
      if($lang->language->symbol == $current){
        $name = $lang->name;
      }
    }

    $breadcrumbs->push($name, route('tag_edit',['id' => $params['tag']->id]));
});

//-------------------------------- SETTINGS ------------------------------------

Breadcrumbs::register('settings', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push(trans('menu.settings'), route('settings'));
});

//-------------------------------- SEO ----------------------------------------

Breadcrumbs::register('seo_smart_links', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push(trans('admin.seo.smart_links_header'), route('seo_smart_links'));
});

Breadcrumbs::register('seo_smart_links_import', function($breadcrumbs)
{
    $breadcrumbs->parent('seo_smart_links');
    $breadcrumbs->push(trans('admin.seo.import'), route('seo_smart_links_create'));
});

Breadcrumbs::register('seo_smart_links_add', function($breadcrumbs)
{
    $breadcrumbs->parent('seo_smart_links');
    $breadcrumbs->push(trans('admin.seo.create_smart_link'), route('seo_smart_links_create'));
});

Breadcrumbs::register('seo_smart_links_edit', function($breadcrumbs,$params)
{
    $breadcrumbs->parent('seo_smart_links');
    $breadcrumbs->push($params['link']->word, route('seo_smart_links_edit',['id' => $params['link']->id]));
});

//------------------------------- PASSWORD -------------------------------------

Breadcrumbs::register('password_reset', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push(trans('admin.password.reset_header'), route('admin_password_change'));
});

//---------------------- VERSION -----------------------------------------------

Breadcrumbs::register('version_update', function($breadcrumbs){
    $breadcrumbs->parent('admin');
    $breadcrumbs->push(trans('admin.version.updates'), route('version_update'));
});
