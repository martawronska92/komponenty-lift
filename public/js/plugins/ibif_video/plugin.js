tinymce.PluginManager.add('ibif_video', function(editor, url) {

    editor.addButton('ibif_video', {
        icon: 'media',
        tooltip: Laravel.lang.editor.insert_video,
        onclick: function() {
            // Open window
            editor.windowManager.open({
                title: Laravel.lang.editor.insert_video,
                body: [{
                    type: 'textbox',
                    name: 'url',
                    label: Laravel.lang.editor.yt_link
                }],
                onsubmit: function(e) {
                    var url = e.data.url;
                    if (url == "") {
                        editor.notificationManager.open({
                            text: Laravel.lang.editor.wrong_yt_url,
                            type: 'error'
                        });
                    } else {
                        url = url.replace('watch?v=', 'embed/');
                        if (url.indexOf('&') != -1) {
                            url = url.substring(0, url.indexOf('&'));
                        }
                        if (url.indexOf('rel=') == -1) {
                            url += url.indexOf('?') == -1 ? '?' : '&';
                            url += 'rel=0&showinfo=0&controls=0';
                        }

                        var html = '<p style="text-align:center;">';
                        html += '<iframe width="' + Laravel.config.youtube_width + '" height="' + Laravel.config.youtube_height + '"';
                        html += ' src="' + url + '" frameborder="0" allowfullscreen style="border:none;">';
                        html += '</iframe>';
                        editor.insertContent(html);
                    }
                }
            });
        }
    });
});
