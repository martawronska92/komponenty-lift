tinymce.PluginManager.add('ibif_image', function(editor, url) {

    editor.addButton('ibif_image', {
        icon: 'image',
        tooltip: Laravel.lang.editor.insert_image,
        onpostrender: function() {

        },
        onclick: function() {
            // Open window
            editor.windowManager.open({
                title: Laravel.lang.editor.insert_image,
                width: 400,
                height: 200,
                //file: '/panel/content/tinymceimageplugin',
                url: '/build/js/plugins/ibif_image/view/dialog.html?v=8',
                // body: [{
                //     name: 'file',
                //     type: 'textbox',
                //     subtype: 'file',
                //     label: 'Upload',
                //     id: 'tiny_upload'
                // }],
                onopen: function(e) {

                },
                onsubmit: function(e) {
                    // Insert content when the window form is submitted
                    //editor.insertContent('Title: ' + '<h2>' + e.data.title + '</h2>');
                }
            }, {
                token: Laravel.csrfToken,
                editor: editor
            });
        }
    });

});
