<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePageTable extends Migration {

	public function up()
	{
		Schema::create('page', function(Blueprint $table) {
			$table->increments('id');
			$table->boolean('active')->default(1);
			$table->integer('parent_page_id')->unsigned()->nullable();
			$table->integer('level')->unsigned()->default(0);
			$table->integer('position')->unsigned()->default(0);
			$table->integer('user_id')->unsigned()->nullable();
			$table->string('default_view')->nullable();
			$table->string('draft')->nullable();
			$table->boolean('for_developer')->default(0)->nullable();
			$table->timestamps();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
			$table->foreign('parent_page_id')->references('id')->on('page')->onDelete('set null');
		});
	}

	public function down()
	{
		Schema::drop('page');
	}
}
