<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserPrivilege extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('user_privilege',function($table){
        $table->increments('id');
        $table->integer('privilege_id')->unsigned();
        $table->integer('user_id')->unsigned();
        $table->integer('entity_id')->unsigned()->nullable();
        $table->foreign('privilege_id')->references('id')->on('privilege')->onDelete('cascade');
        $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
