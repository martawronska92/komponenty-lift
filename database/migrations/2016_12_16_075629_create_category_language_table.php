<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryLanguageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('category_language', function(Blueprint $table) {
        $table->increments('id');
        $table->string('name', 255);
        $table->text('description')->nullable();
        $table->string('meta_title', 255)->nullable();
        $table->string('meta_description', 255)->nullable();        
        $table->integer('language_id')->unsigned();
        $table->integer('category_id')->unsigned();
        $table->timestamps();
        $table->foreign('category_id')->references('id')->on('category')->onDelete('cascade');
        $table->foreign('language_id')->references('id')->on('language')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('category_language');
    }
}
