<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module',function($table){
          $table->increments('id');
          $table->string('symbol');
          $table->boolean('for_page');
          $table->boolean('for_post');
          $table->boolean('for_category')->default(0);
          $table->boolean('for_developer')->default(0);
          $table->boolean('active');
          $table->boolean('bind_entity')->comment('czy bindować cały moduł czy poszczególne encje modułu');
          $table->boolean('has_routing')->comment('czy routing modułu na froncie');
          $table->boolean('has_sitemap')->comment('czy sitemapa modułu na froncie');
          $table->boolean('has_search')->comment('czy ma metodę do wyszukiwania i można po wyszukiwać')->default(0);
          $table->boolean('need_user_priviledge')->comment('czy wymaga uprawnienia dla użytkownika');
          $table->string('module_entity_model')->comment('model modułu');
          $table->integer('category_id')->unsigned()->nullable();
          $table->foreign('category_id')->references('id')->on('category')->onDelete('cascade');
        });

        Schema::table('category',function($table){
          $table->integer('position')->after('user_id')->default(0);
          $table->integer('module_id')->unsigned()->nullable()->after('position');
          $table->foreign('module_id')->references('id')->on('module')->onDelete('cascade');
        });

        Schema::table('category_language',function($table){
          $table->string('url')->after('name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('module');
    }
}
