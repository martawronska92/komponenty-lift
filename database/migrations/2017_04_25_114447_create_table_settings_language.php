<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSettingsLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings_language',function($table){
          $table->increments('id');
          $table->text('value');
          $table->integer('language_id')->unsigned();
          $table->integer('settings_id')->unsigned();
          $table->foreign('language_id')->references('id')->on('language')->onDelete('cascade');
          $table->foreign('settings_id')->references('id')->on('settings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
