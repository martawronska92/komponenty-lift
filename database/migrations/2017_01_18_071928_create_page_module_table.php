<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageModuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_post_module',function($table){
          $table->increments('id');
          $table->morphs('entity');
          $table->integer('module_id')->unsigned();
          $table->integer('module_entity_id')->unsigned()->nullable()->comment('np gallery id');
          $table->integer('position')->unsigned();
          $table->boolean('active')->default(1);
          $table->timestamps();
          $table->foreign('module_id')->references('id')->on('module')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_module');
    }
}
