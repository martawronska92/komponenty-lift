<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagePostLanguageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('page_post_language', function(Blueprint $table) {
        $table->increments('id');
        $table->string('title', 255)->nullable();
        $table->text('content')->nullable();
        $table->morphs('entity');
        $table->integer('language_id')->unsigned();
        $table->string('url', 255)->nullable();
        $table->string('meta_title', 255)->nullable();
        $table->string('meta_description', 255)->nullable();        
        $table->timestamps();
        $table->foreign('language_id')->references('id')->on('language')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_language');
    }
}
