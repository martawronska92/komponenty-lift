<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('gallery', function(Blueprint $table) {
        $table->increments('id');
        $table->integer('show_type_id')->unsigned()->nullable();
        $table->string('type')->comment('video or photo');
        $table->timestamps();
        $table->foreign('show_type_id')->references('id')->on('gallery_show_type')->onDelete('set null');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gallery');
    }
}
