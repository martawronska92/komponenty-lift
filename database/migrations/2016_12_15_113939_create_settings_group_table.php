<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingsGroupTable extends Migration {

	public function up()
	{
		Schema::create('settings_group', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('symbol');
			$table->integer('position')->unsigned();
			$table->boolean('subgroup')->default(0);
		});
	}

	public function down()
	{
		Schema::drop('settings_group');
	}
}
