<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GalleryDownloadDescription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gallery_language',function($table){
          $table->text('description')->nullable()->after('title');
        });

        Schema::table('download_language',function($table){
          $table->text('description')->nullable()->after('title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('gallery_language',function($table){
        $table->dropColumn('description');
      });

      Schema::table('download_language',function($table){
        $table->dropColumn('description');
      });
    }
}
