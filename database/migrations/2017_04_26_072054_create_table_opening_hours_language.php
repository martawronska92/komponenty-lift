<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOpeningHoursLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opening_hours_language',function($table){
          $table->increments('id');
          $table->string("value");
          $table->integer("opening_hours_id")->unsigned();
          $table->integer("language_id")->unsigned();
          $table->foreign('opening_hours_id')->references('id')->on('opening_hours')->onDelete('cascade');
          $table->foreign('language_id')->references('id')->on('language')->onDelete('cascade');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
