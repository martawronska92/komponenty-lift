<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMenuElementLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_element_language',function($table){
          $table->increments('id');
          $table->string('title')->nullable();
          $table->integer('menu_element_id')->unsigned();
          $table->integer('language_id')->unsigned();
          $table->timestamps();
          $table->foreign('menu_element_id')->references('id')->on('menu_element')->onDelete('cascade');
          $table->foreign('language_id')->references('id')->on('language')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menu_item_language');
    }
}
