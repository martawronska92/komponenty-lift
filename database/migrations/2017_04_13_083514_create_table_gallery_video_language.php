<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGalleryVideoLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery_video_language',function($table){
          $table->increments('id');
          $table->string('title')->nullable();
          $table->integer('gallery_video_id')->unsigned();
          $table->integer('language_id')->unsigned();
          $table->timestamps();
          $table->foreign('gallery_video_id')->references('id')->on('gallery_video')->onDelete('cascade');
          $table->foreign('language_id')->references('id')->on('language')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
