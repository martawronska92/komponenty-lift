<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProductOfferLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_offer_language',function($table){
          $table->increments('id');
          $table->string('title',255)->nullable();
          $table->integer('product_offer_id')->unsigned();
          $table->integer('language_id')->unsigned();
          $table->timestamps();
          $table->foreign('language_id')->references('id')->on('language')->onDelete('cascade');
          $table->foreign('product_offer_id')->references('id')->on('product_offer')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_offer_language');
    }
}
