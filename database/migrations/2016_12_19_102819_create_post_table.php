<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostTable extends Migration {

	public function up()
	{
		Schema::create('post', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('category_id')->unsigned()->nullable();
			$table->boolean('public')->default(1);
			$table->string('publish_date')->nullable();
			$table->boolean('active')->default(1);
			$table->string('thumbnail',50)->nullable();
			$table->text('thumbnail_options')->nullable();
			$table->integer('position')->unsigned()->default(0);
			$table->integer('user_id')->unsigned()->nullable();
			$table->string('draft')->nullable();
			$table->timestamps();
			$table->foreign('category_id')->references('id')->on('category')->onDelete('cascade');
			$table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
		});
	}

	public function down()
	{
		Schema::drop('page');
	}
}
