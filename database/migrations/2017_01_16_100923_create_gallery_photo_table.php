<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryPhotoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('gallery_photo', function(Blueprint $table) {
        $table->increments('id');
        $table->integer('gallery_id')->unsigned();
        $table->integer('position')->unsigned();
        $table->boolean('active')->default(1);
        $table->string('ext', 5);
        $table->text('thumbnail_options')->nullable();
        $table->timestamps();
        $table->foreign('gallery_id')->references('id')->on('gallery')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gallery_photo');
    }
}
