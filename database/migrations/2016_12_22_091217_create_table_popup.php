<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePopup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('popup',function(Blueprint $table){
          $table->increments('id');
          $table->string('content_type');
          $table->date('visible_from')->nullable();
          $table->date('visible_to')->nullable();
          $table->boolean('active')->default(1);
          $table->text('thumbnail_options')->nullable();
          $table->string('link_type')->nullable();
          $table->string('link_value')->nullable();
          $table->boolean('link_new_window')->nullable()->default(0);
          $table->timestamps();
        });

        Schema::create('popup_language',function(Blueprint $table){
          $table->increments('id');
          $table->text('content');
          $table->integer('popup_id')->unsigned();
          $table->integer('language_id')->unsigned();
          $table->timestamps();
          $table->foreign('language_id')->references('id')->on('language')->onDelete('cascade');
          $table->foreign('popup_id')->references('id')->on('popup')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('popup_language');
        Schema::drop('popup');
    }
}
