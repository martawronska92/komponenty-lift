<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLanguageTable extends Migration {

	public function up()
	{
		Schema::create('language', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 100);
			$table->string('symbol', 5);
			$table->boolean('active')->default(1);
			$table->boolean('visible_on_front')->default(1);
			$table->boolean('main')->default(0);
			$table->string("domain")->nullable();
		});
	}

	public function down()
	{
		Schema::drop('language');
	}
}
