<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
   	 {
     		Schema::create('category', function(Blueprint $table) {
     			$table->increments('id');
          $table->boolean('active')->default(1);
          $table->string('category_view')->nullable();
          $table->string('post_view')->nullable();
          $table->integer('per_page')->unsigned()->nullable();
          $table->string('loading_type')->nullable();
          $table->integer('user_id')->unsigned()->nullable();
          $table->string('thumbnail')->nullable();
          $table->text('thumbnail_options')->nullable();
          $table->integer('level')->unsigned()->nullable()->default(0);
          $table->integer('category_parent_id')->unsigned()->nullable();
          $table->boolean('show_modules')->default(0)->comment('czy pokazywać moduły czy produkty');
     			$table->timestamps();
          $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
          $table->foreign('category_parent_id')->references('id')->on('category')->onDelete('cascade');
     		});
   	 }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('category');
    }
}
