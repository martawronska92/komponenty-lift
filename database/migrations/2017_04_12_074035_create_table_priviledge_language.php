<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePriviledgeLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('privilege_language',function($table){
          $table->increments('id');
          $table->integer('privilege_id')->unsigned();
          $table->integer('language_id')->unsigned();
          $table->string('name');          
          $table->foreign('privilege_id')->references('id')->on('privilege')->onDelete('cascade');
          $table->foreign('language_id')->references('id')->on('language')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
