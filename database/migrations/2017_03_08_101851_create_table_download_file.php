<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDownloadFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('download_file', function(Blueprint $table) {
        $table->increments('id');
        $table->boolean('active')->default(1);
        $table->string('filename_hashed')->comment('');
        $table->string('filename_slug')->comment('');
        $table->integer('filesize')->unsigned();
        $table->integer('position')->unsigned();
        $table->integer('downloaded')->unsigned();
        $table->string('ext', 5);
        $table->integer('download_id')->unsigned();
        $table->foreign('download_id')->references('id')->on('download')->onDelete('cascade');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('download_file');
    }
}
