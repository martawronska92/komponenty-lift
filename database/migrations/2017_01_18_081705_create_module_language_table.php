<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleLanguageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_language',function($table){
          $table->increments('id');
          $table->integer('module_id')->unsigned();
          $table->integer('language_id')->unsigned();
          $table->string('url')->nullable();
          $table->string('name')->nullable();
          $table->foreign('module_id')->references('id')->on('module')->onDelete('cascade');
          $table->foreign('language_id')->references('id')->on('language')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('module_language');
    }
}
