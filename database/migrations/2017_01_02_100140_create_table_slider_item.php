<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSliderItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('slider_item', function(Blueprint $table) {
        $table->increments('id');
        $table->integer('slider_id')->unsigned();
        $table->string('ext', 50);
        $table->boolean('active')->default(1);
        $table->integer('position')->unsigned();
        $table->boolean('external')->deault(0)->comment('-1 - brak linku, 0 - link wewnętrzny, 1 - link zewnętrzny');
        $table->string('link')->comment('zewnętrzny url lub id strony wewnętrznej')->nullable();
        $table->text('thumbnail_options')->nullable();
        $table->timestamps();
        $table->foreign('slider_id')->references('id')->on('slider')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('slider_item');
    }
}
