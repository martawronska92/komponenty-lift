<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableModuleCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_category_default',function($table){
          $table->increments('id');
          $table->string('category_view')->nullable();
          $table->string('post_view')->nullable();
          $table->integer('per_page')->unsigned()->nullable();
          $table->string('loading_type')->nullable();
          $table->integer('module_id')->unsigned();
          $table->foreign('module_id')->references('id')->on('module')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('module_category_default');
    }
}
