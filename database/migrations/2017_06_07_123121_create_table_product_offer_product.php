<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProductOfferProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_offer_product',function($table){
          $table->increments('id');
          $table->integer('post_id')->unsigned();
          $table->integer('product_offer_id')->unsigned();
          $table->timestamps();
          $table->foreign('post_id')->references('id')->on('post')->onDelete('cascade');
          $table->foreign('product_offer_id')->references('id')->on('product_offer')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_offer_product');
    }
}
