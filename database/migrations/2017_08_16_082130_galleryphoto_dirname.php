<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GalleryphotoDirname extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gallery_photo',function($table){
          $table->string('dirname',255)->nullable()->after('id');
          $table->boolean('draft')->default(0)->comment('wersja robocza')->after('active');
        });

        Schema::table('gallery_video',function($table){
          $table->boolean('draft')->default(0)->after('active')->comment('wersja robocza');
        });

        Schema::table('download_file',function($table){
          $table->boolean('draft')->default(0)->after('active')->comment('wersja robocza');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gallery_photo',function($table){
          $table->dropColumn('dirname');
          $table->dropColumn('draft');
        });

        Schema::table('gallery_video', function($table){
          $table->dropColumn('draft');
        });

        Schema::table('download_file', function($table){
          $table->dropColumn('draft');
        });
    }
}
