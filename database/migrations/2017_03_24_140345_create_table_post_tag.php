<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePostTag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_tag',function($table){
          $table->increments('id');
          $table->integer('post_id')->unsigned();
          $table->integer('tag_id')->unsigned();
          $table->timestamps();
          $table->foreign('tag_id')->references('id')->on('tag')->onDelete('cascade');
          $table->foreign('post_id')->references('id')->on('post')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('post_tag');
    }
}
