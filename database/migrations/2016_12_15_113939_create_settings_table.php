<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingsTable extends Migration {

	public function up()
	{
		Schema::create('settings', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 255);
			$table->text('value');
			$table->integer('settings_group_id')->unsigned();
			$table->integer('settings_subgroup_id')->unsigned()->nullable();
			$table->string('symbol', 255);
			$table->string('type', 50);
			$table->string('placeholder',255)->nullable();
			$table->text('hint')->nullable();
			$table->boolean('required')->default(0);
			$table->text('options')->nullable();
			$table->integer('position')->unsigned()->default(0);
			$table->foreign('settings_group_id')->references('id')->on('settings_group')->onDelete('cascade');
			$table->foreign('settings_subgroup_id')->references('id')->on('settings_group')->onDelete('cascade');
		});
	}

	public function down()
	{
		Schema::drop('settings');
	}
}
