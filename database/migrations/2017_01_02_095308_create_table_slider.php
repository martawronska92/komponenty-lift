<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSlider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('slider', function(Blueprint $table) {
        $table->increments('id');
        $table->string('name', 255);
        $table->string('symbol', 20);
        $table->boolean('active')->default(1);
        $table->timestamps();
      });

      \App\Models\Slider::create([
        'name' => 'admin.slider.homepage',
        'symbol' => 'homepage',
        'active' => 1
      ]);

      \App\Models\Slider::create([
        'name' => 'admin.slider.subpage',
        'symbol' => 'subpage',
        'active' => 1
      ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('slider');
    }
}
