<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('gallery_video', function(Blueprint $table) {
        $table->increments('id');
        $table->integer('gallery_id')->unsigned();
        $table->text('url');
        $table->string('time',255)->nullable();
        $table->text('thumbnail');
        $table->integer('position')->unsigned();
        $table->boolean('active')->default(1);
        $table->timestamps();
        $table->foreign('gallery_id')->references('id')->on('gallery')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gallery_video');
    }
}
