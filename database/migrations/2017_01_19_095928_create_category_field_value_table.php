<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryFieldValueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("category_field_value",function($table){
          $table->increments('id');
          $table->string('value');
          $table->integer('category_field_id')->unsigned();
          $table->integer('post_id')->unsigned();
          $table->foreign('category_field_id')->references('id')->on('category_field')->onDelete('cascade');
          $table->foreign('post_id')->references('id')->on('post')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("category_field_value");
    }
}
