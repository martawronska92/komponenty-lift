<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGalleryListLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('gallery_list_language', function(Blueprint $table) {
        $table->increments('id');
        $table->integer('gallery_list_id')->unsigned();
        $table->string('title', 255)->nullable();
        $table->text('description')->nullable();
        $table->integer('language_id')->unsigned();
        $table->timestamps();
        $table->foreign('language_id')->references('id')->on('language')->onDelete('cascade');
        $table->foreign('gallery_list_id')->references('id')->on('gallery_list')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gallery_list_language');
    }
}
