<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role',function($table){
          $table->increments('id');
          $table->string('symbol');
        });

        Schema::table('users',function($table){
          $table->integer('role_id')->unsigned()->nullable()->after('remember_token');
          $table->foreign('role_id')->references('id')->on('role')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('role');
    }
}
