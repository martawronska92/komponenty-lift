<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSliderItemLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('slider_item_language', function(Blueprint $table) {
        $table->increments('id');
        $table->integer('language_id')->unsigned();
        $table->string('title_1', 255)->nullable();
        $table->string('title_2', 255)->nullable();
        $table->integer('slider_item_id')->unsigned();
        $table->timestamps();
        $table->foreign('slider_item_id')->references('id')->on('slider_item')->onDelete('cascade');
        $table->foreign('language_id')->references('id')->on('language')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('slider_item_language');
    }
}
