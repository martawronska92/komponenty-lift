<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleFieldLanguageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_field_language',function($table){
          $table->increments('id');
          $table->string('name');
          $table->integer('module_field_id')->unsigned();
          $table->integer('language_id')->unsigned();
          $table->foreign('module_field_id')->references('id')->on('module_field')->onDelete('cascade');
          $table->foreign('language_id')->references('id')->on('language')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('module_field_language');
    }
}
