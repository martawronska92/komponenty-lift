<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryFieldLanguageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create("category_field_language",function($table){
        $table->increments('id');
        $table->string('name');
        $table->integer('category_field_id')->unsigned();
        $table->integer('language_id')->unsigned();
        $table->foreign('category_field_id')->references('id')->on('category_field')->onDelete('cascade');
        $table->foreign('language_id')->references('id')->on('language')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("category_field_language");
    }
}
