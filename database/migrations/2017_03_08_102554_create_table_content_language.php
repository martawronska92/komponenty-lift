<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableContentLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('content_language', function(Blueprint $table) {
        $table->increments('id');
        $table->string('title')->nullable();
        $table->text('content')->nullable();
        $table->integer('content_id')->unsigned();
        $table->integer('language_id')->unsigned();
        $table->timestamps();
        $table->foreign('content_id')->references('id')->on('content')->onDelete('cascade');
        $table->foreign('language_id')->references('id')->on('language')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
