<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users',function($table){
          $table->string('first_name')->nullable()->after('email');
          $table->string('last_name')->nullable()->after('first_name');
          $table->date('last_login_history')->nullable()->after('last_name');
          $table->date('last_login_current')->nullable()->after('last_login_history');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
