<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDownloadFileLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('download_file_language', function(Blueprint $table) {
        $table->increments('id');
        $table->string('title');
        $table->integer('download_file_id')->unsigned();
        $table->integer('language_id')->unsigned();
        $table->timestamps();
        $table->foreign('download_file_id')->references('id')->on('download_file')->onDelete('cascade');
        $table->foreign('language_id')->references('id')->on('language')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
