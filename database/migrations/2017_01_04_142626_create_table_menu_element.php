<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMenuElement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('menu_element', function(Blueprint $table) {
        $table->increments('id');
        $table->integer('menu_id')->unsigned();
        $table->integer('parent_id')->unsigned()->nullable();
        $table->integer('level')->unsigned()->nullable()->default(0);
        $table->boolean('active')->default(1);
        $table->boolean('mainpage')->default(0);
        $table->boolean('external')->default(0);
        $table->string('external_url', 255)->nullable();
        $table->integer('entity_id')->unsigned()->nullable();
        $table->string('entity_type', 255)->nullable();
        $table->integer('position')->unsigned();
        $table->timestamps();
        $table->foreign('menu_id')->references('id')->on('menu')->onDelete('cascade');
        $table->foreign('parent_id')->references('id')->on('menu_element')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menu_item');
    }
}
