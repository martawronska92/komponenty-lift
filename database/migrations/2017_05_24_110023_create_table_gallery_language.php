<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGalleryLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('gallery_language', function(Blueprint $table) {
        $table->increments('id');
        $table->integer('gallery_id')->unsigned();
        $table->string('title', 255)->nullable();
        $table->integer('language_id')->unsigned();
        $table->timestamps();
        $table->foreign('language_id')->references('id')->on('language')->onDelete('cascade');
        $table->foreign('gallery_id')->references('id')->on('gallery')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gallery_language');
    }
}
