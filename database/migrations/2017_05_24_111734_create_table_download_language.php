<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDownloadLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('download_language', function(Blueprint $table) {
        $table->increments('id');
        $table->integer('download_id')->unsigned();
        $table->string('title', 255)->nullable();
        $table->integer('language_id')->unsigned();
        $table->timestamps();
        $table->foreign('language_id')->references('id')->on('language')->onDelete('cascade');
        $table->foreign('download_id')->references('id')->on('download')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('download_language');
    }
}
