<?php

use Illuminate\Database\Seeder;

class PrivilegeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $langPl = \App\Models\Language::where('symbol','=','pl')->pluck('id')->first();
        $langEn = \App\Models\Language::where('symbol','=','en')->pluck('id')->first();

        $id = \DB::table('privilege')->insertGetId([
          'symbol' => 'page',
          'entity_privilege' => 1,
          'entity_model' => 'App\\Models\\Page'
        ]);

        if($langPl){
          \App\Models\PrivilegeLanguage::insert([
            'name' => 'Podstrony',
            'privilege_id' => $id,
            'language_id' => $langPl
          ]);
        }

        if($langEn){
          \App\Models\PrivilegeLanguage::insert([
            'name' => 'Pages',
            'privilege_id' => $id,
            'language_id' => $langEn
          ]);
        }

        $id = \DB::table('privilege')->insertGetId([
          'symbol' => 'settings',
          'entity_privilege' => 0
        ]);

        if($langPl){
          \App\Models\PrivilegeLanguage::insert([
            'name' => 'Ustawienia',
            'privilege_id' => $id,
            'language_id' => $langPl
          ]);
        }

        if($langEn){
          \App\Models\PrivilegeLanguage::insert([
            'name' => 'Settings',
            'privilege_id' => $id,
            'language_id' => $langEn
          ]);
        }

        $id = \DB::table('privilege')->insertGetId([
          'symbol' => 'seo',
          'entity_privilege' => 0
        ]);

        if($langPl){
          \App\Models\PrivilegeLanguage::insert([
            'name' => 'Seo',
            'privilege_id' => $id,
            'language_id' => $langPl
          ]);
        }

        if($langEn){
          \App\Models\PrivilegeLanguage::insert([
            'name' => 'Seo',
            'privilege_id' => $id,
            'language_id' => $langEn
          ]);
        }
    }
}
