<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userId = \DB::table('users')->insertGetId([
          'email' => 'biuro@ibif.pl',
          'password' => '$2y$10$iO98jMCEBxBkO043M7FsIOelbbFM0JaxdDG8g4YLbhRdqHHposJ.O',
          'active' => 1,
          'for_developer' => 1,
          'first_name' => 'Paweł',
          'last_name' => 'Pawełczak',
          'role_id' => \DB::table('role')->where('symbol','=','developer')->pluck('id')->first(),
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now()
        ]);


        $modulesForUser = \DB::table('module')->where('need_user_priviledge','=',1)->get();

        if(count($modulesForUser)>0){
          foreach($modulesForUser as $module){
            \DB::table('user_module')->insert([
              'user_id' => $userId,
              'module_id' => $module->id
            ]);            
          }
        }

        $privileges = \DB::table('privilege')->get();
        if(count($privileges)>0){
          foreach($privileges as $privilege){
            \DB::table('user_privilege')->insert([
              'user_id' => $userId,
              'privilege_id' => $privilege->id
            ]);            
          }
        }
    }
}
