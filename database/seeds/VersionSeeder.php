<?php

use Illuminate\Database\Seeder;

class VersionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $component = new \App\Components\VersionUpdate(\Config::get('cms.update_url'));

      \App\Models\Version::insert([
        'version_number' => $component->getVersion()
      ]);      
    }
}
