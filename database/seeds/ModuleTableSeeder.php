<?php

use Illuminate\Database\Seeder;

class ModuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $langPl = \App\Models\Language::where('symbol','=','pl')->pluck('id')->first();
        $langEn = \App\Models\Language::where('symbol','=','en')->pluck('id')->first();

        $id = \App\Models\Module::insertGetId([
          'symbol' => 'galleryphoto',
          'for_page' => 1,
          'for_post' => 1,
          'for_category' => 1,
          'active' => 1,
          'bind_entity' => 0,
          'has_routing' => 0,
          'has_sitemap' => 0,
          'has_search' => 0,
          'need_user_priviledge' => 0,
          'module_entity_model' => 'App\Models\GalleryPhoto'
        ]);

        if($langPl){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Galeria zdjęć',
            'module_id' => $id,
            'language_id' => $langPl
          ]);
        }

        if($langEn){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Photo gallery',
            'module_id' => $id,
            'language_id' => $langEn
          ]);
        }


        //----------------------------------------------------------------------

        $id = \App\Models\Module::insertGetId([
          'symbol' => 'galleryvideo',
          'for_page' => 1,
          'for_post' => 1,
          'for_category' => 1,
          'active' => 1,
          'bind_entity' => 0,
          'has_routing' => 0,
          'has_sitemap' => 0,
          'has_search' => 0,
          'need_user_priviledge' => 0,
          'module_entity_model' => 'App\Models\GalleryVideo'
        ]);

        if($langPl){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Galeria wideo',
            'module_id' => $id,
            'language_id' => $langPl
          ]);
        }

        if($langEn){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Video gallery',
            'module_id' => $id,
            'language_id' => $langEn
          ]);
        }

        //----------------------------------------------------------------------

        $id = \App\Models\Module::insertGetId([
          'symbol' => 'offer',
          'for_page' => 1,
          'for_post' => 0,
          'for_category' => 0,
          'active' => 1,
          'bind_entity' => 0,
          'has_routing' => 0,
          'has_sitemap' => 1,
          'has_search' => 1,
          'need_user_priviledge' => 1,
          'module_entity_model' => 'App\Models\Offer'
        ]);

        if($langPl){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Oferta',
            'module_id' => $id,
            'language_id' => $langPl
          ]);
        }

        if($langEn){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Offer',
            'module_id' => $id,
            'language_id' => $langEn
          ]);
        }

        $fieldId = \App\Models\ModuleField::insertGetId([
          'module_id' => $id
        ]);

        if($langPl){
          \App\Models\ModuleFieldLanguage::insert([
            'name' => "Cena",
            'module_field_id' => $fieldId,
            'language_id' => $langPl
          ]);
        }

        if($langEn){
          \App\Models\ModuleFieldLanguage::insert([
            'name' => "Price",
            'module_field_id' => $fieldId,
            'language_id' => $langEn
          ]);
        }

        //----------------------------------------------------------------------

        $id = \App\Models\Module::insertGetId([
          'symbol' => 'download',
          'for_page' => 1,
          'for_post' => 0,
          'for_category' => 0,
          'active' => 1,
          'bind_entity' => 0,
          'has_routing' => 1,
          'has_sitemap' => 0,
          'has_search' => 0,
          'need_user_priviledge' => 0,
          'module_entity_model' => 'App\Models\Download'
        ]);

        if($langPl){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Pliki',
            'module_id' => $id,
            'language_id' => $langPl,
            'url' => 'download'
          ]);
        }

        if($langEn){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Download',
            'module_id' => $id,
            'language_id' => $langEn,
            'url' => 'download'
          ]);
        }

        //----------------------------------------------------------------------

        $id = \App\Models\Module::insertGetId([
          'symbol' => 'slider',
          'for_page' => 0,
          'for_post' => 0,
          'for_category' => 0,
          'active' => 1,
          'bind_entity' => 0,
          'has_routing' => 0,
          'has_sitemap' => 0,
          'has_search' => 0,
          'need_user_priviledge' => 1,
          'module_entity_model' => 'App\Models\Slider'
        ]);

        if($langPl){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Slider',
            'module_id' => $id,
            'language_id' => $langPl
          ]);
        }

        if($langEn){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Slider',
            'module_id' => $id,
            'language_id' => $langEn
          ]);
        }

        //----------------------------------------------------------------------

        $id = \App\Models\Module::insertGetId([
          'symbol' => 'popup',
          'for_page' => 0,
          'for_post' => 0,
          'for_category' => 0,
          'active' => 1,
          'bind_entity' => 0,
          'has_routing' => 0,
          'has_sitemap' => 0,
          'has_search' => 0,
          'need_user_priviledge' => 1,
          'module_entity_model' => 'App\Models\Popup'
        ]);

        if($langPl){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Popup',
            'module_id' => $id,
            'language_id' => $langPl,
            'url' => 'popup'
          ]);
        }

        if($langEn){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Popup',
            'module_id' => $id,
            'language_id' => $langEn,
            'url' => 'popup'
          ]);
        }

        //----------------------------------------------------------------------

        $id = \App\Models\Module::insertGetId([
          'symbol' => 'category',
          'for_page' => 0,
          'for_post' => 0,
          'for_category' => 0,
          'active' => 1,
          'bind_entity' => 1,
          'has_routing' => 0,
          'has_sitemap' => 0,
          'has_search' => 0,
          'need_user_priviledge' => 0,
          'module_entity_model' => 'App\Models\Category'
        ]);

        if($langPl){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Kategoria',
            'module_id' => $id,
            'language_id' => $langPl,
            'url' => 'popup'
          ]);
        }

        if($langEn){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Category',
            'module_id' => $id,
            'language_id' => $langEn,
            'url' => 'popup'
          ]);
        }

        //----------------------------------------------------------------------

        $id = \App\Models\Module::insertGetId([
          'symbol' => 'sitemap',
          'for_page' => 0,
          'for_post' => 0,
          'for_category' => 0,
          'active' => 1,
          'bind_entity' => 0,
          'has_routing' => 0,
          'has_sitemap' => 0,
          'has_search' => 0,
          'need_user_priviledge' => 0,
          'module_entity_model' => 'App\Models\Sitemap'
        ]);

        if($langPl){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Mapa strony',
            'module_id' => $id,
            'language_id' => $langPl,
            'url' => 'sitemap'
          ]);
        }

        if($langEn){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Sitemap',
            'module_id' => $id,
            'language_id' => $langEn,
            'url' => 'sitemap'
          ]);
        }

        //----------------------------------------------------------------------

        $id = \App\Models\Module::insertGetId([
          'symbol' => 'language',
          'for_page' => 0,
          'for_post' => 0,
          'for_category' => 0,
          'active' => 1,
          'bind_entity' => 0,
          'has_routing' => 0,
          'has_sitemap' => 0,
          'has_search' => 0,
          'need_user_priviledge' => 1,
          'module_entity_model' => 'App\Models\Language'
        ]);

        if($langPl){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Języki',
            'module_id' => $id,
            'language_id' => $langPl,
            'url' => 'language'
          ]);
        }

        if($langEn){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Languages',
            'module_id' => $id,
            'language_id' => $langEn,
            'url' => 'language'
          ]);
        }

        //----------------------------------------------------------------------

        $id = \App\Models\Module::insertGetId([
          'symbol' => 'user',
          'for_page' => 0,
          'for_post' => 0,
          'for_category' => 0,
          'active' => 1,
          'bind_entity' => 0,
          'has_routing' => 0,
          'has_sitemap' => 0,
          'has_search' => 0,
          'need_user_priviledge' => 1,
          'module_entity_model' => 'App\Models\User'
        ]);

        if($langPl){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Użytkownicy',
            'module_id' => $id,
            'language_id' => $langPl,
            'url' => 'user'
          ]);
        }

        if($langEn){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Users',
            'module_id' => $id,
            'language_id' => $langEn,
            'url' => 'user'
          ]);
        }

        //----------------------------------------------------------------------

        $id = \App\Models\Module::insertGetId([
          'symbol' => 'settings',
          'for_page' => 0,
          'for_post' => 0,
          'for_category' => 0,
          'active' => 1,
          'bind_entity' => 0,
          'has_routing' => 0,
          'has_sitemap' => 0,
          'has_search' => 0,
          'need_user_priviledge' => 0,
          'module_entity_model' => 'App\Models\Settings'
        ]);

        if($langPl){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Ustawienia',
            'module_id' => $id,
            'language_id' => $langPl,
            'url' => 'settings'
          ]);
        }

        if($langEn){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Settings',
            'module_id' => $id,
            'language_id' => $langEn,
            'url' => 'settings'
          ]);
        }

        //----------------------------------------------------------------------

        $id = \App\Models\Module::insertGetId([
          'symbol' => 'search',
          'for_page' => 1,
          'for_post' => 0,
          'for_category' => 0,
          'active' => 1,
          'bind_entity' => 0,
          'has_routing' => 0,
          'has_sitemap' => 0,
          'has_search' => 0,
          'need_user_priviledge' => 0,
          'module_entity_model' => 'App\Models\Search'
        ]);

        if($langPl){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Wyszukiwarka',
            'module_id' => $id,
            'language_id' => $langPl,
            'url' => NULL
          ]);
        }

        if($langEn){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Settings',
            'module_id' => $id,
            'language_id' => $langEn,
            'url' => NULL
          ]);
        }

        //----------------------------------------------------------------------

        $id = \App\Models\Module::insertGetId([
          'symbol' => 'content',
          'for_page' => 1,
          'for_post' => 1,
          'for_category' => 1,
          'active' => 1,
          'bind_entity' => 0,
          'has_routing' => 0,
          'has_sitemap' => 0,
          'has_search' => 0,
          'need_user_priviledge' => 1,
          'module_entity_model' => 'App\Models\Content'
        ]);

        if($langPl){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Treść',
            'module_id' => $id,
            'language_id' => $langPl,
            'url' => NULL
          ]);
        }

        if($langEn){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Content',
            'module_id' => $id,
            'language_id' => $langEn,
            'url' => NULL
          ]);
        }

        //----------------------------------------------------------------------

        $id = \App\Models\Module::insertGetId([
          'symbol' => 'tag',
          'for_page' => 0,
          'for_post' => 0,
          'for_category' => 0,
          'active' => 1,
          'bind_entity' => 0,
          'has_routing' => 0,
          'has_sitemap' => 1,
          'has_search' => 0,
          'need_user_priviledge' => 1,
          'module_entity_model' => 'App\Models\Tag'
        ]);

        if($langPl){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Tagi',
            'module_id' => $id,
            'language_id' => $langPl,
            'url' => NULL
          ]);
        }

        if($langEn){
          \App\Models\ModuleLanguage::insert([
            'name' => 'Tags',
            'module_id' => $id,
            'language_id' => $langEn,
            'url' => NULL
          ]);
        }

      //------------------------------------------------------------------------

      $id = \App\Models\Module::insertGetId([
        'symbol' => 'menu',
        'for_page' => 0,
        'for_post' => 0,
        'for_category' => 0,
        'active' => 1,
        'bind_entity' => 0,
        'has_routing' => 0,
        'has_sitemap' => 0,
        'has_search' => 0,
        'need_user_priviledge' => 0,
        'module_entity_model' => 'App\Models\Menu'
      ]);

      if($langPl){
        \App\Models\ModuleLanguage::insert([
          'name' => 'Menu',
          'module_id' => $id,
          'language_id' => $langPl,
          'url' => NULL
        ]);
      }

      if($langEn){
        \App\Models\ModuleLanguage::insert([
          'name' => 'Menu',
          'module_id' => $id,
          'language_id' => $langEn,
          'url' => NULL
        ]);
      }

      //------------------------------------------------------------------------

      $id = \App\Models\Module::insertGetId([
        'symbol' => 'productoffer',
        'for_page' => 1,
        'for_post' => 0,
        'for_category' => 0,
        'active' => 1,
        'bind_entity' => 0,
        'has_routing' => 0,
        'has_sitemap' => 0,
        'has_search' => 0,
        'need_user_priviledge' => 0,
        'module_entity_model' => 'App\Models\Productoffer'
      ]);

      if($langPl){
        \App\Models\ModuleLanguage::insert([
          'name' => 'Produkty oferty',
          'module_id' => $id,
          'language_id' => $langPl,
          'url' => NULL
        ]);
      }

      if($langEn){
        \App\Models\ModuleLanguage::insert([
          'name' => 'Offer products',
          'module_id' => $id,
          'language_id' => $langEn,
          'url' => NULL
        ]);
      }

      //------------------------------------------------------------------------

      $id = \App\Models\Module::insertGetId([
        'symbol' => 'news',
        'for_page' => 1,
        'for_post' => 0,
        'for_category' => 0,
        'active' => 1,
        'bind_entity' => 0,
        'has_routing' => 0,
        'has_sitemap' => 0,
        'has_search' => 0,
        'need_user_priviledge' => 0,
        'module_entity_model' => 'App\Models\Category'
      ]);

      if($langPl){
        \App\Models\ModuleLanguage::insert([
          'name' => 'Aktualności',
          'module_id' => $id,
          'language_id' => $langPl,
          'url' => NULL
        ]);
      }

      if($langEn){
        \App\Models\ModuleLanguage::insert([
          'name' => 'News',
          'module_id' => $id,
          'language_id' => $langEn,
          'url' => NULL
        ]);
      }

      //------------------------------------------------------------------------

      $id = \App\Models\Module::insertGetId([
        'symbol' => 'galleryphotolist',
        'for_page' => 1,
        'for_post' => 0,
        'for_category' => 0,
        'active' => 1,
        'bind_entity' => 0,
        'has_routing' => 0,
        'has_sitemap' => 0,
        'has_search' => 0,
        'need_user_priviledge' => 0,
        'module_entity_model' => 'App\Models\GalleryList'
      ]);

      if($langPl){
        \App\Models\ModuleLanguage::insert([
          'name' => 'Lista gallerii zdjęć',
          'module_id' => $id,
          'language_id' => $langPl,
          'url' => NULL
        ]);
      }

      if($langEn){
        \App\Models\ModuleLanguage::insert([
          'name' => 'Photo gallery list',
          'module_id' => $id,
          'language_id' => $langEn,
          'url' => NULL
        ]);
      }
  }
}
