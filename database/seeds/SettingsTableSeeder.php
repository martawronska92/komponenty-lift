<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = ['admin.settings_group.contact','admin.settings_group.meta_tag',
          'admin.settings_group.map',
          'admin.settings_group.analytics',
          'admin.settings_group.social_media',
          'admin.settings_group.images',
          'admin.settings_group.videos',
          'admin.settings_group.other'];
        $symbols = ['contact','meta_tag','map','analytics','social_media','images','videos','other'];
        $groupsIds = [];

        foreach($groups as $key=>$group){
          $id = \DB::table('settings_group')->insertGetId([
            'name'=>$group,
            'symbol'=>$symbols[$key],
            'position'=>$key
          ]);
          array_push($groupsIds,$id);
        }

        $settings = [
          //contact
          [
            ['name'=>'admin.settings.company_name','value'=>'','symbol'=>'contact_name','type'=>'text','required'=>0],
            ['name'=>'admin.settings.street','value'=>'','symbol'=>'contact_street','type'=>'text','required'=>0],
            ['name'=>'admin.settings.homenr','value'=>'','symbol'=>'contact_homenr','type'=>'text','required'=>0],
            ['name'=>'admin.settings.localnr','value'=>'','symbol'=>'contact_localnr','type'=>'text','required'=>0],
            ['name'=>'admin.settings.postcode','value'=>'','symbol'=>'contact_postcode','type'=>'text','required'=>0],
            ['name'=>'admin.settings.city','value'=>'','symbol'=>'contact_city','type'=>'text','required'=>0],
            ['name'=>'admin.settings.phone','value'=>'','symbol'=>'contact_phone','type'=>'text','required'=>0],
            ['name'=>'admin.settings.mobile_phone','value'=>'','symbol'=>'contact_mobile_phone','type'=>'text','required'=>0],
            ['name'=>'admin.settings.fax','value'=>'','symbol'=>'contact_fax','type'=>'text','required'=>0],
            ['name'=>'admin.settings.email','value'=>'','symbol'=>'contact_email','type'=>'email','required'=>0],
            ['name'=>'admin.settings.nip','value'=>'','symbol'=>'contact_nip','type'=>'text','required'=>0],
            ['name'=>'admin.settings.regon','value'=>'','symbol'=>'contact_regon','type'=>'text','required'=>0],
            ['name'=>'admin.settings.bank_account_nr','value'=>'','symbol'=>'contact_bank_account_nr','type'=>'text','required'=>0],
            ['name'=>'admin.settings.notification_email','value'=>'','symbol'=>'contact_notification_email','type'=>'email','required'=>0]
          ],
          //meta tag
          [
            ['name'=>'admin.settings.meta_title','value'=>'','symbol'=>'meta_title','type'=>'text','required'=>0],
            ['name'=>'admin.settings.meta_description','value'=>'','symbol'=>'meta_description','type'=>'text','required'=>0]
          ],
          //maps
          [
            ['name'=>'admin.settings.maplink','value'=>'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2497.3244947084304!2d22.508449115504767!3d51.24993447959366!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x472259c6357a86b9%3A0x52399db17255221f!2sIBIF!5e0!3m2!1spl!2spl!4v1493129066065" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>','symbol'=>'map_link','type'=>'text','required'=>0],
            ['name'=>'admin.settings.lat','value'=>'','symbol'=>'map_lat','type'=>'text','required'=>0],
            ['name'=>'admin.settings.lng','value'=>'','symbol'=>'map_lng','type'=>'text','required'=>0],
          ],
          //analytics
          [
            ['name'=>'admin.settings.analytics_script','value'=>'','symbol'=>'analytics','type'=>'text','required'=>0],
            ['name'=>'admin.settings.analytics_page_view_id','value'=>'','symbol'=>'page_view_id','type'=>'text','required'=>0],            
          ],
          //social
          [
            ['name'=>'admin.settings.social_media_fb','value'=>'','symbol'=>'facebook_link','type'=>'text','required'=>0],
            ['name'=>'admin.settings.social_media_yt','value'=>'','symbol'=>'youtube_link','type'=>'text','required'=>0],
            ['name'=>'admin.settings.social_media_twitter','value'=>'','symbol'=>'twitter_link','type'=>'text','required'=>0],
            ['name'=>'admin.settings.social_media_instagram','value'=>'','symbol'=>'instagram_link','type'=>'text','required'=>0],
            ['name'=>'admin.settings.social_media_gp','value'=>'','symbol'=>'googleplus_link','type'=>'text','required'=>0],
            ['name'=>'admin.settings.social_media_snapchat','value'=>'','symbol'=>'snapchat_link','type'=>'text','required'=>0],
            ['name'=>'admin.settings.social_media_linkedin','value'=>'','symbol'=>'linkedin_link','type'=>'text','required'=>0],
            ['name'=>'admin.settings.social_media_pinterest','value'=>'','symbol'=>'pinterest_link','type'=>'text','required'=>0],
            ['name'=>'admin.settings.social_media_tumblr','value'=>'','symbol'=>'tumblr_link','type'=>'text','required'=>0],
            ['name'=>'admin.settings.social_media_flickr','value'=>'','symbol'=>'flickr_link','type'=>'text','required'=>0],
            ['name'=>'admin.settings.social_media_vine','value'=>'','symbol'=>'vine_link','type'=>'text','required'=>0]
          ],
          //images
          [
            ['name'=>'admin.settings.post_thumb_width','value'=>'300','symbol'=>'post_thumb_width','type'=>'number','required'=>1],
            ['name'=>'admin.settings.post_thumb_height','value'=>'200','symbol'=>'post_thumb_height','type'=>'number','required'=>1],
            ['name'=>'admin.settings.category_thumb_width','value'=>'300','symbol'=>'category_thumb_width','type'=>'number','required'=>1],
            ['name'=>'admin.settings.category_thumb_height','value'=>'200','symbol'=>'category_thumb_height','type'=>'number','required'=>1],
            ['name'=>'admin.settings.popup_thumb_width','value'=>'300','symbol'=>'popup_thumb_width','type'=>'number','required'=>1],
            ['name'=>'admin.settings.popup_thumb_height','value'=>'200','symbol'=>'popup_thumb_height','type'=>'number','required'=>1],
            ['name'=>'admin.settings.slider_homepage_thumb_width','value'=>'800','symbol'=>'slider_homepage_thumb_width','type'=>'number','required'=>1],
            ['name'=>'admin.settings.slider_homepage_thumb_height','value'=>'300','symbol'=>'slider_homepage_thumb_height','type'=>'number','required'=>1],
            ['name'=>'admin.settings.slider_subpage_thumb_width','value'=>'700','symbol'=>'slider_subpage_thumb_width','type'=>'number','required'=>1],
            ['name'=>'admin.settings.slider_subpage_thumb_height','value'=>'200','symbol'=>'slider_subpage_thumb_height','type'=>'number','required'=>1],
            ['name'=>'admin.settings.galleryphoto_thumb_width','value'=>'200','symbol'=>'galleryphoto_thumb_width','type'=>'number','required'=>1],
            ['name'=>'admin.settings.galleryphoto_thumb_height','value'=>'100','symbol'=>'galleryphoto_thumb_height','type'=>'number','required'=>1],
            ['name'=>'admin.settings.galleryphoto_normal_width','value'=>'1024','symbol'=>'galleryphoto_normal_width','type'=>'number','required'=>1],
            ['name'=>'admin.settings.galleryphoto_normal_height','value'=>'768','symbol'=>'galleryphoto_normal_height','type'=>'number','required'=>1]
          ],
          //videos
          [
            ['name'=>'admin.settings.video_content_width','value'=>'500','symbol'=>'video_content_width','type'=>'number','required'=>1],
            ['name'=>'admin.settings.video_content_height','value'=>'300','symbol'=>'video_content_height','type'=>'number','required'=>1],
            ['name'=>'admin.settings.video_slider_width','value'=>'500','symbol'=>'video_slider_width','type'=>'number','required'=>1],
            ['name'=>'admin.settings.video_slider_height','value'=>'300','symbol'=>'video_slider_height','type'=>'number','required'=>1],
            ['name'=>'admin.settings.video_playlist_width','value'=>'500','symbol'=>'video_playlist_width','type'=>'number','required'=>1],
            ['name'=>'admin.settings.video_playlist_height','value'=>'300','symbol'=>'video_playlist_height','type'=>'number','required'=>1],
            ['name'=>'admin.settings.video_gallery_width','value'=>'500','symbol'=>'video_gallery_width','type'=>'number','required'=>1],
            ['name'=>'admin.settings.video_gallery_height','value'=>'300','symbol'=>'video_gallery_height','type'=>'number','required'=>1],
            ['name'=>'admin.settings.video_popup_width','value'=>'500','symbol'=>'video_popup_width','type'=>'number','required'=>1],
            ['name'=>'admin.settings.video_popup_height','value'=>'300','symbol'=>'video_popup_height','type'=>'number','required'=>1],
          ],
          //rest
          [
            ['name'=>'admin.settings.show_download_pdf','value'=>'','symbol'=>'show_download_pdf','type'=>'checkbox','required'=>0]
          ]
        ];

        foreach($settings as $key=>$group){
          foreach($group as $setting){
            $params = [
              'name'=>$setting['name'],
              'value'=>$setting['value'],
              'symbol'=>$setting['symbol'],
              'type'=>$setting['type'],
              'required'=>$setting['required'],
              'settings_group_id'=>$groupsIds[$key],
            ];
            if(array_key_exists('options',$setting)){
              $params['options'] = $setting['options'];
            }
            \DB::table('settings')->insert($params);
          }
        }

        /**
         * SUBGROUPS
         */

         $groups = [
            ['admin.settings_group.image_post','admin.settings_group.image_popup',
            'admin.settings_group.image_slider', 'admin.settings_group.image_gallery_photo',
            'admin.settings_group.image_category'],
            ['admin.settings_group.video_content','admin.settings_group.video_slider',
            'admin.settings_group.video_playlist','admin.settings_group.video_gallery',
            'admin.settings_group.video_popup']
          ];
         $symbols = [
            ['image_post','image_popup','image_slider','image_gallery_photo','image_category'],
            ['video_content','video_slider','video_playlist','video_gallery','video_popup']
          ];
         $settings = [
           [
             ['post_thumb_width','post_thumb_height'],
             ['popup_thumb_width','popup_thumb_height'],
             ['slider_homepage_thumb_width','slider_homepage_thumb_height','slider_subpage_thumb_width','slider_subpage_thumb_height'],
             ['galleryphoto_thumb_width','galleryphoto_thumb_height','galleryphoto_normal_width','galleryphoto_normal_height'],
             ['category_thumb_width','category_thumb_height'],
           ],
           [
             ['video_content_width','video_content_height'],
             ['video_slider_width','video_slider_height'],
             ['video_playlist_width','video_playlist_height'],
             ['video_gallery_width','video_gallery_height'],
             ['video_popup_width','video_popup_height']
           ]
         ];

         foreach($groups as $outerKey=>$singleGroup){
           foreach($singleGroup as $innerKey=>$group){
             $id = \DB::table('settings_group')->insertGetId([
               'name'=>$group,
               'symbol'=>$symbols[$outerKey][$innerKey],
               'position'=>$key,
               'subgroup'=>1
             ]);
             for($i=0; $i<count($settings[$outerKey][$innerKey]);$i++){
                 \DB::table('settings')
                    ->where('symbol','=',$settings[$outerKey][$innerKey][$i])
                    ->update(['settings_subgroup_id' => $id]);
             }
           }
         }

         /**
          * SETTINGS LANGUAGE
          */

         $langs = \App\Models\Language::getActive();
         foreach($langs as $lang){
           //$metaId = \App\Models\Settings::where('symbol','=','')
         }

    }
}
