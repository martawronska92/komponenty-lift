<?php

use Illuminate\Database\Seeder;

class GalleryShowTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\GalleryShowType::insert([
          'gallery_type' => 'video',
          'symbol' => 'slider'
        ]);

        \App\Models\GalleryShowType::insert([
          'gallery_type' => 'photo',
          'symbol' => 'lightbox'
        ]);

        \App\Models\GalleryShowType::insert([
          'gallery_type' => 'photo',
          'symbol' => 'lightslider'
        ]);

        \App\Models\GalleryShowType::insert([
          'gallery_type' => 'photo',
          'symbol' => 'image_gallery'
        ]);

        \App\Models\GalleryShowType::insert([
          'gallery_type' => 'video',
          'symbol' => 'playlist'
        ]);

        \App\Models\GalleryShowType::insert([
          'gallery_type' => 'video',
          'symbol' => 'video_gallery'
        ]);
    }
}
