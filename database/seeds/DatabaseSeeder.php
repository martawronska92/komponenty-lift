<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(GalleryShowTypeSeeder::class);
        $this->call(LanguageTableSeeder::class);
        $this->call(ModuleTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(PrivilegeTableSeeder::class);
        $this->call(UsersTableSeeder::class);
    }
}
