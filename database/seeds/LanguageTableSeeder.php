<?php

use Illuminate\Database\Seeder;

class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('language')->insert([
          'name' => 'polski',
          'symbol' => 'pl',
          'active' => 1,
          'visible_on_front' => 1,
          'main' => 1,
          'domain' => null
        ]);

        \DB::table('language')->insert([
          'name' => 'angielski',
          'symbol' => 'en',
          'active' => 1,
          'visible_on_front' => 1,
          'main' => 0,
          'domain' => null
        ]);
    }
}
