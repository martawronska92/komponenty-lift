<?php

  return [
      /**
       * domyślna ilość wierszy na stronie w adminie
       */
      'admin_per_page' => 3,
      /**
       * maksymalna ilość odbiorców w newsletterze
       */
      'newsletter_max_recipients' => 100,
      /**
       * czy cron jest dostępny,
       */
      'cron_enabled' => 0,
      /**
       * czy jest podpiętych wiele domen językowych do tego samego systemu
       */
      'lang_per_domain' => 0,
      /**
       * ilość wyszukiwań na stronę
       */
      'search_per_page' => 2,
      /**
       * ilość tagów na stronę
       */
      'tag_per_page' => 1,
      /**
       *  czas sessji admina w sekundach,
       */
      'admin_timeout' => 1800,
      /**
       * poziom zagnieżdżenia stron
       */
      'page_level' => 3,
      /**
       * url do pobierania i sprawdzania aktualizacji
       */
      'update_host' => 'www.cms.local',
      /**
      * token do żądań api aktualizacji
      */
      'api_token' => 'KOJYoYIxHzXi9vIc4QVAjvIttVBSblCv4VpAYESbHmc9M6Dcu74beXdX3Kxg',
      /**
      * token api dla zapytań dla youtube
      */
      'youtube_api_token' => 'AIzaSyBz5Q_ySOAJQQ7hsKE-MGBnjk8Rp3tZAJM'
  ];
