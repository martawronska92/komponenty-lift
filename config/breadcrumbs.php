<?php

$urlParts = explode("/",ltrim($_SERVER['REQUEST_URI'],"/"));

return [

	'view' => count($urlParts)>0 && $urlParts[0] == 'panel' ? 'admin.partial.breadcrumbs' : 'front.partial.breadcrumbs_template'//'breadcrumbs::bootstrap3',

];
