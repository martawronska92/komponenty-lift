<?php

return

    [
        /*
         * The siteId is used to retrieve and display Google Analytics statistics
         * in the admin-section.
         *
         * Should look like: ga:xxxxxxxx.
         * To jest id widoku z ga:
         */
        'siteId' => "ga:91295650",//env('ANALYTICS_SITE_ID'), - noclegideblin.pl

        /*
         * Set the client id
         *
         * Should look like:
         * xxxxxxxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.apps.googleusercontent.com
         */
        //'clientId' => "test-862@tidy-visitor-156714.iam.gserviceaccount.com",//env('ANALYTICS_CLIENT_ID'),
        'clientId' => "520184973053-sg1sklovbneunbf7bbuhe2cnfshrnbjb.apps.googleusercontent.com",
         /*
         * Set the service account name
         *
         * Should look like:
         * xxxxxxxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx@developer.gserviceaccount.com
         */
        //'serviceEmail' => 'test-862@tidy-visitor-156714.iam.gserviceaccount.com',//env('ANALYTICS_SERVICE_EMAIL'),
        //'serviceEmail' => 'piotrek.ibif@gmail.com',
        'serviceEmail' => '520184973053-compute@developer.gserviceaccount.com',

        /*
         * You need to download a p12-certifciate from the Google API console
         * Be sure to store this file in a secure location.
         */
        'certificatePath' => storage_path('laravel-analytics/00988d01973b.p12'),
        /*
         * The amount of minutes the Google API responses will be cached.
         * If you set this to zero, the responses won't be cached at all.
         */
        'cacheLifetime' => 60 * 24,

        /*
         * The amount of seconds the Google API responses will be cached for
         * queries that use the real time query method. If you set this to zero,
         * the responses of real time queries won't be cached at all.
         */
        'realTimeCacheLifetimeInSeconds' => 5,
    ];
