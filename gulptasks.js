const exec = require('child_process').exec;
const Task = Elixir.Task;
const fs = require('fs');
var elixir = require('laravel-elixir');
require('laravel-elixir-fonts');
require('laravel-elixir-jade');
var browserSync = require('laravel-elixir-browsersync-official');

var gulp = require('gulp');
var iconfont = require('gulp-iconfont');
var iconfontCss = require('gulp-iconfont-css');
var newer = require('gulp-newer');
var plumber = require('gulp-plumber');
var replace = require('gulp-replace');
var rename = require("gulp-rename");


exports.tasks = function (elixir) {

    elixir.extend('assets', function () {
        new Task('assets', function () {
            var version = new Date().valueOf();
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            for (var i = 0; i < 10; i++)
                version += possible.charAt(Math.floor(Math.random() * possible.length));
            version = version.split('').sort(function () {
                return 0.5 - Math.random()
            }).join('');
            fs.writeFile('config/assets.php', '<?php return [ "version" => "' + version + '"];');
            // exec('ping localhost', function (err, stdout, stderr) {
            //   console.log(stdout);
            //   //console.log(stderr);
            //   //console.log(err);
            // });
        }).watch(['./public/js/**', './public/css/**']);
    });

    elixir(function (mix) {
        mix.jade({
            baseDir: './',
            blade: true,
            html: false,
            dest: 'resources/views/',
            pretty: true,
            search: '**/*.jade',
            src: 'html/templates/',
            jadephp: false
        });
    });

    elixir.extend('jadeHandlebars', function () {
        new Task('jadeHandlebars', function () {
            var options = {
                logs: {
                    enabled: true
                }
            };
            return gulp.src(['./html/templates/front/category/partials/one_news.jade'], {base: './'})
                .pipe(replace('{{$post->language[0]->title}}', '{{title}}', options))
                .pipe(replace('{{$post->language[0]->content}}', '{{content}}', options))
                .pipe(replace('{{$url}}', '{{link}}', options))
                .pipe(replace('{{$post->publish_date!="" ? $post->publish_date->format("d.m.Y") : $post->created_at->format("d.m.Y")}}', '{{date}}{{year}}', options))
                .pipe(replace('{{$post->getImage()}}', '{{thumb}}', options))
                .pipe(rename("_newsItem.jade"))
                .pipe(gulp.dest('./html/templates/front/partial/handlebars'));
        }).watch(['./html/templates/front/category/partials/one_news.jade']);
    });


    elixir(function (mix) {
        mix.browserSync(
            {
                proxy: "jadecms/",
                open: false
            });
    });

    var fontName = "myfont";
    elixir.extend('iconfont', function () {
        new Task('iconfont', function () {
            return gulp.src(["./html/svg/*.svg"], {base: './'})
                .pipe(newer('./public/fonts/'))
                .pipe(plumber())
                .pipe(iconfontCss({
                    fontName: fontName,
                    path: './html/sass/_icons.scss',
                    targetPath: "../../html/sass/components/_svgicons.scss",
                    fontPath: './public/fonts/'
                }))
                .pipe(plumber())
                .pipe(iconfont({
                    fontName: fontName,
                    prependUnicode: true,
                    formats: ['ttf', 'eot', 'woff', 'woff2'],
                    normalize: true,
                    fontHeight: 1001
                }))
                .pipe(gulp.dest('./public/fonts/'));
        }).watch(['./html/svg/*.svg']);
    });

    elixir(function (mix) {
        mix.fonts('./resources/assets/iconfonts/*.svg', './public/fonts/', {
            css: {
                fontName: "iconfont",
                path: './resources/assets/sass/_iconSource.scss',
                targetPath: '../../resources/assets/sass/icons.scss',
                fontPath: '../fonts/'
            },
            font: {
                fontName: "iconfont",
                prependUnicode: false,
                formats: ['ttf', 'eot', 'woff', 'woff2'],
                normalize: true,
                fontHeight: 1001
            }
        });
    });

};
