
<script src="/js/app_front.js?v={{Config::get('assets.version')}}" defer="defer"></script>
<script src="/js/common.js?v={{Config::get('assets.version')}}" defer="defer"></script>
<script src="/js/front.js?v={{Config::get('assets.version')}}" defer="defer"></script>
@if(App::getLocale()!="en")

<script src="/bower_components/jquery-validation/src/localization/messages_{{App::getLocale()}}.js" defer="defer"></script>
@endif

{!!\App\Models\Popup::renderOnFront()!!}
{!!\App\Components\Cookies::renderOnFront()!!}
@yield('script')
