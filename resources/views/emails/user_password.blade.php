@extends('layouts.email')

@section('content')
  <table class="email-body_inner" align="center" width="570" cellpadding="10" cellspacing="10">
    <tr>
      <td>
        <p style="width:570px;text-align:center;">{!! trans('admin.user.user_password_email_header',['portal'=>$portalUrl]) !!}</p>
      </td>
    </tr>
    <tr>
      <td style="text-align:center;"><strong>{{$params['original_password']}}</strong></td>
    </tr>
    <tr>
      <td>
        <p style="width:570px;text-align:center;">{!! trans('admin.user.user_password_email_footer',['url'=>$loginUrl]) !!}</p>
      </td>
    </tr>
  </table>
@endsection
