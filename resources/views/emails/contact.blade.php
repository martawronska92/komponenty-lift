@extends('layouts.email')

@section('content')
  <table class="email-body_inner" align="center" width="570" cellpadding="10" cellspacing="10">
    <tr>
      <td colspan='2'>
        <p style="width:570px;text-align:center;">{{trans('front.contact.mail_content')}}</p>
      </td>
    </tr>
    <tr>
      <td>{{trans('front.contact.name')}}</td>
      <td>{{$params['name']}}</td>
    </tr>
    <tr>
      <td>{{trans('front.contact.email')}}</td>
      <td>{{$params['email']}}</td>
    </tr>
    <tr>
      <td>{{trans('front.contact.phone')}}</td>
      <td>{{$params['phone']}}</td>
    </tr>
    <tr>
      <td>{{trans('front.contact.message')}}</td>
      <td>{{$params['message']}}</td>
    </tr>
  </table>
@endsection
