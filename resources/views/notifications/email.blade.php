@extends('layouts.email')

@section('content')

                    <!-- Email Body -->
                    <tr>
                        <td class="email-body" width="100%">
                            <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="email-body_cell">
                                        <!-- Greeting -->
                                        <h1>{{trans('email.header')}}</h1>

                                        <!-- Intro -->
                                        <p>{{trans('email.reset_email_header')}}</p>

{{ dd(get_defined_vars()) }}

                                        <!-- Action Button -->
                                        @if (isset($actionText))
                                            <table class="body_action" align="center" width="100%" cellpadding="0" cellspacing="0">
                                              <tr>
                                                  <td align="center">
                                                    <p>{{trans('email.reset_email_click')}}</p>
                                                  </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <a href="{{ $actionUrl }}"
                                                            target="_blank">
                                                            {{ $actionUrl }}
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        @endif

                                        <!-- Outro -->
                                        <p>{{trans('email.reset_email_footer')}}</p>

                                        <!-- Salutation -->
                                        <p>
                                            {{trans('email.footer')}},<br>{{ config('app.name') }}
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
@endsection
