<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
  <head>@if($settings['analytics']!="")
    <script async="" src="https://www.googletagmanager.com/gtag/js?id={{$settings["analytics"]}}"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag() {
          dataLayer.push(arguments);
      }
      gtag('js', new Date());
      gtag('config', '{{$settings["analytics"]}}');
    </script>@endif
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@if(isset($meta_title)){{$meta_title}}@else{{$settings['meta_title']}}@endif</title>
    <meta name="description" content="@if(isset($meta_description)){{$meta_description}}@else{{$settings['meta_description']}}@endif">
    <link rel="canonical" href="@if(isset($canonical_url)){{$canonical_url}}@else{{Request::fullUrl()}}@endif">@include('front.partial.language_alternate',['langsToSwitch'=>\App\Models\Language::getFrontLanguages()])
    <!-- CSRF Token-->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Styles-->
    <link href="{{ elixir('css/app.css')}}?v={{Config::get('assets.version')}}" rel="stylesheet">
    <link href="{{ elixir('css/app_front.css')}}?v={{Config::get('assets.version')}}" rel="stylesheet">
    <link href="{{ elixir('css/front.css')}}?v={{Config::get('assets.version')}}" rel="stylesheet">@yield('head')
    <!-- Scripts-->
    <script>
      var currentLang = "{{\App::getLocale()}}";
      window.Laravel = <?php echo json_encode([
      'csrfToken' => csrf_token(),
      'lang' => trans('front_js')
      ],JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES); ?>;
    </script>
  </head>
  <body>
    <script>
      (function (d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s);
          js.id = id;
          js.src = 'https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.11';
          fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
      
      
    </script>{!!\App\Models\Menu::renderFromPages()!!}
    @include('front.partial.language_switch',['langsToSwitch'=>\App\Models\Language::getFrontLanguages()])
    @include('front.modules.search.search_box')
    {!!\App\Models\Slider::renderOnFront()!!}
    @yield('content')
    <!-- Scripts-->
    <script src="{{ elixir('js/app_front.js') }}?v={{\Config::get('assets.version')}}"></script>
    <script src="{{ elixir('js/common.js') }}?v={{\Config::get('assets.version')}}"></script>
    <script src="{{ elixir('js/front.js') }}?v={{\Config::get('assets.version')}}"></script>@if(\App::getLocale()!="en")
    <script src="/bower_components/jquery-validation/src/localization/messages_{{App::getLocale()}}.js"></script>@endif
    {!!\App\Models\Popup::renderOnFront()!!}
    {!!\App\Components\Cookies::renderOnFront()!!}
    @yield('script')
  </body>
</html>