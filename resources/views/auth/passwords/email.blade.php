@extends('layouts.login')

<!-- Main Content -->
@section('content')
  <div class="adminLogin">
    <div class="adminLogin__form">
      <div class="adminLogin__head"><img src="../img/panel/ibif_logo.png" alt=""></div>

      <div class="adminLogin__main">
        <div class="adminLogin__title">{{trans('auth.reset_header')}}</div>

        @if (session('status'))
          <div class="resultsChanges resultsChanges--success">
            {{ session('status') }}
          </div>
        @endif

        <form class="form-horizontal" role="form" method="POST" action="{{ route('password_email') }}">
          {{ csrf_field() }}

          <div class="universalForm__row">
            <div class="inputBlock {{ $errors->has('email') ? ' has-error' : '' }}">
              <input id="email" type="text" class="inputBlock__field" name="email" placeholder="{{trans('auth.e-mail')}}" value="{{ old('email') }}" required>
            </div>
            @if ($errors->has('email'))
              <div class="inputBlock__error">
                {{ $errors->first('email') }}
              </div>
            @endif
          </div>

          <div class="universalForm__row universalForm__row--mt15 universalForm__row--cols2 universalForm__row--between">
            <div class="universalForm__col">
              <div class="inputBlock">
                <button type="submit" class="button button--small button--blue inputBlock__submit">{{trans('auth.reset_button')}}</button>
              </div>
            </div>
            <div class="universalForm__col">
              <a href="/panel" class="button button--small button--gray">
                {{trans('front.back')}}
              </a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
