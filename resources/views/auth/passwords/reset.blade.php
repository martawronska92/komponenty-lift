@extends('layouts.login')

@section('content')

  <div class="adminLogin">
    <div class="adminLogin__form">
      <div class="adminLogin__head"><img src="../../img/panel/ibif_logo.png" alt=""></div>

      <div class="adminLogin__main">
        <div class="adminLogin__title">{{trans('auth.reset_header')}}</div>

        @if (session('status'))
          <div class="resultsChanges resultsChanges--success">
            {{ session('status') }}
          </div>
        @endif

        <form class="form-horizontal" role="form" method="POST" action="{{ route('password_reset') }}">
          {{ csrf_field() }}
          <input type="hidden" name="token" value="{{ $token }}">

          <div class="universalForm__row">
            <div class="inputBlock {{ $errors->has('email') ? ' has-error' : '' }}">
              <input id="email" type="text" class="inputBlock__field" name="email" placeholder="{{trans('auth.e-mail')}}" value="{{ $email or old('email') }}" required>
            </div>
            @if ($errors->has('email'))
              <div class="inputBlock__error">
                {{ $errors->first('email') }}
              </div>
            @endif
          </div>


          <div class="universalForm__row">
            <div class="inputBlock {{ $errors->has('password') ? ' has-error' : '' }}">
              <input id="email" type="password" class="inputBlock__field" name="password" placeholder="{{trans('auth.password')}}" required>
            </div>
            @if ($errors->has('password'))
              <div class="inputBlock__error">
                {{ $errors->first('password') }}
              </div>
            @endif
          </div>


          <div class="universalForm__row">
            <div class="inputBlock {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
              <input id="email" type="password" class="inputBlock__field" name="password_confirmation" placeholder="{{trans('auth.password_confirm')}}" required>
            </div>
            @if ($errors->has('password_confirmation'))
              <div class="inputBlock__error">
                {{ $errors->first('password_confirmation') }}
              </div>
            @endif
          </div>



          <div class="universalForm__row universalForm__row--mt15 universalForm__row--cols2 universalForm__row--between">
            <div class="universalForm__col">
              <div class="inputBlock">
                <button type="submit" class="button button--small button--blue">
                  {{trans('auth.reset_password')}}
                </button>
              </div>
            </div>
            <div class="universalForm__col">
              <a href="/panel" class="button button--small button--gray">
                {{trans('front.back')}}
              </a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
