@extends('layouts.login')
<style>
  .navbar {
    display: none;
  }
</style>
@section('content')

  <div class="adminLogin">
    <div class="adminLogin__form">
      <div class="adminLogin__head"><img src="../img/panel/ibif_logo.png" alt=""></div>

      <div class="adminLogin__main">
        <div class="adminLogin__title">{{trans('auth.login_header')}}</div>


      @if(isset($MessageBox))
        @include('admin.components.messagebox',['messagebox'=>$MessageBox])
      @endif


        <form class="universalForm" role="form" method="POST" action="{{ route('login') }}">
          {{ csrf_field() }}
          <div class="universalForm__row">
            <div class="inputBlock {{ $errors->has('email') ? ' has-error' : '' }}">
              <input id="email" type="text" class="inputBlock__field" name="email" placeholder="{{trans('auth.e-mail')}}" value="{{ old('email') }}" required>
            </div>
            @if ($errors->has('email'))
              <div class="inputBlock__error">
                {{ $errors->first('email') }}
              </div>
            @endif
          </div>

          <div class="universalForm__row {{ $errors->has('password') ? ' has-error' : '' }}">
            <div class="inputBlock">
              <input id="password" type="password" class="inputBlock__field" placeholder="{{trans('auth.password')}}" name="password" required>
            </div>
          </div>
          @if ($errors->has('password'))
            <div class="inputBlock__error">
              {{ $errors->first('password') }}
            </div>
          @endif

          <div class="universalForm__row universalForm__row--cols2 universalForm__row--between universalForm__row--margin15">
            <div class="universalForm__col">
              <div class="inputBlock__checkbox">
                <input type="checkbox" name="remember" id="remember">
                  <label for="remember" class="inputBlock__checkboxText"><i></i> <span>{{trans('auth.remember')}}</span></label>
              </div>
            </div>
            <div class="universalForm__col">
                <div class="inputBlock__linkArrow">
                  <a class="inputBlock__link" href="{{ route('password_form') }}">
                    {{trans('auth.forgot')}}
                  </a>
                </div>
            </div>
          </div>

          <div class="universalForm__row universalForm__row--mt35">
            <div class="inputBlock -tac">
              <button type="submit" class="button button--big button--blue inputBlock__submit">{{trans('auth.login_button')}}</button>
            </div>
          </div>
        </form>

      </div>
    </div>
  </div>
@endsection
