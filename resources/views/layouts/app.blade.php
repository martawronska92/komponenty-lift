<!DOCTYPE html>
<html lang="{{\App::getLocale()}}" dir="ltr">
  <head>
@if($settings['analytics']!='')

        <script async="" src="https://www.googletagmanager.com/gtag/js?id={{$settings['analytics']}}"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag() {
          	dataLayer.push(arguments);
          }
          gtag('js', new Date());
          gtag('config', '{{$settings["analytics"]}}');
          
        </script>
@endif

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#ffdbe1">
    <meta name="msapplication-navbutton-color" content="#fff">
    <meta name="apple-mobile-web-app-status-bar-style" content="#fff">
    <!-- Touch -->
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=5.0, user-scalable=no, initial-scale=1">
    <title>@if(isset($meta_title)){{$meta_title}}@else{{$settings['meta_title']}}@endif</title>
    <meta name="description" content="@if(isset($meta_description)){{$meta_description}}@else{{$settings['meta_description']}}@endif">
    <link rel="canonical" href="@if(isset($canonical_url)){{$canonical_url}}@else{{Request::fullUrl()}}@endif">
    <meta property="og:title" content="@if(isset($meta_title)){{$meta_title}}@else{{$settings['meta_title']}}@endif">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="{{Request::root()}}">
    <meta property="og:url" content="@if(isset($canonical_url)){{$canonical_url}}@else{{Request::fullUrl()}}@endif">
    <meta property="og:image" content="{{Request::root()}}/images/og_logo.png">
    <meta property="og:description" content="@if(isset($meta_description)){{$meta_description}}@else{{$settings['meta_description']}}@endif">
    <!-- saved from url=(0014)about:internet -->
		<!--[if IE]><meta http-equiv="imagetoolbar" content="no"><![endif]-->
		<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    @include('front.partial.language_alternate',['langsToSwitch'=>\App\Models\Language::getFrontLanguages()])
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="/css/app_front.css?v={{Config::get('assets.version')}}" rel="stylesheet">
    <link href="/css/front.css?v={{Config::get('assets.version')}}" rel="stylesheet">
    <link href="/css/front_components.css?v={{Config::get('assets.version')}}" rel="stylesheet">
    @yield('head')
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700,700i&amp;subset=latin-ext" rel="stylesheet">
    <script>
      var currentLang = "{{\App::getLocale()}}";
      window.Laravel = <?php echo json_encode([
      'csrfToken' => csrf_token(),
      'lang' => trans('front_js')
      ],JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES); ?>;
    </script>
  </head>
  <body>
    <script>
      (function (d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s);
          js.id = id;
          js.src = 'https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.11';
          fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>
    <div class="js-openMenu"></div>
    {!!\App\Models\Menu::renderFromPages()!!}
    @include('front.partial.language_switch',['langsToSwitch'=>\App\Models\Language::getFrontLanguages()])
    @include('front.modules.search.search_box')
    {!!\App\Models\Slider::renderOnFront()!!}
    {!!\App\Models\Menu::renderFromPages('mobile')!!}
@yield('content')

    <script src="/js/app_front.js?v={{Config::get('assets.version')}}" defer></script>
    <script src="/js/common.js?v={{Config::get('assets.version')}}" defer></script>
    <script src="/js/front.js?v={{Config::get('assets.version')}}" defer></script>
@if(App::getLocale()!="en")

        <script src="/bower_components/jquery-validation/src/localization/messages_{{App::getLocale()}}.js" defer></script>
@endif

    {!!\App\Models\Popup::renderOnFront()!!}
    {!!\App\Components\Cookies::renderOnFront()!!}
@yield('script')

  </body>
</html>