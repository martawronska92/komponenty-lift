<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{$title}}{{trans('admin.page_title')}}</title>

  <!-- Styles -->
  <link href="/css/app.css?v={{\Config::get('assets.version')}}" rel="stylesheet">
  <link href="/css/app_admin.css?v={{\Config::get('assets.version')}}" rel="stylesheet">
  <!-- Scripts -->
  <script>
    var currentLang = "{{\App::getLocale()}}";
    window.Laravel = <?php echo json_encode([
      'csrfToken' => csrf_token(),
      'lang' => trans('admin_js'),
      'currentLang' => \App::getLocale()
    ], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES); ?>;
  </script>

  @yield('header')
</head>
<body>
<div id="app">

  <div class="adminContainer">
    <div class="adminSidebar">
      <div class="sidebar">
        <div class="sidebar__head">
          <div class="sidebar__logo">
            <a href="/panel"><img src="/img/panel/ibif_logo.png" alt=""></a>
          </div>
          <div class="sidebar__avatar"></div>
          @if (!Auth::guest())
          <div class="sidebar__user">{{trans('admin.user.welcome')}} {{Auth::user()->first_name}}!</div>
          @endif
          <div class="sidebar__last">{{trans('admin.dashboard.last_logged')}}:
            <span class="sidebar__time">{{\App\Helpers\Format::formatDateWithDots(\Auth::user()->last_login_history)}}</span>
          </div>
        </div>
        <div class="sidebar__middle">
          <div class="sidebar__menu">
            {!!\App\Components\Menu\Menu::render(\App\Components\Menu\MenuInstance::get(),isset($activeMenu) ? $activeMenu : false)!!}
          </div>
        </div>
        <div class="sidebar__footer">
          <span>ibif.pl</span>
          <b>CMS</b>
        </div>
      {{--<a href="{{ route('dashboard') }}">--}}
        {{--{{ config('app.name', 'Laravel') }}--}}
      {{--</a>--}}

    </div>
    </div>

    <div class="adminMain">
      <div class="adminTopbar">
        <div class="adminTopbar__left">
            @include('admin.partial.session_timeout',['activityTimeout' => \Config::get('cms.admin_timeout')])
        </div>
        @if (!Auth::guest())
        <div class="adminTopbar__right">
          <div class="adminTopbar__langs">
            @include('admin.partial.lang.language_switch',['langsToSwitch'=>\App\Models\Language::getActive()])
          </div>
          <div class="adminTopbar__username">{{Auth::user()->first_name.' '.Auth::user()->last_name}}</div>
          <div class="adminTopbar__settings">
            <div class="adminTopbar__settingsIcon"></div>
            <div class="adminTopbar__settingsMenu">
              <ul>
                <li>
                  <a href="{{route('admin_password_change')}}">{{trans('auth.change_password')}}</a>
                </li>
                <li>
                  <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{trans('auth.logout_button')}}</a>
                </li>
              </ul>
            </div>
          </div>
          <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
        </div>
          @endif
      </div>
      <div class="adminWrapper">


      @if(isset($breadcrumbsName))
        @if(isset($breadcrumbsObject) && $breadcrumbsObject)
          {!! Breadcrumbs::render($breadcrumbsName,$breadcrumbsObject) !!}
        @else
          {!! Breadcrumbs::render($breadcrumbsName) !!}
        @endif
      @endif

        @if(isset($header))
            @if ($breadcrumbsName != "admin")
              <div class="titleLine">
                <h1 class="mainTitle">{{$header}}</h1>
              </div>
                @else
                <div style="height:8px"></div>
            @endif
        @endif



      @if(isset($MessageBoxMain))
        @include('admin.components.messagebox',['messagebox'=>$MessageBoxMain])
      @endif

      @if(isset($errors))
        @include('admin.partial.form.errors',['errors'=>$errors])
      @endif

      @yield('content')
    </div></div>
  </div>


</div>

<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext" rel="stylesheet">
<!-- Scripts -->
<script src="/js/app_admin.js?v={{\Config::get('assets.version')}}"></script>
<script src="/js/common.js?v={{\Config::get('assets.version')}}"></script>
<script src="/js/admin.js?v={{\Config::get('assets.version')}}"></script>
@if(\App::getLocale()!="en")
  <script src="/bower_components/jquery-validation/src/localization/messages_{{\App::getLocale()}}.js"></script>
@endif
@yield('footer')
@include('admin.partial.dialog.single')
</body>
</html>
