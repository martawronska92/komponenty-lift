<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="title" content="@if(isset($meta_title)){{$meta_title}}@else{{$settings['meta_title']}}@endif">
    <meta name="description" content="@if(isset($meta_description)){{$meta_description}}@else{{$settings['meta_description']}}@endif">
    <link rel="canonical" href="@if(isset($canonical_url)){{$canonical_url}}@else{{\Request::fullUrl()}}@endif"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>{{$settings['contact_name']}}{{trans('admin.page_title')}}</title>

    <!-- Styles -->
    <link href="{{ elixir('css/app.css')}}?v={{\Config::get('assets.version')}}" rel="stylesheet">
    <link href="{{ elixir('css/app_front.css')}}?v={{\Config::get('assets.version')}}" rel="stylesheet">
    <link href="{{ elixir('css/front.css')}}?v={{\Config::get('assets.version')}}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        var currentLang = "{{\App::getLocale()}}";
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
            'lang' => trans('front_js')
        ],JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES); ?>;
    </script>

    @if($settings['analytics']!="")
      <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', '{{$settings["analytics"]}}', 'auto');
        ga('send', 'pageview');
      </script>
    @endif
</head>
<body>
    <div id="app">

      {!!\App\Models\Slider::renderOnFront()!!}

        @yield('content')

        {!!\App\Models\Popup::renderOnFront()!!}
        {!!\App\Components\Cookies::renderOnFront($settings)!!}

    </div>


    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext" rel="stylesheet">
    <!-- Scripts -->
    <script src="{{ elixir('js/app_front.js') }}?v={{\Config::get('assets.version')}}"></script>
    <script src="{{ elixir('js/common.js') }}?v={{\Config::get('assets.version')}}"></script>
    <script src="{{ elixir('js/front.js') }}?v={{\Config::get('assets.version')}}"></script>
    @if(\App::getLocale()!="en")
      <script src="/bower_components/jquery-validation/src/localization/messages_{{\App::getLocale()}}.js"></script>
    @endif
    @yield('script')
</body>
</html>
