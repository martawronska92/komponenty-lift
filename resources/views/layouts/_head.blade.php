@if($settings['analytics']!="")
<script async="" src="https://www.googletagmanager.com/gtag/js?id={{$settings['analytics']}}"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag() {
      dataLayer.push(arguments);
  }
  gtag('js', new Date());
  gtag('config', '{{$settings["analytics"]}}');
</script>@endif
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<title>@if(isset($meta_title)){{$meta_title}}@else{{$settings['meta_title']}}@endif</title>
<meta name="description" content="@if(isset($meta_description)){{$meta_description}}@else{{$settings['meta_description']}}@endif"/>
<link rel="canonical" href="@if(isset($canonical_url)){{$canonical_url}}@else{{Request::fullUrl()}}@endif"/>@include('front.partial.language_alternate',['langsToSwitch'=>\App\Models\Language::getFrontLanguages()])
<meta name="csrf-token" content="{{ csrf_token() }}"/>
<link href="{{ elixir('css/app.css')}}?v={{Config::get('assets.version')}}" rel="stylesheet"/>
<link href="{{ elixir('css/app_front.css')}}?v={{Config::get('assets.version')}}" rel="stylesheet"/>
<link href="{{ elixir('css/front.css')}}?v={{Config::get('assets.version')}}" rel="stylesheet"/>@yield('head')
<script>
  var currentLang = "{{\App::getLocale()}}";
  window.Laravel = <?php echo json_encode([
  'csrfToken' => csrf_token(),
  'lang' => trans('front_js')
  ],JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES); ?>;
</script>