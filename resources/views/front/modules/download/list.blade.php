
<div class="module">
@if($info && count($info->language)>0 && $info->language[0]->title!="")

  <div class="module__title">{{$info->language[0]->title}}</div>
@endif

@if($info && count($info->language)>0 && $info->language[0]->description!="")

  <div class="module__description">{{$info->language[0]->description}}</div>
@endif

@if(count($list) == 0)

  <div class="module__empty">{{trans('front.list.empty')}}</div>
@else

  <div class="downloads">
@foreach($list as $file)

    <div class="downloads__item downloadItem">
      <div class="downloadItem__icon downloadItem__icon--{{$file->ext}}"></div>
      <div class="downloadItem__text">{{$file->language[0]->title!="" ? $file->language[0]->title : $file->filename_slug.".".$file->ext}}</div><a href="{{route('download_getfile',['hash'=>$file->filename_hashed,str_slug($file->language[0]->title!='' ? $file->language[0]->title : $file->filename_slug).'.'.$file->ext],false)}}" class="downloadItem__button"> 
        <div class="button">{{trans("front.downloads.download")}}</div></a>
      <!-- {{\App\Helpers\File::showSize($file->filesize)}} -->
    </div>
@endforeach

  </div>
@endif

</div>