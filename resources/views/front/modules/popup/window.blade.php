
@if($row && $show)

<div style="display:none;" data-type="@php echo ($row->content_type == 'youtube') ? 'iframe' : 'inline'; @endphp" class="hiddenPopup"></div>
<div id="popup" class="popup mfp-hide">
@if($row->content_type == 'image')

@if($row->link_type!="")
<a href="@if($row->link_type=='page'){{\App\Helpers\Url::getPageUrl($row->link_value,\App::getLocale())}}@else{{$row->link_value}}@endif" target="@if($row->link_new_window == 1) _blank @endif"><img src="/img/popup/image_thumbnail.{{$row->language[0]->content}}"/></a>
@else
<img src="/img/popup/image_thumbnail.{{$row->language[0]->content}}"/>
@endif

@else

  <div class="popup-white">{!!$row->language[0]->content!!}</div>
@endif

</div>
@endif
