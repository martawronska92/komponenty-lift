
@if(\Request::segment(1)!='panel')

<div class="mobileMenu">
  <div class="js-close"></div>
  <div class="mobileMenu__inner">
@if(count($elements->data)>0)

    <ul class="mobileMenu__list">
@foreach($elements->hierarchy[$level][$parentId] as $elementId)

      <li class="mobileMenu__list__item">
@if(array_key_exists($level+1,$elements->hierarchy) && array_key_exists($elementId,$elements->hierarchy[$level+1]))
<a href="">{{$elements->data[$elementId]->title}}</a>
        {!!\App\Models\Menu::render($level+1,$elementId,$elements,$langPrefix,"front.modules.menu.page")!!}
@else

<?php $url = \App\Helpers\Url::getPageUrl($elements->data[$elementId]->id, \App::getLocale()); ?><a href="{{$url}}">{{$elements->data[$elementId]->title}}</a>
@endif

      </li>
@endforeach

    </ul>
    <div class="mobileMenu__socials">
      <div class="mobileMenu__social"><a href="#" rel="nofollow" target="_blank" class="socialFacebok"></a></div>
      <div class="mobileMenu__social"><a href="#" rel="nofollow" target="_blank" class="socialInstagram"></a></div>
    </div>
@endif

  </div>
</div>
<div class="js-overlay"></div>
@endif
