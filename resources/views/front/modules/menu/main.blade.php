
@if(\Request::segment(1)!='panel')

@if(count($elements->data)>0)

<ul>
@foreach($elements->hierarchy[$level][$parentId] as $elementId)

  <li>
@if(array_key_exists($level+1,$elements->hierarchy) && array_key_exists($elementId,$elements->hierarchy[$level+1]))

    <ul>
      <li><a href="#">{{$elements->data[$elementId]->menu_title}}</a>
        {!!\App\Models\Menu::render($level+1,$elementId,$elements,$langPrefix,"front.modules.menu.main")!!}
      </li>
    </ul>
@else

<?php 
    $url = \App\Helpers\Url::getPageUrl($elements->data[$elementId]->entity_id, \App::getLocale());
    if ($elements->data[$elementId]->mainpage == 1)
    	$host = $url = "/";
    else if ($elements->data[$elementId]->external == 1)
    	$host = $elements->data[$elementId]->external_url;
    else
    	$host = $url; ?><a target="@if($elements->data[$elementId]->external==1) _blank @endif" href="{{$host}}" class="NaN">{{$elements->data[$elementId]->menu_title}}</a>
@endif

  </li>
@endforeach

</ul>
@endif

@endif
