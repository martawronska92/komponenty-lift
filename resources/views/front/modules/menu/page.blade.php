
@if(\Request::segment(1)!='panel')

@if(count($elements->data)>0)

<ul>
@foreach($elements->hierarchy[$level][$parentId] as $elementId)

  <li>
@if(array_key_exists($level+1,$elements->hierarchy) && array_key_exists($elementId,$elements->hierarchy[$level+1]))
<a href="">{{$elements->data[$elementId]->title}}</a>
    {!!\App\Models\Menu::render($level+1,$elementId,$elements,$langPrefix,"front.modules.menu.page")!!}
@else

<?php $url = \App\Helpers\Url::getPageUrl($elements->data[$elementId]->id, \App::getLocale()); ?><a href="{{$url}}" class="@if(Request::path()==$url || "/".Request::path()==$url || strpos("/".Request::path(),$url)!==FALSE) active @endif">{{$elements->data[$elementId]->title}}</a>
@endif

  </li>
@endforeach

</ul>
@endif

@endif
