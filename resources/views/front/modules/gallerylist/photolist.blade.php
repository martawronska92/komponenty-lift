
@if(isset($info->language) && count($info->language)>0)

<div class="module">
@if($info->language[0]->title!="")

  <div class="module__title">{{$info->language[0]->title}}</div>
@endif

@if($info->language[0]->description!="")

  <div class="module__description">{{$info->language[0]->description}}</div>
@endif

  <div class="galleryList">
@foreach($galleries as $gallery)

    @php$url = \App\Helpers\Url::getPageUrl($gallery->entity_id,\App::getLocale()); @endphp<a href="{{$url}}" class="galleryList__item">
      <div style="background-image:url(/img/gallery_photo/{{$gallery->photo_id}}/thumbnail.{{$gallery->photo_ext}})" class="galleryItem">
        <div class="galleryItem__desc">{{$gallery->photo_title!="" ? $gallery->photo_title : $gallery->title}}</div>
      </div></a>
@endforeach

  </div>
</div>
@endif
