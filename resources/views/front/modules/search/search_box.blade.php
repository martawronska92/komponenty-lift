@if(\Request::segment(1)!='panel')
  <ul class="nav navbar-nav navbar-right">
    <li>
      <form action="{{\App\Helpers\Url::getSearchUrl()}}" method="get">
        <input type="search" name="s" value="@if(\Request::has('s')){{\Request::get('s')}}@endif"/>
      </form>
    </li>
  </ul>
@endif
