
{{-- view name: zzzz --}}@extends('layouts.app')
@section('content')
  {{$category->language[0]->name}}
  @include('front.partial.breadcrumbs')
  @if(count($category->subcategory) > 0)
    @foreach($category->subcategory as $subcategory)<a href="/{{trim(Request::path(),'/')}}/{{$subcategory->language[0]->url}}">{{$subcategory->language[0]->name}}</a>
    @endforeach
  @endif
  @if(count($category->post)>0)
    @foreach($category->post as $post)<a href="/{{trim(Request::path(),'/')}}/{{$post->language[0]->url}}">{{$post->language[0]->title}}</a>
    @endforeach
  @endif
  @include('front.page.partial.module',['modules'=>$category->modules])<a href="{{URL::previous()}}">{{trans('front.back')}}</a>
@endsection