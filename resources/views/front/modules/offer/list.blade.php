
@if(isset($menu))
  @if(count($menu->data)>0)
    @foreach($menu->hierarchy[$level][$parentId] as $elementId)
      <li id="{{$elementId}}"></li>
      @php$url = \App\Helpers\Url::getCategoryUrl($menu->data[$elementId]->id,\App::getLocale()); @endphp<a href="{{$url}}" class="{{$url == '\'.Request::path() ? 'active' : ''}}">{{$menu->data[$elementId]->language[0]->name}}</a>
      @if(array_key_exists($level+1,$menu->hierarchy) && array_key_exists($elementId,$menu->hierarchy[$level+1]))
        @include('front.modules.offer.list',['level'=>$level+1,'parentId'=>$elementId,'menu'=>$menu])
      @endif
    @endforeach
  @endif
@endif