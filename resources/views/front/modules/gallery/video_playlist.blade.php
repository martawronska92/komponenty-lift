
<div class="module">
@if(isset($videos) && count($videos)>0)

@if($info && count($info->language)>0 && $info->language[0]->title!="")

  <div class="module__title">{{$info->language[0]->title}}</div>
@endif

@if($info && count($info->language)>0 && $info->language[0]->description!="")

  <div class="module__description">{{$info->language[0]->description}}</div>
  <div class="videoSlider">
    <div class="videoSlider__left">
      <div class="inner">
        <iframe data-youtube-gallerypreview="data-youtube-gallerypreview" width="{{$settings['video_playlist_width']}}" height="{{$settings['video_playlist_height']}}" src="{{\App\Helpers\Youtube::embed($videos[0]->url)}}" frameborder="0" allowfullscreen="allowfullscreen" data-target="data-target"></iframe>
      </div>
    </div>
    <div class="videoSlider__right">
      <div class="videoSlide js-videoSlider">
@foreach($videos as $key => $video)

        <div data-videourl="{{\App\Helpers\Youtube::embed($video->url)}}" class="videoSlide__item">
          <div class="videoPreview">
            <div style="background-image:url({{$video->thumbnail}})" class="videoPreview__image"></div>
            <div class="videoPreview__text">
@if(count($video->language)>0)

              <div class="videoPreview__desc">{{$video->language[0]->title}}</div>
@endif

            </div>
          </div>
        </div>
@endforeach

      </div>
    </div>
  </div>
@endif

@endif

</div>