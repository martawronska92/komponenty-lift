
@if(isset($videos) && count($videos)>0)
  @if($info && count($info->language)>0 && $info->language[0]->title!="")
    {{$info->language[0]->title}}
  @endif
  @if($info && count($info->language)>0 && $info->language[0]->description!="")
    {{$info->language[0]->description}}
  @endif
  <!-- data-youtube-slider -->
  <div>
    @foreach($videos as $key => $video)
    <li><iframe data-youtube-gallerypreview width="{{$settings['video_slider_width']}}" height="{{$settings['video_slider_height']}}" src="{{\App\Helpers\Youtube::embed($video->url)}}" frameborder="0" allowfullscreen></iframe></li>
    @endforeach
  </div>
@endif