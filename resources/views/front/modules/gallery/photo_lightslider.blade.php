
@if(isset($photos) && count($photos)>0)
  @if($info && count($info->language)>0 && $info->language[0]->title!="")
    {{$info->language[0]->title}}
  @endif
  @if($info && count($info->language)>0 && $info->language[0]->description!="")
    {{$info->language[0]->description}}
  @endif
  <ul data-gallery-lightslider="{{$photos[0]->gallery->id}}">
    @foreach($photos as $photo)@php $photoName = $photo->language && count($photo->language)>0 && $photo->language[0]->title!=NULL ? $photo->language[0]->filename.'_' : ''; @endphp
    <li data-thumb="/img/gallery_photo/{{$photo->dirname}}/{{$photoName}}thumbnail.{{$photo->ext}}"><img src="/img/gallery_photo/{{$photo->dirname}}/{{$photoName}}normal.{{$photo->ext}}" alt="@if($photo->language && count($photo->language)>0 && $photo->language[0]->title!=""){{$photo->language[0]->title}}@endif">
      @if($photo->language && count($photo->language)>0) {{$photo->language[0]->title}} @endif
    </li>
    @endforeach
  </ul>
@endif