
@if(isset($videos) && count($videos)>0)
  @if($info && count($info->language)>0 && $info->language[0]->title!="")
    {{$info->language[0]->title}}
  @endif
  @if($info && count($info->language)>0 && $info->language[0]->description!="")
    {{$info->language[0]->description}}
  @endif<iframe data-youtube-gallerypreview width="{{$settings['video_gallery_width']}}" height="{{$settings['video_gallery_height']}}" src="{{\App\Helpers\Youtube::embed($videos[0]->url)}}" frameborder="0" allowfullscreen></iframe>
  @foreach($videos as $key => $video)
    <div data-youtube-embed="{{\App\Helpers\Youtube::embed($video->url)}}">
    	<img src="{{$video->thumbnail}}"/>@if(count($video->language)>0){{$video->language[0]->title}}@endif
    </div>
    @if(count($video->language)>0){{$video->language[0]->title}}@endif
  @endforeach
@endif