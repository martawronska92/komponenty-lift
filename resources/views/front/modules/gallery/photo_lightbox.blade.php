
@if(isset($photos) && count($photos)>0)

<div class="module">
@if($info && count($info->language)>0 && $info->language[0]->title!="")

  <div class="module__title">{{$info->language[0]->title}}</div>
@endif

@if($info && count($info->language)>0 && $info->language[0]->description!="")

  <div class="module__description">{{$info->language[0]->description}}</div>
@endif

  <div class="galleryList js-gallery">
@foreach($photos as $photo)

    @php$photoName = $photo->language && count($photo->language)>0 && $photo->language[0]->title!=NULL ? $photo->language[0]->filename.'_' : ''; @endphp<a href="/img/gallery_photo/{{$photo->dirname}}/{{$photoName}}normal.{{$photo->ext}}" title="@if($photo->language && count($photo->language)>0 && $photo->language[0]->title!=""){{$photo->language[0]->title}} @endif" class="galleryList__item">
      <div style="background-image:url(/img/gallery_photo/{{$photo->dirname}}/{{$photoName}}thumbnail.{{$photo->ext}})" class="galleryItem">
        <div class="galleryItem__desc">@if($photo->language && count($photo->language)>0 && $photo->language[0]->title!=""){{$photo->language[0]->title}} @endif</div>
      </div></a>
@endforeach

  </div>
</div>
@endif
