
@if($slides && count($slides) >0 && \Request::segment(1)!='panel')

<div data-front-slider="data-front-slider">
@foreach($slides as $key => $slide)

<?php $src="/img/slider/".$slide->id."/thumbnail.".$slide->ext; ?><a target="@if($slide->external!=-1) _blank @endif" href="@if($slide->external!=-1) {{$slide->external==1 ? $slide->link : \App\Helpers\Url::getPageUrl($slide->link,App::getLocale())}} @endif"><img src="{{$src}}" alt="@if($slide->language[0]->title_1!=""){{$slide->language[0]->title_1}}@endif @if($slide->language[0]->title_2!=""){{$slide->language[0]->title_2}}@endif"/></a>
@if($slide->language[0]->title_1!="")

  {{$slide->language[0]->title_1}}
@endif

@if($slide->language[0]->title_2!="")

  {{$slide->language[0]->title_2}}
@endif

@endforeach

</div>
@endif
