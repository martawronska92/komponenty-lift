<?php

View::composer('front.home',function($view){
	$moduleId = \App\Models\Module::where('symbol','=','news')->pluck('id')->first();
	$categoryId = \App\Models\PagePostModule::where('module_id','=',$moduleId)->pluck('module_entity_id')->first();
  $model = new \App\Models\Post();
  $view->with('news',$categoryId ? $model->getFrontList($categoryId,false) : []);
});
