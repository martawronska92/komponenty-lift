
<div class="oneNews">
  <div class="oneNews__title">
    <div class="oneNews__tit">{{$post->language[0]->title}}</div>
    <div class="oneNews__dots">
      <div class="oneNews__dot"></div>
      <div class="oneNews__dot"></div>
      <div class="oneNews__dot"></div>
    </div>
  </div>
  <div class="oneNews__text">{{$post->language[0]->content}}</div>
  <div class="oneNews__bottom">
    <div class="oneNews__left">
      <div class="oneNews__date">{{$post->publish_date!="" ? $post->publish_date->format("d.m.Y") : $post->created_at->format("d.m.Y")}}</div>
    </div>
    <div class="oneNews__right">
      <div class="oneNews__link"><a href="{{$url}}" class="button button--wider">WIĘCEJ</a></div>
    </div>
  </div>
</div>