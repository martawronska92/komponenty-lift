
<div class="module">
@if(count($category->post) == 0)

  <div class="module__empty">{{trans('front.list.empty')}}</div>
@else

  <div data-category-page="1" data-category-id="{{$category->id}}" class="newsList">
    <div data-category-container="" class="newsList__list">
@foreach($posts as $key=>$post)

<?php $url = \App\Helpers\Url::getFrontPostUrl($post->id,\App::getLocale()); ?>
      @include('front.category.partials.one_news')
@endforeach

    </div>
    <div class="newsList__button">
      <div data-category-loadpage="" class="hidden button @if(count($posts)<=$category->per_page) @endif">{{trans('front.category.show_next')}}</div>
    </div>
  </div>
@endif

</div>