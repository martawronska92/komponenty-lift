
@if(count($langsToSwitch) > 1)

@foreach($langsToSwitch as $lang)
<a href="@php if(isset($lang_switcher_links) && array_key_exists($lang->symbol, $lang_switcher_links)) { echo $lang_switcher_links[$lang->symbol];} else if (Request::segment(1) != 'panel' || !Auth::user()) {echo route('homepage_language', ['frontlanguage' => $lang->symbol], false);} else {echo route('language_change', ['symbol' => $lang->symbol], false);} @endphp"><img src="/img/langs/{{$lang->symbol}}.png"/></a>
@endforeach

@endif
