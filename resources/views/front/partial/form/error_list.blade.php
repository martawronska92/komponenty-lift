
@if(isset($errors) && count($errors)>0)

<div class="formErrors">
  <ul class="formErrors__ul">
    <li class="red">Formularz nie został wysłany</li>
@foreach($errors->all() as $error)

    <li>{{$error}}</li>
@endforeach

  </ul>
</div>
@endif

@if(session()->has('MessageBoxMain'))

<div class="formErrors__item false">{{session()->get('MessageBoxMain')['msg']}}</div>
@endif
