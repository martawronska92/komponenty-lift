
<div data-cookies="data-cookies" class="cookiesInfo">
  <div class="cookiesInfo__inner">
    <div class="cookiesInfo__icon"></div>
    <div class="cookiesInfo__text">Serwis korzysta z plików cookies w celu realizacji usług zgodnie z&nbsp;<a href="/polityka_cookies.pdf" target="_blank" rel="nofollow" title="Polityka prywatności">polityką prywatności</a>&nbsp;. Możesz określić warunki przechowywania lub dostępu do cookies w Twojej przeglądarce lub konfiguracji usługi.</div>
    <div title="{{trans('front.close')}}" data-closecookies="data-closecookies" class="cookiesInfo__close"></div>
  </div>
</div>