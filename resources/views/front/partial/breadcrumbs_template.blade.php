
@if($breadcrumbs)

<div class="breadcrumbs">
  <div class="breadcrumbs__name">{{trans("front.breadcrumbs.main")}}</div>
  <ol class="breadcrumbs__breads">
@foreach($breadcrumbs as $breadcrumb)

@if($breadcrumb->url && !$breadcrumb->last)

    <li><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
@else

    <li class="active">{{ $breadcrumb->title }}</li>
@endif

@endforeach

  </ol>
</div>
@endif
