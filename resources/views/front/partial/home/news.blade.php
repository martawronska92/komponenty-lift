
@if(count($news) > 0)

<div class="newsList">
  <div class="newsList__list">
@for($i=0; $i<(count($news) > 3 ? 3 : count($news)); $i++)

<?php $url = \App\Helpers\Url::getFrontPostUrl($news[$i]->id); ?>
    <div class="oneNews">
      <div class="oneNews__title">
        <div class="oneNews__tit">{{$news[$i]->getTitle()}}</div>
        <div class="oneNews__dots">
          <div class="oneNews__dot"></div>
          <div class="oneNews__dot"></div>
          <div class="oneNews__dot"></div>
        </div>
      </div>
      <div class="oneNews__text">{{$news[$i]->language[0]->content}}</div>
      <div class="oneNews__bottom">
        <div class="oneNews__left">
          <div class="oneNews__date">{{$news[$i]->publish_date!="" ? $news[$i]->publish_date->format('d.m.Y') : $news[$i]->created_at->format('d.m.Y')}}</div>
        </div>
        <div class="oneNews__right">
          <div class="oneNews__link"><a href="{{$url}}" class="button button--wider">WIĘCEJ</a></div>
        </div>
      </div>
    </div>
@if($news[$i]->getImage()!="")
<a href="{{$url}}"></a><img src="/img/post/{{$news[$i]->id}}/thumbnail.{{$news[$i]->thumbnail}}" width="200" alt="{{$news[$i]->getTitle()}}"/>
@endif
<a href="{{$url}}">{{$news[$i]->getTitle()}}</a>
@endfor

  </div>
</div>
@endif
