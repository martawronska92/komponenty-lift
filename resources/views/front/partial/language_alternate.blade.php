
@if(count($langsToSwitch) > 1)

@foreach($langsToSwitch as $key=>$lang)

<?php 
if(isset($lang_switcher_links) && array_key_exists($lang->symbol,$lang_switcher_links)){
	$url = $lang_switcher_links[$lang->symbol];
}else if(\Request::segment(1)!='panel' || !\Auth::user()){
	$url = route('homepage_language',['frontlanguage'=>$lang->symbol],false);
}else{
	$url = route('language_change',['symbol' => $lang->symbol],false);
}
 ?>
<link rel="alternate" hreflang="{{$lang->symbol}}" href="{{Request::root()}}{{$url}}"/>
@if($key===0)

<link rel="alternate" hreflang="x-default" href="{{Request::root()}}{{$url}}"/>
@endif

@endforeach

@endif
