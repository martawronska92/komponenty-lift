<script id="entry-template" type="text/x-handlebars-template">{{#posts}}
  <a href="{{link}}" class="blog-item">
      <div class="blog-img">
          <img src="{{thumb}}" alt="{{title}}">
      </div>
      <div class="blog-text">
          {{title}}
      </div>
      <div class="blog-date">
          <div class="data">
              <span>{{date}}</span><br>
              <span>/{{year}}</span>
          </div>
      </div>
      <div class="more-blog">
          więcej
      </div>
  </a>{{/posts}}
</script>
