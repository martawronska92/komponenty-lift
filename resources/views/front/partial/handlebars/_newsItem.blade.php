
<div class="oneNews">
  <div class="oneNews__title">
    <div class="oneNews__tit">{{title}}</div>
    <div class="oneNews__dots">
      <div class="oneNews__dot"></div>
      <div class="oneNews__dot"></div>
      <div class="oneNews__dot"></div>
    </div>
  </div>
  <div class="oneNews__text">{{content}}</div>
  <div class="oneNews__bottom">
    <div class="oneNews__left">
      <div class="oneNews__date">{{date}}{{year}}</div>
    </div>
    <div class="oneNews__right">
      <div class="oneNews__link"><a href="{{link}}" class="button button--wider">WIĘCEJ</a></div>
    </div>
  </div>
</div>