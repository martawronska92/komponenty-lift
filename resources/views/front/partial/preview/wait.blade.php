
@extends('layouts.app')

@section('content')

<div class="waitPopup">
  <div class="waitPopup__title">{{trans('front.wait_for_preview')}}</div>
</div>
@stop
