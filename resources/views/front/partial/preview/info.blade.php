
@if(strpos(Request::path(),'showpreview')!==false)

<div class="previewBlockFront">
  <div class="previewBlockFront__title">{!!trans('front.preview_info')!!} {{trans('front.preview_info_'.$type)}} </div>
  <div class="previewBlockFront__link"><a href="javascript:window.top.close();">{{trans('front.back_to_panel')}}</a></div>
</div>
@endif
