
@if(isset($breadcrumbsName))
  @if(isset($breadcrumbsObject) && $breadcrumbsObject)
    {!! Breadcrumbs::render($breadcrumbsName,$breadcrumbsObject) !!}
  @else
    {!! Breadcrumbs::render($breadcrumbsName) !!}
  @endif
@endif