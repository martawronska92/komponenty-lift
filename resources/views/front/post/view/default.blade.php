
@extends('layouts.app')

@section('head')

<meta property="og:url" content="{{Request::url()}}"/>
<meta property="og:type" content="article"/>
<meta property="og:title" content="{{$post->getTitle()}}"/>
<meta property="og:description" content="How much does culture influence creative thinking?"/>
@if($post->getImage()!="")

<meta property="og:image" content="{{$post->getImage('original')}}"/>
@endif

@stop

@section('content')

<h1>{{$post->getTitle()}}</h1>
@include('front.partial.breadcrumbs')
<div data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-size="small" data-mobile-iframe="true" class="fb-share-button"><a target="_blank" href="{{Request::url()}}" class="fb-xfbml-parse-ignore">{{trans('front.share')}}</a></div>
@if($post->getImage()!="")
<img src="{{$post->getImage()}}" alt="{{$post->getTitle()}}"/>
@endif

{!!$post->language[0]->content!!}
@foreach($post->field as $field)

{{$field->language[0]->pivot->name}} {{$field->pivot->value}}
@endforeach

@include('front.page.partial.module',['modules' => $post->module])
@if(count($post->tag) > 0)

{{trans('front.post.tags')}}
@foreach($post->tag as $tag)

<li>
<?php $routeParams = ['tagName' => $tag->language[0]->friendly_name]; if(\App\Helpers\Language::hasPrefix()) $routeParams['frontlanguage'] = \App::getLocale(); ?><a href="{{route('front_tag',$routeParams,false)}}">{{$tag->language[0]->name}}</a>
</li>
@endforeach

@endif

@include('front.partial.preview.info')
@stop
