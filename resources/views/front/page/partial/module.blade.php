
@if(is_a($modules, 'Illuminate\Database\Eloquent\Collection') && count($modules) >0)

@foreach($modules as $module)

@if(method_exists($module->module->module_entity_model,'renderOnFront'))

{!!call_user_func([$module->module->module_entity_model,'renderOnFront'],$module)!!}
@endif

@endforeach

@endif
