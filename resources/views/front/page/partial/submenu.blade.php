
@if(\Request::segment(1)!='panel')

<ul class="sidebarMenu__list">
@if(count($elements->data)>0)

@foreach($elements->hierarchy[$level][$parentId] as $elementId)
@php $url = \App\Helpers\Url::getPageUrl($elements->data[$elementId]->id, \App::getLocale()); $act = ''; @endphp
  <li>
@if($elements->data[$elementId]->external ==0 && ("/".\Request::path()==$url || \Request::path()==$url || strstr("/".\Request::path(),$url)!==FALSE))
@php $act = 'active'; @endphp
@endif
<a href="{{$url}}" class="{{$act}}">{{$elements->data[$elementId]->language[0]->title}}</a>
  </li>
@if(array_key_exists($level+1,$elements->hierarchy) && array_key_exists($elementId,$elements->hierarchy[$level+1]))

  {!! \App\Models\Menu::render($level+1,$elementId,$elements,$langPrefix,"front.page.partial.submenu") !!}
@endif

@endforeach

@endif

</ul>
@endif
