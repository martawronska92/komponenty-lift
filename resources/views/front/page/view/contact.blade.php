
@extends('layouts.app')

@section('script')

<script>
  var startDateTime = new Date();
  function beforeSubmit(){
  var clickDateTime = new Date();
  return parseInt((clickDateTime - startDateTime) / 1000) > 5;
  }
  
</script>
@stop

@section('content')

{{$page->language[0]->title}}
@include('front.partial.breadcrumbs')
@if(isset($MessageBox))

@include('admin.components.messagebox',['messagebox'=>$MessageBox])
@endif

@if($settings['map_link'])

{!!$settings['map_link']!!}
@endif

{{$settings['contact_name']}}
{{$settings['contact_street']}} {{$settings['contact_homenr']}}
@if($settings['contact_localnr']!="")/{{$settings['contact_localnr']}}@endif
{{$settings['contact_postcode']}} {{$settings['contact_city']}}
@if(isset($openinghours) && count($openinghours)>0)

{{trans('admin.openinghours.header')}}
@foreach($openinghours as $term)

{{$term->language[0]->value}}
@if($term->open == 1)

{{$term->from}}
{{$term->to}}
@else

{{trans('admin.openinghours.close')}}
@endif

@endforeach

@endif

@include('front.partial.form.error_list')
@if(session()->has('MessageBoxMain'))

<div class="{{session()->get('MessageBoxMain')['type'] == 'success' ? 'success' : 'error'}}">{{session()->get('MessageBoxMain')['msg']}}</div>
@endif

<div class="exampleContainer">
  <form id="contactForm" action="{{route('contact'),false,false}}" method="post" class="styledForm">
    <div class="styledForm__row">
      <div class="styledForm__left">
        <label for="name" class="styledForm__label">{{trans('front.contact.name')}}</label>
      </div>
      <div class="styledForm__right"> 
        <input type="text" name="name" id="name" required="required" value="{{old('name')}}" class="styledFormItems styledFormItems--input"/>
      </div>
    </div>
    <div class="styledForm__row">
      <div class="styledForm__left">
        <label for="email" class="styledForm__label">{{trans('front.contact.email')}}</label>
      </div>
      <div class="styledForm__right"> 
        <input type="email" name="email" id="email" required="required" value="{{old('email')}}" class="styledFormItems styledFormItems--input"/>
      </div>
    </div>
    <div class="styledForm__row">
      <div class="styledForm__left">
        <label for="phone" class="styledForm__label">{{trans('front.contact.phone')}}</label>
      </div>
      <div class="styledForm__right"> 
        <input type="tel" name="phone" id="phone" required="required" value="{{old('phone')}}" class="styledFormItems styledFormItems--input"/>
      </div>
    </div>
    <div class="styledForm__row">
      <div class="styledForm__left">
        <label for="message" class="styledForm__label">{{trans('front.contact.message')}}</label>
      </div>
      <div class="styledForm__right"> 
        <textarea name="message" id="message" required="required" class="styledFormItems styledFormItems--textarea">{{old('message')}}</textarea>
      </div>
    </div>
    <div class="styledForm__row">
      <div class="styledForm__left">
        <label for="sel" class="styledForm__label">{{trans('front.contact.message')}}</label>
      </div>
      <div class="styledForm__right">
        <select name="sel" id="sel" required="required" class="styledFormItems styledFormItems--select">
          <option>{{trans('front.contact.message')}}</option>
          <option>{{trans('front.contact.email')}}</option>
          <option>{{trans('front.contact.name')}}</option>
        </select>
      </div>
    </div>
    <div class="styledForm__row">
      <div class="styledForm__left"></div>
      <div class="styledForm__right">
        <div class="styledFormItems__checkbox">
          <div class="styledCheckbox">
            <input type="checkbox" name="ch1" id="ch1" required="required"/>
            <label for="ch1"></label><span>Oświadczam, że wyrażam zgodę na przetwarzanie danych osobowych przez Lubelską Akademię Sportu "CampiSportivi" w celach związanych z realizacją zadań statutowych <a href="#">stowarzyszenia</a>, w tym w celach informacyjnych, marketingowych, archiwalnych i statystycznych zgodnie z ustawą z dnia 29 sierpnia 1997 r. o ochronie danych osobowych ( Dz. U. 2016 poz.922). Zostałem poinformowany o prawie dostępu do treści swoich danych oraz ich poprawiania, aktualizacji, uzupełniania i usuwania.</span>
          </div>
        </div>
      </div>
    </div>
    <div class="styledForm__row">
      <div class="styledForm__left"> 
        <label>Radio</label>
      </div>
      <div class="styledForm__right">
        <div class="styledFormItems__checkbox">
          <div class="styledRadio">
            <input type="radio" name="ch1" id="ch2" required="required"/>
            <label for="ch2"></label><span>Tak</span>
          </div>
          <div class="styledRadio">
            <input type="radio" name="ch1" id="ch3" required="required"/>
            <label for="ch3"></label><span>Nie</span>
          </div>
        </div>
      </div>
    </div>
    <div class="styledForm__row styledForm__row--button">
      <input type="hidden" name="_token" value="{{csrf_token()}}"/>
      <input type="submit" value="{{trans('front.contact.send')}}" class="button"/>
    </div>
  </form><br/><br/><br/>
  <form id="contactForm" action="{{route('contact'),false,false}}" method="post" class="styledForm styledForm--placeholder">
    <div class="styledForm__row">
      <input type="text" name="name" required="required" value="{{old('name')}}" placeholder="{{trans('front.contact.name')}}" class="styledFormItems styledFormItems--input"/>
    </div>
    <div class="styledForm__row">
      <input type="email" name="email" required="required" value="{{old('email')}}" placeholder="{{trans('front.contact.email')}}" class="styledFormItems styledFormItems--input"/>
    </div>
    <div class="styledForm__row">
      <input type="tel" name="phone" required="required" value="{{old('phone')}}" placeholder="{{trans('front.contact.phone')}}" class="styledFormItems styledFormItems--input"/>
    </div>
    <div class="styledForm__row">
      <textarea name="message" required="required" placeholder="{{trans('front.contact.message')}}" class="styledFormItems styledFormItems--textarea">{{old('message')}}</textarea>
    </div>
    <div class="styledForm__row">
      <div class="styledCheckbox">
        <input type="checkbox" name="ch1" id="ch1" required="required"/>
        <label for="ch1"></label><span>Oświadczam, że wyrażam zgodę na przetwarzanie danych osobowych przez Lubelską Akademię Sportu "CampiSportivi" w celach związanych z realizacją zadań statutowych <a href="#">stowarzyszenia</a>, w tym w celach informacyjnych, marketingowych, archiwalnych i statystycznych zgodnie z ustawą z dnia 29 sierpnia 1997 r. o ochronie danych osobowych ( Dz. U. 2016 poz.922). Zostałem poinformowany o prawie dostępu do treści swoich danych oraz ich poprawiania, aktualizacji, uzupełniania i usuwania.</span>
      </div>
    </div>
    <div class="styledForm__row">
      <div class="styledForm__label">Wyrażam zgodę</div>
      <div class="styledFormItems__checkbox">
        <div class="styledRadio">
          <input type="radio" name="ch1" id="ch2" required="required"/>
          <label for="ch2"></label><span>Tak</span>
        </div>
        <div class="styledRadio">
          <input type="radio" name="ch1" id="ch3" required="required"/>
          <label for="ch3"></label><span>Nie</span>
        </div>
      </div>
    </div>
    <div class="styledForm__row styledForm__row--button">
      <input type="hidden" name="_token" value="{{csrf_token()}}"/>
      <input type="submit" value="{{trans('front.contact.send')}}" class="button"/>
    </div>
  </form><br/><br/><br/><br/><br/>
  <form id="contactForm" action="{{route('contact'),false,false}}" method="post" class="styledForm styledForm--twocols">
    <div class="styledForm__row">
      <div class="styledForm__left">
        <input type="text" name="name" required="required" value="{{old('name')}}" placeholder="{{trans('front.contact.name')}}" class="styledFormItems styledFormItems--input"/>
      </div>
      <div class="styledForm__right">
        <input type="text" name="name2" required="required" value="{{old('name2')}}" placeholder="{{trans('front.contact.name2')}}" class="styledFormItems styledFormItems--input"/>
      </div>
    </div>
    <div class="styledForm__row">
      <div class="styledForm__left">
        <input type="email" name="email" required="required" value="{{old('email')}}" placeholder="{{trans('front.contact.email')}}" class="styledFormItems styledFormItems--input"/>
      </div>
      <div class="styledForm__right">
        <input type="tel" name="phone" required="required" value="{{old('phone')}}" placeholder="{{trans('front.contact.phone')}}" class="styledFormItems styledFormItems--input"/>
      </div>
    </div>
    <div class="styledForm__row">
      <textarea name="message" required="required" placeholder="{{trans('front.contact.message')}}" class="styledFormItems styledFormItems--textarea">{{old('message')}}</textarea>
    </div>
    <div class="styledForm__row">
      <div class="styledCheckbox">
        <input type="checkbox" name="ch1" id="ch1" required="required"/>
        <label for="ch1"></label><span>Oświadczam, że wyrażam zgodę na przetwarzanie danych osobowych przez Lubelską Akademię Sportu "CampiSportivi" w celach związanych z realizacją zadań statutowych <a href="#">stowarzyszenia</a>, w tym w celach informacyjnych, marketingowych, archiwalnych i statystycznych zgodnie z ustawą z dnia 29 sierpnia 1997 r. o ochronie danych osobowych ( Dz. U. 2016 poz.922). Zostałem poinformowany o prawie dostępu do treści swoich danych oraz ich poprawiania, aktualizacji, uzupełniania i usuwania.</span>
      </div>
    </div>
    <div class="styledForm__row">
      <div class="styledForm__label">Wyrażam zgodę</div>
      <div class="styledFormItems__checkbox">
        <div class="styledRadio">
          <input type="radio" name="ch1" id="ch2" required="required"/>
          <label for="ch2"></label><span>Tak</span>
        </div>
        <div class="styledRadio">
          <input type="radio" name="ch1" id="ch3" required="required"/>
          <label for="ch3"></label><span>Nie</span>
        </div>
      </div>
    </div>
    <div class="styledForm__row styledForm__row--button">
      <input type="hidden" name="_token" value="{{csrf_token()}}"/>
      <input type="submit" value="{{trans('front.contact.send')}}" class="button"/>
    </div>
  </form>
</div>
@stop
