@extends('layouts.app')
@section('content')
  {{$page->language[0]->title}}
  @include('front.partial.breadcrumbs')
  {!!$page->language[0]->content!!}
  @include('front.page.partial.module',['modules'=>$page->module])
@endsection