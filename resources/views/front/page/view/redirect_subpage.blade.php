<?php
  if($page->subpage->count() > 0){
    $pageId = $page->subpage()->orderBy('position','DESC')->first()->id;
    $url = "http://".$_SERVER['HTTP_HOST'].\App\Helpers\Url::getPageUrl($pageId);
    header("Location: ".$url);
    exit();
  }
