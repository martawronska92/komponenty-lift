@extends('layouts.app')
@section('content')
  {{$page->language[0]->title}}
  @include('front.partial.breadcrumbs')
  {!!$page->language[0]->content!!}
  @include('front.page.partial.module',['modules'=>$page->module])
  @if(isset($modules))
    @include('front.page.partial.module',['modules'=>$modules])
  @endif
  <div class="sidebarMenu">
    <div class="sidebarMenu__title">{{trans('front.cat')}}</div>
    @if(isset($menu) && count($menu['elements']->data)>0 )
      @include('front.page.partial.submenu',['elements'=>$menu['elements'], 'level'=>1, 'parentId' => $menu['parent_page_id']])
    @endif
  </div>
  @include('front.partial.preview.info')
@endsection