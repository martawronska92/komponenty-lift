@extends('layouts.admin')

@section('content')

  <div class="addForm">
    <form class="form2col" action="" method="post" enctype="multipart/form-data" data-formvalidation>
      <div class="form2col__wrapRow">
        <div class="form2col__row">
          <div class="form2col__left">{{trans('admin.seo.word')}}</div>
          <div class="form2col__right inputBlock"><input type="text" name="word" class="required inputBlock__field"
            maxlength="255"
            value="@if(isset($row)){{$row->word}}@endif"
            data-formvalidation-label="{{trans('admin.seo.word')}}"/></div>
        </div>
      </div>
      <div class="form2col__wrapRow">
        <div class="form2col__row">
          <div class="form2col__left">{{trans('admin.seo.link')}}</div>
          <div class="form2col__right inputBlock"><input type="text" name="link" class="required inputBlock__field"
            maxlength="255"
            value="@if(isset($row)){{$row->link}}@endif"
            data-formvalidation-label="{{trans('admin.seo.link')}}"/></div>
        </div>
      </div>

      @include('admin.partial.form.save',['cancel_url'=>route('seo_smart_links')])

    </form>
  </div>

@endsection
