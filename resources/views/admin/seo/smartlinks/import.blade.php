@extends('layouts.admin')

@section('content')
<div class="addForm importForm">
  <form action="" method="post" enctype="multipart/form-data" data-formvalidation>
{{--    {{trans('admin.post.choose_thumbnail_file')}}--}}
        <div class="importForm__row">
          <label class="importForm__logo">
            <input type="file" name="file" class="required hidden" accept=".csv">
          </label>
          <div class="importForm__text">{{trans('admin.seo.file_csv')}}</div>
        </div>

    @include('admin.partial.form.save',['cancel_url'=>route('seo_smart_links')])

  </form>
</div>

@endsection
