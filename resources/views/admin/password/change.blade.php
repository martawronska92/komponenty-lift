@extends('layouts.admin')

@section('content')

  <div class="addForm">
    <form class="form2col" action="" method="post" data-formvalidation novalidate="">
      <div class="form2col__wrapRow">
        <div class="form2col__row">
        <div class="form2col__left form2col__left--wide"><label for="old_password">{{trans('admin.password.old_password')}}</label></div>
        <div class="form2col__right inputBlock"><input type="password" class="required inputBlock__field" id="old_password" name="old_password"/></div>
        </div>
      </div>
      <div class="form2col__wrapRow">
        <div class="form2col__row">
        <div class="form2col__left form2col__left--wide"><label for="new_password">{{trans('admin.password.new_password')}}</label></div>
        <div class="form2col__right inputBlock"><input type="password" class="required inputBlock__field current_password" id="new_password" name="new_password"/></div>
      </div>
      </div>
      <div class="form2col__wrapRow">
        <div class="form2col__row">
        <div class="form2col__left form2col__left--wide"><label for="new_password_confirm">{{trans('admin.password.new_password_confirm')}}</label></div>
        <div class="form2col__right inputBlock"><input type="password" class="required inputBlock__field password_confirm" id="new_password_confirm" name="new_password_confirm"/></div>
      </div>
      </div>
      @include('admin.partial.form.save',['cancel_url'=> route('dashboard')])
    </form>
  </div>

@endsection
