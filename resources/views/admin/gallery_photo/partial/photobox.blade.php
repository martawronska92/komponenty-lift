<tr data-id="{{$photo['id']}}" class="{{$photo['draft'] ? 'is_draft' : ''}}" title="{{$photo['draft'] ? trans('admin.list.draft') : ''}}">
  <td>
    <div class="inputBlock__checkbox inputBlock__checkbox--noMargin">
      <input type="checkbox" name="photo[]" value="{{$photo['id']}}" id="photo_{{$photo['id']}}" data-list-itemcheckbox/>
      <label class="inputBlock__checkboxText" for="photo_{{$photo['id']}}"><i></i>
      </label>
    </div>
  </td>
  <td>
    <label for="photo_{{$photo['id']}}">
      <img src="/img/gallery_photo/{{$photo['dirname']}}/thumbnail.{{$photo['ext']}}?v={{md5($photo['updated_at'])}}"/>
    </label>
  </td>
  <td>
    @if(isset($title) && $title!="")
      {{$title}}
    @elseif(is_object($photo) && count($photo->language)>0 && $photo->language[0]->title!="")
      {{$photo->language[0]->title}}
    @else
    <i>{{trans('admin.gallery.empty_title')}}</i>
    @endif
  </td>
  <td>
    {{$photo['main'] ? strtoupper(trans('admin.yes')) : '-'}}
  </td>
  <td>
    {!!\App\Components\Clist\ListColumnFormat::switchValue($photo['active'],'gallery_photo',$photo['id'],'active')!!}
  </td>
  <td>
    <button type="button" class="button button--circle button--blue button--iconSquare" data-galleryphoto-main="{{$photo['id']}}" title="{{trans('admin.galleryphoto.setmain')}}">{{trans('admin.galleryphoto.setmain')}}</button>
    <button type="button" class="button button--circle button--blue button--iconPencil" data-galleryphoto-edit="{{$photo['id']}}" title="{{trans('admin.edit')}}">{{trans('admin.edit')}}</button>
    <button type="button" class="button button--circle button--gray button--iconDel" data-galleryphoto-remove="{{$photo['id']}}" title="{{trans('admin.delete')}}">{{trans('admin.delete')}}</button>
  </td>
</tr>
