<div class="tabsForm__row">
  @include('admin.partial.lang.input',[
    'label' => trans('admin.list.title'),
    'field' => 'title',
    'variable'=> 'photo'
  ])
</div>

<?php
  $params = [
    'pt_width'=>$settings['galleryphoto_thumb_width'],
    'pt_height'=>$settings['galleryphoto_thumb_height'],
    'pt_exists'=>0,
    'pt_delete_button' => 0,
    'pt_label' => 'admin.galleryphoto.add_button',
    'pt_callback' => 'galleryphoto.addPhoto()'
  ];
  if(isset($photo)){
    $params['pt_value'] = "";
    $params['pt_path'] = "/img/gallery_photo/".$photo->dirname."/original.".$photo->ext."?v=".md5($photo['updated_at']);
    $params['pt_nobutton'] = 1;
    if(isset($photo->thumbnail_options))
      $params['pt_options'] = $photo->thumbnail_options;
    unset($params['pt_callback']);
  }
?>
@include('admin.partial.image.thumbnail',$params)

<div class="tabsForm__row">
  <div class="text-center -w100">
    @if(isset($photo))
      <input type="hidden" value="{{$photo->id}}" id="photo_id">
      <button type="button" data-editable-save class="button button--small button--green button--withIcon button--iconGalka">{{trans('admin.save')}}</button>
      <button type="button" data-editable-cancel class="button button--small button--gray button--withIcon button--iconClose">{{trans('admin.cancel')}}</button>
    @else
      <button type="button" data-galleryphoto-add class="button button--small button--green button--withIcon button--iconAdd hidden">{{trans('admin.galleryphoto.add_button')}}</button>
    @endif
  </div>
</div>
