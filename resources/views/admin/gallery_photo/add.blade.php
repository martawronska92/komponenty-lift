@extends('layouts.admin')

@section('footer')
  <script>

    function template(data, container) {
      var $state = $(
        '<span><img src="/img/langs/' + data.id.toLowerCase() + '.png"></span>'
      );
      return $state;
    }

    function formatState (state) {
      if (!state.id) { return state.text; }
      var $state = $(
        '<span><img src="/img/langs/' + state.element.value.toLowerCase() + '.png"></span>'
      );
      return $state;
    }

    function select2Init(){
      $(".grayBg [data-lang-component]").select2({
        templateResult: formatState,
        templateSelection: template,
        minimumResultsForSearch: -1
      });

      $(".grayBg [data-lang-component]").bind("change",function(){
        var current = $(this).find("option:selected").val();
        var elements = $(this).closest("[data-lang-box]").find("[data-lang-input]");
        elements.fadeOut();

        var elem = $(this);

        $(this).closest("[data-lang-box]").find("label.error").remove();
        $(this).closest("[data-lang-box]").find(".error").removeClass("error");

        elements.promise().done(function() {
          $(this).closest("[data-lang-box]").find("[data-lang-input='"+current+"']").fadeIn();
        });
      });
    }

    $(document).ready(function(){
      galleryphoto.bindEvents();

      var edit = new editable("galleryphoto", function() {
          galleryphoto.addPhoto();
          $("[data-editabletab]").fadeOut();
      }, function() {
          galleryphoto.clearPreview();
          post.uploadThumb.bind();
          $("[data-editabletab]").fadeOut();
          setTimeout(function() {
            select2Init();
          },400);
      });
      $("[data-galleryphoto-list]").on("click", "[data-galleryphoto-edit]", function() {
          $("[data-editabletab]").fadeIn();
          $('html, body').animate({
            scrollTop: $("[data-galleryphoto-editable]").offset().top
          }, 1500);
          edit.getData($(this).attr('data-galleryphoto-edit'),function(){
            $("[data-galleryphoto-tabs]").find("li:last a").trigger("click");
            setTimeout(function(){
              post.uploadThumb.bind();
              select2Init();
            },400);
          });
      });

      page.bindEvents();
    });
  </script>
@endsection

@section('content')

<div class="addForm addForm--noPad">
<form class="form2col" action="" method="post" name="galleryForm" id="galleryForm"
      data-form-topreview  data-formvalidation novalidate="">

<div class="form2col__wrapRow">
  <div class="form2col__row photogalFix">
      @include('admin.partial.lang.input',[
        'label' => trans('admin.list.title'),
        'field' => 'title'
      ])
  </div>
</div>
<div class="form2col__wrapRow">
  <div class="form2col__row photogalFix">
      @include('admin.partial.lang.textarea',[
        'label' => trans('admin.list.description'),
        'field' => 'description'
      ])
  </div>
</div>
<div class="form2col__wrapRow">
  <div class="form2col__row">
  <div class="tabsForm__col2 tabsForm--vcenter">
    <label class="tabsForm__label" for='type'>{{trans('admin.gallery.show_type')}}</label>
  </div>
  <div class="tabsForm__col10">
    <select name="show_type_id" class="required form-control js-select2" id="type">
      @foreach($types as $typeId=>$typeName)
        <option
          @if(isset($row) && $row->show_type_id==$typeId) selected @endif
          value="{{$typeId}}">{{$typeName}}</option>
      @endforeach
    </select>
  </div>
</div>
</div>

<input type="hidden" id="pagePostModuleId" value="{{$pagePostModuleId}}"/>
<input type="hidden" id="id" value="{{$id}}"/>
@include('admin.partial.form.save',['cancel_url'=>$redirect_url,'generate_preview'=>'galleryphoto'])
</form>
</div>

@if(isset($row))
<div class="addToGallery">
  <div class="addForm" data-galleryphoto-editable>
    <div class="tabsForm__row tabsForm__row--wrap">
      <ul class="tabs" data-galleryphoto-tabs>
        <li class="active"><a data-toggle="tab" href="#add">{{trans('admin.galleryphoto.add_tab')}}</a></li>
        <li style="display:none;" data-editabletab><a data-toggle="tab" href="#edit">{{trans('admin.galleryphoto.edit_tab')}}</a></li>
      </ul>
      <div class="grayBg">
      <div class="tab-content">
        <div id="add" class="tab-pane fade in active text-center" data-editable-blockcontainer>
          <input type="file" name="multiple" id="data-galleryphoto-upload">
        </div>
        <div style="display:none;" data-editabletab id="edit" class="tab-pane fade text-center addOnePhoto" data-editable-replacecontainer>
          <p>{{trans('admin.galleryphoto.choose_to_edit')}}</p>
        </div>
      </div>
    </div>
    </div>
    <div data-editable-blockcontainer class="blockerBlock">
      <div class="downloadTable__buttons">
        <button type="button" data-galleryphoto-deletemarked class="button button--small button--blue button--withIcon button--iconDel">{{trans('admin.gallery.delete_marked')}}</button>
        <button type="button" data-galleryphoto-deleteall class="button button--small button--gray button--withIcon button--iconDel">{{trans('admin.gallery.delete_all')}}</button>
      </div>
      <div class="tabsForm__row addToGallery__list">
        <div class="downloadTable photoGalleryList">
          <table>
            <thead>
            <tr>
              <th>
              </th>
              <th><span>{{trans('admin.preview')}}</span>
              </th>
              <th><span>{{trans('admin.list.title')}}</span>
              </th>
              <th><span>{{trans('admin.galleryphoto.main')}}</span>
              </th>
              <th><span>{{trans('admin.galleryphoto.active')}}</span>
              </th>
              <th><span></span>
              </th>
            </tr>
            </thead>
            <tbody data-galleryphoto-list data-list-sortable="galleryphoto">
              @each('admin.gallery_photo.partial.photobox', $row->photo, 'photo')
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="tabsForm__row">
      <div class="resultsChanges resultsChanges--info @if(isset($row) && count($row->photo)>0) hidden @endif" data-list-empty>{{trans('admin.galleryphoto.empty')}}</div>
    </div>
  </div>
</div>
@endif

@include('admin.partial.dialog.confirm')

@if(isset($id))
  <input type="hidden" name="gallery_id" id="id" value="{{$id}}"/>
@endif
<input type="hidden" id="pagePostModuleId" value="{{$pagePostModuleId}}"/>

@endsection
