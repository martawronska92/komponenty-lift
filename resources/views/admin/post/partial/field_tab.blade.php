@if(count($fields) > 0)

  @foreach($fields as $field)
    <div class="tabsForm__row">
      <div class="tabsForm__col3 tabsForm--vcenter">
        @foreach($field->language as $lang)
          @if($lang->symbol == \App::getLocale())
            <label class="tabsForm__label" for="field_{{$field->id}}">{{$lang->pivot->name}}</label>
          @endif
        @endforeach
      </div>
      <div class="tabsForm__col9 tabsForm__innerRow form-group inputBlock">
        <input type="text" class="required form-control inputBlock__field" name="category_field_id[{{$field->id}}]"
          @if(isset($field->value))
            value="@if($field->pivot){{$field->pivot->value}}@endif"
          @endif
          @foreach($field->language as $lang)
            @if($lang->symbol == \App::getLocale())
              data-formvalidation-label="{{$lang->pivot->name}}"
            @endif
          @endforeach
        />
      </div>
    </div>
  @endforeach
@endif
