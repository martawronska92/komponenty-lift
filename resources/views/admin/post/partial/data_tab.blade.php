<div class="tabsForm__row">
    @include('admin.partial.lang.input',[
      'label' => trans('admin.list.title'),
      'field' => 'title',
      'required' => true,
      'linkgenerator' => 1
    ])
</div>

<div class="tabsForm__row">
  @include('admin.partial.lang.friendly_url',[
    'type' => 'post',
    'el' => 'title',
    'required' => true
  ])
</div>

<div class="tabsForm__row">
   @if(!isset($row))
     @include('admin.partial.lang.input',[
       'label' => trans('admin.settings.meta_title'),
       'field_name' => 'meta_title',
       'field_id' => 'meta_title',
       'field' => 'value',
       'required' => true,
       'variable' => 'settings',
       'variable_key' => 'meta_title'
     ])
   @else
    @include('admin.partial.lang.input',[
      'label' => trans('admin.settings.meta_title'),
      'field' => 'meta_title',
      'field_id' => 'meta_title',
      'required' => true
    ])
  @endif
</div>

<div class="tabsForm__row">
  @if(!isset($row))
    @include('admin.partial.lang.textarea',[
      'label' => trans('admin.settings.meta_description'),
      'field_name' => 'meta_description',
      'field_id' => 'meta_description',
      'field' => 'value',
      'required' => true,
      'variable' => 'settings',
      'variable_key' => 'meta_description',
      'maxlength' => 255
    ])
  @else
    @include('admin.partial.lang.textarea',[
      'label' => trans('admin.settings.meta_description'),
      'field' => 'meta_description',
      'required' => true,
      'maxlength' => 255
    ])
  @endif
</div>

<div class="tabsForm__row">
  @if(!isset($row))
    @include('admin.partial.lang.textarea',[
      'label' => trans('admin.post.intro'),
      'field_name' => 'content',
      'field_id' => 'content',
      'field' => 'value',
      'variable' => 'language',
      'variable_key' => 'content'
    ])
  @else
   @include('admin.partial.lang.textarea',[
     'label' => trans('admin.post.intro'),
     'field' => 'content',
     'field_id' => 'content'
   ])
  @endif
</div>

<input type="hidden" value="@if(isset($row)){{$row->category_id}}@else{{$category}}@endif" name="category_id" id="category_id"/>
