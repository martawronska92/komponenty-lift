
<div class="tabsForm__row tabsForm__row--start">

    <select data-multiple-select multiple="multiple" style="width:450px" name="tag[]">
      @foreach($tags as $tag)
        <option value="{{$tag->id}}" @if(isset($row) && in_array($tag->id,$row->tag->pluck('id')->toArray())) selected @endif>{{$tag->text}}</option>
      @endforeach
    </select>


    <button class="button button--xs button--blue" type="button" data-toggle="collapse" data-target="#tagAddition"
      aria-expanded="false" aria-controls="tagAddition" data-tag-showbox>
      {{trans('admin.tag.add_new')}}
    </button>

</div>

<div class="tabsForm__row">

    <div class="collapse" id="tagAddition">

      <div class="" data-tag-box>
        <div class="tabsForm__row">

              @include('admin.partial.lang.input',[
                  'label' => trans('admin.list.name'),
                  'field' => 'name'
              ])

        </div>
        <div class="tabsForm__row">
            <div class="-tac -w100">
            <button type="button" class="button button--xs button--green" data-tag-add>{{trans('admin.list.add')}}</button>
            <button type="button" class="button button--xs button--gray" data-toggle="collapse" data-target="#tagAddition" data-tag-hidebox>{{trans('admin.cancel')}}</button>
            </div>

        </div>
      </div>
    </div>

</div>
