@if(!isset($from_module))
  <div class="previewBlock">
    <div class="previewBlock__name">
      {{trans('admin.post.preview')}}
    </div>
    <div class="text-center">
      <button type="button" class="button button--small button--green button--withIcon button--iconAdd" data-post-preview class="btn btn-default">{{trans('admin.post.generate')}}</button>
    </div>
  </div>
@endif



  @if(isset($from_module))
    <input type="hidden" name="public" value="1"/>
  @else
    <div class="thumbnailStatus thumbnailStatus--without">
      <div class="inputBlock__checkbox">
        <input type="checkbox" name="public" id="post_public"
          @if( (isset($row) && $row->public) || !isset($row))
          checked
          @endif
        >
        <label class="inputBlock__checkboxText" for="post_public"><i></i><span></span></label>
      </div>
      <div class="thumbnailStatus__name">
        <label for="post_public">{{trans('admin.post.publish_now')}}</label>
      </div>


    </div>
    <div class="datetime inputBlock">

        <input type="text" data-datetimepicker="HH:mm, dd.MM.yyyy" name="publish_date" id="publish_date" readonly
          class="inputBlock__field @if((isset($row) && $row->publish_date=="") || !isset($row)) hidden @endif"
          placeholder="{{trans('admin.post.choose_publish_date')}}"
          value="@if(isset($row)){{\App\Helpers\Format::formatDateTime($row->publish_date)}}@endif"/>

    </div>
  @endif

  <div class="thumbnailStatus">
    <div class="thumbnailStatus__name">{{trans('admin.post.status')}}</div>
      <div class="inputBlock__checkbox">
        <input type="checkbox" name="active" id="active"
        @if( (isset($row) && $row->active) || !isset($row))
          checked
        @endif
        >
      <label class="inputBlock__checkboxText" for="active"><i></i><span>{{trans('admin.list.active')}}</span></label>
    </div>
  </div>

<hr>

<div class="imgPreview">
<div class="imgPreview__name">{{trans('admin.post.thumbnail')}}</div>
<div class="imgPreview__block">
  @include('admin.partial.image.thumbnail',[
    'pt_width' => $settings['post_thumb_width'],
    'pt_height' => $settings['post_thumb_height'],
    'pt_exists' => isset($row) && $row->thumbnail!="" ? 1 : 0,
    'pt_path' => isset($row) ? "/img/post/".$row->id."/thumbnail.".$row->thumbnail : "",
    'pt_value' => isset($row) ? $row->thumbnail : "",
    'pt_delete_button' => 1,
    'pt_options' => isset($row) ? $row->thumbnail_options : "",
    'pt_nobutton' => 1
  ])
</div>
</div>
