@extends('layouts.admin')

@section('footer')
  <script>
    $(document).ready(function(){
      category.field.bindEvents();
    });
  </script>
@endsection

@section('content')

@if(isset($row))
  <div class="row">
    <div class="col-lg-12 text-right">
      <a class="btn btn-primary" href="{{route('post',['pagePostModuleId'=> $pagePostModuleId,'id'=> $id])}}">{{trans('admin.category.showposts')}}</a><br/><br/>
    </div>
  </div>
@endif

<form action="" method="post" novalidate data-formvalidation>

  <div class="row">

    <div class="col-lg-6">
      <div class="panel panel-default">
        <div class="panel-heading">{{trans('admin.category.basic_data')}}</div>
        <div class="panel-body">
          @include('admin.category.partial.data')
        </div>
      </div>
    </div>

    <div class="col-lg-6">
      <div class="panel panel-default">
        <div class="panel-heading">{{trans('admin.category.additional_fields')}}
          <button type="button" class="btn btn-success pull-right"
              data-category-addfield>{{trans('admin.category.addfield')}}</button>
          <div class="clearfix"></div>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-lg-12">
              <div data-category-fieldbox>
                @if(isset($row))
                  @foreach($row->field as $key=>$field)
                    @include('admin.category.partial.field',['field'=>'field','field_data'=>$field, 'index'=>$key])
                  @endforeach
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <input type="hidden" name="deleted_fields" value=""/>

  @include('admin.partial.form.save',['cancel_url'=>$redirect_url,'save_and_stay'=>1])
</form>

<div class="hidden" data-category-fieldpartial>
  @include('admin.category.partial.field',['field'=>'field'])
</div>

@endsection
