@extends('layouts.admin')

@section('footer')
  <script>

    function template(data, container) {
      var $state = $(
        '<span><img src="/img/langs/' + data.id.toLowerCase() + '.png"></span>'
      );
      return $state;
    }

    function formatState (state) {
      if (!state.id) { return state.text; }
      var $state = $(
        '<span><img src="/img/langs/' + state.element.value.toLowerCase() + '.png"></span>'
      );
      return $state;
    }

    function select2Init(){
      $(".grayBg [data-lang-component]").select2({
        templateResult: formatState,
        templateSelection: template,
        minimumResultsForSearch: -1
      });

      $(".grayBg [data-lang-component]").bind("change",function(){
        var current = $(this).find("option:selected").val();
        var elements = $(this).closest("[data-lang-box]").find("[data-lang-input]");
        elements.fadeOut();

        var elem = $(this);

        $(this).closest("[data-lang-box]").find("label.error").remove();
        $(this).closest("[data-lang-box]").find(".error").removeClass("error");

        elements.promise().done(function() {
          $(this).closest("[data-lang-box]").find("[data-lang-input='"+current+"']").fadeIn();
        });
      });
    }

    $(document).ready(function(){
      var edit = new editable("galleryvideo", function() {
          galleryvideo.addVideo();
      }, function() {
          galleryvideo.clearPreview();
        setTimeout(function() {
          select2Init();
        },400);
      });
      $("[data-galleryvideo-list]").on("click", "[data-galleryvideo-edit]", function() {
          $('html, body').animate({
            scrollTop: $("[data-editable-replacecontainer]").offset().top
          }, 1500);
          edit.getData($(this).attr('data-galleryvideo-edit'),function(){
            setTimeout(function() {
              select2Init();
            },200);
          });
      });

      page.bindEvents();

    });
  </script>
@endsection

@section('content')
<div class="addForm addForm--noPad">
<form class="form2col" action="" method="post" name="galleryForm" id="galleryForm"
      data-form-topreview  data-formvalidation novalidate="">

<div class="form2col__wrapRow">
    <div class="form2col__row">
        @include('admin.partial.lang.input',[
          'label' => trans('admin.list.title'),
          'field' => 'title',
        ])
    </div>
</div>
<div class="form2col__wrapRow">
    <div class="form2col__row">
        @include('admin.partial.lang.textarea',[
          'label' => trans('admin.list.description'),
          'field' => 'description'
        ])
    </div>
</div>

<div class="form2col__wrapRow">
  <div class="form2col__row">
  <div class="tabsForm__col2 tabsForm--vcenter">
    <label class="tabsForm__label" for='type'>{{trans('admin.gallery.show_type')}}</label>
  </div>
  <div class="tabsForm__col10">
    <select class="js-select2" name="show_type_id" class="required form-control" id="type">
      @foreach($types as $typeId=>$typeName)
        <option
          @if(isset($row) && $row->show_type_id==$typeId) selected @endif
          value="{{$typeId}}">{{$typeName}}</option>
      @endforeach
    </select>
  </div>
</div>
</div>
<input type="hidden" id="pagePostModuleId" value="{{$pagePostModuleId}}"/>
<input type="hidden" id="id" value="{{$id}}"/>
@include('admin.partial.form.save',['cancel_url'=>$redirect_url,'generate_preview'=>'galleryvideo'])

</form>
</div>

@if(isset($row))
  <hr>
  <div class="addToGallery">
    <div class="addForm">
    <div class="grayBg" data-galleryvideo-leftcontainer data-editable-replacecontainer>
      @include('admin.gallery_video.partial.video_form')
    </div>
    <div class="" data-galleryvideo-rightcontainer data-editable-blockcontainer>
      <div class="downloadTable__buttons">
      <button type="button" data-galleryvideo-deletemarked class="button button--small button--blue button--withIcon button--iconDel">{{trans('admin.gallery.delete_marked')}}</button>
      <button type="button" data-galleryvideo-deleteall class="button button--small button--gray button--withIcon button--iconDel">{{trans('admin.gallery.delete_all')}}</button>
      </div>
      <div class="tabsForm__row addToGallery__list">
      <div class="videoTable photoGalleryList">
        <table>
          <thead>
          <tr>
            <th>
            </th>
            <th><span>Podgląd</span>
            </th>
            <th><span>Tytuł</span>
            </th>
            <th><span>Aktywny</span>
            </th>
            <th><span></span>
            </th>
          </tr>
          </thead>
          <tbody data-galleryvideo-list data-list-sortable="galleryvideo">
          @each('admin.gallery_video.partial.videobox', $row->video, 'video')
          </tbody>
        </table>
      </div>
      </div>
      <div class="tabsForm__row">
      <div class="resultsChanges resultsChanges--info @if(isset($row) && count($row->video)>0) hidden @endif" data-list-empty>{{trans('admin.galleryvideo.empty')}}</div>
      </div>
    </div>
  </div>
  </div>

  <input type="hidden" id="galleryId" value="{{$row->id}}"/>
@endif

@include('admin.partial.dialog.confirm')

@endsection
