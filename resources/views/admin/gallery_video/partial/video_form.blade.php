<form action="" method="post" data-formvalidation novalidate="" id="galleryvideoForm" name="galleryvideoForm">
  <div class="tabsForm__row">
    <div class="col-lg-12 text-center">
      <img src="{{(isset($video)) ? $video->thumbnail : '/images/default_video_thumb.jpg'}}" data-galleryvideo-preview />
    </div>
  </div>
  <div class="tabsForm__row tabsForm__row--center">
      <p data-galleryvideo-title class="-tac tabsForm__label tabsForm__label--small">{{trans('admin.galleryvideo.name')}}</p>
  </div>
  <div class="tabsForm__row">
    <div class="tabsForm__col2 tabsForm--vcenter">
      <label class="tabsForm__label">{{trans('admin.galleryvideo.video_url')}}</label>
    </div>
    <div class="tabsForm__innerRow tabsForm__col10 inputBlock">
      <input type="text" name="youtube_link" class="required youtube_link inputBlock__field"
        @if(isset($video)) value="{{$video->url}}" @endif
        maxlength="255" />
    </div>
  </div>
  <div class="tabsForm__row">
      @include('admin.partial.lang.input',[
        'label' => trans('admin.list.title'),
        'field' => 'title',
        'variable' => 'video'
      ])    
  </div>
  <div class="tabsForm__row tabsForm__row--center">

      @if(!isset($video))
        <button type="button" data-galleryvideo-check class="button button--small button--green">{{trans('admin.galleryvideo.add_button')}}</button>
      @else
        <button type="button" data-editable-save class="button button--small button--blue">{{trans('admin.save')}}</button>
        <button type="button" data-editable-cancel class="button button--small button--gray">{{trans('admin.cancel')}}</button>
      @endif

  </div>
  @if(isset($video))
    <input type="hidden" id="video_id" value="{{$video->id}}"/>
  @endif
</form>
