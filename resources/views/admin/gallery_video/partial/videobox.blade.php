<tr data-id="{{$video['id']}}" class="{{$video['draft'] ? 'is_draft' : ''}}" title="{{$video['draft'] ? trans('admin.list.draft') : ''}}">
  <td>
  <div class="inputBlock__checkbox inputBlock__checkbox--noMargin">
    <input type="checkbox" name="video[]" value="{{$video['id']}}" id="video_{{$video['id']}}" data-list-itemcheckbox/>
    <label class="inputBlock__checkboxText" for="video_{{$video['id']}}"><i></i>
    </label>
  </div>
  </td>
  <td>
    <label for="video_{{$video['id']}}">
      <img src="{{$video['thumbnail']}}"/>
    </label>
  </td>
  <td>
    <a href="{{$video['url']}}" target="_blank">
      @if(array_key_exists('title',$video) && $video['title']!="")
        {{$video['title']}}
      @elseif(is_object($video) && count($video->language)>0 && $video->language[0]->title!="")
        {{$video->language[0]->title}}
      @else
        <i>{{trans('admin.gallery.empty_title')}}</i>
      @endif
    </a>
  </td>
  <td>
    {!!\App\Components\Clist\ListColumnFormat::switchValue($video['active'],'gallery_video',$video['id'],'active')!!}
  </td>
  <td>
    <button type="button" class="button button--circle button--blue button--iconPencil" data-galleryvideo-edit="{{$video['id']}}" title="{{trans('admin.edit')}}"><i class="fa fa-pencil"></i> {{trans('admin.edit')}}</button>
    <button type="button" class="button button--circle button--gray button--iconDel" data-galleryvideo-remove="{{$video['id']}}" title="{{trans('admin.delete')}}"><i class="fa fa-trash-o"></i> {{trans('admin.delete')}}</button>
  </td>
</tr>
