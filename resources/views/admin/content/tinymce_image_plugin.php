<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="title" content="@if(isset($meta_title)){{$meta_title}}@else{{$settings['meta_title']}}@endif">
    <meta name="description" content="@if(isset($meta_description)){{$meta_description}}@else{{$settings['meta_description']}}@endif">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ elixir('css/app.css')}}" rel="stylesheet">
    <link href="{{ elixir('css/app_admin.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        var currentLang = "{{\App::getLocale()}}";
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
            'lang' => trans('front_js')
        ],JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES); ?>;
    </script>
</head>
<body>
    <div id="app">
        asdasdas

    </div>

    <!-- Scripts -->
    <script src="{{ elixir('js/app_admin.js') }}"></script>
    <script src="{{ elixir('js/common.js') }}"></script>
    <script src="{{ elixir('js/admin.js') }}"></script>
</body>
</html>
