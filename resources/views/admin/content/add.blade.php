@extends('layouts.admin')

@section('footer')

  <script>
    $(document).ready(function(){
      //editor.bindEvents();
      page.bindEvents();
    });
    window.Laravel.config = <?php echo json_encode([
      'youtube_width' => $settings['video_content_width'],
      'youtube_height' => $settings['video_content_height']
    ],JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES); ?>;
  </script>

@endsection

@section('content')

      <form action="" method="post" name="contentForm" id="contentForm" class="addPost" data-formvalidation novalidate="" data-form-topreview>

        <div class="tabsForm__row">
                @include('admin.partial.lang.input',[
                  'label' => trans('admin.list.title'),
                  'field' => 'title'
                ])
        </div>


              @include('admin.partial.lang.wyswig',[
                'label' => trans('admin.list.content'),
                'field' => 'content'
              ])

        <input type="hidden" id="pagePostModuleId" value="{{$pagePostModuleId}}"/>
        <input type="hidden" id="id" value="{{$id}}">

        @include('admin.partial.form.save',['cancel_url'=>$redirect_url,'generate_preview'=>'content'])

      </form>

      <input name="image" type="file" id="upload" class="hidden" onchange="">
@endsection
