@extends('layouts.admin')

@section('footer')
  <script>
    $(document).ready(function(){
      page.init();
    });
  </script>
@endsection

@section('content')
<div class="addCategory">
  <form action="" method="post" data-formvalidation>
    <div class="addCategory__row">
        <ul class="tabs" role="tablist" data-tabs>
            <li role="presentation" class="{{!isset($row) ? 'active' : ''}}">
              <a href="#page_data" aria-controls="page_data" role="tab" data-toggle="tab">{{trans('admin.page.tab_data')}}</a></li>
            <li role="presentation" class="{{isset($row) ? 'active' : ''}}">
              <a href="#page_modules" aria-controls="page_modules" role="tab" data-toggle="tab">{{trans('admin.page.tab_modules')}}</a></li>
        </ul>
    </div>
    <div class="addCategory__row">
      <div class="addCategory__left">
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane {{!isset($row) ? 'active' : ''}}" id="page_data">
            <div class="tabsForm">
              @include('admin.page.partial.tab_data')
            </div>
          </div>
          <div role="tabpanel" class="tab-pane {{isset($row) ? 'active' : ''}}" id="page_modules">
            <div class="tabsForm">@include('admin.page.partial.tab_modules')</div>
          </div>
        </div>
      </div>
      <div class="addCategory__right">
        @include('admin.page.partial.tab_options')
      </div>
    </div>

  @include('admin.partial.form.save',['cancel_url'=> $back_url,'save_and_stay'=>1])

  </form>
</div>

<input type="hidden" id="id" value="@if(isset($id)){{$id}}@endif"/>
<input type="hidden" id="entity_type" value="page"/>

<div data-page-modulepartial class="hidden">
  {!!\View::make('admin.partial.module.choose_row')!!}
</div>

@include('admin.partial.dialog.confirm')

@endsection
