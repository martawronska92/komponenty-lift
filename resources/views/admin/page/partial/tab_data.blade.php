{{-- title --}}

<div class="tabsForm__row">
    @include('admin.partial.lang.input',[
      'label' => trans('admin.list.title'),
      'field' => 'title',
      'required' => true,
      'linkgenerator' => 1
    ])
</div>

{{-- friendly url --}}

<div class="tabsForm__row">
  @include('admin.partial.lang.friendly_url',[
    'type' => 'page',
    'el' => 'title',
    'required' => true
  ])
</div>



{{-- meta title --}}

<div class="tabsForm__row">
   @if(!isset($row))
     @include('admin.partial.lang.input',[
       'label' => trans('admin.settings.meta_title'),
       'field_name' => 'meta_title',
       'field_id' => 'field_id',
       'field' => 'value',
       'required' => true,
       'variable' => 'settings',
       'variable_key' => 'meta_title'
     ])
   @else
    @include('admin.partial.lang.input',[
      'label' => trans('admin.settings.meta_title'),
      'field' => 'meta_title',
      'required' => true
    ])
  @endif
</div>

{{-- meta description --}}

<div class="tabsForm__row">
  @if(!isset($row))
    @include('admin.partial.lang.textarea',[
      'label' => trans('admin.settings.meta_description'),
      'field_name' => 'meta_description',
      'field_id' => 'field_id',
      'field' => 'value',
      'variable' => 'settings',
      'variable_key' => 'meta_description',
      'maxlength' => 255
    ])
  @else
    @include('admin.partial.lang.textarea',[
      'label' => trans('admin.settings.meta_description'),
      'field' => 'meta_description',
      'maxlength' => 255
    ])
  @endif
</div>


{{-- strona nadrzędna --}}
@if(isset($nested_level))
{{$nested_level}}
@endif
<div class="tabsForm__row">
  <div class="tabsForm__col3 tabsForm--vcenter">
    <label class="tabsForm__label" for="parent_page_id">{{trans('admin.page.parent_page')}}</label>
  </div>
  <div class="tabsForm__col9 tabsForm__innerRow form-group">
      <select id="parent_page_id" name="parent_page_id" class="form-control js-select2">
        <option value="">{{trans('admin.none')}}</option>
        @foreach($pages as $page)
          @if($page->level < \Config::get('cms.page_level') - 1)
            <option value="{{$page->id}}" @if( (isset($row) && $page->id == $row->parent_page_id) || (isset($parentPageId) && $page->id == $parentPageId) ) selected @endif>{{$page->title}}</option>
          @endif
        @endforeach
      </select>
  </div>
</div>

{{-- widok strony --}}

@if(\App\Models\User::isDeveloper())
  <div class="tabsForm__row">
    <div class="tabsForm__col3 tabsForm--vcenter">
      <label class="tabsForm__label" for="default_view">{{trans('admin.page.page_view')}}</label>
    </div>

     <div class="tabsForm__col9 tabsForm__innerRow form-group">
          <select id="default_view" name="default_view" class="form-control js-select2">
            @foreach($views as $view)
              <option value="{{$view}}" @if(isset($row) && $view == $row->default_view) selected
                @elseif(!isset($row) && isset($parentRow) && $view == $parentRow->default_children_view) selected
                @endif>{{$view}}</option>
            @endforeach
          </select>
      </div>
    </div>

    <div class="tabsForm__row">
      <div class="tabsForm__col3 tabsForm--vcenter">
        <label class="tabsForm__label" for="default_view">{{trans('admin.page.page_children_view')}}</label>
      </div>

       <div class="tabsForm__col9 tabsForm__innerRow form-group">
            <select id="default_children_view" name="default_children_view" class="form-control js-select2">
              @foreach($views as $view)
                <option value="{{$view}}" @if(isset($row) && $view == $row->default_children_view) selected
                @elseif(!isset($row) && isset($parentRow) && $view == $parentRow->default_children_view) selected
                @endif>{{$view}}</option>
              @endforeach
            </select>
        </div>
      </div>
@else
  <input type="hidden" name="default_view" value="<?php if(isset($row)) echo $row->default_view; else if(!isset($row) && isset($parentRow) && $parentRow->default_children_view!=NULL) echo $parentRow->default_children_view; else echo 'page.view.default'; ?>"/>
  <input type="hidden" name="default_children_view" value="<?php if(isset($row)) echo $row->default_children_view; else if(!isset($row) && isset($parentRow) && $parentRow->default_children_view!=NULL) echo $parentRow->default_children_view; else echo 'page.view.default'; ?>"/>
@endif
