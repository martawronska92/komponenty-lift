<div class="tabsForm__row">
   @if(!isset($row))
     @include('admin.partial.lang.input',[
       'label' => trans('admin.settings.meta_title'),
       'field_name' => 'meta_title',
       'field_id' => 'field_id',
       'field' => 'value',
       'required' => true,
       'variable' => 'settings',
       'variable_key' => 'meta_title'
     ])
   @else
    @include('admin.partial.lang.input',[
      'label' => trans('admin.settings.meta_title'),
      'field' => 'meta_title',
      'required' => true
    ])
  @endif
</div>

<div class="tabsForm__row">
  @if(!isset($row))
    @include('admin.partial.lang.textarea',[
      'label' => trans('admin.settings.meta_description'),
      'field_name' => 'meta_description',
      'field_id' => 'field_id',
      'field' => 'value',
      'required' => true,
      'variable' => 'settings',
      'variable_key' => 'meta_description',
      'maxlength' => 255
    ])
  @else
    @include('admin.partial.lang.textarea',[
      'label' => trans('admin.settings.meta_description'),
      'field' => 'meta_description',
      'required' => true,
      'maxlength' => 255
    ])
  @endif
</div>
