<div class="previewBlock">
  <div class="previewBlock__name">{{trans('admin.page.page_preview')}}</div>

  <div class="text-center">
    <button type="button" class="button button--small button--green button--withIcon button--iconAdd" data-page-preview>{{trans('admin.page.preview')}}</button>
  </div>
</div>



{{-- active --}}
<div class="previewBlock__checkbox">
<div class="tabsForm__row">
  <div class="inputBlock__checkbox">    
    <input id="active" name="active" value="1" type="checkbox"
      @if((isset($row) &&  $row->active == 1) || !isset($row)) checked @endif >
    <label class="inputBlock__checkboxText" for="active"><i></i><span>{{trans('admin.gallery.active')}}</span></label>
  </div>
</div>
</div>
