
    <input type="hidden" name="loading_type" value="show_button"/>

    <div class="newsBlock__row newsBlock__row--margin">
      <div class="newsBlock__col50 newsBlock__col50--Vcenter">
        <label class="tabsForm__label tabsForm__label--small">{{trans('admin.category.per_page')}}</label>
      </div>
      <div class="newsBlock__col50">
        <input type="number" min="1" name="per_page" class="required inputBlock__field"
        value="@if(isset($row)){{$row->per_page}}@else{{old('per_page')}}@endif"/>
      </div>
    </div>

    @if(!\App\Models\User::isDeveloper())

    @else
        <div class="newsBlock__row newsBlock__row--margin">
          <div class="newsBlock__col50 newsBlock__col50--Vcenter">
            <label class="tabsForm__label tabsForm__label--small">{{trans('admin.category.category_view')}}</label>
          </div>
          <div class="newsBlock__col50">
            <select name='category_view' class="required js-select2">
              @foreach($category_view as $viewSymbol => $viewName){
                <option
                  @if((isset($row) && $row->category_view==$viewSymbol) || (old('category_view')==$viewSymbol) ) selected @endif
                  value="{{$viewSymbol}}">{{$viewName}}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="newsBlock__row newsBlock__row--margin">
          <div class="newsBlock__col50 newsBlock__col50--Vcenter">
            <label class="tabsForm__label tabsForm__label--small">{{trans('admin.category.post_view')}}</label>
          </div>
          <div class="newsBlock__col50">
            <select name='post_view' class="required js-select2">
              @foreach($post_view as $view){
                <option
                  @if((isset($row) && $row->post_view==$view) || (old('post_view')==$view) ) selected @endif
                  value="{{$view}}">{{$view}}</option>
              @endforeach
            </select>
          </div>
        </div>
        @endif

