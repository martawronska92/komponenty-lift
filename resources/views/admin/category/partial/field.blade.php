
  <div class="newsBlock__item" data-category-fieldrow>
    <div class="newsBlock__col1 tabsForm__label tabsForm__label--small">
      {{trans('admin.category.attribute_name')}}
    </div>
    <div class="newsBlock__col2">
      @foreach($langs as $lang)
          <div class="">
          @if(count($langs)>1)
            <div class="">{{$lang->name}}</div>
          @endif
          <div class="inputBlock">
            <input type="text" data-language="{{$lang->id}}"
                 data-field="{{$field}}"
                 name="{{$field}}[@if(isset($index)){{$index}}@endif][{{$lang->id}}]"
                 class="required inputBlock__field"


          @if(isset($field_data))
            @foreach($field_data->language as $fieldlang)
              @if($fieldlang->id == $lang->id)
                value="{{$fieldlang->pivot->name}}"
              @endif
            @endforeach
          @else
            value="{{old($field."[".$lang->id."]")}}"
          @endif
        />
        </div>
      </div>
      @endforeach

      @if(isset($field_data))
        <input type="hidden" data-category-fieldid name="fieldId[@if(isset($index)){{$index}}@endif]" value="{{$field_data->id}}"/>
      @endif
    </div>
    <div class="newsBlock__col3">
      <button class="button button--circle button--gray button--iconDel" type="button" data-category-fielddelete>{{trans('admin.delete')}}</button>
    </div>
  </div>
