@extends('layouts.admin')

@section('footer')
  <script>
    $(document).ready(function(){
      category.field.bindEvents();
    });
  </script>
@endsection

@section('content')
<div class="newsBlock">
@if(isset($row))
  <div class="listButtons">
      <a class="button button--small button--blue button--withIcon button--iconList" href="{{route('post',['pagePostModuleId'=> $pagePostModuleId,'id'=> $id])}}">{{trans('admin.category.showposts')}}</a><br/><br/>
  </div>
@endif

<div class="newsBlock__inner">
<form action="" method="post" novalidate data-formvalidation>
<div class="newsBlock__wrap">
  <div class="newsBlock__row">

    <div class="newsBlock__col50">
      <div class="panel panel-default">
        <div class="panel-heading tabsForm__label">{{trans('admin.category.basic_data')}}</div>
        <div class="panel-body">
          @include('admin.category.partial.data')
        </div>
      </div>
    </div>

    <div class="newsBlock__col50">
      <div class="panel panel-default">
        <div class="panel-heading tabsForm__label">
          <span>{{trans('admin.category.additional_fields')}}</span>
          <button type="button" class="button button--xs button--green"
              data-category-addfield>{{trans('admin.category.addfield')}}</button>
        </div>
        <div class="panel-body">
          <div class="" data-category-fieldbox>


                @if(isset($row))
                  @foreach($row->field as $key=>$field)
                    @include('admin.category.partial.field',['field'=>'field','field_data'=>$field, 'index'=>$key])
                  @endforeach
                @endif


          </div>
        </div>
      </div>
    </div>
  </div>

  <input type="hidden" name="deleted_fields" value=""/>
</div>
  @include('admin.partial.form.save',['cancel_url'=>$redirect_url,'save_and_stay'=>1])

</form>
</div>

<div class="hidden" data-category-fieldpartial>
  @include('admin.category.partial.field',['field'=>'field'])
</div>
</div>

@endsection
