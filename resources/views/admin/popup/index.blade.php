@extends('layouts.admin')

@section('script')
  <script>
    $(document).ready(function () {
      $("#popupForm ")
    });
  </script>
@endsection

@section('content')

  <div class="addCategory">
    <form id="popupForm" action="" method="post" novalidate data-formvalidation data-form-beforesubmit="post.uploadThumb.crop">

      <div class="addCategory__row">
        <div class="addCategory__left">
          <div class="fullWidth popupBlock">
            <div class="popupBlock__row">

              <div class="popupBlock__col1">
                <label>{{trans('admin.popup.status')}}:</label>
              </div>
              <div class="popupBlock__col2">

                <div class="inputBlock__checkbox">
                  <input type="checkbox" name="active" id="active" @if(($row && $row->active) || !$row) checked @endif />
                  <label class="inputBlock__checkboxText" for="active"><i></i><span>{{strtolower(trans('admin.list.active'))}}</span></label>
                </div>

                <div class="inputBlock__checkbox">
                  <input type="checkbox" name="visible" id="visible" @if( ($row && $row->visible_from=="" && $row->visible_to=="") || !$row) checked @endif />
                  <label class="inputBlock__checkboxText" for="visible"><i></i><span>{{trans('admin.popup.visible')}}</span></label>
                </div>

              </div>
            </div>
            <div class="popupBlock__row @if(!$row || ($row->visible_from=='' || $row->visible_to=='')) hidden @endif form-group" id="popupVisibleBox">
              <div class="popupBlock__col50">
              <label for="visible_from">{{trans('admin.popup.visible_from')}}</label>
              <input type="text" name="visible_from" id="visible_from"
                class="@if(isset($row) && $row->visible_from!=null) required date_pl @endif inputBlock__field"
                value="@if(isset($row)){{\App\Helpers\Format::formatDate($row->visible_from)}}@else{{old('visible_from')}}@endif"
                data-datetimepicker="dd-MM-yyyy"
                data-formvalidation-label="{{ucfirst(trans('admin.popup.visible_from'))}}"/>
              </div>

              <div class="popupBlock__col50">
              <label for="visible_to">{{trans('admin.popup.visible_to')}}</label>
              <input type="text" name="visible_to" id="visible_to"
                class="@if(isset($row) && $row->visible_to!=null) required date_pl date_greater_than @endif inputBlock__field"
                value="@if(isset($row)){{\App\Helpers\Format::formatDate($row->visible_to)}}@else{{old('visible_to')}}@endif"
                data-datetimepicker="dd-MM-yyyy"
                data-formvalidation-label="{{ucfirst(trans('admin.popup.visible_to'))}}"/>
              </div>
            </div>

            <div class="popupBlock__row" id="linkTypePopupBox" @if(isset($row) && $row->content_type!="image")style="display:none;"@endif>
              <div class="popupBlock__col1">
                <label>{{trans('admin.popup.link_for_element')}}:</label>
              </div>
              <div class="popupBlock__col2" data-popup-linkchoose>

                <div class="inputBlock__checkbox">
                  <input type="radio" name="link_type" value="" class="required"
                    data-formvalidation-label="{{trans('admin.popup.link_for_element')}}"
                    @if( (isset($row) && $row->link_type=="") || !isset($row)) checked @endif
                  id="link_type_none">
                  <label class="inputBlock__checkboxText" for="link_type_none"><i></i><span>{{trans('admin.none')}}</span></label>
                </div>

                <div class="inputBlock__checkbox">
                  <input type="radio" name="link_type" value="page" class="required"
                    data-formvalidation-label="{{trans('admin.popup.link_for_element')}}"
                    @if(isset($row) && $row->link_type=="page") checked @endif
                  id="link_type_page">
                  <label class="inputBlock__checkboxText" for="link_type_page"><i></i><span>{{trans('admin.popup.link_for_page')}}</span></label>
                </div>


                <select name="page" data-popup-linktype='page'
                  class="js-select2 @if(!isset($row) || (isset($row) && $row->link_type!='page')) hidden @endif"
                  data-formvalidation-label="{{trans('admin.popup.link_for_element')}} ({{trans('admin.popup.link_for_page')}})">
                  @foreach($pages->pages as $page)
                    @if($page->level == 0)
                      <option value="{{$page->id}}"
                        @if(isset($row) && $row->link_type=="page" && $row->link_value == $page->id) selected @endif
                          >{{$page->title}}</option>
                        @if(array_key_exists($page->id,$pages->parent))
                          @foreach($pages->parent[$page->id] as $subpage1)
                            <option data-level="1" @if(isset($row) && $row->link_type=="page" && $row->link_value == $subpage1->id) selected @endif
                              value="{{$subpage1->title}}">&nbsp;&nbsp;&nbsp;{{$subpage1->title}}</option>
                            @if(array_key_exists($subpage1->id,$pages->parent))
                              @foreach($pages->parent[$subpage1->id] as $subpage2)
                                <option data-level="2" @if(isset($row) && $row->link_type=="page" && $row->link_value == $subpage2->id) selected @endif
                                  value="{{$subpage2->title}}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$subpage2->title}}</option>
                              @endforeach
                            @endif
                          @endforeach
                        @endif
                    @endif
                  @endforeach
                </select>

                <div class="inputBlock__checkbox">
                  <input type="radio" name="link_type" value="link" class="required"
                    data-formvalidation-label="{{trans('admin.popup.link_for_element')}}"
                    @if(isset($row) && $row->link_type=="link") checked @endif
                  id="link_type_link">
                  <label class="inputBlock__checkboxText" for="link_type_link"><i></i><span>{{trans('admin.popup.link_for_link')}}</span></label>
                </div>
                  <div class="inputBlock">
                <input type="text" name="url" data-popup-linktype='link'
                  data-formvalidation-label="{{trans('admin.popup.link_for_element')}} ({{trans('admin.popup.link_for_link')}})"
                  class="inputBlock__field @if(!isset($row) || (isset($row) && $row->link_type!='link')) hidden @endif @if(isset($row) && $row->link_type=='link') url_http url @endif"
                  value="{{ (isset($row) && $row->link_type == 'link') ? $row->link_value : 'http://www.' }}">
                  </div>

              </div>

            </div>
            <div class="popupBlock__row @if((isset($row) && $row->link_type=='') || !isset($row)) hidden @endif" data-popup-newwindow>
              <div class="popupBlock__col1">{{trans('admin.popup.open_new_window')}}</div>
              <div class="popupBlock__col2">

                <div class="inputBlock__checkbox">
                  <input type="radio" name="link_new_window" value="1"
                    id="new_window_yes"
                    @if(isset($row) && $row->link_new_window == 1) checked @endif/>
                  <label class="inputBlock__checkboxText" for="new_window_yes"><i></i><span>{{trans('admin.yes')}}</span></label>
                </div>

                <div class="inputBlock__checkbox">
                  <input type="radio" name="link_new_window" value="0"
                    id="new_window_no"
                    @if(!isset($row) || (isset($row) && $row->link_new_window == 0)) checked @endif
                  />
                  <label class="inputBlock__checkboxText" for="new_window_no"><i></i><span>{{trans('admin.no')}}</span></label>
                </div>

              </div>
            </div>
          </div>
        </div>

        <div class="addCategory__right">

          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

            <div class="panel imgPreview">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title imgPreview__name">
                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseImage"
                    data-popup-collapse="image" aria-expanded="true" aria-controls="collapseOne">
                    <input type="radio" id="radio_image" name="content_type" value="image" data-popup-radio
                      class="hidden"
                      @if(($row && $row->content_type=='image') || !isset($row)) checked @endif />
                    {{trans('admin.popup.image')}}
                  </a>
                </h4>
              </div>
              <div id="collapseImage" class="panel-collapse collapse imgPreview__block @if((isset($row) && $row->content_type=='image') || !$row) in @endif" role="tabpanel" aria-labelledby="headingOne">
                <div class="">
                  @include('admin.partial.image.thumbnail',[
                    'pt_width' => $settings['popup_thumb_width'],
                    'pt_height' => $settings['popup_thumb_height'],
                    'pt_exists' => isset($row) && $row->content_type=='image' ? 1 : 0,
                    'pt_path' => isset($row) && $row->content_type=='image'  ? "/img/popup/image_thumbnail.".$row->language[0]->content : "",
                    'pt_value' => isset($row) && $row->content_type=='image'  ? $row->language[0]->content : "",
                    'pt_delete_button' => 0,
                    'pt_options' => isset($row) ? $row->thumbnail_options : "",
                    'pt_nobutton' => 1,
                    'pt_required' => ((isset($row) && $row->content_type=='image') || !isset($row)) ? 1 : 0
                  ])
                </div>
              </div>
                <br>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingTwo">
                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseYoutube"
                    data-popup-collapse="youtube" aria-expanded="true" aria-controls="collapseTwo">
                    <input type="radio" id="radio_collapse" name="content_type" value="youtube" data-popup-radio
                      class="hidden"
                      @if($row && $row->content_type=='youtube') checked @endif />
                    {{trans('admin.popup.youtube')}}
                  </a>
                </h4>
              </div>
              <div id="collapseYoutube" class="panel-collapse collapse @if($row && $row->content_type=='youtube') in @endif" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                  <div class="row" data-popup-youtubebox>
                    <div class="youtubePreview">
                      <img src="@if($row && $row->content_type=='youtube') {{$row->language[0]->content->thumbnail}} @else /images/default_video_thumb.jpg @endif"/>
                    </div>
                      <p class="youtubePreviewName">@if($row && $row->content_type=='youtube') {{$row->language[0]->content->title}} @endif</p>
                      <input name="youtube_content" type="text" data-popup-field="youtube"
                        maxlength="255"
                        class="youtube_link inputBlock__field @if($row && $row->content_type=='youtube') required @endif"
                        @if($row && $row->content_type=='youtube')
                        value="{{$row->language[0]->content->url}}"
                        @endif
                        placeholder="{{trans('admin.popup.link_to_video')}}"
                        data-formvalidation-label="{{trans('admin.popup.yt_link')}}"/>

                  </div>
                  <input type="hidden" name="youtube_title" value=""/>
                  <input type="hidden" name="youtube_thumbnail" value=""/>
                </div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseHtml"
                    data-popup-collapse="html" aria-expanded="true" aria-controls="collapseThree">
                    <input type="radio" id="radio_collapse" name="content_type" value="html" data-popup-radio
                      class="hidden"
                      @if($row && $row->content_type=='html') checked @endif />
                    {{trans('admin.popup.html')}}
                  </a>
                </h4>
              </div>
              <div id="collapseHtml" class="panel-collapse collapse @if($row && $row->content_type=='html') in @endif" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                  <div class="row">
                    @php
                      $params = [
                        'label' => '',
                        'field' => 'content',
                        'field_name' => 'html_content',
                      ];
                      if(isset($row) && $row->content_type!="html")   $params['variable'] = 'abc';
                    @endphp

                    @include('admin.partial.lang.textarea',$params)
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>

      @include('admin.partial.form.save',['cancel_url'=>route('popup')])
    </form>
  </div>

@endsection
