<form action="" method="post" id="{{$group[0]->group->symbol}}SettingsForm" class="tabsForm otherTab">
  @foreach($group as $key=>$set)
    <div class="tabsForm__row">
      <div class="tabsForm__col6 tabsForm--vcenter">
        <label class="tabsForm__label" for="{{$set->symbol}}">{{trans($set->name)}}</label>
        @if($set->hint!='')
          {!!\App\Helpers\Format::iconHint(trans($set->hint))!!}
        @endif
      </div>
      <div class="tabsForm__innerRow tabsForm__col6">
        @if($set->type=="text")
          <input type="text" class="inputBlock__field" name="{{$set->symbol}}" value="{{$set->value}}" id="{{$set->symbol}}" @if($set->placeholder!="") placeholder="{{trans($set->placeholder)}}" @endif>
        @elseif($set->type=="number")
          <input type="number" class="inputBlock__field" name="{{$set->symbol}}" value="{{$set->value}}" id="{{$set->symbol}}" min="0" @if($set->placeholder!="") placeholder="{{trans($set->placeholder)}}" @endif>
        @elseif($set->type=="email")
          <input type="email" class="inputBlock__field" name="{{$set->symbol}}" value="{{$set->value}}" id="{{$set->symbol}}" @if($set->placeholder!="") placeholder="{{trans($set->placeholder)}}" @endif>
        @elseif($set->type=="textarea")
          <textarea class="inputBlock__textarea" name="{{$set->symbol}}" id="{{$set->symbol}}" @if($set->placeholder!="") placeholder="{{trans($set->placeholder)}}" @endif>{{$set->value}}</textarea>
        @elseif($set->type=="radio")
          @foreach($set->options as $key=>$value)
            <p><input type="radio"
              @if($key==$set->value)
                checked
              @endif
              name="{{$set->symbol}}" value="{{$key}}">{{trans($value)}}</p>
          @endforeach
        @elseif($set->type=="checkbox")
          <div class="inputBlock__checkbox">
          <input type="hidden" name="{{$set->symbol}}" value="0"/>
          <input type="checkbox" name="{{$set->symbol}}" value="1"
          @if($set->value==1)
            checked
          @endif
          id="{{$set->symbol}}" />
            <label for="{{$set->symbol}}"><i></i><span></span></label>
          </div>
        @endif
      </div>
    </div>
  @endforeach

  @include('admin.partial.form.save',['cancel_url'=>route('settings')])
</form>
