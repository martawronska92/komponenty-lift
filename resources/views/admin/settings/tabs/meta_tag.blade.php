<form action="" method="post" id="metaSettingsForm" class="tabsForm">
  @foreach($group as $set)
    <div class="tabsForm__row">
        @if($set->type=="text")
          @include('admin.partial.lang.input',[
            'label' => trans($set->name),
            'field' => 'value',
            'field_name' => $set->symbol,
            'required' => true,
            'variable' => 'set',
            'mypage' => 'settings'
          ])
        @endif
    </div>
  @endforeach

  @include('admin.partial.form.save',['cancel_url'=>route('settings')])
</form>
