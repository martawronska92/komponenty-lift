<form action="" method="post" data-formvalidation novalidate="" id="contactSettingsForm" class="contactTab">
  <div class="contactTab__row contactTab__row--title">
    <div class="contactTab__col">
      <div class="contactTab__name">{{trans('admin.category.basic_data')}}</div>
    </div>
    <div class="contactTab__col">
      <div class="contactTab__row contactTab__row--Vcenter">
      <div class="contactTab__name">{{trans('admin.openinghours.header')}}</div>
        <button type="button" class="button button--small button--green button--withIcon button--iconAdd" data-openinghours-add>{{trans('admin.openinghours.add_button')}}</button>
      </div>
    </div>
  </div>
  <div class="contactTab__row">
    <div class="contactTab__col">
      @foreach($group as $set)
        <div class="contactTab__row contactTab__row--form">
          <div class="contactTab__col4">
            <label class="tabsForm__label" for="{{$set->symbol}}">{{trans($set->name)}}</label>
          </div>
          <div class="contactTab__col8 inputBlock">
            @if($set->type=="text")
              <input type="text" class="inputBlock__field" name="{{$set->symbol}}" value="{{$set->value}}" id="{{$set->symbol}}" @if($set->placeholder!="") placeholder="{{trans($set->placeholder)}}" @endif>
            @elseif($set->type=="number")
              <input type="number" class="inputBlock__field" name="{{$set->symbol}}" value="{{$set->value}}" id="{{$set->symbol}}" min="0" >
            @elseif($set->type=="email")
              <input type="email" class="inputBlock__field" name="{{$set->symbol}}" value="{{$set->value}}" id="{{$set->symbol}}" @if($set->placeholder!="") placeholder="{{trans($set->placeholder)}}" @endif>
            @elseif($set->type=="textarea")
              <textarea class="inputBlock__textarea" name="{{$set->symbol}}" id="{{$set->symbol}}" @if($set->placeholder!="") placeholder="{{trans($set->placeholder)}}" @endif>{{$set->value}}</textarea>
            @elseif($set->type=="radio")
              @foreach($set->options as $key=>$value)
                <div class="inputBlock__checkbox"><input type="radio"
                  @if($key==$set->value)
                    checked
                  @endif
                  name="{{$set->symbol}}" id="{{$set->symbol}}" value="{{$key}}"><label class="inputBlock__checkboxText" for="{{$set->symbol}}"><i></i><span>{{trans($value)}}</span></label></div>
              @endforeach
            @elseif($set->type=="checkbox")
              <div class="inputBlock__checkbox">
                <input type="hidden" name="{{$set->symbol}}" value="0"/>
                <input type="checkbox" name="{{$set->symbol}}" value="1"
                @if($set->value==1)
                  checked
                @endif
                id="{{$set->symbol}}" />
                <label class="inputBlock__checkboxText" for="{{$set->symbol}}"><i></i><span></span></label>
              </div>
            @endif
          </div>
        </div>
      @endforeach

    </div>
    <div class="contactTab__col">

          <div class="" data-openinghours-box data-list-sortable="openinghours" data-list-sortablenoajax>
            @if(isset($openinghours) && count($openinghours) > 0)
              @foreach($openinghours as $term)
                {!!View::make('admin.settings.partial.openinghours',['term' => $term,'langs'=>$langs])->render()!!}
              @endforeach
            @endif
          </div>

    </div>
  </div>
  @include('admin.partial.form.save',['cancel_url'=>route('settings')])
</form>

<div data-select-noBind data-openinghours-template class="hidden">
  <?php unset($term); ?>
  @include('admin.settings.partial.openinghours')
</div>
