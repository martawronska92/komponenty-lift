<form action="" method="post" id="imagesSettingsForm" class="imagesTab">

  <?php $subgroup_id = 0; $col_index = 0; ?>

  @foreach($group as $set)
    @if($subgroup_id!=$set->settings_subgroup_id)
        @if($subgroup_id != 0)
              </div> {{-- panel body --}}
            </div> {{-- panel --}}
          </div> {{-- col-md-6 --}}

          @if($col_index % 2 == 0 && $col_index!=0)
            </div> {{-- row --}}
          @endif
        @endif

      @if($col_index % 2 == 0 )
        <div class="row">
      @endif

        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading tabsForm__label">{{trans($set->subgroup->name)}}</div>
            <div class="panel-body form-group">

      <?php
        $col_index++;
        $subgroup_id = $set->settings_subgroup_id;
      ?>
    @endif

    <div class="row">
      <div class="col-lg-9">
        <label class="tabsForm__label tabsForm__label--small" for="{{$set->symbol}}">{{trans($set->name)}}</label>
      </div>
      <div class="col-lg-3">
        <input type="number" class="inputBlock__field required" name="{{$set->symbol}}"
            @if($set->placeholder!="") {{trans($set->placeholder)}} @endif
            data-formvalidation-label = "{{trans($set->subgroup->name)}} {{lcfirst(trans($set->name))}}"
            value="{{$set->value}}" id="{{$set->symbol}}" min="0">
      </div>
    </div>

  @endforeach

</div> {{-- panel body --}}
</div> {{-- panel --}}
</div> {{-- col-md-6 --}}
</div> {{-- row --}}


  @include('admin.partial.form.save',['cancel_url'=>route('settings')])
</form>
