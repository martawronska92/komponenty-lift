@extends('layouts.admin')

@section("footer")
  <script>
    $(document).ready(function(){
      $(".nav-tabs a").bind("click",function(){
        $(".tab-content").find("form").removeAttr("data-formvalidation");
        $(".tab-content").find(".tab-pane.active form").attr("data-formvalidation",1);
        form.validate();
      });
    });
  </script>
@endsection

@section('content')

<div class="settings">
<div class="settings__inner">
  <!-- Nav tabs -->
  <ul class="tabs" role="tablist" data-tabs>
    @foreach($list as $group)
      @if(\Auth::user()->isDeveloper() || (!in_array($group->symbol,['images','videos']) && !\Auth::user()->isDeveloper()))
        <li role="presentation"
            class="@if($loop->first) active @endif">
          <a href="#{{$group->symbol}}" aria-controls="{{$group->symbol}}" role="tab" data-toggle="tab">{{trans($group->name)}}</a></li>
      @endif
    @endforeach
  </ul>

    <div class="settings__gray">
    <div class="tab-content">
      @foreach($list as $group)
        @if(\Auth::user()->isDeveloper() || (!in_array($group->symbol,['images','videos']) && !\Auth::user()->isDeveloper()))
          <div role="tabpanel" class="tab-pane @if($loop->first) active @endif" id="{{$group->symbol}}">
            @include('admin.settings.tabs.'.$group->symbol,['group'=>$group->settings])
          </div>
        @endif
      @endforeach
    </div>
    </div>
</div>
</div>

@endsection
