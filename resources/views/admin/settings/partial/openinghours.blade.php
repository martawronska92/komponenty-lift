<?php $hash = str_random(12); ?>

<div class="contactTab__row contactTab__row--notMargin" data-openinghours-item>

  <div class="contactTab__inp">
    @include('admin.partial.lang.input',[
      'label' => trans('admin.list.name'),
      'field_name' => 'openinghours_name['.$hash.']',
      'field' => 'value',
      'required' => true,
      'label' => '',
      'variable' => 'term',
      'placeholder' => trans('admin.openinghours.day_placeholder')
    ])
  </div>

  <div class="contactTab__time">
  <div class="inputBlock__checkbox contactTab__checkOpen">
  <input type="checkbox" name="openinghours_open[{{$hash}}]" id="openinghours_open[{{$hash}}]" value="1" data-openinghours-open
  @if((isset($term) && $term->open==1) || !isset($term)) checked  @endif/>
  <label class="inputBlock__checkboxText" for="openinghours_open[{{$hash}}]" data-openinghours-label><i></i><span>{{trans('admin.openinghours.'.(isset($term) && $term->open ==0 ? 'close' : 'open'))}}</span></label>
  </div>

    <div class="contactTab__row contactTab__row--Vcenter contactTab__row--line">
      <div class="contactTab__colTime">
        <input type="text" name="openinghours_from[{{$hash}}]" id="openinghours_from[{{$hash}}]"
          class="required time inputBlock__field" data-openinghours-from
          @if(isset($term) && $term->open ==0) disabled @endif
          @if(isset($term)) value="{{$term->from}}" @endif
          placeholder="00:00" maxlength="5"/>
      </div>

      <div class="contactTab__colTime">
        <input type="text" name="openinghours_to[{{$hash}}]" id="openinghours_to[{{$hash}}]"
          class="required time inputBlock__field" data-openinghours-to
          @if(isset($term) && $term->open ==0) disabled @endif
          @if(isset($term)) value="{{$term->to}}" @endif
          placeholder="00:00" maxlength="5"/>
      </div>
    </div>
  </div>

  <div class="contactTab__del">
    <i class="button button--circle button--gray button--iconDel" data-openinghours-remove></i>
  </div>

</div>
