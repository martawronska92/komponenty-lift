@extends('layouts.admin')

@section('content')

  <div class="addForm">
    <form action="" method="post" data-formvalidation enctype="multipart/form-data">

      <div class="importForm__row">
        <label class="importForm__logo">
          <input type="file" name="file" class="required hidden" accept=".csv">
        </label>
        <div class="importForm__text">{{trans('admin.offer.csv_file')}}</div>
      </div>
    
    @include('admin.partial.form.save',['cancel_url'=>route('offer')])

    </form>
  </div>

@endsection
