@extends('layouts.admin')

@section('footer')
  <script>
    $(document).ready(function(){
      page.bindEvents();
      category.bindEvents();
    });
  </script>
@endsection

@section('content')
<div class="addCategory">
<form action="" method="post" novalidate data-formvalidation data-form-beforesubmit="post.uploadThumb.crop">
  <div class="addCategory__row">
    <ul class="tabs" role="tablist" data-tabs>
      @foreach($tabs as $tab_symbol=>$tab_label)
        <li role="presentation" id="tab_li_{{$tab_symbol}}"
          @if($tab_symbol == 'module' && (!isset($row) || (isset($row) && $row->show_modules==0))) style="display:none;" @endif
          class="@if( ( (!isset($row) && $loop->first) || (isset($row) && $row->show_modules==0 && $loop->first) ) || (isset($row) && $row->show_modules==1 && $tab_symbol == 'module')) active @endif">
          <a href="#tab_{{$tab_symbol}}" aria-controls="tab_{{$tab_symbol}}" role="tab" data-toggle="tab"
          @if(!isset($row) && !$loop->first) {{-- data-disabled --}} @endif>{{$tab_label}}</a></li>
      @endforeach
    </ul>
  </div>
  <div class="addCategory__row">
    <div class="addCategory__left">
      <div class="tab-content">
        @foreach($tabs as $tab_symbol=>$tab_label)
          <div role="tabpanel"
              class="tab-pane @if( ( (!isset($row) && $loop->first) || (isset($row) && $row->show_modules==0 && $loop->first) ) || (isset($row) && $row->show_modules==1 && $tab_symbol == 'module')) active @endif" id="tab_{{$tab_symbol}}">
            <div class="tabsForm">
              @include('admin.module.partial.tab_category_'.$tab_symbol)
            </div>
          </div>
        @endforeach
      </div>
    </div>
    <div class="addCategory__right">
      <div class="imgPreview">
        <div class="imgPreview__name">{{trans('admin.post.thumbnail')}}</div>
        <div class="imgPreview__block">
          @include('admin.partial.image.thumbnail',[
                'pt_width' => $settings['category_thumb_width'],
                'pt_height' => $settings['category_thumb_height'],
                'pt_exists' => isset($row) && $row->thumbnail!="" ? 1 : 0,
                'pt_path' => isset($row) ? "/img/category/".$row->id."/thumbnail.".$row->thumbnail : "",
                'pt_value' => isset($row) ? $row->thumbnail : "",
                'pt_delete_button' => 1,
                'pt_options' => isset($row) ? $row->thumbnail_options : "",
                'pt_nobutton' => 1
              ])
        </div>
      </div>
    </div>
  </div>

  <input type="hidden" name="module_id" value="{{$module_id}}"/>
  <input type="hidden" id="id" name="id" value="@if(isset($row)){{$row->id}}@endif"/>
  <input type="hidden" id="entity_type" value="category"/>

  @if(!\App\Models\User::isDeveloper())
    <input type="hidden" name="category_view" value="{{isset($row)?$row->category_view:'modules.offer.category.default'}}"/>
    <input type="hidden" name="post_view" value="{{isset($row)?$row->post_view:'post.view.default'}}"/>
  @endif
  @include('admin.partial.form.save',['cancel_url'=> isset($back_url) ? $back_url : route($module),'save_and_stay'=>1])
</form>
</div>

@include('admin.partial.dialog.confirm')

@endsection
