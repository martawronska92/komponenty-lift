<div class="row">
  <div class="col-lg-3">
    @foreach($field_data->language as $lang)
      @if($lang->symbol == \App::getLocale())
        {{$lang->pivot->name}}
      @endif
    @endforeach
  </div>
  <div class="col-lg-7">
    @foreach($langs as $lang)
        @if(count($langs)>1)
          <p>{{$lang->name}}</p>
        @endif
        <input type="text" data-language="{{$lang->id}}"
               maxlength="255"
               data-field="{{$field}}"
               name="{{$field}}[@if(isset($index)){{$index}}@endif][{{$lang->id}}]"
               class="required"
               @foreach($field_data->language as $fieldlang)
                 @if($fieldlang->symbol == \App::getLocale())
                   data-formvalidation-label = "{{$fieldlang->pivot->name}} @if(count($langs) >0)({{$lang->name}})@endif"
                 @endif
               @endforeach
      />

    @endforeach
  </div>
  <div class="col-lg-2">
    <button class="btn btn-danger" type="button" data-category-fielddelete>{{trans('admin.delete')}}</button>
  </div>
</div>
