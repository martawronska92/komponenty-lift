<div class="tabsForm__row">
  <div class="tabsForm__col3 tabsForm--vcenter"><label class="tabsForm__label">{{trans('admin.category.category_view')}}</label></div>
  <div class="tabsForm__col9 tabsForm__innerRow form-group">
    <select name='category_view' class="required form-control langSelect tabsForm__input js-select2">
      @foreach($category_view as $viewSymbol=>$viewName)
        <option
          @if((isset($row) && $row->category_view==$viewSymbol) || (old('category_view')==$viewSymbol) ) selected @endif
          value="{{$viewSymbol}}">{{$viewName}}</option>
      @endforeach
    </select>
  </div>
</div>

<div class="tabsForm__row">
  <div class="tabsForm__col3 tabsForm--vcenter"><label class="tabsForm__label">{{trans('admin.'.$module.'.post_view')}}</label></div>
  <div class="tabsForm__col9 tabsForm__innerRow form-group">
    <select name='post_view' class="form-control tabsForm__input js-select2">
      @foreach($post_view as $viewSymbol => $viewName)
        <option
          @if((isset($row) && $row->post_view==$viewSymbol) || (old('post_view')==$viewSymbol) ) selected @endif
          value="{{$viewSymbol}}">{{$viewName}}</option>
      @endforeach
    </select>
  </div>
</div>
