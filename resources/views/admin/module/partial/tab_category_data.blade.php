<div class="tabsForm__row">
    @include('admin.partial.lang.input',[
      'label' => trans('admin.list.name'),
      'field' => 'name',
      'required' => true,
      'linkgenerator' => 1
    ])
</div>

<div class="tabsForm__row">
  @include('admin.partial.lang.friendly_url',[
    'type' => 'category',
    'prefix' => trans('admin.page.url_prefix'),
    'el' => 'name',
    'required' => true
  ])
</div>

<div class="tabsForm__row">
      @include('admin.partial.lang.textarea',[
        'label' => trans('admin.description'),
        'field' => 'description'
      ])
</div>

<div class="tabsForm__row">
   @if(!isset($row))
     @include('admin.partial.lang.input',[
       'label' => trans('admin.settings.meta_title'),
       'field_name' => 'meta_title',
       'field_id' => 'meta_title',
       'field' => 'value',
       'required' => true,
       'variable' => 'settings',
       'variable_key' => 'meta_title'
     ])
   @else
    @include('admin.partial.lang.input',[
      'label' => trans('admin.settings.meta_title'),
      'field' => 'meta_title',
      'field_id' => 'meta_title',
      'required' => true
    ])
  @endif
</div>

<div class="tabsForm__row">
  @if(!isset($row))
    @include('admin.partial.lang.textarea',[
      'label' => trans('admin.settings.meta_description'),
      'field_name' => 'meta_description',
      'field_id' => 'meta_description',
      'field' => 'value',
      'required' => true,
      'variable' => 'settings',
      'variable_key' => 'meta_description',
      'maxlength' => 255
    ])
  @else
    @include('admin.partial.lang.textarea',[
      'label' => trans('admin.settings.meta_description'),
      'field' => 'meta_description',
      'required' => true,
      'maxlength' => 255
    ])
  @endif
</div>

<div class="tabsForm__row">
   <div class="tabsForm__col3">
     <label class="tabsForm__label" for="show_modules">Kategoria tekstowa</label>
  </div>
  <div class="tabsForm__innerRow tabsForm__col9 form-group tabsForm--vcenter">
      <div class="inputBlock__checkbox">
        <input type="hidden" name="show_modules" value="0">
        <input type="checkbox" name="show_modules" value="1" @if(isset($row) && $row->show_modules==1) checked @endif id="show_modules">
        <label for="show_modules"><i></i><span></span></label>
      </div>
    </div>
</div>

<div class="tabsForm__row">
  <div class="tabsForm__col3 tabsForm--vcenter">
    <label class="tabsForm__label" for="category_parent_id">{{trans('admin.offer.parent_category')}}</label>
  </div>
  <div class="tabsForm__col9 tabsForm__innerRow form-group">
    <select name="category_parent_id" id="category_parent_id" class="form-control tabsForm__input js-select2">
      <option value="">{{trans('admin.none')}}</option>
      @foreach($categories as $category)
        @if($category->level < \Config::get('cms.page_level') - 1)
          <option value="{{$category->id}}" @if((isset($categoryParentId) && $categoryParentId==$category->id) || (isset($row) && $row->category_parent_id == $category->id)) selected @endif>{{$category->language[0]->name}}</option>
        @endif
      @endforeach
    </select>
  </div>
</div>
