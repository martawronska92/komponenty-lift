@extends('layouts.admin')

@section('footer')
  <script>
    $(document).ready(function(){
      page.init();
    });
  </script>
@endsection

@section('content')
<div class="addCategory">
<form action="" method="post" data-formvalidation data-form-beforesubmit="post.uploadThumb.crop">
<div class="addCategory__row">
  <ul class="tabs" role="tablist" data-tabs>
      <li role="presentation" class="{{!isset($row) ? 'active' : ''}}">
        <a href="#post_data" aria-controls="post_data" role="tab" data-toggle="tab">{{trans('admin.post.tab_data')}}</a></li>
      @if(isset($fields) && count($fields) > 0)
        <li role="presentation">
          <a href="#post_field" aria-controls="post_field" role="tab" data-toggle="tab">{{trans('admin.post.tab_field')}}</a></li>
      @endif
      <li role="presentation" class="{{isset($row) ? 'active' : ''}}">
        <a href="#post_module" aria-controls="post_module" role="tab" data-toggle="tab">{{trans('admin.post.tab_module')}}</a></li>
  </ul>
</div>
  <div class="addCategory__row">
    <div class="addCategory__left">
      <div class="tab-content">
          <div role="tabpanel" class="tab-pane {{!isset($row) ? 'active' : ''}}" id="post_data">
            @include('admin.post.partial.data_tab')
          </div>
          @if(isset($fields) && count($fields) > 0)
            <div role="tabpanel" class="tab-pane" id="post_field">
              @include('admin.post.partial.field_tab')
            </div>
          @endif
          <div role="tabpanel" class="tab-pane {{isset($row) ? 'active' : ''}}" id="post_module">
            @include('admin.post.partial.module_tab')
          </div>
      </div>
    </div>
    <div class="addCategory__right">
      @include('admin.post.partial.options')
    </div>
  </div>


  {{--<div class="col-lg-3">--}}
    {{--@include('admin.post.partial.options')--}}
  {{--</div>--}}


@include('admin.partial.form.save',['cancel_url'=> $redirect_url,'save_and_stay'=>1])

<input type='hidden' name='from_module' value="{{$from_module}}" />


{{-- id wpisu --}}
@if(isset($id))
  <input type="hidden" name="id" value="{{$id}}"/>
@endif

</form>
</div>

<input type="hidden" id="id" value="@if(isset($id)){{$id}}@endif"/>
<input type="hidden" id="entity_type" value="post"/>

{{-- parial dla wiersza modułu na liście dodawanych modułów --}}
<div data-page-modulepartial class="hidden">
  {!!\View::make('admin.partial.module.choose_row')!!}
</div>

@include('admin.partial.dialog.confirm')

@endsection
