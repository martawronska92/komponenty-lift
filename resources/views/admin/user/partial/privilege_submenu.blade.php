@if(count($elements->data)>0)
<ul class="userPagesMenu">
  @foreach($elements->hierarchy[$level][$parentId] as $elementId)
      <li>
    <div class="userBlock__item">
      <div class="inputBlock__checkbox">
        <input type="checkbox" id="{{$entity['type']}}_details[{{$entity['id']}}][{{$elements->data[$elementId]->id}}]"
            name="{{$entity['type']}}_details[{{$entity['id']}}][{{$elements->data[$elementId]->id}}]"
            data-privilege-submenuparent="{{$parentId}}"
            @if(isset($row) && $row!=null)
              @foreach($row->privilege as $userPrivilege)
                @if($userPrivilege->symbol == $entity['symbol'] && $elements->data[$elementId]->id==$userPrivilege->pivot->entity_id)
                  checked
                @endif
              @endforeach
            @endif
            />
        <label class="inputBlock__checkboxText"
          style="padding-left:{{$level*2.14}}vw;"
          for="{{$entity['type']}}_details[{{$entity['id']}}][{{$elements->data[$elementId]->id}}]"><i></i><span>{{$elements->data[$elementId]->title}}</span></label>
      </div>
    </div>

    @if(array_key_exists($level+1,$elements->hierarchy) && array_key_exists($elementId,$elements->hierarchy[$level+1]))
      @include('admin.user.partial.privilege_submenu',[
        'level' => $level+1,
        'row' => $row,
        'parentId' => $elementId,
        'elements' => $elements
      ])
    @endif


  @endforeach
      </li>
</ul>
@endif
