<div class="previ" data-user-privilegebox>
  @foreach($modules as $entityName=>$entity)
    <div class="userBlock__item">

      <div class="inputBlock__checkbox" data-user-privilegelist="{{$entity['symbol']}}">
        <input type="checkbox" name="{{$entity['type']}}[]" value="{{$entity['id']}}" id="{{$entity['type']}}_{{$entity['id']}}"
          @if(isset($row))

          @if($entity['type'] == "privilege")
          @foreach($row->privilege as $userPrivilege)
          @if($entity['symbol'] == $userPrivilege->symbol)
          checked
          @endif
          @endforeach
          @endif

          @if($entity['type'] == "module")
          @foreach($row->module as $userModule)
          @if($entity['symbol'] == $userModule->symbol)
          checked
          @endif
          @endforeach
          @endif

          @endif
          id="{{$entity['type']}}[{{$entity['id']}}]">
        <label class="inputBlock__checkboxText" for="{{$entity['type']}}_{{$entity['id']}}"><i></i>
          <span>{{$entityName}}</span>
        </label>
      </div>

          @if($entity['entity_privilege'] && array_key_exists($entity['symbol'],$entityDetails) && count($entityDetails[$entity['symbol']])>0)
            <i class="openPodMenu" data-toggle="collapse" data-user-privilegesublistcheckbox
              aria-expanded="false"
              data-collapse-element="#collapse{{$entity['symbol']}}"></i>
          @endif
    </div>
  @endforeach
</div>




  <div class="hiddenPodPrev hidden" id="collapsebox" data-user-privilegecheckboxes>
    @foreach($modules as $entityName=>$entity)
      @if($entity['entity_privilege'] && array_key_exists($entity['symbol'],$entityDetails) && count($entityDetails[$entity['symbol']])>0)
        <div class="collapse" id="collapse{{$entity['symbol']}}" data-user-privilegesublist="{{$entity['symbol']}}">

                @include('admin.user.partial.privilege_submenu',[
                  'level'=>0,
                  'elements' => $entityDetails[$entity['symbol']],
                  'parentId' => 0,
                  'row' => isset($row) ? $row : null
                ])

        </div>
      @endif
    @endforeach
  </div>
