@extends('layouts.admin')

@section('footer')
<script>
  $(document).ready(function(){
    user.bindEvents();
  });
</script>
@endsection

@section('content')

  <div class="userBlock">
<form action="" method="post" novalidate="" data-formvalidation>
<div class="userBlock__inner" data-user-privilegecontainer data-user-privilegecheckboxes>
  <div class="userBlock__line">
    <div class="userBlock__left">
      <div class="userBlock__title">{{trans('admin.data')}}</div>
    </div>
    <div class="userBlock__right">
      <div class="userBlock__check">
        <div class="userBlock__title">{{trans('admin.user.privileges')}}</div>
        @if(!isset($row) || (isset($row) && $row->id != \Auth::user()->id))
          <div>
            <button type="button" class="button button--small button--green button--withIcon button--iconGalka" data-user-checkall>{{trans('admin.user.mark_all')}}</button>
            <button type="button" class="button button--small button--gray button--withIcon button--iconSquare" data-user-uncheckall>{{trans('admin.user.unmark_all')}}</button>
          </div>
        @endif
      </div>
    </div>
  </div>

<div class="userBlock__row">
  <div class="userBlock__left">
    <div class="userBlock__item">
      <div class="userBlock__namecol userBlock__title">{{trans('admin.user.email')}}</div>
      <div class="userBlock__inputcol">
        <div class="userBlock__input inputBlock">
          <input type="email" name="email" class="required inputBlock__field" id="email"
            maxlength="255"
            value="@if(isset($row)){{$row->email}}@else{{old('email')}}@endif"
            data-formvalidation-label="{{trans('admin.user.email')}}">
        </div>
      </div>
    </div>

    <div class="userBlock__item">
      <div class="userBlock__namecol userBlock__title">{{trans('admin.user.first_name')}}</div>
      <div class="userBlock__inputcol">
        <div class="userBlock__input inputBlock">
          <input type="text" name="first_name" id="first_name"
            class="inputBlock__field required"
            data-formvalidation-label="{{trans('admin.user.first_name')}}"
            value="@if(isset($row)){{$row->first_name}}@else{{old('first_name')}}@endif">
        </div>
      </div>
    </div>


    <div class="userBlock__item">
      <div class="userBlock__namecol userBlock__title">{{trans('admin.user.last_name')}}</div>
      <div class="userBlock__inputcol">
        <div class="userBlock__input inputBlock">
          <input type="text" name="last_name" id="last_name"
            class="inputBlock__field required"
            maxlength="255"
            data-formvalidation-label="{{trans('admin.user.last_name')}}"
            value="@if(isset($row)){{$row->last_name}}@else{{old('last_name')}}@endif">
        </div>
      </div>
    </div>

    <div class="userBlock__item">
      <div class="userBlock__namecol userBlock__title">{{trans('auth.password')}}</div>
      <div class="userBlock__inputcol">
        <button class="buttton button--small button--gray"
          data-password="{{$password}}"
          maxlength="255"
          data-formvalidation-label="{{trans('auth.password')}}"
          type="button" data-user-password>{{trans('admin.user.'.(isset($row) ? 'generate_' : 'show_').'password')}}</button>
      </div>
    </div>

    <div class="userBlock__item">
      <div class="userBlock__namecol userBlock__title">{{trans('admin.list.active')}}</div>
      <div class="userBlock__inputcol">
        <div class="inputBlock__checkbox">
          <input type="checkbox" id="active" name="active" value="1" @if(isset($row) && $row->active) checked="checked" @endif>
          <label class="inputBlock__checkboxText" for="active"><i></i>
            <span>Tak</span>
          </label>
        </div>

      </div>
    </div>

  </div>
  <div class="userBlock__right userBlock__right--flex">
    @if(isset($row) && $row->id == \Auth::user()->id)
      <div class="resultsChanges resultsChanges--info" role="alert">{{trans('admin.user.himself_permission')}}</div>
    @else
      @include('admin.user.privileges')
    @endif
  </div>
</div>

</div>
  @include('admin.partial.form.save',['cancel_url'=>route('user'),'save_and_stay'=>1])
</form>
  </div>
<input type="hidden" id="id" value="@if(isset($row)){{$row->id}}@endif"/>

@endsection
