@extends('layouts.admin')


@section('content')

  <div class="row">
    <div class="col-lg-1"></div>
    <div class="col-lg-1"></div>
    <div class="col-lg-2"></div>
    <div class="col-lg-1"></div>
    <div class="col-lg-3"></div>
  </div>


  @if(count($results) == 0)
    <div class="alert alert-info">{{trans('admin.version.no_updates')}}</div>
  @else
    @foreach($results as $key=>$result)
      <div class="row">
        <div class="col-lg-2"><div class="alert">{{$key}}</div></div>
        <div class="col-lg-10">
          @foreach($result as $row)
            <?php switch($row->code){
                case 0: $alertClass="danger"; break;
                case 1: $alertClass="success"; break;
                case 2: $alertClass="info"; break;
               };
            ?>
            <div class="row">
              <div class="col-lg-5">
                <div class="alert alert-{{$alertClass}}">
                  @if(property_exists($row,'version'))
                    {{$row->version}} -
                  @endif
                  {!!$row->msg!!}
                </div>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    @endforeach
  @endif

@endsection
