@extends('layouts.admin')


@section('content')

<form method="post" action="" novalidate="" data-formvalidation>

  <div class="row">
    <div class="col-lg-3">
      <label for="active">{{trans('admin.list.active')}}</label>
    </div>
    <div class="col-lg-9">
      <input type="checkbox" name="active" value="1"
        @if(isset($row) && $row->active)
          checked
        @endif
        />
    </div>
  </div>

  <div class="row">
    <div class="col-lg-3">
      <label for="main">{{trans('admin.list.set_main')}}</label>
    </div>
    <div class="col-lg-9">
      <input type="checkbox" name="main" value="1"
        @if(isset($row) && $row->main)
          checked
        @endif
        />
    </div>
  </div>

  <div class="row form-group">
    <div class="col-lg-3">
      <label for="main">{{trans('admin.language.domain')}}</label>
    </div>
    <div class="col-lg-5">
      <input type="text" name="domain" value="{{$row->domain}}" class="form-control"/>
    </div>
  </div>

  @include('admin.partial.form.save',['cancel_url'=>route('language')])

</form>

@endsection
