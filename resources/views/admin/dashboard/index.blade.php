@extends('layouts.admin')

@section('footer')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <style>
        .mainTitle {
            display: none;
        }
    </style>
    <script>
        google.charts.load('current', {packages: ['corechart', 'line']});
        google.charts.setOnLoadCallback(drawLineColors);

        function drawLineColors() {
            var data = new google.visualization.DataTable();
            data.addColumn('date', 'X');
            data.addColumn('number', Laravel.lang.analytics.visits);

            data.addColumn({
                type: 'string',
                label: 'Tooltip Chart',
                role: 'tooltip',
                'p': {'html': true}
            });

            data.addColumn('number', Laravel.lang.analytics.pageviews);

            data.addColumn({
                type: 'string',
                label: 'Tooltip Chart',
                role: 'tooltip',
                'p': {'html': true}
            });

            var rows = $.parseJSON('{!!$analytics!!}');
            var ticks = [];

            if (rows.length > 0) {
                var tooltip, tooltipBase;

                for (var i = 0; i < rows.length; i++) {
                    rows[i][0] = new Date(rows[i][0]);

                    tooltipBase = "<div style='padding:10px;font-size:14px;font-family:Arial;width:150px;color:rgb(34,34,34);'><p><b>";
                    tooltipBase += (rows[i][0].getDate() < 10 ? "0" : "") + rows[i][0].getDate();
                    tooltipBase += "-";
                    tooltipBase += (rows[i][0].getMonth() + 1 < 10 ? "0" : "") + (rows[i][0].getMonth() + 1);
                    tooltipBase += "-" + rows[i][0].getFullYear() + "</b></p>";

                    tooltip = tooltipBase;
                    tooltip += "<p>" + Laravel.lang.analytics.visits + ": <b>" + rows[i][1] + "</b></p></div>";
                    rows[i][2] = tooltip;

                    tooltip = tooltipBase;
                    tooltip += "<p>" + Laravel.lang.analytics.pageviews + ": <b>" + rows[i][3] + "</b></p></div>";
                    rows[i][4] = tooltip;

                    ticks.push({v: rows[i][0]});
                }
                data.addRows(rows);
            }

            var options = {
                colors: ['#052b5f', '#06c2f1'],
                hAxis: {
                    slantedText: 1,
                    slantedTextAngle: 50,
                    ticks: ticks,
                    format: 'dd-MM-yyyy'
                },
                //curveType: 'function',
                pointSize: 5,
                chartArea: {
                    height: '60%',
                    left: 60,
                    width: '80%',
                    top: 20
                },
                //width: 500,
                height: 280,
                tooltip: {isHtml: true},
                legend: {'position': 'bottom'}
            };

            var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
            chart.draw(data, options);
        }

        google.charts.load('current', {
          'packages': ['geochart'],
          'mapsApiKey': 'AIzaSyBLE_ID8jRLponHYp6FKfrI1MHP7PYKRDs'
        });

        google.charts.setOnLoadCallback(drawGeoMap);

        function drawGeoMap() {
            var dataToChart = [];
            var points = $.parseJSON('{!!json_encode($geo)!!}');
            dataToChart.push(['Miasto',   'Ilość wizyt']);
            if(points!=null){
              for(var i=0;i<points.length;i++){
                if(points[i][0]=="PL" && points[i][2]!="(not set)"){
                    views = parseInt()
                    dataToChart.push([points[i][2],parseInt(points[i][3])]);
                }
              }
            }

            var data = google.visualization.arrayToDataTable(dataToChart);

            var options = {
              region: 'PL',
              displayMode: 'markers',
              colorAxis: {colors: ['#06C2F1','#304E79']}
            };

            var chart = new google.visualization.GeoChart(document.getElementById('mapchart_div'));
            chart.draw(data, options);
        }

        $(window).resize(function () {
            drawLineColors();
            drawGeoMap();
        });

        localStorage.removeItem('logged_in');
    </script>
@endsection

@section('content')


    {{--<div class="row">--}}
    {{--<div class="col-lg-8">--}}
    {{--<h3>Witaj Admin</h3>--}}
    {{--<p>Donec sollicitudin molestie malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
    {{--</div>--}}
    {{--<div class="col-lg-4">--}}
    {{--<br/>--}}
    {{--<div class="panel panel-default">--}}
    {{--<div class="panel-body text-center">--}}
    {{--<p>{{trans('admin.dashboard.last_logged')}}: {{\App\Helpers\Format::formatDate(\Auth::user()->last_login_history)}}</p>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<br/>--}}

    <div class="dashboard">
        <div class="flexRow">
            <div class="dashboardBlocks popularPages col1">
                <div class="dashboardTitle">{{trans('admin.dashboard.most_visited_pages')}}</div>
                <div class="dashboardBlocks__inner">
                    <div class="popularPages__inner">
                        <div class="owl-carousel verticalcarousel">
                            <div>
                                @foreach($topPages as $key=>$page)
                                  @if($key!=0 && $key%6 == 0)</div><div>@endif
                                  <a href="{{$page['url']}}" target="_blank" class="popularPages__item">{{$key+1}}. {{$page['url']}}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dashboardBlocks popularDevices col1">
                <div class="dashboardTitle">{{trans('admin.dashboard.devices')}}</div>
                <div class="dashboardBlocks__inner">
                    @php $percentageSum = 0; @endphp
                    @foreach($devices->list as $device=>$visits)
                      @php $percentage = round(($visits/$devices->sum)*100); @endphp
                      <div class="popularDevices__device">
                          <div class="popularDevices__icon popularDevices__icon--{{$device}}"></div>
                          <div class="popularDevices__name">{{trans('admin.dashboard.device_'.$device)}}</div>
                          <div class="popularDevices__percent">{{$loop->last ? 100 - $percentageSum : $percentage}}%</div>
                      </div>
                      @php $percentageSum+=$percentage;@endphp
                    @endforeach
                </div>
            </div>
            <div class="dashboardBlocks usersOnline col1">
                <div class="dashboardTitle">{{trans('admin.dashboard.users_online')}}</div>
                <div class="dashboardBlocks__inner">
                    <div class="usersOnline__inner">
                        <div class="usersOnline__circle">{{$activeUsers}}</div>
                        <div class="usersOnline__desc">{{trans('admin.dashboard.order_positioning')}}</div>
                        <div class="usersOnline__button">
                            <a target="_blank" href="http://www.ibif.pl/zamawiam-pozycjonowanie" class="button button--small button--green button--withIcon button--iconAdd">{{trans('admin.dashboard.ordering_positioning')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="flexRow">
            <div class="dashboardBlocks col2">
                <div class="dashboardBlocks__line">
                    <div class="dashboardTitle">{{trans('admin.dashboard.traffic')}}</div>
                    <div class="dashboardBlocks__count">{{trans('admin.dashboard.count')}} <b>{{$sumUsers}}</b></div>
                </div>
                <div class="dashboardBlocks__inner">
                    <div class="text-center" id="chart_div">
                        <img src="/css/ajax-loader.gif"/>
                    </div>
                </div>
            </div>

            <div class="dashboardBlocks col1 citiesBlock">
                <div class="dashboardTitle">{{trans('admin.dashboard.cities')}}</div>
                <div class="dashboardBlocks__inner">
                    <div class="citiesBlock__inner">
                        <div id="mapchart_div" style="width:100%;height:100%;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--<div class="chartBox">--}}
        {{--<div class="text-center" id="chart_div">--}}
            {{--<img src="/css/ajax-loader.gif"/>--}}
        {{--</div>--}}
    {{--</div>--}}



@endsection
