@extends('layouts.admin')

@section('content')
  <h1>Buttons</h1><br>
  <a href="#" class="button button--big button--green">Button </a>
  <a href="#" class="button button--big button--blue">Button </a>
  <a href="#" class="button button--big button--gray">Button</a>
  <br><br>
  <a href="#" class="button button--small button--green">Button </a>
  <a href="#" class="button button--small button--blue">Button </a>
  <a href="#" class="button button--small button--gray">Button</a>
  <br><br>
  <a href="#" class="button button--xs button--green">Button </a>
  <a href="#" class="button button--xs button--blue">Button </a>
  <a href="#" class="button button--xs button--gray">Button</a>
  <br><br>
  <a href="#" class="button button--small button--green button--withIcon button--iconPencil">Button </a>
  <a href="#" class="button button--small button--blue button--withIcon button--iconList">Button </a>
  <a href="#" class="button button--small button--gray button--withIcon button--iconEye">Button</a>
  <a href="#" class="button button--small button--blue button--withIcon button--iconDel">Button</a>
  <a href="#" class="button button--small button--gray button--withIcon button--iconDownload">Button</a>
  <a href="#" class="button button--small button--blue button--withIcon button--iconUpload">Button</a>
  <a href="#" class="button button--small button--green button--withIcon button--iconGalka">Button</a>
  <a href="#" class="button button--small button--blue button--withIcon button--iconAdd">Button</a>
  <a href="#" class="button button--small button--gray button--withIcon button--iconSample">Button</a>
  <a href="#" class="button button--small button--green button--withIcon button--iconSquare">Button</a>
  <a href="#" class="button button--small button--gray button--withIcon button--iconTree">Button</a>
  <a href="#" class="button button--small button--blue button--withIcon button--iconClose">Button</a>
  <br><br>
  <a href="#" class="button button--circle button--green button--iconPencil">Button </a>
  <a href="#" class="button button--circle button--blue button--iconList">Button </a>
  <a href="#" class="button button--circle button--gray button--iconEye">Button</a>
  <a href="#" class="button button--circle button--blue button--iconDel">Button</a>
  <a href="#" class="button button--circle button--gray button--iconDownload">Button</a>
  <a href="#" class="button button--circle button--gray button--iconUpload">Button</a>
  <a href="#" class="button button--circle button--green button--iconGalka">Button</a>
  <a href="#" class="button button--circle button--blue button--iconAdd">Button</a>
  <a href="#" class="button button--circle button--gray button--iconSample">Button</a>
  <a href="#" class="button button--circle button--green button--iconSquare">Button</a>
  <a href="#" class="button button--circle button--gray button--iconTree">Button</a>
  <a href="#" class="button button--circle button--blue button--iconClose">Button</a>
  <br><br>

  <h1>Checkboxes toggle</h1><br>
  <label class="toggle"><input type="checkbox" class="toggle_input">
    <div class="toggle-control">
      <span data-before="tak" data-after="nie"></span>
    </div>
  </label>

  <label class="toggle"><input type="checkbox" checked class="toggle_input">
    <div class="toggle-control">
      <span data-before="tak" data-after="nie"></span>
    </div>
  </label>
  <br><br>
  <h1>Checkboxes</h1><br>
  <div class="inputBlock__checkbox">
    <input type="checkbox" name="la1" id="la1" checked="">
    <label class="inputBlock__checkboxText" for="la1"><i></i><span>Aktywny</span></label>
  </div>
  <br>
  <div class="inputBlock__checkbox">
    <input type="checkbox" name="la2" id="la2">
    <label class="inputBlock__checkboxText" for="la2"><i></i><span>Nie aktywny</span></label>
  </div>
  <br><br>

  <h1>Radio</h1><br>
  <div class="inputBlock__checkbox">
    <input type="radio" name="la3" id="la3" checked="">
    <label class="inputBlock__checkboxText" for="la3"><i></i><span>Aktywny</span></label>
  </div>
  <br>
  <div class="inputBlock__checkbox">
    <input type="radio" name="la3" id="la4">
    <label class="inputBlock__checkboxText" for="la4"><i></i><span>Nie aktywny</span></label>
  </div>
  <br><br>

  <h1>Selects</h1><br>

  <div style="width: 400px">
  <select name="ww" id="ww" class="js-select2">
    <option value="">One</option>
    <option value="">Two</option>
  </select>
  </div>
  <br>

  <div style="width:60px;">
    <select class="form-control" data-lang-component>
      <option value="pl" selected >polski</option>
      <option value="en">angielski</option>
    </select>
  </div>
  <br><br>

  <h1>Form 1</h1><br>
  <div style="width:600px; margin: 0 auto;">
    <form class="universalForm" >
      <div class="universalForm__row">
        <div class="inputBlock">
          <input id="email3" type="text" class="inputBlock__field" name="email1" placeholder="Login" value="" required="">
        </div>
      </div>
      <div class="universalForm__row">
        <div class="inputBlock">
          <input id="password3" type="password" class="inputBlock__field" placeholder="Hasło" name="password2" required="">
        </div>
      </div>
      <div class="universalForm__row">
        <div class="inputBlock">
          <input id="ww" type="text" class="inputBlock__field" placeholder="Placeholder" name="ww" required="">
        </div>
      </div>
      <div class="universalForm__row universalForm__row--cols2 universalForm__row--between">
        <div class="universalForm__col">
          <div class="inputBlock">
            <input id="ww" type="text" class="inputBlock__field" placeholder="Placeholder" name="ww" required="">
          </div>
        </div>
        <div class="universalForm__col">
          <div class="inputBlock">
            <input id="ww" type="text" class="inputBlock__field" placeholder="Placeholder" name="ww" required="">
          </div>
        </div>
      </div>
      <div class="universalForm__row universalForm__row--mt35">
        <div class="inputBlock -tac">
          <button type="submit" class="button button--big button--blue inputBlock__submit">Zaloguj</button>
        </div>
      </div>
    </form>
  </div>
  <br>

  <hr><br>

  <h1>Form 2</h1><br>
  <div class="addForm">
    <form class="form2col">
      <div class="form2col__wrapRow">
        <div class="form2col__row">
          <div class="form2col__left">Nazwa</div>
          <div class="form2col__right inputBlock"><input type="text" name="name" class="required inputBlock__field" maxlength="255" value="Nazwa" data-formvalidation-label="Nazwa" aria-required="true"></div>
        </div>
      </div>
      <div class="form2col__wrapRow">
        <div class="form2col__row">
          <div class="form2col__left">Symbol</div>
          <div class="form2col__right inputBlock"><input type="text" name="symbol" class="required inputBlock__field" maxlength="255" value="symb" data-formvalidation-label="Symbol" aria-required="true"></div>
        </div>
      </div>

      <div class="saveLine">
        <button type="submit" class="button button--small button--blue" name="save">Zapisz</button>
        <a href="//localhost:3000/panel/menu" class="button button--small button--gray">Anuluj</a>
      </div>

      <input type="hidden" name="_token" value="kT1zgheJJNRlyus8aCLOE8DZHiX6nUV1isXBGyA8">

    </form>
  </div>

  <br><br>

  <h3>Inputs</h3>
  <div style="width: 600px; margin: 0 auto;">
    <div class="inputBlock">
      <label for="visible_from">date picker</label>
      <input type="text" name="visible_from" id="visible_from"
        class=" required date_pl  inputBlock__field"
        value="17-07-2017"
        data-datetimepicker="dd-MM-yyyy"
        data-formvalidation-label="Widoczny od"/>
    </div>
    <br>

    <div class="inputBlock">
      <input id="ww" type="text" class="inputBlock__field" placeholder="Placeholder" name="ww" required="">
    </div>

  </div>
  <br><br>


  <h1>Tabs</h1><br>

  <ul class="tabs" role="tablist" data-tabs="">
    <li role="presentation" class=" active ">
      <a href="#contact" aria-controls="contact" role="tab" data-toggle="tab">Kontakt</a></li>
    <li role="presentation" class="">
      <a href="#meta_tag" aria-controls="meta_tag" role="tab" data-toggle="tab">Meta tagi</a></li>
    <li role="presentation" class="">
      <a href="#map" aria-controls="map" role="tab" data-toggle="tab">Google Maps</a></li>
  </ul>

  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="contact">
      Kontakt
    </div>
    <div role="tabpanel" class="tab-pane" id="meta_tag">
      Meta tagi
    </div>
    <div role="tabpanel" class="tab-pane " id="map">
      Google Maps
    </div>
  </div>

  <br><br><br><br>


  <h1>Tooltips</h1><br>
    
    <div class="popupTooltip js-popupTooltip" title="Etiam dictum lectus est, a egestas est ultrices quis purus magna scelerisque sit amet."></div>
  <br><br>
  <div style="display:inline-block;" class="js-popupTooltip" title="Etiam dictum lectus est, a egestas est ultrices quis purus magna scelerisque sit amet.">Bla bla bla bla bla</div>


  <br><br><br><br>
@endsection
