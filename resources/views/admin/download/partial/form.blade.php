<form class="form2col" action="" method="post" name="downloadForm" id="downloadForm">

      <div class="form2col__wrapRow">
        <div class="form2col__row">
          @include('admin.partial.lang.input',[
            'label' => trans('admin.list.title'),
            'field' => 'title',
            'variable' => 'file'
          ])
      </div>
      </div>

    <div class="form2col__wrapRow">
      <div class="form2col__row">
        <div class="tabsForm__col2 tabsForm--vcenter">
          <label class="tabsForm__label" for="active">{{trans('admin.list.active')}}</label>
        </div>
        <div class="tabsForm__col10">
          <input type="hidden" name="active" value="1"/>
          <div class="inputBlock__checkbox">
            <input type="checkbox" id="qqq" name="active" value="1" @if(isset($file) && $file->active) checked @endif />
            <label class="inputBlock__checkboxText" for="qqq"><i></i><span></span></label>
          </div>

          </div>
    </div>
    </div>


    <div class="form2col__wrapRow">
      <div class="form2col__row">
      <div class="tabsForm__col2 tabsForm--vcenter">
        <label class="tabsForm__label" for='file'>{{trans('admin.download.file')}}</label>
      </div>
      <div class="tabsForm__col10">
        <p class="downloadTable__name" data-download-filename>@if(isset($file)){{$file->language[0]->title!="" ? $file->language[0]->title : $file->filename_slug.".".$file->ext}}@endif</p>
        <input id="file" type="file" name="file" data-download-upload />
      </div>
    </div>
    </div>

    @if(isset($file))
      <br/>
      <div class="form2col__wrapRow" data-download-formbuttons>
        <div class="form2col__row">
        <div class="col-lg-12 text-center">
          <button type="button" data-editable-save class="button button--xs button--blue">{{trans('admin.save')}}</button>
          <button type="button" data-editable-cancel class="button button--xs button--gray">{{trans('admin.cancel')}}</button>
        </div>
      </div>
      <input type="hidden" name="file_id" id="file_id" value="{{$file->id}}"/>
      </div>
    @endif

</form>
