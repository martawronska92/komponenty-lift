<tr data-id="{{$file->id}}" class="{{$file->draft ? 'is_draft' : ''}}" title="{{$file->draft ? trans('admin.list.draft') : ''}}">
  <td>
    <div class="inputBlock__checkbox inputBlock__checkbox--noMargin">
      <input type="checkbox" id="{{$file->id}}" value="{{$file->id}}" data-list-itemcheckbox>
      <label class="inputBlock__checkboxText" for="{{$file->id}}"><i></i>
      </label>
    </div>

  </td>
  <td>{{count($file->language) > 0 && $file->language[0]->title!="" ? $file->language[0]->title : $file->filename_slug.".".$file->ext}}</td>
  <td>{{strtoupper($file->ext)}}</td>
  <td>{{\App\Helpers\File::showSize($file->filesize)}}</td>
  <td>{{$file->downloaded}}</td>
  <td>{!!\App\Components\Clist\ListColumnFormat::switchValue($file->active,'download_file',$file->id,'active')!!}</td>
  <td>
    <button type="button" class="button button--circle button--blue button--iconPencil" data-download-edit="{{$file->id}}" title="{{trans('admin.edit')}}">{{trans('admin.edit')}}</button>
    <a href="{{route('download_get',['id'=>$file->id])}}" class="button button--circle button--green button--iconDownload" title="{{trans('admin.download.get')}}">{{trans('admin.download.get')}}</a>
    <button type="button" class="button button--circle button--gray button--iconDel"
      title="{{trans('admin.delete')}}"
      data-download-remove="{{$file->id}}"
      data-question="{{trans('admin.delete_question')}} {{trans('admin.download.remove_module_confirm')}} <b><i>{{count($file->language) >0 && $file->language[0]->title!="" ? $file->language[0]->title : ($file->filename_slug.".".$file->ext)}}</i></b>">{{trans('admin.delete')}}</button>
  </td>
</tr>
