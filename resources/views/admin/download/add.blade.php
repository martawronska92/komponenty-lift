@extends('layouts.admin')

@section('footer')
  <script>

    function template(data, container) {
      var $state = $(
        '<span><img src="/img/langs/' + data.id.toLowerCase() + '.png"></span>'
      );
      return $state;
    }

    function formatState (state) {
      if (!state.id) { return state.text; }
      var $state = $(
        '<span><img src="/img/langs/' + state.element.value.toLowerCase() + '.png"></span>'
      );
      return $state;
    }

    function select2Init(){
      $("#downloadForm [data-lang-component]").select2({
        templateResult: formatState,
        templateSelection: template,
        minimumResultsForSearch: -1
      });

      $("#downloadForm [data-lang-component]").bind("change",function(){
        var current = $(this).find("option:selected").val();
        var elements = $(this).closest("[data-lang-box]").find("[data-lang-input]");
        elements.fadeOut();

        var elem = $(this);

        $(this).closest("[data-lang-box]").find("label.error").remove();
        $(this).closest("[data-lang-box]").find(".error").removeClass("error");

        elements.promise().done(function() {
          $(this).closest("[data-lang-box]").find("[data-lang-input='"+current+"']").fadeIn();
        });
      });
    }

    $(document).ready(function(){
      download.bindEvents();

      var edit = new editable("download", function() {
          download.store();
      }, function() {
          download.uploadify();
          setTimeout(function(){
            select2Init();
          },200);
      });

      $("[data-download-list]").on("click", "[data-download-edit]", function() {
          $('html, body').animate({
            scrollTop: $("[data-editable-replacecontainer]").offset().top
          }, 1500);
          edit.getData($(this).attr('data-download-edit'),function(){
            download.uploadify(1);
            setTimeout(function(){
              select2Init();
            },200);
          });
      });

      page.bindEvents();
    });
  </script>

@endsection

@section('content')
<div class="addForm addForm--noPad">
<form class="form2col" action="" method="post" name="downloadMainForm" id="downloadMainForm"
    data-form-topreview  data-formvalidation novalidate="">

<div class="form2col__wrapRow">
    <div class="form2col__row">
      @include('admin.partial.lang.input',[
        'label' => trans('admin.list.title'),
        'field' => 'title',
        'variable' => 'info'
      ])
    </div>
</div>
<div class="form2col__wrapRow">
    <div class="form2col__row">
      @include('admin.partial.lang.textarea',[
        'label' => trans('admin.list.description'),
        'field' => 'description',
        'variable' => 'info'
      ])
    </div>
</div>

<input type="hidden" id="id" value="{{$id}}"/>
<input type="hidden" id="pagePostModuleId" value="{{$pagePostModuleId}}"/>
@include('admin.partial.form.save',['cancel_url'=>$redirect_url,'generate_preview' => 'download'])
</form>
</div>

<div class="addForm addForm--noPad">
  <div class="" data-galleryvideo-leftcontainer data-editable-replacecontainer>
    @include('admin.download.partial.form')
  </div>

  <div class="downloadTable" data-galleryvideo-rightcontainer data-editable-blockcontainer>
    <div class="downloadTable__buttons">
      <button type="button" data-download-deletemarked class="button button--small button--blue button--withIcon button--iconDel">{{trans('admin.gallery.delete_marked')}}</button>
      <button type="button" data-download-deleteall class="button button--small button--gray button--withIcon button--iconDel">{{trans('admin.gallery.delete_all')}}</button>
    </div>

    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th></th>
          <th>{{trans('admin.list.title')}}</th>
          <th>{{trans('admin.download.extension')}}</th>
          <th>{{trans('admin.download.filesize')}}</th>
          <th>{{trans('admin.download.downloaded')}}</th>
          <th>{{trans('admin.list.active')}}</th>
          <th></th>
        </tr>
      </thead>
      <tbody data-download-list data-list-sortable="download">
          @each('admin.download.partial.filebox', $row, 'file')
      </tbody>
    </table>
    <div class="resultsChanges resultsChanges--info @if(isset($row) && count($row)>0) hidden @endif" data-list-empty>{{trans('admin.list.empty')}}</div>

  </div>
</div>

<input type="hidden" id="id" value="{{\Route::current()->getParameter('id')}}"/>
<input type="hidden" id="pagePostModuleId" value="{{\Route::current()->getParameter('pagePostModuleId')}}"/>

@endsection

@include('admin.partial.dialog.confirm')
