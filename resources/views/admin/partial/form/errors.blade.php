@if(count($errors->all())>0)
<div class="resultsChanges resultsChanges--error">
    @foreach ($errors->all() as $message)
        <div class="resultsChanges__item">{{$message}}</div>
    @endforeach
    <div class="js-closeResult"></div>
</div>
@endif