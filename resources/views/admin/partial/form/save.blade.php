<div class="saveLine">
  <button type="submit" class="button button--small button--blue" name="save">@if(isset($label)){{$label}}@else{{trans('admin.save')}}@endif</button>

  @if(isset($save_and_stay))
    <button type="submit" class="button button--small button--blue" name="save_and_stay">{{trans('admin.save_and_stay')}}</button>
  @endif

  @if(isset($cancel_url) && $cancel_url!="")
  <a href="{{$cancel_url}}" class="button button--small button--gray">{{trans('admin.cancel')}}</a>
  @endif

  @if(isset($generate_preview) && $generate_preview!="")
  <button type="button" class="button button--small button--green" data-module-preview="{{$generate_preview}}">{{trans('admin.page.preview')}}</button>
  @endif
</div>

<input type="hidden" name="_token" value="{{csrf_token()}}">
