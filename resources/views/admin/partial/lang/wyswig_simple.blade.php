@if($label!='')
  <div class="col-lg-2">
    <label for='{{$field}}'>{{$label}}</label>
  </div>

@include('admin.partial.lang.component')

<div class="col-lg-{{count($langs)==1?10:8}}" data-lang-box>
  @foreach($langs as $lang)
    <div data-lang-input="{{$lang->symbol}}">
      <textarea type="text" name="{{$field}}[{{$lang->id}}]" data-wyswigeditor-simple
        data-lang-input = "{{$lang->symbol}}"
        class="@if(isset($required) && $required) required @endif"
        >
          @if(isset($row))
            @foreach($row->language as $postlang)
              @if($postlang->language_id == $lang->id)
                {{$postlang->$field}}
              @endif
            @endforeach
          @endif
        </textarea>
      </div>
  @endforeach
</div>
