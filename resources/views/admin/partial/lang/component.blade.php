@if(count($langs)>1)
  <div class="selectLangs">
    <select class="" data-lang-component>
      @foreach($langs as $lang)
        <option value="{{$lang->symbol}}" @if(\App::getLocale() == $lang->symbol) selected @endif>{{$lang->name}}</option>
      @endforeach
    </select>
  </div>
@endif
