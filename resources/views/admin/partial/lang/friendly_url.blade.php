<div class="tabsForm__col2 tabsForm--vcenter">
  <label class="tabsForm__label" for='url'>{{trans('admin.friendly_url')}}</label>
</div>

  <div data-linkgenerator-container  class="form-group tabsForm__innerRow tabsForm__col{{$langs==''?12:10}}" data-lang-box>
    @include('admin.partial.lang.component')
    <div class="tabsForm__input inputBlock">
      @foreach($langs as $lang)
        <input type="text" class="form-control inputBlock__field @if(isset($required)) required @endif"
          data-lang-input = "{{$lang->symbol}}"
          name="url[{{$lang->id}}]"
          id="url_{{$lang->id}}"
          maxlength="255"
          data-formvalidation-label = "{{trans('admin.friendly_url')}} @if(count($langs) > 1)({{$lang->name}})@endif"

          data-linkgenerator
          data-linkgenerator-source="#{{$el}}_{{$lang->id}}"
          data-linkgenerator-lang="{{$lang->id}}"
          data-linkgenerator-type="{{$type}}"
          data-linkgenerator-id="@if(isset($id)){{$id}}@endif"
          data-linkgenerator-dest="#url_{{$lang->id}}"

          @if(isset($row))
          @foreach($row->language as $postlang)
          @if($postlang->language_id == $lang->id)
          value="{{$postlang->url}}"
          @endif
          @endforeach
          @endif

          @if($lang->symbol!=\App::getLocale()) style='display:none;' @endif

        class="required slug"/>

      @endforeach
    </div>
  </div>
