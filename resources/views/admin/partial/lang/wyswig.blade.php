
@if(!isset($no_label))
  <div class="tabsForm__row langLine">
    <label class="tabsForm__label" for='{{$field}}'>{{$label}}</label>
    @include('admin.partial.lang.component')
  </div>
@endif

  <div class="tabsForm__row form-group js-wyswig">



      @foreach($langs as $lang)
        <div data-lang-input="{{$lang->symbol}}">
          <textarea type="text" name="{{$field}}[{{$lang->id}}]" data-wyswigeditor
            class="@if(isset($required) && $required) required @endif"

            >
              @if(isset($row))
                @foreach($row->language as $postlang)
                  @if($postlang->language_id == $lang->id)
                    {{$postlang->$field}}
                  @endif
                @endforeach
              @endif
          </textarea>
        </div>
      @endforeach

</div>
