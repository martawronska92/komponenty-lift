@if($label!='')
  <div class=" @if(isset($mypage)) tabsForm__col6 @else tabsForm__col2 @endif tabsForm--vcenter">
    <label class="tabsForm__label" for='{{$field}}'>{{$label}}</label>
  </div>
@endif
<div class="form-group tabsForm__innerRow inputBlock @if(isset($mypage)) tabsForm__col6 @else tabsForm__col10 @endif" data-lang-box>
      @include('admin.partial.lang.component')
    @if($label != trans('admin.settings.meta_description') && $label != trans('admin.settings.meta_title')) <div class="tabsForm__input inputBlock"> @endif
        @foreach($langs as $lang)
          <input type="text"
            name="<?php echo (isset($field_name) ? $field_name : $field).'['.$lang->id.']'.(isset($multi) ? '[]' : '');?>"
            data-lang-input = "{{$lang->symbol}}"
            data-lang-id = "{{$lang->id}}"
            @if(!isset($multi))
              id="<?php echo isset($field_id) ? $field_id : $field; ?>_{{$lang->id}}"
            @endif

            @if(isset($linkgenerator))
              data-linkgenerator-trigger="url_{{$lang->id}}"
            @endif

            @if(isset($placeholder))
              placeholder = "{{$placeholder}}"
            @endif

            @if($label!='')
              data-formvalidation-label = "{{$label}} @if(count($langs)>1)({{$lang->name}})@endif"
            @endif

            class="inputBlock__field @if(isset($required) && $required) required @endif"
            maxlength="255"

            @if($lang->symbol!=\App::getLocale()) style='display:none;' @endif

          @if((isset($row) && property_exists($row,'language') || isset($row->language)) && !isset($variable))
              @foreach($row->language as $postlang)
                @if($postlang->language_id == $lang->id)
                  value="{{$postlang->$field}}"
                @endif
              @endforeach
          @elseif(isset($variable) && isset($$variable))
            @if(is_object($$variable) && (property_exists($$variable,'language') || isset($$variable->language)))
              @foreach($$variable->language as $rowlang)
                @if($rowlang->language_id == $lang->id)
                  value="{{$rowlang->$field}}"
                @endif
              @endforeach
            @elseif(is_array($$variable) && array_key_exists($variable_key,$$variable))
              <?php $temp = $$variable; ?>
              @if(array_key_exists($variable_key,$temp) && (property_exists($temp[$variable_key],'language') || isset($temp[$variable_key]->language)))
                @foreach($temp[$variable_key]->language as $rowlang)
                  @if($rowlang->language_id == $lang->id)
                    value="{{$rowlang->$field}}"
                  @endif
                @endforeach
              @endif
            @endif
          @else
            value="{{old($field."[".$lang->id."]")}}"
          @endif
        />
        @endforeach
        @if($label != trans('admin.settings.meta_description') && $label != trans('admin.settings.meta_title')) </div> @endif
</div>
