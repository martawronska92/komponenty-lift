@if($label!='')
  <div class="tabsForm__col2">
    <label class="tabsForm__label" for='{{$field}}'>{{$label}}</label>
  </div>
@endif

<div class="tabsForm__innerRow tabsForm__col10" data-lang-box>
      @include('admin.partial.lang.component')
      <div class="tabsForm__input inputBlock">
      @foreach($langs as $lang)
          <textarea type="text"
            name="<?php echo (isset($field_name) ? $field_name : $field).'['.$lang->id.']'.(isset($multi) ? '[]' : '');?>"
            id="{{$field}}_{{$lang->id}}"

            data-lang-input="{{$lang->symbol}}"

            @if(isset($maxlength))
              maxlength = "{{$maxlength}}"
            @endif

            @if(isset($textareaRows))
              rows = "{{$textareaRows}}"
            @endif

            @if($label!='')
              data-formvalidation-label = "{{$label}} @if(count($langs)>1)({{$lang->name}})@endif"
            @endif

            @if($lang->symbol!=\App::getLocale()) style='display:none;'; @endif

            class="inputBlock__textarea @if(isset($required) && $required) required @endif form-control"><?php
              if((isset($row) && property_exists($row,'language') || isset($row->language)) && !isset($variable)){
                  foreach($row->language as $postlang){
                    if($postlang->language_id == $lang->id)
                      echo $postlang->$field;
                  }
              }else if(isset($variable) && isset($$variable)){
                  if(is_object($$variable) && (property_exists($$variable,'language') || isset($$variable->language))){
                    foreach($$variable->language as $rowlang){
                      if($rowlang->language_id == $lang->id)
                        echo $rowlang->$field;
                    }
                  }else if(is_array($$variable) && array_key_exists($variable_key,$$variable)){
                    $temp = $$variable;
                    if(array_key_exists($variable_key,$temp) && (property_exists($temp[$variable_key],'language') || isset($temp[$variable_key]->language))){
                      foreach($temp[$variable_key]->language as $rowlang){
                        if($rowlang->language_id == $lang->id)
                          echo $rowlang->$field;
                      }
                    }
                  }
             }else{
                echo old($field."[".$lang->id."]");
            }
            ?></textarea>
        @endforeach
    </div>
</div>
