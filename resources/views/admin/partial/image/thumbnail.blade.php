<div data-thumbnail-container data-thumbnail-width="{{$pt_width}}" data-thumbnail-height="{{$pt_height}}">

  <?php
    $file = "";
    if(isset($pt_path)){
      $file = rtrim(public_path(),"/").(strpos($pt_path,'?')!==FALSE ? substr($pt_path,0,strpos($pt_path,'?')) : $pt_path);
    }
  ?>

  <img data-thumbnail-preview class="hidden"
  @if(($pt_exists==1 || (isset($pt_path) && $pt_path!="")) && \File::exists($file))
    src = "{{$pt_path}}"
  @endif
  />

  <div data-thumbnail-box>
    <div data-thumbnail-editor>

    </div>
    <div class="text-center">
        <label class="button button--small button--blue">{{trans('admin.post.choose_thumbnail_file')}}
          <input type="file" data-thumbnail-upload name="upload" accept="image/*" class="hidden">
        </label>
        @if(!isset($pt_nobutton))
          <button type="button" class="button button--small button--green button--withIcon button--iconAdd" data-thumbnail-result
          @if(isset($pt_callback))
            data-thumbnail-callback="{{$pt_callback}}"
          @endif
            >{{trans(isset($pt_label) ? $pt_label : 'admin.post.set_thumbnail')}}
          </button>
        @endif
        @if($pt_delete_button)
        <button type="button" class="button button--small button--gray" style="@if($pt_exists==0)display:none;@endif"
          data-thumbnail-remove>{{trans('admin.delete')}}</button>
        @endif
    </div>
  </div>

  <input type="hidden" name="thumbnail" value="@if($pt_exists==1){{$pt_value}}@endif"/>
  <input type="hidden" name="thumbnail_encoded" value=""/>
  <input type="hidden" name="thumbnail_original" value=""
    @if(isset($pt_required) && $pt_required==1 && $pt_value=="") class='required' @endif
    data-formvalidation-label="{{trans('admin.validation.thumbnail')}}" />
  <input type="hidden" name="thumbnail_mime" value=""/>
  <input type="hidden" name="thumbnail_options" data-thumbnail-options value="@if(isset($pt_options)){{$pt_options}}@endif"/>
</div>
