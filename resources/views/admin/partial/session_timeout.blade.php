@if(!session('rememberMe'))
 <div class="adminTopbar__time">
   <span>{{trans('admin.user.session')}} </span>
   <b>
    <span data-activitytimeout-box = "{{$activityTimeout}}">
        <span data-activitytimeout-minute>{{$activityTimeout/60 < 10 ? "0" + $activityTimeout/60 : $activityTimeout/60 }}</span><span>:</span><span data-activitytimeout-second>00</span>
    </span>
  minut</b> 
 </div>
@endif
