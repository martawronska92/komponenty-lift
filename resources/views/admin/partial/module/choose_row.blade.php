<div class="modules" data-page-modulerow data-id="@if(isset($row)){{$row->module_id}}@endif">
  <div class="modules__row">

    <div class="modules__col modules__col--w170">
      <span data-page-modulename>@if(isset($row)){{$row->module_name}}@else-@endif</span>
    </div>
    <div class="modules__col modules__col--w210">
      <span data-page-entitydetails>
        @if(isset($row) && property_exists($row,'entity_name') && $row->entity_name!="")
          {{$row->entity_name}}
        @elseif(isset($row) && property_exists($row,'entity_details') && $row->entity_details!="")
          {!!$row->entity_details!!}
        @else
        -
        @endif
      </span>
    </div>
    <div class="modules__col modules__col--w90">
      @if(isset($row))
        {!!\App\Components\Clist\ListColumnFormat::switchValue($row->active,'page_post_module',$row->id,'active')!!}
      @else
        {!!\App\Components\Clist\ListColumnFormat::switchValue(1,'page_post_module',0,'active')!!}
      @endif
    </div>
    <div class="modules__col modules__col--w95">
      @if(\App\Models\User::isDeveloper())
      <a href="@if(isset($row) && (($row->symbol=='category' && $row->category_id!="") || $row->module_entity_model=='App\\Models\Category')) {{route('category_edit',['id'=>$row->module_entity_id,'pagePostModuleId'=>$row->id])}} @endif"
          class="button button--blue button--circle button--iconPencil @if(isset($row) && $row->symbol!='category' && $row->module_entity_model!="App\\Models\Category") hidden @endif"
          data-page-categorybutton='edit' title="{{trans('admin.category.edit')}}">{{trans('admin.category.edit')}}</a>
      @endif

      <a href="@if(isset($row) && ($row->symbol=='category' || $row->category_id!=""  || $row->module_entity_model=='App\Models\Category')) {{route('post',['category'=>$row->module_entity_id,'pagePostModuleId'=>$row->id])}} @endif"
          class="button button--blue button--circle button--iconList @if(isset($row) && $row->symbol!='category' && $row->module_entity_model!="App\\Models\Category") hidden @endif"
          data-page-categorybutton='showposts' title="{{trans('admin.category.showposts')}}">{{trans('admin.category.showposts')}}</a>

      @if(isset($row) && (\Route::has($row->symbol.'_edit') || \Route::has($row->symbol)))
        <a href="@if(isset($row) && ($row->symbol!='category' && $row->category_id=="")) {{\Route::has($row->symbol.'_edit') ? route($row->symbol.'_edit',['id'=>$row->module_entity_id,'pagePostModuleId'=>$row->module_id]) :  (\Route::has($row->symbol) ? route($row->symbol) : 'javascript:void(0)')}}@endif"
            class="button button--circle button--blue button--iconEye @if(isset($row) && $row->symbol=='category' && $row->category_id!="") hidden @endif"
            data-page-modulebutton title="{{trans('admin.edit')}}"></a>
      @endif

      <button type="button" class="button button--circle button--gray button--iconDel"
        data-question="{{trans('admin.delete_question')}} {{trans('admin.page.remove_module_confirm')}} <b><i>@if(isset($row)){{$row->module_name}}@else-@endif</i></b>"
        data-page-removemodule="@if(isset($row)){{$row->module_id}}@endif" title="{{trans('admin.delete')}}"></button>
    </div>
  </div>
</div>
