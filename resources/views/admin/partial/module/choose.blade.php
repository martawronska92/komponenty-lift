@if(!isset($id))
<div class="resultsChanges resultsChanges--info">{{trans('admin.module.savebeforeadd')}}</div>
@else
  <div class="tabsForm__row modules__block">
    <div class="tabsForm__col tabsForm--vcenter tabsForm--pl40">
      <label class="tabsForm__label" for="available_modules">{{trans('admin.page.available_modules')}}</label>
    </div>
    <div class="tabsForm__col">
      <select data-page-modulelist class="js-select2-modules select--w240">
        <option value="">{{trans('admin.page.choose_module')}}</option>
        @foreach($modules as $module)
          <option value="{{$module->id}}" data-symbol="{{$module->symbol}}"
              data-url="@if(\Route::has($module->symbol)){{route($module->symbol)}}@else @endif"
              data-bindentity={{$module->bind_entity}}>{{$module->name}}</option>
        @endforeach
      </select>
    </div>
    <div class="tabsForm__col">
      <select data-page-moduleid class="hidden">

      </select>
      <button type="button" data-page-addmodule class="button button--xs button--green button--withIcon button--iconAdd">{{trans('admin.list.add')}}</button>
    </div>
  </div>

  <div class="fullWidth">
          <div class="tabsForm__row tabsForm__titles @if(isset($row) && count($row->module)==0) hidden  @endif" data-page-moduleboxheader>
            <div class="modules__col modules__col--w170"><label>{{trans('admin.module.name')}}</label></div>
            <div class="modules__col modules__col--w210"><label>{{trans('admin.module.details')}}</label></div>
            <div class="modules__col modules__col--w90"><label>{{trans('admin.list.active')}}</label></div>
            <div class="modules__col modules__col--w95"></div>
          </div>
          <div data-page-modulebox data-list-sortable="pagepost">
            @if(isset($row))
              @php $modules = isset($row->modules) ? $row->modules : $row->module; @endphp
              @if(count($modules) == 0)
                <div class="resultsChanges resultsChanges--info">{{trans('admin.module.added_list_empty')}}</div>
              @else
                @foreach($modules as $module)
                    {!!\View::make('admin.partial.module.choose_row',['row'=>$parsed_modules[$module->id]])!!}
                @endforeach
              @endif
            @endif
          </div>
   </div>
@endif
