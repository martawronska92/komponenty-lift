<script id="singleDialog" type="text/x-handlebars-template"><div id="singleDialogContent" class="modal fade mypopup" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="mypopup__content">
          <div class="mypopup__body">
            <p>@{{{text}}}</p>
          </div>
          @{{#if showButton}}
            <div class="mypopup__footer">
              <button type="button" class="button button--small button--blue" data-singledialogbox-button>@{{{buttonText}}}</button>
            </div>
          @{{/if}}
        </div>
      </div>
    </div>
</script>
