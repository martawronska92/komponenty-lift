<script id="confirmDialog" type="text/x-handlebars-template"><div id="confirmDialogContent" class="modal fade mypopup" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="mypopup__content">
          {{--<div class="modal-header">--}}
            {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>--}}
            {{--<h4 class="modal-title">{{trans('admin.confirmation')}}</h4>--}}
          {{--</div>--}}
          <div class="mypopup__body">
            <p>@{{{question}}}</p>
          </div>
          <div class="mypopup__footer">
            <button type="button" class="button button--small button--blue" data-confirmationbox-yes>{{trans('admin.yes')}}</button>
            <button type="button" class="button button--small button--gray" data-dismiss="modal">{{trans('admin.no')}}</button>
          </div>
        </div>
      </div>
    </div>
</script>
