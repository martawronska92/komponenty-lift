@extends('layouts.admin')

@section('footer')
  <script>

    function template(data, container) {
      var $state = $(
        '<span><img src="/img/langs/' + data.id.toLowerCase() + '.png"></span>'
      );
      return $state;
    }

    function formatState (state) {
      if (!state.id) { return state.text; }
      var $state = $(
        '<span><img src="/img/langs/' + state.element.value.toLowerCase() + '.png"></span>'
      );
      return $state;
    }

    function select2Init(){
      $(".grayBg [data-lang-component]").select2({
        templateResult: formatState,
        templateSelection: template,
        minimumResultsForSearch: -1
      });

      $(".grayBg [data-lang-component]").bind("change",function(){
        var current = $(this).find("option:selected").val();
        var elements = $(this).closest("[data-lang-box]").find("[data-lang-input]");
        elements.fadeOut();

        var elem = $(this);

        $(this).closest("[data-lang-box]").find("label.error").remove();
        $(this).closest("[data-lang-box]").find(".error").removeClass("error");

        elements.promise().done(function() {
          $(this).closest("[data-lang-box]").find("[data-lang-input='"+current+"']").fadeIn();
        });
      });
    }
  </script>
@endsection

@section('content')

<div class="addForm addForm--noPad">
  <form class="form2col" action="" method="post" name="galleryForm" id="galleryListForm"
        data-form-topreview  data-formvalidation novalidate="">

  <div class="form2col__wrapRow">
    <div class="form2col__row">
        @include('admin.partial.lang.input',[
          'label' => trans('admin.list.title'),
          'field' => 'title'
        ])
    </div>
  </div>
  <div class="form2col__wrapRow">
    <div class="form2col__row">
        @include('admin.partial.lang.textarea',[
          'label' => trans('admin.list.description'),
          'field' => 'description'
        ])
    </div>
  </div>

  <input type="hidden" id="pagePostModuleId" value="{{$pagePostModuleId}}"/>
  <input type="hidden" id="id" value="{{$id}}"/>
  @include('admin.partial.form.save',['cancel_url'=>$redirect_url,'generate_preview'=>'galleryphotolist'])
  </form>
</div>

@endsection
