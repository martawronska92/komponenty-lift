@extends('layouts.admin')

@section('content')
  <div class="sliderBlock">

    <form action="@if(isset($sliderId)){{route('slider_store')}}@else{{route('slider_store',['id'=>$id])}}@endif"
      method="post" data-formvalidation id="sliderForm" data-form-beforesubmit="post.uploadThumb.crop">
      <div class="sliderBlock__inner">

        <div class="sliderBlock__row">
          <div class="sliderBlock__preview">
            <label class="sliderBlock__previewName">{{ucfirst(trans('admin.slider.image'))}}</label>
            @include('admin.partial.image.thumbnail',[
              'pt_exists' => isset($row) && $row->ext!="" ? 1 : 0,
              'pt_path' => isset($row) ? "/img/slider/".$row->id."/thumbnail.".$row->ext : "",
              'pt_value' => isset($row) ? $row->ext : "",
              'pt_delete_button' => 0,
              'pt_required' => 1,
              'pt_width' => $slider->symbol == 'homepage'? $settings['slider_homepage_thumb_width'] : $settings['slider_subpage_thumb_width'],
              'pt_height' => $slider->symbol == 'homepage' ? $settings['slider_homepage_thumb_height'] : $settings['slider_subpage_thumb_height'],
              'pt_options' => isset($row) ? $row->thumbnail_options : "",
              'pt_nobutton' => 1
            ])
          </div>
        </div>


        <div class="sliderBlock__row sliderBlock__row--flex">
          <div class="sliderBlock__col50">
            <div class="sliderBlock__innerRow">
              @include('admin.partial.lang.input',[
                'label' => trans('admin.list.header_1'),
                'field' => 'title_1'
              ])
            </div>

            <div class="sliderBlock__innerRow">
              @include('admin.partial.lang.input',[
                'label' => trans('admin.list.header_2'),
                'field' => 'title_2'
              ])
            </div>

            <div class="sliderBlock__innerRow">
              <div class="sliderBlock__col3">
                <label class="tabsForm__label">{{trans('admin.slider.status')}}</label>
              </div>
              <div class="sliderBlock__col9">
              <div class="inputBlock__checkbox">
                <input type="checkbox" name="active" id="active" @if(isset($row) && $row->active) checked @endif/>
                <label class="inputBlock__checkboxText" for="active"><i></i>
                  <span>{{trans('admin.list.active')}}</span>
                </label>
              </div>
              </div>
            </div>


          </div>

          <div class="sliderBlock__col50">
                <div class="sliderBlock__innerRow sliderBlock__innerRow--top" data-slider-linktypecontainer>
                  <div class="sliderBlock__col2">
                  <label class="tabsForm__label">{{trans('admin.slider.link_type')}}</label>
                  </div>
                  <div class="sliderBlock__col10">
                    <div class="sliderBlock__check">
                  <div class="inputBlock__checkbox">
                    <input type='radio' name='external' value='-1' id="external_-1"
                      @if((isset($row) && $row->external==-1) || !isset($row) || old('external')==-1) checked @endif />
                    <label class="inputBlock__checkboxText" for="external_-1"><i></i>
                      <span>{{trans('admin.none')}}</span>
                    </label>
                  </div>
                    </div>

                  <div class="sliderBlock__check">
                  <div class="inputBlock__checkbox">
                    <input type='radio' name='external' value='0' id="external_0"
                      @if((isset($row) && $row->external==0) || (old('external')!=NULL && old('external')==0)) checked @endif />
                    <label class="inputBlock__checkboxText" for="external_0"><i></i>
                      <span>{{trans('admin.slider.internal_link')}}</span>
                    </label>
                  </div>
                  <div class="{{!isset($row) || (isset($row) && $row->external!=0) || old('external')!=0 ? 'hidden' : '' }}" data-slider-linktypebox="0">
                    <select name="internal_content" data-slider-linktype="0"
                      class="js-select2 {{ (isset($row) && $row->external==0) ? 'required' : '' }}"
                      data-formvalidation-label="{{ucfirst(trans('admin.slider.internal_link'))}}">
                      <option value="">{{trans('admin.choose_page')}}</option>
                      @foreach($pages->pages as $page)
                        @if($page->level == 0)
                          <option value="{{$page->id}}"
                            @if( (isset($row) && $row->external==0 && $row->link==$page->id) || old('internal_content')==$page->id) selected @endif
                              >{{$page->title}}</option>
                            @if(array_key_exists($page->id,$pages->parent))
                              @foreach($pages->parent[$page->id] as $subpage1)
                                <option @if( (isset($row) && $row->external==0 && $row->link==$subpage1->id) || old('internal_content')==$subpage1->id) selected @endif
                                  value="{{$subpage1->title}}">&nbsp;&nbsp;&nbsp;{{$subpage1->title}}</option>
                                @if(array_key_exists($subpage1->id,$pages->parent))
                                  @foreach($pages->parent[$subpage1->id] as $subpage2)
                                    <option @if( (isset($row) && $row->external==0 && $row->link==$subpage2->id) || old('internal_content')==$subpage2->id) selected @endif
                                      value="{{$subpage2->title}}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$subpage2->title}}</option>
                                  @endforeach
                                @endif
                              @endforeach
                            @endif
                        @endif
                      @endforeach
                    </select>
                  </div>
                  </div>

                  <div class="sliderBlock__check">
                  <div class="inputBlock__checkbox">
                    <input type='radio' name='external' value='1' id="external_1"
                      @if((isset($row) && $row->external==1) || old('external')==1) checked @endif/>
                    <label class="inputBlock__checkboxText" for="external_1"><i></i>
                      <span>{{trans('admin.slider.external_link')}}</span>
                    </label>
                  </div>

                  <div class="{{ (isset($row) && $row->external!=1) || (!isset($row) && old('external')!=1) ? 'hidden' : '' }} "
                    data-slider-linktypebox="1">
                    <input name="external_content"
                      id="external_content"
                      value="@if(isset($row) && $row->external==1){{$row->link}}@else{{old('external_content')}}@endif"
                      class="inputBlock__field {{ (isset($row) && $row->external==1) ? 'required url_http' : '' }}"
                      data-slider-linktype="1"
                      data-formvalidation-label="{{ucfirst(trans('admin.slider.external_link'))}}"/>
                  </div>

                  </div>

                </div>
                </div>

          </div>
        </div>

        @if(isset($sliderId))
          <input type="hidden" name="slider_id" value="{{$sliderId}}"/>
        @endif


      </div>
      @include('admin.partial.form.save',['cancel_url'=>route('slides', ['sliderId'=> isset($row) ? $row->slider_id : $sliderId])])
    </form>

  </div>


@endsection
