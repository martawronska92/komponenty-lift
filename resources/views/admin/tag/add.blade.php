@extends('layouts.admin')

@section('content')
<div class="addForm">
<form action="" method="post" data-formvalidation>

<div class="form2col__wrapRow">
  <div class="form2col__row">
      @include('admin.partial.lang.input',[
          'label' => trans('admin.list.name'),
          'field' => 'name',
          'required' => true
      ])
  </div>
</div>

@include('admin.partial.form.save',['cancel_url'=>route('tag')])

</form>
</div>

@endsection
