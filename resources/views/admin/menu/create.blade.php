@extends('layouts.admin')

@section('footer')
  <script>
    $(document).ready(function () {
      tree.bindEvents();
    });
  </script>
@endsection

@section('content')

  <div class="addForm">
    <form class="form2col" id="menuForm" method="post" action="" novalidate="" data-formvalidation>
      <div class="form2col__wrapRow">
        <div class="form2col__row">
          <div class="form2col__left">{{trans('admin.name')}}</div>
          <div class="form2col__right inputBlock"><input type="text" name="name" class="required inputBlock__field"
              maxlength="255"
              value="@if(isset($menuRow)){{$menuRow->name}}@else{{old('name')}}@endif"
              data-formvalidation-label="{{trans('admin.name')}}"/></div>
        </div>
      </div>
      <div class="form2col__wrapRow">
        <div class="form2col__row">
          <div class="form2col__left">{{trans('admin.symbol')}}</div>
          <div class="form2col__right inputBlock"><input type="text" name="symbol" class="required inputBlock__field"
              maxlength="255"
              value="@if(isset($menuRow)){{$menuRow->symbol}}@else{{old('symbol')}}@endif"
              data-formvalidation-label="{{trans('admin.symbol')}}"/></div>
        </div>
      </div>

      @include('admin.partial.form.save',['cancel_url'=>route('menu')])

    </form>
  </div>

  @if(isset($menuRow))

    <br>

    <div class="addForm" id="menuTree">
      <div class="addForm__buttons">
        <button type="button" class="button button--xs button--blue" data-menu-expandall>{{trans('admin.menu.expand_all')}}</button>
        <button type="button" class="button button--xs button--gray" data-menu-collapseall>{{trans('admin.menu.hide_all')}}</button>
      </div>

      <div id="tree">
        {!!\App\Models\Menu::render(0,0,$elements,"")!!}
      </div>
      <br>
      <div class="-tac">
        <button type="button" class="button button--small button--green button--withIcon button--iconAdd" data-menu-add-element>{{trans('admin.menu.add_element')}}</button>
        <button type="button" class="button button--small button--green button--withIcon button--iconAdd" data-menu-add-subelement>{{trans('admin.menu.add_subelement')}}</button>
        <button type="button" class="button button--small button--blue button--withIcon button--iconPencil" data-menu-edit-element>{{trans('admin.edit')}}</button>
        <button type="button" class="button button--small button--gray button--withIcon button--iconDel" data-menu-delete-element>{{trans('admin.delete')}}</button>
      </div>

    </div>

    <input type="hidden" id="menu_id" value="@if(isset($id)){{$id}}@endif"/>

    <div class="addForm addForm--padBot" id="nodeDetailsForm" style="display:none;">
      <div class="form2col"  novalidate="" >
        <form data-formvalitdation novalidate="novalidate">
          <div class="menuBlock__flex">
            <div class="menuBlock__col50">
              <input type="hidden" name="id" value="" id="id"/>
              <div class="form2col__wrapRow">
                <div class="form2col__row">
                  @include('admin.partial.lang.input',[
                    'field' => 'title',
                    'label' => trans('admin.menu.element_title'),
                    'langs' => $langs,
                    'required' => 1
                  ])
                </div>
              </div>
              <div class="form2col__wrapRow">
                <div class="form2col__row">
                  <div class="tabsForm__col2 tabsForm--vcenter">
                    <label class="tabsForm__label" for="active">{{trans('admin.list.active')}}</label></div>
                  <div class="tabsForm__innerRow tabsForm__col10">
                    <div class="inputBlock__checkbox inputBlock__checkbox--noMargin">
                      <input type="checkbox" name="active" value="1" id="active"/>
                      <label class="inputBlock__checkboxText" for="active"><i></i></label>
                    </div>


                  </div>
                </div>
              </div>
            </div>

            <div class="menuBlock__col50">
              <div class="form2col__wrapRow">
                <div class="form2col__row form2col__row--top">
                  <div class="tabsForm__col2">
                    <label class="tabsForm__label">{{trans('admin.menu.connect_with')}}</label>
                  </div>


                  <div class="tabsForm__col10">

                    <div class="sliderBlock__check">

                      <div class="inputBlock__checkbox">
                        <input type="radio" name="menu_element_type" value="page" class="required" id="menu_element_type_page">
                        <label class="inputBlock__checkboxText" for="menu_element_type_page"><i></i>
                          <span>{{trans('admin.menu.connect_with_page')}}</span>
                        </label>
                      </div>

                      <select name="page" data-menu-element-type='page' class="js-select2">
                        @foreach($pages->pages as $page)
                          @if($page->level == 0)
                            <option value="{{$page->id}}"

                                >{{$page->title}}</option>
                              @if(array_key_exists($page->id,$pages->parent))
                                @foreach($pages->parent[$page->id] as $subpage1)
                                  <option
                                    value="{{$subpage1->title}}">&nbsp;&nbsp;&nbsp;{{$subpage1->title}}</option>
                                  @if(array_key_exists($subpage1->id,$pages->parent))
                                    @foreach($pages->parent[$subpage1->id] as $subpage2)
                                      <option
                                        value="{{$subpage2->title}}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$subpage2->title}}</option>
                                    @endforeach
                                  @endif
                                @endforeach
                              @endif
                          @endif
                        @endforeach
                      </select>
                    </div>


                    <div class="sliderBlock__check">

                      <div class="inputBlock__checkbox">
                        <input type="radio" name="menu_element_type" value="link" class="required" id="menu_element_type_link">
                        <label class="inputBlock__checkboxText" for="menu_element_type_link"><i></i>
                          <span>{{trans('admin.menu.connect_with_link')}}</span>
                        </label>
                      </div>


                      <div class="inputBlock">
                        <input type="text" name="url" data-menu-element-type='link' class="inputBlock__field"
                          data-formvalidation-label="{{trans('admin.menu.connect_with')}} {{trans('admin.menu.connect_with_link')}}">
                      </div>
                    </div>


                  </div>

                </div>

              </div>
            </div>
          </div>


          @include('admin.partial.form.save',['cancel_url'=>''])
        </form>

      </div>
    </div>
  @endif

  @include('admin.partial.dialog.confirm')

@endsection
