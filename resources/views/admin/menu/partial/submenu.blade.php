<ul data-level="{{$level}}" @if($level==0) class="fancytree-connectors" id="treeData" style="display: none;" @endif>
  @if(count($elements->data)>0)
    @foreach($elements->hierarchy[$level][$parentId] as $elementId)
      <li id="{{$elementId}}"
          @if(array_key_exists($level+1,$elements->hierarchy) && array_key_exists($elementId,$elements->hierarchy[$level+1]))
            class="expanded"
          @endif
      >

        {{$elements->data[$elementId]->language[0]->title}}

        @if(array_key_exists($level+1,$elements->hierarchy) && array_key_exists($elementId,$elements->hierarchy[$level+1]))
            {!! \App\Models\Menu::render($level+1,$elementId,$elements,"") !!}
        @endif
      </li>
    @endforeach
  @endif
</ul>
