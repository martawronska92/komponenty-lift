@extends('layouts.'.(\Request::ajax() ? 'empty' : 'admin'))

@if(!\Request::ajax())
  @section('footer')
    <script>
      $(document).ready(function(){
        productoffer.checkedProducts = $.parseJSON("{{json_encode($products)}}");
        productoffer.bindEvents();
        list.callback = function(){
          productoffer.markChecked();
        };
        page.bindEvents();
      });
    </script>
  @endsection
@endif
@section('content')
  @if(!\Request::ajax())

    <div class="titleLine">
        <h1 class="mainTitle">{{trans('admin.productoffer.header')}}</h1>
    </div>

    <form class="form2col" action="" method="post" name="contentForm" id="contentForm"
        data-form-topreview  data-formvalidation novalidate="">
      <div class="addForm">

          <div class="form2col__wrapRow">
            <div class="form2col__row">
                @include('admin.partial.lang.input',[
                  'label' => trans('admin.list.title'),
                  'field' => 'title'
                ])
            </div>
          </div>


          <div data-list-container>
            {!!$list!!}
          </div>

       <input type="hidden" id="pagePostModuleId" value="{{$pagePostModuleId}}"/>
       <input type="hidden" id="id" value="{{$id}}"/>

        @include('admin.partial.form.save',['cancel_url'=>$redirect_url,'generate_preview'=>'productoffer'])
      </div>
    </form>

  @endif

@endsection

@if(\Request::ajax())
  {!!$list!!}
@endif
