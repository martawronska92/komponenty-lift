@if(count($search)>0)
  <div class="form2col__wrapRow">
<div class="form2col__row">
  {{-- wyszukiwarka --}}

    @if(count($search)>0)
      @include('admin.components.clist.search',['search'=>$search])
    @endif
</div>
</div>
@endif

@if(count($rows) == 0)
  <div class="resultsChanges resultsChanges--info">{{trans('admin.list.empty')}}</div>
@else

  <br>
  <br>

<form action="" method="post" data-formvalidation>
  <div class="universalTable universalTable--firstCheck">
  <table width="100%" class="table table-striped table-hover">
    <thead>
      <tr>
        <th>

          <div class="inputBlock__checkbox inputBlock__checkbox--noMargin">
            <input type="checkbox" id="15" value="15" data-productoffer-checkboxall>
            <label class="inputBlock__checkboxText" for="15"><i></i>
            </label>
          </div>
        </th>

        {{-- headery kolumn --}}
        @foreach($columns as $column)
          <th @if($column->getOrder()) data-list-ordercolumnfield="{{$column->getField()}}" data-list-ordercolumn="@if(\Request::has('order_field') && \Request::has('order') && \Request::get('order_field') == $column->getField()){{\Request::get('order')}} @endif" @endif>
            @if($column->getOrder()) <i class="fa @if(\Request::has('order_field') && \Request::has('order') && \Request::get('order_field') == $column->getField()) fa-sort-{{\Request::get('order')}} @else fa-sort @endif"></i> @endif
            {{trans($column->getLabel())}}</th>
        @endforeach
      </tr>
    </thead>

    <tbody @if($sortable) data-list-sortable="{{$sortable}}" @endif data-list-body>
      {{-- rows --}}
      @foreach($rows as $row)
        <tr>
          <td>
            <div class="inputBlock__checkbox inputBlock__checkbox--noMargin">
              <input type="checkbox" id="product[{{$row->id}}]" name="product[{{$row->id}}]" value="{{$row->id}}"
                @if(in_array($row->id,$products))
                checked
                @endif
                data-productoffer-checkbox/>
              <label class="inputBlock__checkboxText" for="product[{{$row->id}}]"><i></i>
              </label>
            </div>


            </td>

          {{-- wartości pól --}}
          @foreach($columns as $key => $column)
            <td @if($key==0) data-id="{{$row->id}}"  @endif>
              {!!$column->render($row)!!}
            </td>
          @endforeach

        </tr>
      @endforeach
    </tbody>

  </table>
  </div>

  <input type="hidden" data-list-globalorder value="@if(\Request::has('order')){{\Request::get('order')}}@endif">
  <input type="hidden" data-list-globalorderfield value="@if(\Request::has('order_field')){{\Request::get('order_field')}}@endif">
@endif
