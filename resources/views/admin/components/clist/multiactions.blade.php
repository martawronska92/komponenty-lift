<div div-multiaction-box class="sliderList__todo">
  <label class="tabsForm__label sliderList__left">{{trans('admin.slider.todo')}}</label>
  <div class="sliderList__right">
    <select data-multiaction-list class="js-select2">
      <option value="">{{trans('admin.list.choose_action')}}</option>
      @foreach($multiActions as $action)
        <option value="{{$action->getUrl()}}" data-multiaction-confirm="{{$action->getConfirm()}}">{{$action->getLabel()}}</option>
      @endforeach
    </select>

  <button type="button" class="button button--small button--green button--withIcon button--iconGalka" data-multiaction-execute>{{trans('admin.execute')}}</button>
</div>
</div>
