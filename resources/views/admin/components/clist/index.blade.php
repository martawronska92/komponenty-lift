@extends('layouts.'.(\Request::ajax() ? 'empty' : 'admin'))

@if(!\Request::ajax())
  {{-- widok akcji index dla listy --}}

  @section('content')

  <div class="lala" data-list-container>
    {!!$list!!}
  </div>


  @include('admin.partial.dialog.confirm')

  @endsection

@else
  {!!$list!!}
@endif
