@if($url !="")
  <a href="{{rtrim($url,'/')}}"
@else
  <button type="button"
@endif

  class="{{$class}}"
  @if(count($attributes)>0)
      @foreach($attributes as $name=>$value)
        {{$name}} = "{{$value}}"
      @endforeach
  @endif

  @if($confirm)
    data-confirmationbox="1" data-question="{{$confirm}}"
  @endif

  >@if($faclass!="")<i class="fa {{$faclass}}" aria-hidden="true"></i> @endif

  @if(is_string($label))
    {{trans($label)}}
  @else
    <?php echo call_user_func($label,[$row]); ?>
  @endif

@if($url!="")
  </a>
@else
  </button>
@endif
