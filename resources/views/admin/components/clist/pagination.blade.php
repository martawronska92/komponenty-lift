
<div class="row">
  <div class="col-lg-2">
    <div class="input-group">
      <span class="input-group-addon" title="{{trans('admin.list.go_to_page')}}"><i class="fa fa-arrow-right"></i></span>
      <input data-pagination-gotopage data-pagination-baseurl="{{\Request::url()}}" type="text"
        pattern="[0-9]"
        value = "{{app('request')->input('page') == NULL ? 1 : app('request')->input('page')}}"
        class="form-control small-input" title="{{trans('admin.list.go_to_page')}}">
      <span class="input-group-addon">/ {{$pagination ? ceil($pagination->getRows()->total()/(session('rows_per_page') ? session('rows_per_page') : \Config::get('cms.admin_per_page'))) : 0}}</span>
    </div>  
  </div>
  <div class="col-lg-2">
    <div class="input-group"><span class="input-group-addon" title="{{trans('admin.list.rows_on_page')}}"><i class="fa fa-th-list"></i></span>
      <input data-pagination-rowsperpage
      value="{{session('rows_per_page') ? session('rows_per_page') : \Config::get('cms.admin_per_page')}}"
      type="text" class="form-control small-input"
      pattern="[0-9]"
      title="{{trans('admin.list.rows_on_page')}}">
    </div>
  </div>
  <div class="col-lg-2">

  </div>
  <div class="col-lg-6">
    <div class="pull-right">
      @if($pagination && $pagination->getRows()->count()>0)
        {{$pagination->getRows()->links()}}
      @endif
    </div>
  </div>

</div>

<div class="clearfix"></div>
