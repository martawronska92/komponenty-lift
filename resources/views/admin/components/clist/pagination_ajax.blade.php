<div class="row">
  <div class="col-lg-12 text-center">      
    <button type="button" data-list-loadnextpage class="btn btn-primary"
      data-loading-text = "<i class='fa fa-spinner fa-spin'></i> {{trans('admin.list.button_loader')}}"
      data-list-url="{{$pagination->getUrl()}}" data-list-currentpage="1">{{trans('admin.list.load_next_page')}}</button>
  </div>
</div>
