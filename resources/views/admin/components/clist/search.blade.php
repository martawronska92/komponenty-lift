<div class="searchBlock">
  <div class="searchBlock__select">
    <select name="search_column" class="js-select2">
      @foreach($search as $row)
        <option value="{{$row->getField()}}"
          @if(app('request')->input('search_column') == $row->getField())
          selected
          @endif
        >{{$row->getLabel()}}</option>
      @endforeach
    </select>
  </div>


  <div class="searchBlock__input">
    <input type="search" name="search_value" class="inputBlock__field"
      placeholder="{{trans('admin.list.search_placeholder')}}"
      autocomplete="off"
      value="{{app('request')->input('search_value')}}"/>
  </div>


  <div class="searchBlock__button">
    <button type="button" data-pagination-search
      data-pagination-baseurl="{{\Request::url()}}"
      class="button button--small button--blue button--withIcon button--iconEye">{{trans('admin.list.search')}}</button>
  </div>
</div>



