
@if($header!="" || count($multiActions)>0 || count($buttons)>0 || count($search)>0)
<div class="titleButtonsFlex">
  <h1 class="mainTitle">{{$header}}</h1>

  <div class="listButtons">
    {{-- przyciski akcji nad listą --}}
    @if(count($buttons)>0)
      @foreach($buttons as $button)
        {!!$button->render()!!}
      @endforeach
    @endif
  </div>
</div>

  {{-- akcje dla kilku wierszy --}}
    @if(count($multiActions)>0)
      @include('admin.components.clist.multiactions',['multiActions'=>$multiActions])
    @endif


  {{-- wyszukiwarka --}}
    @if(count($search)>0)
      @include('admin.components.clist.search',['search'=>$search])
    @endif


@endif


@if(isset($MessageBoxList))
  @include('admin.components.messagebox',['messagebox'=>$MessageBoxList])
@endif

@if(count($rows) == 0)
  <div class="resultsChanges resultsChanges--info">{{trans('admin.list.empty')}}</div>
@else

  @if($form)
    <form action="" method="post" data-formvalidation>
  @endif

  <div class="langsTable">
  <table>
    <thead>
      <tr>
        @if(count($multiActions)>0)
            <th style="width:85px">
              <div class="inputBlock__checkbox inputBlock__checkbox--noMargin">
                <input type="checkbox" name="all" id="all" data-list-checkboxall>
                <label class="inputBlock__checkboxText" for="all"><i></i>
                </label>
              </div>
        @endif

        {{-- headery kolumn --}}
        @foreach($columns as $column)
          <th @if($column->getOrder()) data-list-ordercolumnfield="{{$column->getField()}}" data-list-ordercolumn="@if(\Request::has('order_field') && \Request::has('order') && \Request::get('order_field') == $column->getField()){{\Request::get('order')}} @endif" @endif><span>{{trans($column->getLabel())}}</span>
            @if($column->getOrder()) <i class="fa @if(\Request::has('order_field') && \Request::has('order') && \Request::get('order_field') == $column->getField()) fa-sort-{{\Request::get('order')}} @else fa-sort @endif"></i> @endif
            </th>
        @endforeach
        <th></th>
      </tr>
    </thead>

    <tbody @if($sortable) data-list-sortable="{{$sortable}}" @endif data-list-body>
      {{-- rows --}}
      @foreach($rows as $row)
        <tr>
          {{-- checkboxy dla multi akcji --}}
          @if(count($multiActions)>0)
            <td>

              <div class="inputBlock__checkbox inputBlock__checkbox--noMargin">
                <input type="checkbox" name="checkbox[{{$row->id}}]" id="checkbox[{{$row->id}}]" value="{{$row->id}}" data-list-checkbox>
                <label class="inputBlock__checkboxText" for="checkbox[{{$row->id}}]"><i></i>
                </label>
              </div>

              </td>
          @endif

          {{-- wartości pól --}}
          @foreach($columns as $key => $column)
            <td @if($key==0) data-id="{{$row->id}}"  @endif>
              {!!$column->render($row)!!}
            </td>
          @endforeach

          {{-- akcje na wierszu --}}
          @if(count($actions)>0)
            <td>
              @foreach($actions as $action)
                {!!$action->render($row)!!}
              @endforeach
            </td>
          @endif
        </tr>
        @if(array_key_exists($row->id,$subrows))
          <tr style="display:none">
            <td colspan="<?php echo (int)(count($multiActions)>0) + count($columns) + (int)(count($actions)>0); ?>">
              <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-11">
                  {!!$subrows[$row->id]!!}
                </div>
              </div>
            </td>
          </tr>
        @endif
      @endforeach
    </tbody>

    @if(count($sumColumns)>0)
      <tfoot>
        <tr>
          @foreach($columns as $key => $column)
            @if(in_array($column->getField(),$sumColumns))
              <td>
                <?php
                  $sum = 0;
                  $field = $column->getField();
                  foreach($rows as $row){
                    $sum+= $row->$field;
                  }
                  echo "<b>".trans('admin.list.sum')."</b>: <span class='countField'>".$sum."</span>"; ?>
              </td>
            @else
              <td></td>
            @endif
          @endforeach
          @if(count($actions)>0)
            <td></td>
          @endif
        </tr>
      </tfoot>
    @endif

  </table>
  </div>

  @if($form)
    @include('admin.partial.form.save',['cancel_url'=>"/".\Request::path()])
    </form>
  @endif

  <input type="hidden" data-list-globalorder value="@if(\Request::has('order')){{\Request::get('order')}}@endif">
  <input type="hidden" data-list-globalorderfield value="@if(\Request::has('order_field')){{\Request::get('order_field')}}@endif">
@endif

{{-- paginacja --}}
  @if($pagination)
    @if($pagination->isAjax())
      @if($pagination->getRows()->hasMorePages())
        @include('admin.components.clist.pagination_ajax',['pagination'=>$pagination])
      @endif
    @else
      @include('admin.components.clist.pagination',['pagination'=>$pagination])
    @endif
  @endif
