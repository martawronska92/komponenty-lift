&nbsp;<a href="{{$url!="" ? rtrim($url,'/') : 'javascript:void(0)'}}" class="{{$class}}"
  @foreach($attributes as $name=>$value)
    {{$name}}="{{$value}}"
  @endforeach
>@if($faclass!="")<i class="fa {{$faclass}}" aria-hidden="true"></i> @endif{{trans($label)}}</a>
