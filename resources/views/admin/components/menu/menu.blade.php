<ul>
  @foreach($menu as $item)
    <li>		
      <a class="@if(isset($activeMenu) && is_array($activeMenu) && in_array($item->getAlias(),$activeMenu)) active @endif"
        href="{{$item->getUrl()}}">{{$item->getName()}}</a>
        @if(count($item->getElements()))
          <ul>
            @foreach($item->getElements() as $sub)
              <li><a class="@if(isset($activeMenu) && is_array($activeMenu) && in_array($sub->getAlias(),$activeMenu)) active @endif"
                  href="{{$sub->getUrl()}}">{{$sub->getName()}}</a>
                  @if(count($sub->getElements()))
                    <ul>
                      @foreach($sub->getElements() as $subb)
                        <li><a class="@if(isset($activeMenu) && is_array($activeMenu) && in_array($subb->getAlias(),$activeMenu)) active @endif"
                            href="{{$subb->getUrl()}}">{{$subb->getName()}}</a>
                        </li>
                      @endforeach
                    </ul>
                  @endif
              </li>
            @endforeach
          </ul>
        @endif
    </li>
  @endforeach
</ul>
