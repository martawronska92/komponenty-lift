@extends('layouts.email')

@section('content')

                    <!-- Email Body -->
                    <tr>
                        <td class="email-body" width="100%">
                            <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="email-body_cell">
                                        <!-- Greeting -->
                                        <h1>{{$greeting}}</h1>

                                        <!-- Intro -->
                                        @foreach($introLines as $line)
                                          <p>{{$line}}</p>
                                        @endforeach



                                        <!-- Action Button -->
                                        @if (isset($actionText))
                                            <table class="body_action" align="center" width="100%" cellpadding="0" cellspacing="0">
                                              <tr>
                                                  <td align="center">
                                                    <p>{{$actionText}}</p>
                                                  </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <a href="{{ $actionUrl }}"
                                                            target="_blank">
                                                            {{ $actionUrl }}
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        @endif

                                        <!-- Outro -->
                                        @foreach($outroLines as $line)
                                          <p>{{$line}}</p>
                                        @endforeach

                                        <!-- Salutation -->
                                        <p>
                                            {{trans('email.footer')}},<br>{{ config('app.name') }}
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
@endsection
