@extends('layouts.admin')

@section('content')
    <div class="content">
      <?php $headers = $exception->getHeaders(); ?>
      @if($headers && array_key_exists('header',$headers))
        <div class="titleLine">
          <h1 class="mainTitle">{{$headers['header']}}</h1>
        </div>
      @endif
      <div class="resultsChanges resultsChanges--error" role="alert">{{$exception->getMessage()!="" ? $exception->getMessage() : trans('error.403')}}</div>
    </div>

@endsection
