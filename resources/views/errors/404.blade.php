@extends('layouts.'.(\Request::segment(1) == 'panel' ? 'admin' : 'app'))

@section('content')

  @if(\Request::segment(1)!='panel')
    <div class="innerPageHeader">
        <div class="innerPageHeader__inner">
            <div class="innerLogo"><a href="/"><img src="/images/logo.png"></a></div>
            <div class="innerRight">
                <div style="background-image:url(/images/kontakt.jpg)" class="innerRight__bg"></div>
            </div>
            <div class="blackTitle js-animation">404
                <div class="transparentTitle js-animation">404</div>
            </div>

        </div>
    </div>

    <div class="contentBlock">
      <div class="content">
        <div class="titleLine">
          <h1 class="mainTitle">{{trans('error.404')}}</h1>
        </div>
        <div class="resultsChanges resultsChanges--error" role="alert">{{trans('error.404')}}</div>
      </div>
    </div>
  @else
    <div class="contentBlock">
      <div class="content">
        <div class="resultsChanges resultsChanges--error" role="alert">{{trans('error.404')}}</div>
      </div>
    </div>
  @endif
@endsection
