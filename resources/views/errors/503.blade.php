@extends('layouts.app')

@section('content')
<div class="content">
  <div class="titleLine">
    <h1 class="mainTitle">{{trans('error.503')}}</h1>
  </div>
  <div class="resultsChanges resultsChanges--error" role="alert">{{trans('error.503')}}</div>
</div>
@endsection
