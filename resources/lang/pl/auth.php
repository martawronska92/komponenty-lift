<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Błędne dane logowania lub konto jest nieaktywne.',
    'throttle' => 'Za dużo nieudanych prób logowania. Proszę spróbować za :seconds sekund.',
    'password_confirmation' => 'Hasła muszą się zgadzać',

    'login_header' => 'Logowanie do panelu',
    'e-mail' => 'E-mail',
    'login' => 'Login',
    'password' => 'Hasło',
    'remember' => 'Zapamiętaj mnie',
    'login_button' => 'Zaloguj',
    'forgot' => 'Zapomniałeś hasła?',
    'logout_button' => 'Wyloguj',
    'change_password' => 'Zmień hasło',
    'password_confirm' => 'Potwierdź hasło',

    'reset_header' => 'Zresetuj hasło',
    'reset_button' => 'Wyślij',
    'reset_password' => 'Zresetuj hasło',
];
