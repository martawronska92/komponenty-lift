<?php

return [
  'yes' => 'tak',
  'no' => 'nie',
  'loader' => 'Trwa wykonywanie operacji. Proszę czekać ... ',
  'delete_question' => 'Czy chcesz usunąć ',
  'fill_valid' => "Wypełnij prawidłowo formularz",
  'operation_failure' => 'Wystąpił błąd podczas wykonywania operacji',
  'logged_out' => 'Nastąpiło wylogowanie z systemu',
  'date' => [
    'january' => 'Styczeń',
    'february' => 'Luty',
    'march' => 'Marzec',
    'april' => 'Kwiecień',
    'may' => 'Maj',
    'june' => 'Czerwiec',
    'july' => 'Lipiec',
    'august' => 'Sierpień',
    'september' => 'Wrzesień',
    'october' => 'Październik',
    'november' => 'Listopad',
    'december' => 'Grudzień',
    'jan' => 'Sty',
    'feb' => 'Lut',
    'mar' => 'Mar',
    'apr' => 'Kwi',
    'may' => 'Maj',
    'jun' => 'Cze',
    'jul' => 'Lip',
    'aug' => 'Sie',
    'sep' => 'Wrz',
    'oct' => 'Paź',
    'nov' => 'Lis',
    'dec' => 'Gru',
    'sunday' => 'Niedziela',
    'monday' => 'Poniedziałek',
    'tuesday' => 'Wtorek',
    'wednesday' => 'Środa',
    'thursday' => 'Czwartek',
    'friday' => 'Piątek',
    'saturday' => 'Sobota',
    'sun' => 'Niedz',
    'mon' => 'Pon',
    'tue' => 'Wt',
    'wed' => 'Śr',
    'thu' => 'Czw',
    'fri' => 'Pt',
    'sat' => 'Sob',
    'su' => 'Nd',
    'mo' => 'Pn',
    'tu' => 'Wt',
    'we' => 'Śr',
    'th' => 'Cz',
    'fr' => 'Pt',
    'sa' => 'Sb',
    'year' => 'Rok',
    'month' => 'Miesiąc',
    'day' => 'Dzień',
    'hour' => 'Godzina',
    'minute' => 'Minuta',
    'second' => 'Sekunda',
    'millisecond' => 'Milisekunda',
    'Microsecond' => 'Mikrosekunda',
    'Timezone' => 'Strefa czasowa'
  ],
  'validation' => [
    'url' => 'Adres url może zawierać tylko litery,cyfry i znak `-`',
    'youtube_link' => 'Podany adres nie jest prawidłowym adresem pliku wideo Youtube',
    'date_greater_than' => 'Data zakończenia musi być większa lub równa od daty rozpoczęcia',
    'password' => 'Hasło musi się składać co najmniej z 8 znaków (dużych i małych liter oraz cyfr)',
    'invalid' => 'Formularz nie został prawidłowo wypełniony',
    'newsletter_recipient_exists' => 'Odbiorca o podanym adresie e-mail istnieje już w systemie',
    'email' => 'Proszę o podanie prawidłowego adresu email',
    'password_not_same' => 'Podane hasła muszą się zgadzać',
    'new_password_must_be_different' => 'Nowe hasło musi się różnić od starego',
    'invalid_nickname' => 'Nazwa użytkownika powinna składać się jedynie z liter,cyfr i znaku _',
    'unique_nickname' => 'Użytkownik o podanej nazwie znajduje się już w systemie',
    'unique_email' => 'Użytkownik o podanym adresie znajduje się już w systemie',
    'url_http' => 'Adres powinien zaczynać się od http(s)://www',
    'invalid_date' => 'Proszę wpisać prawidłową datę',
    'time' => 'Proszę wpisać prawidłową godzinę',
    'empty_file' => 'Brak pliku',
    'url' => 'Adres url jest nieprawidłowy'
  ],
  'galleryvideo' => [
    'add_success' => 'Film został dodany',
    'add_failure' => 'Nie udało się dodać filmu',
    'remove_success' => 'Film został usnięty',
    'remove_failure' => 'Nie udało się usunąć filmu'
  ],
  'menu' => [
    'choose_node' => 'Wybierz element menu, aby dokończyć akcję',
    'invalid_key' => 'Wybrany element jest nieprawidłowy',
    'max_level' => 'Nie możesz dodać więcej subelementów na tym poziomie',
    'item_delete_success' => 'Element został usunięty',
    'item_delete_failure' => 'Element nie został usunięty',
    'item_delete_noexists' => 'Element nie istnieje',
    'delete_question' => 'Usunięcie elementu może spowodować usunięcie wszystkich jego elementów podrzędnych. Czy na pewno chcesz usunąć element: ',
  ],
  'list' => [
    'choose_multi_action' => 'Wybierz akcję do wykonania',
    'no_rows_checked' => 'Brak zaznaczonych wierszy'
  ],
  'datatables' => [
    'search' => 'Wyszukaj',
    "lengthMenu" => "Pokaż _MENU_ wierszy na stronę",
    "zeroRecords" => "Nie znaleziono rekordów",
    "info" => "Wyświetlanie storny _PAGE_ z _PAGES_",
    "infoEmpty" => "Brak dostępnych rekordów",
    "infoFiltered" => "(przefiltrowano _MAX_ rekordów)",
    "paginate" => [
      "first" =>    "«",
      "previous" => "‹",
      "next"  =>   "›",
      "last"  =>   "»"
    ]
  ],
  'tab' => [
    'in_order' => 'Wypełnij po kolei wszystkie części formularza'
  ],
  'newsletter' => [
    'invalid_email' => 'Adres e-mail jest nieprawidłowy',
    'preview_send_success' => 'Wiadomość została wysłana',
    'preview_send_failure' => 'Nie udało się wysłać wiadomości',
    'choose_recipient' => 'Wybierz odbiorców wiadomości'
  ],
  'galleryphoto' => [
    'nothing_selected' => 'Zaznacz wybrane zdjęcia',
    'set_thumbnail' => 'Brak ustawionej miniaturki',
    'delete_question' => 'Czy chcesz usunąć wybrane zdjęcia ',    
    'choose_files' => 'Wybierz pliki'
  ],
  'galleryvideo' => [
    'nothing_selected' => 'Zaznacz wybrane filmy',
    'delete_question' => 'Czy chcesz usunąć wybrane filmy ',
  ],
  'category' => [
    'header' => 'Kategoria'
  ],
  'page' => [
    'module_on_page' => 'Wybrany moduł został już dodany',
    'category_on_page' => 'Wybrana kategoria została już dodana do strony',
    'choose_entity' => 'Wybierz obiekt z modułu do dodania',
    'choose_category' => 'Wybierz kategorię do dodania',
    'choose_module' => 'Wybierz moduł do dodania',
    'show_subpages' => 'Pokaż podstrony',
    'hide_subpages' => 'Ukryj podstrony'
  ],
  'editor' => [
    'h1' => 'Nagłówek h1',
    'h2' => 'Nagłówek h2',
    'h3' => 'Nagłówek h3',
    'h4' => 'Nagłówek h4',
    'h5' => 'Nagłówek h5',
    'h6' => 'Nagłówek h6',
    'p' => 'Akapit',
    'pre' => 'Tekst preformatowany',
    'code' => 'Fragment kodu',
    'insert_image' => 'Wstaw obrazek',
    'yt_link' => 'Link do video Youtube',
    'insert_video' => 'Wstaw film',
    'wrong_yt_url' => 'Adres filmu jest nieprawidłowy'
  ],
  'download' => [
    'delete_question' => 'Czy chcesz usunąć zaznaczone pliki ',
    'nothing_selected' => 'Nie zaznaczono żadnych plików',
    'add_file' => 'dodaj pliki',
    'change_file' => 'zmień plik'
  ],
  'tag' => [
    'choose' => 'wybierz tagi',
    'no_results' => 'Brak pasujących wyników'
  ],
  'module' => [
    'delete_success' => 'Moduł został usunięty',
    'delete_failure' => 'Moduł nie został usunięty',
    'add_success' => 'Moduł został dodany',
    'added_list_empty' => 'Brak dodanych modułów'
  ],
  'openinghours' => [
    'open' => 'Otwarte',
    'close' => 'Zamknięte'
  ],
  'analytics' => [
    'pageviews' => 'Wyświetlenia',
    'visits' => 'Wizyty'
  ],
  'uploadify' => [
    'completed' => ' - Zakończono',
    'cancelled' => ' - Anulowano',
    'max_queue_1' => 'W kolejce znajduje się maksymalna liczba plików (',
    'max_queue_2' => '). Proszę wybrać mniej plików.',
    'upload_limit' => 'Limit uploadu został wykorzystany',
    'file_exists_1' => 'Plik ',
    'file_exists_2' => ' istnieje już w folderze uploadu. Czy chciałbyś go zastąpić ?',
    'error404' => 'Błąd 404',
    'error403' => 'Błąd 403',
    'forbiden_file_type' => 'Niewłaściwe rozszerzenie pliku',
    'file_to_large' => 'Plik jest za duży',
    'unknown_error' => 'Nieznany błąd'
  ],
  'user' => [
    'password' => 'Hasło'
  ]
];
