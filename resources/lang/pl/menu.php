<?php
  return [
    'page' => 'Podstrony',
    'post' => 'Wpisy',
    'category' => 'Kategorie wpisów',
    'module' => 'Moduły',
    'galleryphoto' => 'Galeria zdjęć',
    'galleryvideo' => 'Galeria wideo',
    'download' => 'Download',
    'newsletter' => 'Newsletter',
    'popup' => 'Popup',
    'slider' => 'Slider',
    'language' => 'Języki',
    'offer' => 'Oferta',
    'menu' => 'Menu',
    'user' => 'Użytkownicy',
    'settings' => 'Ustawienia',
    'tag' => 'Tagi',
    'seo' => 'Linki wewnętrzne',
    'mainpage' => 'Strona główna'
  ];
