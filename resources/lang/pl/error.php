<?php

/**
 * messages for errors
 */

 return [
   '403' => 'Brak uprawnień do danego modułu lub strony',
   '404' => 'Strona nie została znaleziona',
   '500' => 'Wystąpił błąd na stronie',
   '503' => 'Wystąpił błąd na stronie'
 ];
