<?php

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'CMS Instalator',
    'next' => 'Następny krok',
    'finish' => 'Instaluj'


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title'   => 'Instalacja CMS',
        'message' => 'Witaj w kreatorze instalacji.',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'Wymagania systemowe ',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'Uprawnienia',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'Ustawienia środowiska',
        'save' => 'Zapisz .env',
        'success' => 'Plik .env został poprawnie zainstalowany.',
        'errors' => 'Nie można zapisać pliku .env, Proszę utworzyć go ręcznie.',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'Instalacja zakończona',
        'finished' => 'Aplikacja została poprawnie zainstalowana.',
        'exit' => 'Kliknij aby zakończyć',
    ],

    'install' => 'Instaluj'
];
