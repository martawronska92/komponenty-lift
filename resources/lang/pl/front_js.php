<?php

  return [
    'validation' => [
      'invalid' => 'Formularz nie został prawidłowo wypełniony',
      'phone' => 'Numer telefonu powinien składać się z 9 cyfr'
    ]
  ];
