<?php

  return [
    'header' => 'Witaj',
    'footer' => 'Pozdrawiamy',

    'reset_email_footer' => 'Jeśli nie prosiłeś o reset hasła, nie podejmuj żadnej akcji.',
    'reset_email_header' => 'Otrzymałeś/aś tą wiadomość, ponieważ otrzymaliśmy żądanie resetu hasła dla Twojego konta w systemie.',
    'reset_email_click' => 'Aby zresetować hasło kliknij poniższy link'
  ];
