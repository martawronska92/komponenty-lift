<?php
  return [
    'homepage' => 'Strona główna',
    'close' => 'Zamknij',
    'email' => 'Adres e-mail',
    'name' => 'Imię',
    'surname' => 'Nazwisko',
    'confirm' => 'Potwierdź',
    'back' => 'Powrót',
    'preview_info' => 'Pamiętaj, że jest to jedynie podgląd ',
    'preview_info_page' => ' danej strony',
    'preview_info_post' => ' danego wpisu',
    'wait_for_preview' => 'Trwa generowanie przeglądu. Proszę czekać ...',
    'back_to_panel' => 'Powrót do panelu',
    'goto_page' => 'Przejdź do strony',
    'cat' => 'Kategorie',
    'newsletter' => [
      'recipient_exists' => 'Wybrany adres znajduje się już w systemie',
      'invalid_email' => 'Adres e-mail jest nieprawidłowy',
      'edit_success' => 'Adres został zapisany do newslettera',
      'edit_failure' => 'Nie udało się dodać adresu',
      'subscribe' => 'Zapisz',
      'subscribe_header' => 'Newsletter',
      'unsubscribe_header' => 'Rezygnacja z newslettera',
      'recipient_no_exists' => 'Wybrany adres e-mail nie istnieje w systemie',
      'unsubscribe_success' => 'Adres e-mail został usunięty z newslettera',
      'unsubscribe_failure' => 'Nie udało się usunąć adresu e-mail',
      'unsubscribe_send' => 'Na podany adres został wysłany link do usunięcia adresu e-mail z newslettera',
      'unsubscribe_email'=> 'Aby usunąć adres e-mail z bazy newslettera kliknij poniższy link ',
      'unsubscribe_subject' => 'Rezygnacja z newslettera'
    ],
    'cookies' => [
      'text' => 'Strona wykorzystuje pliki cookies. Służą one jedynie do poprawy jej funkcjonalności.'
    ],
    'list' => [
      'empty' => 'Lista jest pusta'
    ],
    'search' => [
      'no_results' => 'Brak pasujących wyników do podanego zapytania',
      'total_results' => 'Ilość znalezionych wyników',
      'no_query' => 'Brak frazy do wyszukiwania'
    ],
    'category' => [
      'show_next' => 'pokaż następne'
    ],
    'post' => [
      'tags' => 'Tagi'
    ],
    'tag' => [
      'post_header' => 'Lista wpisów dla tagu: ',
      'no_results' => 'Brak wpisów dla danego tagu',
      'breadcrumbs' => 'Tagi'
    ],
    'contact' => [
      'route' => 'kontakt',
      'name' => 'Imię i nazwisko',
      'firstname' => 'Imię',
      'lastname' => 'Nazwisko',
      'email' => 'E-mail',
      'phone' => 'Telefon',
      'message' => 'Wiadomość',
      'message_send_success' => 'Wiadomość została wysłana',
      'message_send_failure' => 'Wiadomość nie została wysłana',
      'send' => 'Wyślij',
      'subject' => 'Wiadomość z formularza kontaktowego',
      'mail_content' => 'Otrzymałeś/aś nową wiadomość z formularza kontaktowego:'
    ],
	  'downloads' => [
	  	'download' => 'Pobierz'
	  ],
	  'breadcrumbs' => [
	  	'main' => 'Aktualnie znajdujesz się na:'
	  ]
  ];
