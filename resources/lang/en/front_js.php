<?php

  return [
    'validation' => [
      'invalid' => 'The form was not properly filled in',
      'phone' => 'The phone number should consist of 9 digits'
    ]
  ];
