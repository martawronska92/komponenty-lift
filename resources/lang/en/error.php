<?php

/**
 * messages for errors
 */

return [
  '403' => 'No permissions for this module or page',
  '404' => 'Page was not found',
  '500' => 'An error occurred on the page',
  '503' => 'An error occurred on the page'
];
