<?php

  return [
    'header' => 'Hello',
    'footer' => 'Regards',

    'reset_email_footer' => 'If you did not request a password reset, no further action is required. '
    'reset_email_header' => 'You are receiving this email because we received a password reset request for your account.',
    'reset_email_click' => 'To change the password click link bellow'
  ];
