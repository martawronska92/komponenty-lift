<?php
  return [
    'page' => 'Pages',
    'post' => 'Posts',
    'category' => 'Posts category',
    'module' => 'Modules',
    'galleryphoto' => 'Photo gallery',
    'galleryvideo' => 'Video gallery',
    'download' => 'Download',
    'newsletter' => 'Newsletter',
    'popup' => 'Popup',
    'slider' => 'Slider',
    'language' => 'Languages',
    'offer' => 'Offer',
    'menu' => 'Menu',
    'user' => 'Users',
    'settings' => 'Settings',
    'tag' => 'Tags',
    'seo' => 'Internal links',
    'mainpage' => 'Homepage'
  ];
