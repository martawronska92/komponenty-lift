<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'password_confirmation' => 'Passwords must match',

    'login_header' => 'Panel login',
    'e-mail' => 'E-mail address',
    'login' => 'Login',
    'password' => 'Password',
    'remember' => 'Remember me',
    'login_button' => 'Login',
    'forgot' => 'Forgot password?',
    'logout_button' => 'Logout',
    'change_password' => 'Change password',
    'password_confirm' => 'Confirm password',

    'reset_header' => 'Reset password',
    'reset_button' => 'Send',
    'reset_password' => 'Reset password',
];
