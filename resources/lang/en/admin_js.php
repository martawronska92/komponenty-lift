<?php

return [
  'yes' => 'yes',
  'no' => 'no',
  'loader' => 'Operation is in progress. Please wait ...',
  'delete_question' => 'Do you want to delete ',
  'fill_valid' => "Fill out the form correctly",
  'operation_failure' => 'An error occurred while performing the operation',
  'logged_out' => 'Logged out of the system',
  'date' => [
    'january' => 'January',
    'february' => 'February',
    'march' => 'March',
    'april' => 'April',
    'may' => 'May',
    'june' => 'June',
    'july' => 'July',
    'august' => 'August',
    'september' => 'September',
    'october' => 'October',
    'november' => 'November',
    'december' => 'December',
    'jan' => 'Jan',
    'feb' => 'Feb',
    'mar' => 'Mar',
    'apr' => 'Apr',
    'may' => 'May',
    'jun' => 'Jun',
    'jul' => 'Jul',
    'aug' => 'Aug',
    'sep' => 'Sep',
    'oct' => 'Oct',
    'nov' => 'Nov',
    'dec' => 'Dec',
    'sunday' => 'Sunday',
    'monday' => 'Monday',
    'tuesday' => 'Tuesday',
    'wednesday' => 'Wednesday',
    'thursday' => 'Thursday',
    'friday' => 'Friday',
    'saturday' => 'Saturday',
    'sun' => 'Sun',
    'mon' => 'Mon',
    'tue' => 'Tue',
    'wed' => 'Wed',
    'thu' => 'Thu',
    'fri' => 'Fri',
    'sat' => 'Sat',
    'su' => 'Su',
    'mo' => 'Mo',
    'tu' => 'Tu',
    'we' => 'We',
    'th' => 'Th',
    'fr' => 'Fr',
    'sa' => 'Sa',
    'year' => 'Year',
    'month' => 'Month',
    'day' => 'Day',
    'hour' => 'Hour',
    'minute' => 'Minute',
    'second' => 'Second',
    'millisecond' => 'Milisecond',
    'Microsecond' => 'Microsecond',
    'Timezone' => 'Timezone'
  ],
  'validation' => [
    'url' => 'Adres url może zawierać tylko litery,cyfry i znak `-`',
    'youtube_link' => 'Podany adres nie jest prawidłowym adresem pliku wideo Youtube',
    'date_greater_than' => 'Data zakończenia musi być większa lub równa od daty rozpoczęcia',
    'password' => 'Hasło musi się składać co najmniej z 8 znaków (dużych i małych liter oraz cyfr)',
    'invalid' => 'Formularz nie został prawidłowo wypełniony',
    'newsletter_recipient_exists' => 'Odbiorca o podanym adresie e-mail istnieje już w systemie',
    'email' => 'Proszę o podanie prawidłowego adresu email',
    'password_not_same' => 'Podane hasła muszą się zgadzać',
    'new_password_must_be_different' => 'Nowe hasło musi się różnić od starego',
    'invalid_nickname' => 'Nazwa użytkownika powinna składać się jedynie z liter,cyfr i znaku _',
    'unique_nickname' => 'Użytkownik o podanej nazwie znajduje się już w systemie',
    'unique_email' => 'Użytkownik o podanym adresie znajduje się już w systemie',
    'url_http' => 'Adres powinien zaczynać się od http(s)',
    'invalid_date' => 'Proszę wpisać prawidłową datę',
    'time' => 'Please enter a valid time',
    'empty_file' => 'No file',
    'url' => 'Adres url jest nieprawidłowy'
  ],
  'galleryvideo' => [
    'add_success' => 'Film został dodany',
    'add_failure' => 'Nie udało się dodać filmu',
    'remove_success' => 'Film został usnięty',
    'remove_failure' => 'Nie udało się usunąć filmu'
  ],
  'menu' => [
    'choose_node' => 'Wybierz element menu, aby dokończyć akcję',
    'invalid_key' => 'Wybrany element jest nieprawidłowy',
    'max_level' => 'Nie możesz dodać więcej subelementów na tym poziomie',
    'item_delete_success' => 'Element został usunięty',
    'item_delete_failure' => 'Element nie został usunięty',
    'item_delete_noexists' => 'Element nie istnieje',
    'delete_question' => 'Usunięcie elementu może spowodować usunięcie wszystkich jego elementów podrzędnych. Czy na pewno chcesz usunąć element: ',
  ],
  'list' => [
    'choose_multi_action' => 'Wybierz akcję do wykonania',
	'no_rows_checked' => 'Brak zaznaczonych wierszy'
  ],
  'datatables' => [
    "search" => "Search",
    "lengthMenu" => "Display _MENU_ records per page",
    "zeroRecords" => "Nothing found - sorry",
    "info" => "Showing page _PAGE_ of _PAGES_",
    "infoEmpty" => "No records available",
    "infoFiltered" => "(filtered from _MAX_ total records)",
    "paginate" => [
      "first" =>    "«",
      "previous" => "‹",
      "next"  =>   "›",
      "last"  =>   "»"
    ]
  ],
  'tab' => [
    'in_order' => 'Fill in all sections of the form'
  ],
  'newsletter' => [
    'invalid_email' => 'E-mail address is invalid',
    'preview_send_success' => 'Message was sent',
    'preview_send_failure' => 'Failed to send the message',
    'choose_recipient' => 'Select recipients'
  ],
  'galleryphoto' => [
    'nothing_selected' => 'Mark selected pictures',
    'set_thumbnail' => 'No thumbnail set',
    'delete_question' => 'Do you want to delete the selected photos ',
    'choose_files' => 'Choose files'
  ],
  'galleryvideo' => [
    'nothing_selected' => 'Mark selected videos',
    'delete_question' => 'Do you want to delete the selected videos ',
  ],
  'category' => [
    'header' => 'Category'
  ],
  'page' => [
    'module_on_page' => 'The selected module has already been added',
    'category_on_page' => 'The selected category has already been added to the page',
    'choose_entity' => 'Select the object from the module to be added',
    'choose_category' => 'Select a category to add',
    'choose_module' => 'Select a module to add',
	  'show_subpages' => 'Show supages',
    'hide_subpages' => 'Hide subpages'
  ],
  'editor' => [
    'h1' => 'Nagłówek h1',
    'h2' => 'Nagłówek h2',
    'h3' => 'Nagłówek h3',
    'h4' => 'Nagłówek h4',
    'h5' => 'Nagłówek h5',
    'h6' => 'Nagłówek h6',
    'p' => 'Paragraph',
    'pre' => 'Preformatted text',
    'code' => 'Fragment kodu',
    'insert_image' => 'Insert image',
    'yt_link' => 'Link do video Youtube',
    'insert_video' => 'Insert video',
    'wrong_yt_url' => 'Adres filmu jest nieprawidłowy'
  ],
  'download' => [
    'delete_question' => 'Czy chcesz usunąć zaznaczone pliki ',
    'nothing_selected' => 'Nie zaznaczono żadnych plików',
    'add_file' => 'add files',
    'change_file' => 'change file'
  ],
  'tag' => [
    'choose' => 'choose tags',
    'no_results' => 'Brak pasujących wyników'
  ],
  'module' => [
    'delete_success' => 'The module has been removed',
    'delete_failure' => 'The module has not been removed',
    'add_success' => 'The module has been added',
    'added_list_empty' => 'No modules added'
  ],
  'openinghours' => [
    'open' => 'open',
    'close' => 'closed'
  ],
  'analytics' => [
    'pageviews' => 'Views',
    'visits' => 'Visits'
  ],
  'uploadify' => [
    'completed' => ' - Completed',
    'cancelled' => ' - Canceled',
    'max_queue_1' => 'W kolejce znajduje się maksymalna liczba plików (',
    'max_queue_2' => '). Proszę wybrać mniej plików.',
    'upload_limit' => 'Limit uploadu został wykorzystany',
    'file_exists_1' => 'File ',
    'file_exists_2' => ' istnieje już w folderze uploadu. Czy chciałbyś go zastąpić ?',
    'error404' => 'Błąd 404',
    'error403' => 'Błąd 403',
    'forbiden_file_type' => 'Niewłaściwe rozszerzenie pliku',
    'file_to_large' => 'Plik jest za duży',
    'unknown_error' => 'Uknown error'
  ],
  'user' => [
    'password' => 'Password'
  ]
];
