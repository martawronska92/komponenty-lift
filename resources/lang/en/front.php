<?php
  return [
    'homepage' => 'Homepage',
    'close' => 'Close',
    'email' => 'Adres e-mail',
    'name' => 'Name',
    'surname' => 'Nazwisko',
    'confirm' => 'Confirm',
    'back' => 'Back',
    'preview_info' => 'Pami�taj, �e jest to jedynie podgl�d ',
    'preview_info_page' => ' danej strony',
    'preview_info_post' => ' danego wpisu',
    'wait_for_preview' => 'Trwa generowanie przegl�du. Prosz� czeka� ...',
    'back_to_panel' => 'Return to the panel',
    'goto_page' => 'Go to page',
    'newsletter' => [
      'recipient_exists' => 'Wybrany adres znajduje si� ju� w systemie',
      'invalid_email' => 'Adres e-mail jest nieprawid�owy',
      'edit_success' => 'Adres zosta� zapisany do newslettera',
      'edit_failure' => 'Nie uda�o si� doda� adresu',
      'subscribe' => 'Save',
      'subscribe_header' => 'Newsletter',
      'unsubscribe_header' => 'Rezygnacja z newslettera',
      'recipient_no_exists' => 'Wybrany adres e-mail nie istnieje w systemie',
      'unsubscribe_success' => 'Adres e-mail zosta� usuni�ty z newslettera',
      'unsubscribe_failure' => 'Nie uda�o si� usun�� adresu e-mail',
      'unsubscribe_send' => 'Na podany adres zosta� wys�any link do usuni�cia adresu e-mail z newslettera',
      'unsubscribe_email'=> 'Aby usun�� adres e-mail z bazy newslettera kliknij poni�szy link ',
      'unsubscribe_subject' => 'Rezygnacja z newslettera'
    ],
    'cookies' => [
      'text' => 'Strona wykorzystuje pliki cookies. S�u�� one jedynie do poprawy jej funkcjonalno�ci.'
    ],
    'list' => [
      'empty' => 'The list is empty'
    ],
    'search' => [
      'no_results' => 'Brak pasuj�cych wynik�w do podanego zapytania',
      'total_results' => 'Ilo�� znalezionych wynik�w',
      'no_query' => 'Brak frazy do wyszukiwania'
    ],
    'category' => [
      'show_next' => 'show next'
    ],
    'post' => [
      'tags' => 'Tags'
    ],
    'tag' => [
      'post_header' => 'Lista wpis�w dla tagu: ',
      'no_results' => 'Brak wpis�w dla danego tagu',
      'breadcrumbs' => 'Tags'
    ],
    'contact' => [
      'route' => 'contact',
      'name' => 'Imi� i nazwisko',
      'firstname' => 'Name',
      'lastname' => 'Surname',
      'email' => 'E-mail',
      'phone' => 'Phone',
      'message' => 'Message',
      'message_send_success' => 'Wiadomo�� zosta�a wys�ana',
      'message_send_failure' => 'Wiadomo�� nie zosta�a wys�ana',
      'send' => 'Send',
      'subject' => 'Wiadomo�� z formularza kontaktowego',
      'mail_content' => 'Otrzyma�e�/a� now� wiadomo�� z formularza kontaktowego:'
    ]
  ];
