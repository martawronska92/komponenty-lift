var popup = new function() {
    var me = this;

    this.bindEvents = function() {
        $("#popupForm input[name='visible']").bind("click", function() {
            if ($(this).is(":checked")) {
                $("#popupVisibleBox").addClass('hidden');
                $("#popupForm input[name='visible_from'],#popupForm input[name='visible_to']").removeClass('required date_pl date_greater_than');
            } else {
                $("#popupVisibleBox").removeClass('hidden');
                $("#popupForm input[name='visible_from'],#popupForm input[name='visible_to']").addClass('required date_pl');
                $("#popupForm input[name='visible_to']").addClass('date_greater_than');
            }
        });

        $('#popupForm [data-popup-collapse]').bind("click", function() {
            var collapseType = $(this).attr("data-popup-collapse");
            $(this).find("input[type='radio']").prop('checked', true);

            $("#accordion").find("label.error").remove();

            $("[data-popup-field='youtube']").removeClass("required error");
            $("input[name='thumbnail_original']").removeClass("required error");

            if(collapseType=="image"){
              $("input[name='thumbnail_original']").addClass("required");
              $("#linkTypePopupBox").fadeIn();
            }else{
              $("#linkTypePopupBox").fadeOut();
              $("input[name='link_type']:first").trigger("click");
              if(collapseType=="youtube"){
                $("[data-popup-field='youtube']").addClass("required");
              }
            }
        });

        $("#popupForm input[name='youtube_content']").bind('paste', function() {
            var el = $(this);
            setTimeout(function() {
                var url = el.val();
                var videoId = youtube.getIdFromUrl(url);
                if (videoId) {
                    youtube.getTitle(videoId, function(title) {
                        $("[data-popup-youtubebox]").find("p").html(title);
                        $("#popupForm input[name='youtube_title']").val(title);
                    });
                    var thumbnail = youtube.getThumbnail(videoId);
                    $("#popupForm input[name='youtube_thumbnail']").val(thumbnail);
                    $("[data-popup-youtubebox]").find("img").attr("src", thumbnail);
                    $("[data-popup-youtubebox]").removeClass("hidden");
                    $(this).val(youtube.getUrl(videoId));
                }
            }, 100);
        });

        $("#popupForm input[name='link_type']").bind("click",function(){
          var value = $(this).val();
          $("[data-popup-linktype]").addClass("hidden");
          $("[data-popup-linktype]").removeClass("required url_http url");
          $("[data-popup-linktype='"+value+"']").removeClass("hidden");
          $("[data-popup-linktype='"+value+"']").addClass("required");

          if(value=="link"){
            $("[data-popup-linktype='"+value+"']").addClass("required url_http url");
          }

          value=="" ? $("[data-popup-newwindow]").addClass("hidden") : $("[data-popup-newwindow]").removeClass("hidden");
        });

        $("#popupForm input[name='link_type']").bind("change", function(){
          $(this).closest("[data-popup-linkchoose]").find("label.error").remove();
        });

    };
};

$(document).ready(function() {
    popup.bindEvents();
});
