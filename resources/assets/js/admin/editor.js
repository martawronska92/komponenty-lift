var editor = new function() {
    var me = this;

    this.bindEvents = function() {
        tinymce.PluginManager.add('stylebuttons', function(editor, url) {
            ['pre', 'p', 'code', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'].forEach(function(name) {
                editor.addButton("style-" + name, {
                    tooltip: Laravel.lang.editor[name],
                    text: name.toUpperCase(),
                    onClick: function() {
                        editor.execCommand('mceToggleFormat', false, name);
                    },
                    onPostRender: function() {
                        var self = this,
                            setup = function() {
                                editor.formatter.formatChanged(name, function(state) {
                                    self.active(state);
                                });
                            };
                        editor.formatter ? setup() : editor.on('init', setup);
                    }
                })
            });
        });

        tinymce.init({
            selector: '[data-wyswigeditor]',
            height: 300,
            menubar: false,
            statusbar: false,
            language: currentLang,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime paste code',
                'stylebuttons paste ibif_image ibif_video imagetools table'
            ],
            toolbar: 'undo redo | bold italic underline strikethrough | style-h2 style-h3 style-h4 blockquote | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | table | link ibif_image ibif_video code', // | paste removeformat',
            advlist_bullet_styles: "", //"disc", //"default,circle,disc,square"
            advlist_number_styles: "", //"default", //"default,lower-alpha,lower-greek,lower-roman,upper-alpha,upper-roman",

            paste_as_text: true,

            table_advtab: false,
            table_cell_advtab: false,
            table_row_advtab: false,

            // media_poster: false,
            // media_dimensions: false,
            // media_alt_source: false,
            // media_live_embeds: true,
            // media_advtab: false,
            // video_template_callback: function(data) {
            //     console.log(data);
            // },

            //image_advtab: true,
            // paste_data_images: true,
            // image_dimensions: false,
            // image_description: false,
            //image_caption: true,

            // paste_postprocess: function(plugin, args) {
            //     console.log(args.node);
            //     //args.node.setAttribute('id', '42');
            // },

            // file_picker_types: 'image',
            // images_upload_url: 'postAcceptor.php',
            // images_upload_base_path: '/some/basepath',
            // images_upload_credentials: true,
            // images_upload_handler: function(blobInfo, success, failure) {
            //     var xhr, formData;
            //
            //     xhr = new XMLHttpRequest();
            //     xhr.withCredentials = false;
            //     xhr.open('POST', 'postAcceptor.php');
            //
            //     xhr.onload = function() {
            //         var json;
            //
            //         if (xhr.status != 200) {
            //             failure('HTTP Error: ' + xhr.status);
            //             return;
            //         }
            //
            //         json = JSON.parse(xhr.responseText);
            //
            //         if (!json || typeof json.location != 'string') {
            //             failure('Invalid JSON: ' + xhr.responseText);
            //             return;
            //         }
            //
            //         success(json.location);
            //     };
            //
            //     formData = new FormData();
            //     formData.append('file', blobInfo.blob(), blobInfo.filename());
            //
            //     xhr.send(formData);
            // },
            // file_picker_callback: function(callback, value, meta) {
            // console.log(value);
            // console.log(meta);
            // callback();

            // if (meta.filetype == 'image') {
            //     $('#upload').trigger('click');
            //     $('#upload').on('change', function() {
            //         var file = this.files[0];
            //         var reader = new FileReader();
            //         reader.onload = function(e) {
            //             callback(e.target.result, {
            //                 alt: ''
            //             });
            //         };
            //         reader.readAsDataURL(file);
            //     });
            // } else {
            //     alert("sadasd");
            // }
            // },

            // file_picker_callback: function(callback, value, meta) {
            //     // Provide file and text for the link dialog
            //     if (meta.filetype == 'file') {
            //         callback('mypage.html', {
            //             text: 'My text'
            //         });
            //     }
            //
            //     // Provide image and alt text for the image dialog
            //     if (meta.filetype == 'image') {
            //         callback('myimage.jpg', {
            //             alt: 'My alt text'
            //         });
            //     }
            //
            //     // Provide alternative source and posted for the media dialog
            //     if (meta.filetype == 'media') {
            //         callback('movie.mp4', {
            //             source2: 'alt.ogg',
            //             poster: 'image.jpg'
            //         });
            //     }
            // },
            style_formats: [{
                    title: 'Image Left',
                    selector: 'img',
                    styles: {
                        'float': 'left',
                        'margin': '0 10px 0 10px'
                    }
                },
                {
                    title: 'Image Right',
                    selector: 'img',
                    styles: {
                        'float': 'right',
                        'margin': '0 0 10px 10px'
                    }
                }
            ],

            csrfToken: Laravel.csrfToken,

            setup: function(ed) {
                ed.on('init', function() {
                   ed.buttons.table.menu.splice(1,1);
                   ed.buttons.table.menu[3].menu.splice(0,1);
                   ed.buttons.table.menu[4].menu.splice(3,1);

                   delete ed.buttons.tableprops;

                   var editorLang = $("textarea[id='" + ed.id + "']").closest("[data-lang-input]").attr("data-lang-input");
                   if(editorLang!=window.Laravel.currentLang){
                     $("textarea[id='" + ed.id + "']").closest("[data-lang-input]").fadeOut();
                   }
                });
                ed.on('change', function(e) {
                    tinymce.triggerSave();
                    $("textarea[id='" + ed.id + "']").valid();
                });
            }
        });

        tinymce.init({
            selector: '[data-wyswigeditor-simple]',
            height: 200,
            menubar: false,
            statusbar: false,
            language: currentLang,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link',
            relative_urls: false,

            csrfToken: Laravel.csrfToken,

            setup: function(ed) {
                ed.on('init', function() {
                   var editorLang = $("textarea[id='" + ed.id + "']").closest("[data-lang-input]").attr("data-lang-input");
                   if(editorLang!=window.Laravel.currentLang){
                     $("textarea[id='" + ed.id + "']").closest("[data-lang-input]").fadeOut();
                   }
                });
            }
        });
    };
};

$(document).ready(function() {
    editor.bindEvents();
});
