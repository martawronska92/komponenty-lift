var tabs = new function() {
    this.bindEvents = function() {
        $("[data-tabs] a").bind("click", function(e) {
            e.preventDefault();

            var activeIndex = $("[data-tabs] a").index($("[data-tabs] li.active a"));
            var currentIndex = $("[data-tabs] a").index($(this));

            if (typeof $(this).attr('data-disabled') != "undefined" && currentIndex - activeIndex > 1) {
                message.show('danger', Laravel.lang.tab.in_order);
                return false;
            } else if (typeof $(this).attr("data-callback") == "undefined" || (typeof $(this).attr("data-callback") != "undefined" && eval($(this).attr("data-callback")))) {
                $(this).removeAttr('data-disabled');
                $(this).tab('show');
            } else {
                return false;
            }
        });
    };
};

var dialog = new function() {

    var me = this;

    this.confirmDialogHtml = "";
    this.confirmUrl = "";
    this.singleDialogHtml = "";

    this.init = function() {
        if ($("#confirmDialog").length)
            me.confirmDialogHtml = Handlebars.compile($("#confirmDialog").html());
        if ($("#singleDialog").length)
            me.singleDialogHtml = Handlebars.compile($("#singleDialog").html());
    };


    this.show = function(text,options,callback) {
        var html = me.singleDialogHtml({
            'text': text,
            'showButton': typeof options.showButton!== "undefined",
            'buttonText': typeof options.buttonText!== "undefined" ? options.buttonText : "Ok"
        });

        dialogButtonClicked = false;

        $(html).modal('show').on('shown.bs.modal', function() {
            $("[data-singledialogbox-button]").unbind("click");
            $("[data-singledialogbox-button]").bind("click", function() {
                dialogButtonClicked = true;
                if (typeof callback != "undefined") {
                    callback();
                }
                $('.modal').modal('hide');
            });
        }).on('hide.bs.modal', function(){
          if (typeof callback != "undefined" && !dialogButtonClicked) {
              callback();
          }
        });
    };

    this.confirm = function(question, url, callback) {
        me.confirmUrl = url;

        var html = me.confirmDialogHtml({
            'question': question
        });

        $(html).modal('show').on('shown.bs.modal', function() {
            $("[data-confirmationbox-yes]").unbind("click");
            $("[data-confirmationbox-yes]").bind("click", function() {
                if (typeof callback != "undefined") {
                    callback();
                } else if (typeof me.confirmUrl != "undefined" && me.confirmUrl != "")
                    top.location.href = me.confirmUrl;
                $('.modal').modal('hide');
            });
        });
    };

    this.events = function() {
        $("[data-confirmationbox]").bind("click", function() {
            if ($("#confirmDialog").length) {
                var question = Laravel.lang.delete_question + ' ' + $(this).attr('data-question');
                me.confirm(question, $(this).attr('href'));
                return false;
            }
        });
    };

    this.bindEvents = function() {
        me.init();
        me.events();
    };
};

var youtube = new function() {

    var me = this;

    this.getIdFromUrl = function(url) {
        if (url.indexOf("?") !== false) {
            url = url.split('?')[1];
        }

        if (typeof url != "undefined") {
            var params = url.split('&'),
                parsed = {};
            for (var i = 0; i < params.length; i++) {
                var p = params[i].split('=', 2);
                if (p.length == 1)
                    parsed[p[0]] = "";
                else
                    parsed[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
            }
            return parsed.v;
        } else {
            return false;
        }
    };

    this.getThumbnail = function(videoId) {
        /**
         * 0.jpg - fullsize
         * hqdefault - high quality
         * mqdefault - medium quality
         * sddefault - standard definition
         * maxresdefault - maximum resolution
         */
        return "https://img.youtube.com/vi/" + videoId + "/mqdefault.jpg";
    };

    this.getTitle = function(videoId, callback) {
        $.getJSON('https://noembed.com/embed', {
            format: 'json',
            url: 'https://www.youtube.com/watch?v=' + videoId
        }, function(data) {
            if (typeof callback != "undefined") {
                callback(data.title);
            }
        });
    };

    this.getUrl = function(videoId) {
        return "https://www.youtube.com/watch?v=" + videoId;
    };
};

var datetimepicker = new function() {

    var me = this;

    this.init = function(format) {
        $.intimidatetime.i18n[currentLang] = {
            format: format,
            units: {
                year: {
                    format: 'yyyy', // how should hour be forrmated in drowdown
                    label: Laravel.lang.date.year // year label
                },
                month: {
                    format: 'MMM', // how should hour be forrmated in drowdown
                    label: Laravel.lang.date.month, // month label
                    names: [Laravel.lang.date.january, Laravel.lang.date.february, Laravel.lang.date.march, Laravel.lang.date.April, Laravel.lang.date.may, Laravel.lang.date.june, Laravel.lang.date.july, Laravel.lang.date.august, Laravel.lang.date.september, Laravel.lang.date.october, Laravel.lang.date.november, Laravel.lang.date.december],
                    namesAbbr: [Laravel.lang.date.jan, Laravel.lang.date.feb, Laravel.lang.date.mar, Laravel.lang.date.apr, Laravel.lang.date.may, Laravel.lang.date.jun, Laravel.lang.date.jul, Laravel.lang.date.aug, Laravel.lang.date.sep, Laravel.lang.date.oct, Laravel.lang.date.nov, Laravel.lang.date.dec]
                },
                day: {
                    format: 'd', // how should hour be forrmated in drowdown
                    label: Laravel.lang.date.day, // day label
                    names: [Laravel.lang.date.sunday, Laravel.lang.date.monday, Laravel.lang.date.tuesday, Laravel.lang.date.wednesday, Laravel.lang.date.thursday, Laravel.lang.date.friday, Laravel.lang.date.saturday],
                    namesAbbr: [Laravel.lang.date.sun, Laravel.lang.date.mon, Laravel.lang.date.tue, Laravel.lang.date.wed, Laravel.lang.date.thu, Laravel.lang.date.fri, Laravel.lang.date.sat],
                    namesHead: [Laravel.lang.date.su, Laravel.lang.date.mo, Laravel.lang.date.tu, Laravel.lang.date.we, Laravel.lang.date.th, Laravel.lang.date.fr, Laravel.lang.date.sa]
                },
                hour: {
                    format: 'HH', // how should hour be forrmated in drowdown
                    label: Laravel.lang.date.hour, // hour label
                    am: ['AM', 'A'], // possible am names
                    pm: ['PM', 'P'] // possible pm names
                },
                minute: {
                    format: 'mm', // how should minute be formatted in dropdown
                    label: Laravel.lang.date.minute // minute label
                },
                second: {
                    format: 'ss', // how should second be formatted in dropdown
                    label: Laravel.lang.date.second // second label
                },
                millisecond: {
                    format: 'l', // how should millisecond be formatted in dropdown
                    label: Laravel.lang.date.milisecond // millisecond label
                },
                microsecond: {
                    format: 'c', // how should millisecond be formatted in dropdown
                    label: Laravel.lang.date.microsecond // microsecond label
                },
                timezone: {
                    format: 'z', // how should timezone be formatted in dropdown
                    label: Laravel.lang.date.timezone // minute label
                }
            },
            rtl: false
        };
        $.intimidatetime.setDefaults($.intimidatetime.i18n[currentLang]);
    };

    this.bind = function() {
        var dateTimeFormat, value;
        $("[data-datetimepicker]").each(function(i, el) {
            dateTimeFormat = $(el).attr('data-datetimepicker');
            value = $(this).val();


            if (i == 0)
                me.init(dateTimeFormat);

            var config = {
                groups: [{
                        name: 'date',
                        units: ['day', 'month', 'year']
                    },
                    {
                        name: 'time',
                        units: ['hour', 'minute', 'second', 'millisecond', 'microsecond', 'timezone']
                    }
                ],
                events: {
                    open: function(e, inst) {
                        if ($(e.target).val() == "")
                            inst.value(new Date());
                    }
                },
                value: value != "" ? value : new Date(dateTimeFormat),
                previewFormat: dateTimeFormat
            };

            $(el).intimidatetime(config);
        });
    };
};

var loader = new function() {

    this.show = function() {
        HoldOn.open({
            theme: "sk-cube-grid",
            message: Laravel.lang.loader,
            //backgroundColor: "#1847B1",
            textColor: "white"
        });
    };

    this.hide = function() {
        HoldOn.close();
    };
};

var switchbutton = new function() {

    var me = this;

    this.bindEvents = function() {
        $('.toggle').each(function(i, val) {

            var field = $(this).attr("data-item-field");
            var itemId = $(this).attr("data-item-id");
            var item = $(this).attr("data-item");

            $(val).find("input").on('change', function(e) {
                var active = $(this).is(':checked');
                $(".resultsChanges").fadeOut();
                if (typeof item != "undefined" && typeof itemId != "undefined" && typeof field != "undefined") {
                    var params = {
                        '_token': Laravel.csrfToken,
                        'field': field,
                        'item': item,
                        'itemId': itemId,
                        'value': active === true ? 1 : 0
                    };
                    if (typeof $(val).attr('data-callback') != 'undefined' && $(val).attr('data-callback') != '') {
                        var nameParts = $(val).attr('data-callback').split('.');
                        var callback = "";
                        $.each(nameParts, function(i, val) {
                            callback = i == 0 ? window[val] : callback[val];
                        });
                        callback($(this), params);
                    } else {
                        $.post('/panel/list/checkfield', params, function(response) {
                            //console.log('test');
                        });
                    }
                }
            });
        });
    };
};

/**
 * edit for gallery_video,gallery_photo,download
 */
var editable = function(type, saveCallback, cancelCallback) {
    var me = this;

    this.type = type;
    this.id;

    this.getData = function(id, callback) {
        loader.show();
        me.id = id;
        $.get("/panel/" + me.type + "/getdata/" + id, function(response) {
            $("[data-editable-blockcontainer]").each(function(i, val) {
                $(val).prepend("<div data-editblocker style='width:100%;height:100%;position:absolute;z-index:9999;'></div>");
                $(val).css({
                    'opacity': 0.2
                });
            });

            // $("[data-" + me.type + "-rightcontainer]").find("[data-id='" + id + "']").css({
            //     'opacity': 0.6
            // });

            if (response.code == 1) {
                $("[data-editable-replacecontainer]").html(response.view);
                if (typeof callback != "undefined")
                    callback();
            } else {
                me.editable.cancel();
                message.show("danger", response.msg);
            }
            loader.hide();
        }, "json");
    };
    this.setData = function() {
        storeCallback();
    };
    this.cancel = function() {
        loader.show();
        $.get("/panel/" + me.type + "/getdata", function(response) {
            if (response.code == 1) {
                $("[data-editable-replacecontainer]").html(response.view);
            }
            $("[data-editable-blockcontainer]").each(function(i, val) {
                $(val).find("[data-editblocker]").remove();
                $(val).css({
                    'opacity': 1
                });
            });

            cancelCallback();
            loader.hide();
        });
    };
    this.bindEvents = function() {
        $("[data-editable-replacecontainer]").on("click", "[data-editable-cancel]", function() {
            me.cancel();
        });
        $("[data-editable-replacecontainer]").on("click", "[data-editable-save]", function() {
            saveCallback();
            //me.cancel();
        });
    };
    me.bindEvents();
};

function sessionTimeout() {
    if ($("[data-activitytimeout-box]").length == 1) {
        var seconds;
        sessionTimeInterval = setInterval(function() {
            seconds = parseInt($("[data-activitytimeout-box]").attr("data-activitytimeout-box"));
            seconds--;
            $("[data-activitytimeout-box]").attr("data-activitytimeout-box", seconds);
            if (seconds == 0) {
                clearInterval(sessionTimeInterval);
                top.location.href = "/panel";
            } else {
                $("[data-activitytimeout-box]").find("[data-activitytimeout-minute]").html(Math.floor(seconds / 60));
                $("[data-activitytimeout-box]").find("[data-activitytimeout-second]").html((seconds % 60 < 10 ? "0" : "") + seconds % 60);
            }
        }, 1000);
    }
}

/**
 * logout event
 */

$(window).bind('storage',function(e){
  console.log(e.originalEvent);
  if(e.originalEvent.key == 'logged_in' && e.originalEvent.newValue == 0) {
      clearInterval(sessionTimeInterval);
      dialog.show(Laravel.lang.logged_out,{'showButton':1},function(){
        top.location.href = "/";
      });
  }
});

$(document).ready(function() {
    switchbutton.bindEvents();
    sessionTimeout();

    tabs.bindEvents();
    dialog.bindEvents();
    datetimepicker.bind();

  /**
   * scrollbar sidebar menu
   */
  $(".sidebar__menu").mCustomScrollbar();

});
