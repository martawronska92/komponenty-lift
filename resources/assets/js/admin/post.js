var post = new function() {

    var me = this;

    var newWidth = 0;
    var newHeight = 0;

    var th = $("[name='thumbnail_original']").val();

    this.uploadThumb = {
        "obj": "",
        "width": "",
        "height": "",
        "container": '[data-thumbnail-editor]',
        //"blob": "",
        "init": function() {
            me.uploadThumb.width = parseInt($("[data-thumbnail-container]").attr('data-thumbnail-width'));
            me.uploadThumb.height = parseInt($("[data-thumbnail-container]").attr('data-thumbnail-height'));
            //var columnWidth = $(".addCategory__right").width() - 50;
            var columnWidth = $("[data-thumbnail-container]").width() - 50;

            var s = me.uploadThumb.width / columnWidth;
            var ratio = me.uploadThumb.width / me.uploadThumb.height;

            newWidth = me.uploadThumb.width;
            newHeight = me.uploadThumb.height;

            if(s > 1) {
                var vw = columnWidth;
                var vh = vw / ratio;

                me.uploadThumb.width = vw;
                me.uploadThumb.height = vh;
            }

            me.uploadThumb.object = $(me.uploadThumb.container).croppie({
                enableExif: true,
                viewport: {
                    width: me.uploadThumb.width,
                    height: me.uploadThumb.height,
                    type: 'square'
                },
                boundary: {
                    width: me.uploadThumb.width + 50,
                    height: me.uploadThumb.height + 50
                },
                update: function(object) {
                    me.uploadThumb.obj = object;
                }
            });

            //me.uploadThumb.object.croppie('setZoom', 0);
            if (typeof $("[data-thumbnail-preview]").attr('src') != "undefined" && $("[data-thumbnail-preview]").attr('src') != "") {
                var params = {
                    'url': $("[data-thumbnail-preview]").attr('src').replace('thumbnail.', 'original.')
                };
                if ($("[data-thumbnail-options]").val() != "")
                    jQuery.extend(params, JSON.parse($("[data-thumbnail-options]").val()));


                me.uploadThumb.object.croppie('bind', params);
                $('[data-thumbnail-upload]').trigger('change');
            }

            $("img.cr-image").attr("alt","");
        },
        "readFile": function(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {

                    $("input[name='thumbnail_original']").val(e.target.result);
                    me.uploadThumb.object.croppie('bind', {
                        url: e.target.result
                    }).then(function() {
                        //console.log('jQuery bind complete');
                    });
                    // me.uploadThumb.object.croppie('result', 'blob').then(function(blob) {
                    //     // do something with cropped blob
                    //     me.uploadThumb.blob = blob;
                    // });
                    $("input[name='thumbnail_mime']").val(e.target.result.split(",")[0].split(":")[1].split(";")[0]);
                    $("[data-thumbnail-remove]").fadeIn();
                };

                reader.readAsDataURL(input.files[0]);
            } else {
                console.log("Sorry - you're browser doesn't support the FileReader API");
            }
        },
        "crop":function(el){
          if ($("input[name='thumbnail_original']").val() != "" || $("input[name='thumbnail']").val() != "") {

              me.uploadThumb.object.croppie('result', {
                  type: 'canvas',
                  size: { width: newWidth, height: newHeight },
                  circle: false
              }).then(function(resp) {
                  $("[data-thumbnail-preview]").attr('src', resp);
                  $('input[name="thumbnail_encoded"]').val(resp);
                  $("input[name='thumbnail_options']").val(JSON.stringify(me.uploadThumb.obj));
                  if (typeof el!="undefined" && typeof $(el).attr("data-thumbnail-callback") != "undefined") {
                      eval($(el).attr("data-thumbnail-callback"))
                  }
              });
          }
          return true;
        },
        "bind": function() {
            me.uploadThumb.init();
            $('[data-thumbnail-upload]').on('change', function() {
                $("[data-thumbnail-remove]").trigger("click");
                me.uploadThumb.readFile(this);
            });

            $("[data-thumbnail-remove]").bind("click", function() {
                $('input[name="thumbnail"]').val("");
                $('input[name="thumbnail_encoded"]').val("");
                $('input[name="thumbnail_original"]').val("");
                $('input[name="thumbnail_options"]').val("");
                $("[data-thumbnail-preview]").removeAttr("src");
                $(".cr-image").removeAttr('src');
                $(this).fadeOut();

                me.uploadThumb.object.croppie('destroy');
                me.uploadThumb.init();
            });
        }
    };

    this.generateFriendlyUrl = function(title, el) {
        if (title != "") {
            $.post('/panel/pagepost/generatelink', {
                'title': title,
                'lang': el.attr('data-linkgenerator-lang'), //id langa
                'type': el.attr('data-linkgenerator-type'), //typ encji: strona,wpis,kategoria
                'id': el.attr('data-linkgenerator-id'), //id encji
                'category_id': $("#category_id").val(),
                'parent_page_id': parseInt($("#parent_page_id").find("option:selected").val()),
                'module_id': parseInt($("input[name='module_id']").val()),
                'parent_category_id': parseInt($("#category_parent_id").find("option:selected").val()),
                '_token': Laravel.csrfToken
            }, function(response) {
                $(el.attr('data-linkgenerator-dest')).val(response);
            });
        }
    };

    this.tag = {
        "add": function(choose) {
            var valid = $("[data-tag-box] input").valid();
            if (valid) {
                loader.show();
                $.post('/panel/tag/store', {
                    'params': $("[data-tag-box] input").serialize(),
                    '_token': Laravel.csrfToken
                }, function(response) {
                    if (response.code) {
                        var selected = ' selected ';
                        for (var i = 0; i < response.tags.length; i++) {
                            if ($("[data-multiple-select]").find('option[value="' + response.tags[i].id + '"]').length == 0) {
                                if (i == 0)
                                    $("[data-multiple-select]").prepend("<option " + selected + " value='" + response.tags[i].id + "' >" + response.tags[i].text + "</option>");
                                else
                                    $("[data-multiple-select]").find("option").eq(i - 1).after("<option " + selected + " value='" + response.tags[i].id + "' >" + response.tags[i].text + "</option>");
                            }
                        }

                        message.show('success', response.msg);
                        $("[data-tag-hidebox]").trigger("click");
                    } else {
                        message.show('danger', response.msg);
                    }

                    loader.hide();
                }, "json").fail(function() {
                    loader.hide();
                    message.show('danger', Laravel.lang.operation_failure);
                });
            }
        },
        "bindEvents": function() {
            $("[data-tag-add]").bind("click", function() {
                me.tag.add();
            });
            $("[data-tag-showbox]").bind("click", function() {
                $("[data-tag-box]").find("input").addClass("required");
            });
            $("[data-tag-hidebox]").bind("click", function() {
                $("[data-tag-box]").find("input").removeClass("required").val("");
                $("[data-tag-box]").find("label.error").remove();
            });
        }
    };

    this.bindEvents = function() {
        /**
         * generowanie przyjaznych linków
         */

        /* pole title */
        $("[data-linkgenerator-trigger]").bind("blur", function() {
            var destElementId = "#" + $(this).attr("data-linkgenerator-trigger");
            var title = $(this).val();
            $(destElementId).val(title);
            me.generateFriendlyUrl(title, $(destElementId));
        });

        /* pole przyjaznego linku */
        $("[data-linkgenerator]").bind("blur", function() {
            //var title = $(this);//.closest(".row").find("[data-linkgenerator]");
            me.generateFriendlyUrl($(this).val(),$(this));
        });

        /**
         * upload thumbnail
         */
        if ($("[data-thumbnail-container]").length) {
            setTimeout(function() {
                post.uploadThumb.bind();
            }, 100);
        }

        /**
         * tag events
         */

        $("[data-multiple-select]").select2({
            placeholder: Laravel.lang.tag.choose,
            allowClear: true,
            language: {
                "noResults": function() {
                    return Laravel.lang.tag.no_results;
                }
            }
        });

        /**
         * rest events
         */
        $("#post_public").bind("click", function() {
            if ($(this).is(":checked")) {
                $("#publish_date").addClass("hidden");
                $("#publish_date").removeClass("required");
            } else {
                $("#publish_date").addClass("required");
                $("#publish_date").removeClass("hidden");
            }
        });

        $("form[data-form-pagepost]").bind("submit", function() {
            return false;
        });

        $("[data-post-preview]").bind("click", function() {
            page.generatePreview("post");
        });

        $("#category_parent_id").bind("change",function(){
          $("[data-linkgenerator-trigger]:visible").trigger("blur");
        });
    };
};

$(document).ready(function() {
    post.bindEvents();
    post.tag.bindEvents();

  $(".js-select2").select2({
    minimumResultsForSearch: -1,
    containerCssClass : "withoutFlags"
  });

  $(".js-select2-modules").select2({
    minimumResultsForSearch: -1,
    containerCssClass : "withoutFlags",
    dropdownCssClass: "select2-dropdownMaxHeight"
  });
});
