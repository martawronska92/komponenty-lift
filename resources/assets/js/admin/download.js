var download = new function() {

    var me = this;

    this.uploadify = function(nomultiple) {
        var upl = $('[data-download-upload]');
        if (upl.length == 1) {
            upl.uploadifive({
                'formData': {
                    '_token': Laravel.csrfToken
                },
                'auto': false,
                'buttonText': $("#file_id").length == 0 ? Laravel.lang.download.add_file : Laravel.lang.download.change_file,
                'uploadScript': '/panel/download/edit/' + $("#pagePostModuleId").val() + "/" + $("#id").val(),
                'multi': typeof nomultiple != "undefined" ? false : true,
                'removeCompleted': true,
                'simUploadLimit': 1,
                'onSelect': function(queue) {
                    upl.data('uploadifive').settings.formData = {
                        'form': $("#downloadForm").serialize(),
                        '_token': Laravel.csrfToken
                    };
                    upl.uploadifive('upload');
                },
                'onUploadComplete': function(file, data) {
                    //alert('The file ' + file.name + ' was successfully uploaded with a response of ' + response + ':' + data);
                    var info = $.parseJSON(data);
                    if (info.code == 0) {
                        message.show('danger', info.msg);
                    } else {
                        if (typeof $("#file_id").val() != "undefined") {
                            $("[data-download-list]").find("[data-id='" + info.id + "']").replaceWith(info.view);
                        } else {
                            $("[data-download-list]").prepend(info.view);
                        }
                        me.clearPreview();
                        $("[data-editblocker]").remove();
                        $("[data-editable-blockcontainer]").css({
                            'opacity': 1
                        });

                        $("[data-list-empty]").addClass("hidden");
                    }
                },
                'onQueueComplete': function(uploads) {
                    upl.uploadifive('clearQueue');
                    upl.uploadifive('destroy');
                    me.uploadify();
                }
            });
        }
    };

    this.removeFile = function(fileId, removeQuestion) {
        var question = typeof removeQuestion == "undefined" ? Laravel.lang.download.delete_question : removeQuestion;
        dialog.confirm(question, "", function() {
            loader.show();
            $.get("/panel/download/deletefiles/" + fileId, {},
                function(response) {
                    loader.hide();
                    if (parseInt(response.code) == 1) {
                        if ($.isArray(fileId)) {
                            for (var i = 0; i < fileId.length; i++)
                                $("[data-download-list]").find("[data-id='" + fileId[i] + "']").remove();
                        } else
                            $("[data-download-list]").find("[data-id='" + fileId + "']").remove();
                        message.show('success', response.msg);
                        if ($("[data-download-list] [data-id]").length == 0)
                            $("[data-list-empty]").removeClass("hidden");
                    } else {
                        message.show('danger', response.msg);
                    }
                });
        });
    };

    this.removeCheckedFile = function() {
        var ids = [];
        $("[data-download-list] input[data-list-itemcheckbox]:checked").each(function(i, val) {
            ids.push($(val).val());
        });
        if (ids.length == 0) {
            message.show('info', Laravel.lang.download.nothing_selected);
        } else {
            me.removeFile(ids);
        }
    };

    this.store = function() {
        loader.show();
        $.post('/panel/download/edit/' + $("#pagePostModuleId").val() + "/" + $("#id").val(), {
            '_token': window.Laravel.csrfToken,
            'form': $("#downloadForm").serialize()
        }, function(response) {
            loader.hide();
            if (parseInt(response.code) == 1) {
                $("[data-editblocker]").remove();
                $("[data-editable-blockcontainer]").css({
                    'opacity': 1
                });
                $("[data-download-list]").find("[data-id='" + response.id + "']").replaceWith(response.view);
                $("[data-editable-replacecontainer]").html(response.left_view);
                select2Init();
                me.uploadify();
                me.clearPreview();
            }
            select2Init();
        }).fail(function() {
            loader.hide();
            $("[data-editable-cancel]").trigger('click');
            message.show('danger', Laravel.lang.operation_failure);
        });;
    };

    this.clearPreview = function() {
        $("#downloadForm [data-lang-input]").val("");
        $("#downloadForm [name='active']").prop('checked', false);
        $("#downloadForm [data-download-filename]").html("");
        $("#file_id").remove();
        $("[data-download-formbuttons]").remove();
        switchbutton.bindEvents();
    };

    this.bindEvents = function() {
        me.uploadify();

        $("[data-download-deletemarked]").bind("click", function() {
            me.removeCheckedFile();
        });

        $("[data-download-list]").on("click", "[data-download-remove]", function() {
            me.removeFile($(this).attr('data-download-remove'), $(this).attr('data-question'));
        });

        $("[data-download-deleteall]").bind("click", function() {
            $("[data-download-list] input[type='checkbox']").prop('checked', true);
            me.removeCheckedFile();
        });

    };

};
