var language = new function() {

    var me = this;

    this.switchValue = function(el, params) {
        $.post('/panel/language/switchvalue', params, function(response) {
          var result = response.code;
          var statusBlock = $(".resultsChanges");
          var success = statusBlock.data("success");
          var error = statusBlock.data("error");
          if(result === 1) {
            statusBlock.text(success);
          } else{
            statusBlock.text(error);
          }
          statusBlock.fadeIn();
            // var msgType = response.code == 0 ? 'danger' : 'success'
            // var msg = typeof response.msg != "undefined" ? response.msg : "";
            //
            // if (msg != "") message.show(msgType, msg);
            //
            // if (typeof response.main != "undefined") {
            //     $("[data-list-container] table tr").find("[data-language-main]").html("");
            // }
        }, "json");
    };

  function template(data, container) {
    var $state = $(
      '<span><img src="/img/langs/' + data.id.toLowerCase() + '.png"></span>'
    );
    return $state;
  }
  function formatState (state) {
    if (!state.id) { return state.text; }
    var $state = $(
      '<span><img src="/img/langs/' + state.element.value.toLowerCase() + '.png"></span>'
    );
    return $state;
  };


$("body").find("[data-lang-component]").each(function(index,el){
  if(!$(el).closest("[data-select-noBind]").length){

    $(el).select2({
      templateResult: formatState,
      templateSelection: template,
      minimumResultsForSearch: -1
    });
  }

});


    this.bindEvents = function(){
      $("body").on("change","[data-lang-component]", function(){
        var current = $(this).find("option:selected").val();
        var elements = $(this).closest("[data-lang-box]").find("[data-lang-input]");
        elements.fadeOut();

        var elem = $(this);

        $(this).closest("[data-lang-box]").find("label.error").remove();
        $(this).closest("[data-lang-box]").find(".error").removeClass("error");

        elements.promise().done(function() {
          $(this).closest("[data-lang-box]").find("[data-lang-input='"+current+"']").fadeIn();

          if($('body').find('.js-wyswig')) {
            //its add post page
            if(elem.closest('.langLine').length) {
              $('body').find('.js-wyswig').find("[data-lang-input]:visible").fadeOut(400,function(){
                $('body').find('.js-wyswig').find("[data-lang-input='"+current+"']").fadeIn();
              });
            }

          }
        });
      });
    };

};


$(document).ready(function(){
  language.bindEvents();
});
