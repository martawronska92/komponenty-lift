var galleryphoto = new function() {

    var me = this;

    this.base64ToBlob = function(base64, mime) {
        mime = mime || '';
        var sliceSize = 1024;
        var byteChars = window.atob(base64);
        var byteArrays = [];

        for (var offset = 0, len = byteChars.length; offset < len; offset += sliceSize) {
            var slice = byteChars.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        return new Blob(byteArrays, {
            type: mime
        });
    }

    this.addPhotoCallback = function() {
        var thumbnail = $("input[name='thumbnail_encoded']").val();
        var original = $("input[name='thumbnail_original']").val();
        var mime = $("input[name='thumbnail_mime']").val();
        var photoId = typeof $("#photo_id").val() != "undefined" ? $("#photo_id").val() : "";

        if (thumbnail == "" && photoId == "") {
            message.show('info', Laravel.lang.galleryphoto.set_thumbnail);
        } else {
            //loader.show();

            var blobThumbnail = "",
                blobOriginal = "";

            if (thumbnail != "") {
                var parts = thumbnail.split(',');
                var base64ImageContent = parts[1]; //thumbnail.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
                var blobThumbnail = me.base64ToBlob(base64ImageContent, mime);
            }

            if (original != "") {
                parts = original.split(',');
                base64ImageContent = parts[1]; //original.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
                var blobOriginal = me.base64ToBlob(base64ImageContent, mime);
            }

            var formData = new FormData();
            if (blobThumbnail != "") {
                formData.append('thumbnail', blobThumbnail);
            }
            if (blobOriginal != "") {
                formData.append('original', blobOriginal);
            }
            formData.append('_token', Laravel.csrfToken);
            formData.append('galleryId', $("input[name='gallery_id']").val());
            formData.append('language', $("input[data-lang-input]").serialize());
            formData.append('thumbnail_options', $("input[name='thumbnail_options']").val());
            if (photoId != "")
                formData.append('photo_id', photoId);

            loader.show();
            $.ajax({
                    url: "/panel/galleryphoto/addphoto",
                    type: "POST",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: formData
                })
                .done(function(response) {
                    loader.hide();
                    if (parseInt(response.code) == 1) {
                        if (photoId != "") {
                            $("[data-editblocker]").remove();
                            $("[data-editable-blockcontainer]").css({
                                'opacity': 1
                            });
                            $("[data-galleryphoto-list]").find("[data-id='" + photoId + "']").replaceWith(response.view);
                        } else {
                            $("[data-galleryphoto-list]").prepend(response.view);
                        }

                        switchbutton.bindEvents();

                        $("[data-list-empty]").addClass("hidden");


                        setTimeout(function(){
                          post.uploadThumb.bind();
                          select2Init();
                        },100);

                    } else {
                        message.show('danger', response.msg);
                    }
                    me.clearPreview();
                })
                .error(function(jq, status, error) {
                    loader.hide();
                    message.show('danger', Laravel.lang.operation_failure);
                    $("[data-editable-cancel]").trigger("click");
                });
        }
    };

    this.addPhoto = function() {
        if (typeof $("[data-thumbnail-preview]").attr('src') != "undefined" && $("[data-thumbnail-preview]").attr('src') != "") {
            post.uploadThumb.object.croppie('result', {
                type: 'canvas',
                size: 'viewport',
                circle: false
            }).then(function(resp) {
                $("input[name='thumbnail_options']").val(JSON.stringify(post.uploadThumb.obj));
                $("[data-thumbnail-preview]").attr('src', resp);
                $('input[name="thumbnail_encoded"]').val(resp);
                me.addPhotoCallback();
            });
        } else {
            me.addPhotoCallback();
        }


    };

    this.removePhoto = function(photoId) {
        dialog.confirm(Laravel.lang.galleryphoto.delete_question, "", function() {
            loader.show();
            $.get("/panel/galleryphoto/removephoto/" + photoId, {},
                function(response) {
                    loader.hide();
                    if (parseInt(response.code) == 1) {
                        if ($.isArray(photoId)) {
                            for (var i = 0; i < photoId.length; i++)
                                $("[data-galleryphoto-list]").find("[data-id='" + photoId[i] + "']").remove();
                        } else {
                            $("[data-galleryphoto-list]").find("[data-id='" + photoId + "']").remove();
                        }
                        message.show('success', response.msg);
                        if ($("[data-galleryphoto-list] [data-id]").length == 0)
                            $("[data-list-empty]").removeClass("hidden");
                    } else {
                        message.show('danger', response.msg);
                    }
                });
        });
    };

    this.removeCheckedPhoto = function() {
        var ids = [];
        $("[data-galleryphoto-list] input[data-list-itemcheckbox]:checked").each(function(i, val) {
            ids.push($(val).val());
        });
        if (ids.length == 0) {
            message.show('info', Laravel.lang.galleryphoto.nothing_selected);
        } else {
            me.removePhoto(ids);
        }
    };

    this.clearPreview = function() {
        $("[data-editable-replacecontainer] [data-lang-input]").val("");
        $("[name='thumbnail_encode'],[name='thumbnail_options']").val("");
        $(".cr-image").attr("src", "");
        $("#photo_id").remove();
        $("[data-galleryphoto-tabs]").find("li:first a").trigger("click");
    };

    this.uploadify = function() {
        var upl = $('#data-galleryphoto-upload');
        if (upl.length == 1) {
            upl.uploadifive({
                'formData': {
                    '_token': Laravel.csrfToken,
                    'galleryId': $("#id").val()
                },
                'buttonText': 'dodaj',
                'fileObjName': 'multiple',
                'simUploadLimit': 1,
                'uploadScript': '/panel/galleryphoto/addphoto',
                'onUploadComplete': function(file, data) {
                    var info = $.parseJSON(data);
                    if (info.code == 0) {
                        if (typeof info.msg != "undefined")
                            message.show('danger', info.msg);
                    } else {
                        $("[data-galleryphoto-list]").prepend(info.view);
                        switchbutton.bindEvents();
                        if ($("[data-galleryphoto-list] [data-id]").length == 0)
                            $("[data-list-empty]").removeClass("hidden");
                        else {
                          $("[data-list-empty]").addClass("hidden");
                        }
                    }
                },
                'onQueueComplete': function(queue) {
                    upl.uploadifive('clearQueue');
                }
            });
        }
    };

    this.setMain = function(el){
      var photoId = el.attr("data-galleryphoto-main");
      loader.show();
      $.post('/panel/galleryphoto/setmain',{
          photoId:photoId,
          galleryId:parseInt($("#id").val()),
          '_token': Laravel.csrfToken
        },function(response){
          var td;
          for(var i=0;i<$("[data-editable-blockcontainer] table tbody tr").length;i++){
            td = $("[data-editable-blockcontainer] table tbody tr").eq(i).find("td:eq(3)");
            td.html("-");
          }
          loader.hide();
          el.closest("tr").find("td:eq(3)").html(Laravel.lang.yes.toUpperCase());
        });
    };

    this.bindEvents = function() {
        $("[data-galleryphoto-list]").on("click", "[data-galleryphoto-remove]", function() {
            me.removePhoto($(this).attr('data-galleryphoto-remove'));
        });

        $("[data-galleryphoto-deletemarked]").bind("click", function() {
            me.removeCheckedPhoto();
        });

        $("[data-galleryphoto-deleteall]").bind("click", function() {
            $("[data-galleryphoto-list] input[data-list-itemcheckbox]").prop('checked', true);
            me.removeCheckedPhoto();
        });

        $("[data-galleryphoto-editable]").on("click","[data-thumbnail-result]",function(){
          if($("input[name='thumbnail_original']").val() == "" && $("input[name='thumbnail']").val() == "") {
            message.show('info',Laravel.lang.validation.empty_file);
          }else{
            post.uploadThumb.crop($(this));
          }
        });

        $("[data-galleryphoto-list]").on("click", "[data-galleryphoto-main]", function(){
          me.setMain($(this));
        });

        me.uploadify();
    };

};
