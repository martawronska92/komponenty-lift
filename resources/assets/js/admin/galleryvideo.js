var galleryvideo = new function() {

    var me = this;
    this.videoId = "";
    this.videoThumbnail = "";
    this.videoTitle = "";
    this.videoUrl = "";

    this.previewTitle = "[data-galleryvideo-title]";
    this.previewThumbnail = "[data-galleryvideo-preview]";

    this.addVideo = function() {
        loader.show();

        var params = {
            'language': $("[data-editable-replacecontainer] input[data-lang-input]").serialize(),
            'video_id': typeof $("#video_id").val() != "undefined" ? $("#video_id").val() : "",
            '_token': Laravel.csrfToken
        };
        var gallery_id = typeof $("#galleryId").val() != "undefined" ? $("#galleryId").val() : "";
        if (gallery_id != "") params.gallery_id = gallery_id;
        if (me.videoThumbnail != "") params.thumbnail = me.videoThumbnail;
        params.url = me.videoUrl != "" ? me.videoUrl : $("input[name='youtube_link']").val();

        $.post('/panel/galleryvideo/addvideo', params, function(response) {
            loader.hide();
            if (parseInt(response.code) == 1) {
                me.clearPreview();
                if (params.video_id != "") {
                    $("[data-galleryvideo-list]").find("[data-id='" + params.video_id + "']").replaceWith(response.view);
                } else
                    $("[data-galleryvideo-list]").prepend(response.view);
                switchbutton.bindEvents();
                $("[data-editblocker]").remove();
                $("[data-editable-blockcontainer]").css({
                    'opacity': 1
                });
                $("[data-list-empty]").addClass("hidden");
            } else {
                message.show('danger', Laravel.lang.galleryvideo.add_failure);
            }
            select2Init();
        }).fail(function() {
            loader.hide();
            $("[data-editable-cancel]").trigger('click');
            message.show('danger', Laravel.lang.operation_failure);
        });
    };

    this.removeVideo = function(videoId) {
        dialog.confirm(Laravel.lang.galleryvideo.delete_question, "", function() {
            loader.show();
            $.get('/panel/galleryvideo/removevideo/' + videoId, {}, function(response) {
                loader.hide();
                if (parseInt(response.code) == 1) {
                    message.show('success', response.msg);
                    if ($.isArray(videoId)) {
                        for (var i = 0; i < videoId.length; i++)
                            $("[data-galleryvideo-list]").find("[data-id='" + videoId[i] + "']").remove();
                    } else
                        $("[data-galleryvideo-list]").find("[data-id='" + videoId + "']").remove();

                    if ($("[data-galleryvideo-list] [data-id]").length == 0)
                        $("[data-list-empty]").removeClass("hidden");
                } else {
                    message.show('danger', response.msg);
                }
            });
        });
    };

    this.removeCheckedVideo = function() {
        var ids = [];
        $("[data-galleryvideo-list] input[data-list-itemcheckbox]:checked").each(function(i, val) {
            ids.push($(val).val());
        });
        if (ids.length == 0) {
            message.show('info', Laravel.lang.galleryvideo.nothing_selected);
        } else {
            me.removeVideo(ids);
        }
    };

    this.getVideoParams = function(url) {
        me.videoId = youtube.getIdFromUrl(url);
        me.videoThumbnail = youtube.getThumbnail(me.videoId);
        me.videoUrl = youtube.getUrl(me.videoId);
    };

    this.clearPreview = function() {
        me.videoId = me.videoTitle = me.videoThumbnail = "";
        $("input[name='youtube_link']").val("");
        $(me.previewTitle).html("");
        $(me.previewThumbnail).attr("src", "/images/default_video_thumb.jpg");
        $("[data-editable-replacecontainer] [data-lang-input]").val("");
    };

    this.setPreview = function(url) {
        youtube.getTitle(me.videoId, function(title) {
            me.videoTitle = title;
            $(me.previewTitle).html(me.videoTitle);
            $("[data-editable-replacecontainer] [data-lang-input]").val(me.videoTitle);
        });
        $("#galleryvideoForm input[name='youtube_link']").val(me.videoUrl);
        $(me.previewThumbnail).attr("src", me.videoThumbnail);
    };

    this.bindEvents = function() {
        $("[data-galleryvideo-leftcontainer]").on('paste', "input[name='youtube_link']", function() {
            setTimeout(function() {
                if ($("#galleryvideoForm").valid()) {
                    me.getVideoParams($("#galleryvideoForm input[name='youtube_link']").val());
                    me.setPreview();
                }
            }, 100);
        });

        $("[data-galleryvideo-leftcontainer]").on("click", "[data-galleryvideo-check]", function() {
            if ($("#galleryvideoForm").valid()) {
                me.addVideo();
            }
        });

        $("#galleryvideoForm").bind("submit", function() {
            return false;
        });

        $("[data-galleryvideo-list]").on("click", "[data-galleryvideo-remove]", function() {
            me.removeVideo($(this).attr('data-galleryvideo-remove'));
        });

        $("[data-galleryvideo-deletemarked]").bind("click", function() {
            me.removeCheckedVideo();
        });

        $("[data-galleryvideo-deleteall]").bind("click", function() {
            $("[data-galleryvideo-list] input[type='checkbox']").prop('checked', true);
            me.removeCheckedVideo();
        });

    };
};

$(document).ready(function() {
    galleryvideo.bindEvents();
});
