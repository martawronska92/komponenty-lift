var user = new function() {

    var me = this;

    this.collapseTab = "";

    this.bindEvents = function() {
        $("[data-user-password]").bind('click', function() {
            if ($("input[name='password']").length == 0) {
                $(this).before("<div class='userBlock__input inputBlock'><input type='text' name='password' id='password' data-formvalidation-label='"+Laravel.lang.user.password+"' value='" + $(this).attr('data-password') + "' class='required password inputBlock__field'/></div>");
                $(this).hide();
            }
        });

        $("[data-user-checkall]").bind('click', function() {
            $(this).closest("[data-user-privilegecheckboxes]").find("[data-user-privilegebox] input").prop('checked', true);
            $("[data-user-privilegesublist]").find("input[type='checkbox']").prop('checked', true);
        });

        $("[data-user-uncheckall]").bind('click', function() {
            $(this).closest("[data-user-privilegecheckboxes]").find("[data-user-privilegebox] input").prop('checked', false);
            $("[data-user-privilegesublist]").find("input[type='checkbox']").prop('checked', false);
        });

        /**
         * strzałka do podlisty
         */
        $("[data-user-privilegebox] [data-user-privilegesublistcheckbox]").bind('click',function(){
            var expanded = $(this).attr('aria-expanded')=="true";
            me.collapseTab = $(this);
            var classCss = "fa fa-chevron-circle-";
            classCss+= expanded ? "right" : "left collapsed";

            expanded ? $(this).closest(".list-group-item").removeClass('active') : $(this).closest(".list-group-item").addClass('active');

            if(expanded){
              $($(this).attr("data-collapse-element")).fadeOut();
              $("#collapsebox").addClass('hidden');
              setTimeout(function(){
                //$("[data-user-privilegecontainer]").attr("class","col-lg-12");
                user.collapseTab.attr('aria-expanded','false')
              },300);
            }else{
              //$("[data-user-privilegecontainer]").attr("class","col-lg-6");
              setTimeout(function(){
                $("#collapsebox").removeClass('hidden');
                $(user.collapseTab.attr("data-collapse-element")).fadeIn();
                user.collapseTab.attr('aria-expanded','true')
              },300);
            }

            //$(this).attr("class",classCss);
            $(this).toggleClass("is-opened");
        });

        /**
         * checkbox na głównej liście
         */
        $("[data-user-privilegelist] label").bind("click",function(){
          var checked = $(this).closest("[data-user-privilegelist]").find("input[type='checkbox']").is(":checked");
          var privilege = $(this).closest("[data-user-privilegelist]").attr('data-user-privilegelist');
          if($("[data-user-privilegesublist='"+privilege+"']").length){
              $("[data-user-privilegesublist='"+privilege+"']").find("input[type='checkbox']").each(function(i,val){
                if(!checked)
                  $(val).prop('checked','checked');
                else
                  $(val).removeAttr('checked');
              });
          }
        });

        /**
         * checkbox na podliście
         */
         $("[data-user-privilegesublist] label").bind("click",function(){
           clickedInput = $(this);
           setTimeout(function(){
             var privilege = clickedInput.closest("[data-user-privilegesublist]").attr("data-user-privilegesublist");
             var checkedCount = clickedInput.closest("[data-user-privilegesublist]").find("input[type='checkbox']:checked").length;
             if(checkedCount > 0){
               $("[data-user-privilegelist='"+privilege+"']").find("input[type='checkbox']").prop('checked','checked');
             }
           },100);
         });
    };

};
