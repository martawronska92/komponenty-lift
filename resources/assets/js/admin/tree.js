var tree = new function() {

    var me = this;

    this.baseElement = "#tree";

    this.node = {
        "activeKey": 0,
        "setActive": function(data, nohide) {
            var tempKey = data.node.key;
            if (tempKey != me.node.activeKey || typeof nohide != "undefined")
                me.form.hide();
            me.node.activeKey = tempKey;
        },
        "getActiveKey": function() {
            return me.node.activeKey;
        },
        "getActiveNode": function() {
            var node = $("#tree").fancytree("getTree").getNodeByKey(me.node.activeKey);
            if (node == null)
                node = $("#tree").fancytree("getActiveNode");
            return node;
        },
        "getParentId": function() {
            if (me.node.activeKey == 0)
                return 0;
            else
                return me.node.getActiveNode().key;
        },
        "getLevel": function() {
            if (me.node.activeKey == 0)
                return 0;
            else
                return me.node.getActiveNode().getLevel();
        },
        "checkKey": function() {
            var valid = me.node.activeKey % 1 === 0;
            if (!valid)
                message.show('danger', Laravel.lang.menu.invalid_key);
            return valid;
        },
        "addSubElement": function(id, title) {
            var node = me.node.getActiveNode(),
                newData = {
                    id: id,
                    title: title
                },
                newSibling = node.addChildren(newData);
            newSibling.setActive();
        },
        "addElement": function(id, title, folder) {
            var rootNode = $("#tree").fancytree("getRootNode");
            var newNode = rootNode.addChildren({
                title: title,
                key: id
                //folder: true
            });

            newNode.setActive();
            newNode.setFocus();
            //newNode.setSelected(true);
            //$("#tree").fancytree("getTree").getNodeByKey(id).setActive();
            me.node.activeKey = id;
        },
        "delete": function() {
            me.node.getActiveNode().remove();
        },
        "setTitle": function(title) {
            var node = me.node.getActiveNode();
            if (!node) return;
            node.setTitle(title);
        }
    };

    this.form = {
        "show": function() {
            $("#nodeDetailsForm").fadeIn(); //removeClass("hidden");
        },
        "hide": function() {
            $("#nodeDetailsForm").fadeOut(); //addClass("hidden");
        },
        "clear": function() {
            $("#nodeDetailsForm").find("input[type='text']").val("");
            $("#nodeDetailsForm").find("input[type='hidden']").val("");
            $("#nodeDetailsForm input[name='menu_element_type'][value='page']").trigger('click');
        },
        "submit": function() {
            $("#nodeDetailsForm").on("submit", "form", function() {
                //$(this).validate();

                if ($(this).valid()) {
                    loader.show();
                    var formParams = {
                        "id": $("#nodeDetailsForm #id").val(),
                        "menu_element_type": $("#nodeDetailsForm input[name='menu_element_type']:checked").val(),
                        "page": $("#nodeDetailsForm select[name='page'] option:selected").val(),
                        "category": $("#nodeDetailsForm select[name='category'] option:selected").val(),
                        "url": $("#nodeDetailsForm input[name='url']").val(),
                        "active": $("#nodeDetailsForm input[name='active']").is(":checked") ? 1 : 0,
                        "menu_id": $("#menu_id").val(),
                        "parent_id": me.node.getParentId(),
                        "level": me.node.getLevel(),
                        "_token": Laravel.csrfToken
                    };

                    $("[id^='title_']").each(function(i, val) {
                        formParams[$(val).attr('name')] = $(val).val();
                    });

                    $.post('/panel/menuelement/store', formParams, function(response) {
                        loader.hide();
                        var code = parseInt(response.code);
                        if (code) {
                            if (formParams.id == "") {
                                if (formParams.parent_id == 0)
                                    me.node.addElement(response.id, response.text);
                                else
                                    me.node.addSubElement(response.id, response.text);
                                $("#nodeDetailsForm #id").val(response.id);
                            } else {
                                me.node.setTitle(response.text);
                            }
                        }

                        message.show(code ? 'success' : 'danger', response.msg);
                    }).fail(function() {
                        loader.hide();
                        message.show('danger', Laravel.lang.operation_failure);
                    });
                }
                return false;
            });
        }
    };

    this.element = {
        "addElement": function() {
            me.form.clear();
            me.form.show();
            me.node.activeKey = 0;
        },
        "addSubelement": function() {
            if (me.node.getLevel() == 3) {
                message.show('danger', Laravel.lang.menu.max_level);
            } else {
                me.form.clear();
                me.form.show();
            }
        },
        "edit": function() {
            if (me.node.checkKey()) {
                loader.show();
                me.form.clear();
                $.get('/panel/menuelement/get/' + me.node.getActiveKey(), {},
                    function(response) {
                        if (response.code == 0) {
                            message.show('danger', response.msg);
                        } else {
                            var data = response.msg,
                                menu_type_value;

                            /**
                             * assign connect
                             */

                            if (data.external == 1) {
                                menu_type_value = 'link';
                                $("#nodeDetailsForm input[name='url']").val(data.external_url);
                            }else if(data.mainpage == 1){
                               menu_type_value = 'mainpage';                               
                            } else if (data.entity_type == 'App\\Models\\Page') {
                                menu_type_value = 'page';
                                setTimeout(function(){
                                  $("#nodeDetailsForm select[name='page']").val(data.entity_id).trigger('change.select2');
                                },100);
                            } else if (data.entity_type == 'App\\Models\\Category') {
                                menu_type_value = 'category';
                                setTimeout(function(){
                                  $("#nodeDetailsForm select[name='category']").val(data.entity_id).trigger('change.select2');
                                },100);
                            }

                            $("#nodeDetailsForm input[name='active']").prop('checked', data.active ? true : false);

                            $("#nodeDetailsForm input[name='menu_element_type'][value='" + menu_type_value + "']").prop('checked', true);
                            $("#nodeDetailsForm input[name='menu_element_type'][value='" + menu_type_value + "']").trigger('click');
                            $("#nodeDetailsForm #id").val(data.id);
                            /**
                             * assign langauage
                             */
                            for (var i = 0; i < data.language.length; i++) {
                                $("#nodeDetailsForm #title_" + data.language[i].language_id).val(data.language[i].title);
                            }
                            loader.hide();
                            me.form.show();
                        }
                    });
            }
        },
        "delete": function() {
            if (me.node.checkKey()) {
                loader.show();
                var activeKey = me.node.getActiveKey();
                $.get('/panel/menuelement/delete/' + activeKey, {},
                    function(response) {
                        var code = parseInt(response);
                        if (code) {
                            var currentViewing = $("#nodeDetailsForm #id").val();
                            if (activeKey == currentViewing) {
                                me.form.hide();
                            }
                            me.node.delete();
                            message.show('success', Laravel.lang.menu.item_delete_success);
                            me.node.activeKey = 0;
                        } else if (code == -1) {
                            message.show('danger', Laravel.lang.menu.item_delete_noexists);
                        } else {
                            message.show('danger', Laravel.lang.menu.item_delete_failure);
                        }
                        loader.hide();
                    });
            }
        },
        "sort": function() {
            var tree = $("#tree").fancytree("getTree");
            var d = tree.toDict(true);
            $.post("/panel/menuelement/setpositions", {
                "data": d,
                "_token": Laravel.csrfToken
            }, function(response) {

            }).fail(function() {
                loader.hide();
                message.show('danger', Laravel.lang.operation_failure);
            });
            //$.post()
        }
    };

    this.bindEvents = function() {
        $(me.baseElement).fancytree({
            extensions: ["dnd"],
            click: function(event, data) {
                me.node.setActive(data);
                //data.node.toggleExpanded();
            },
            dnd: {
                autoExpandMS: 400,
                focusOnClick: true,
                preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
                preventRecursiveMoves: true, // Prevent dropping nodes on own descendants
                dragStart: function(node, data) {
                    /** This function MUST be defined to enable dragging for the tree.
                     *  Return false to cancel dragging of node.
                     */
                    return true;
                },
                dragEnter: function(node, data) {
                    /** data.otherNode may be null for non-fancytree droppables.
                     *  Return false to disallow dropping on node. In this case
                     *  dragOver and dragLeave are not called.
                     *  Return 'over', 'before, or 'after' to force a hitMode.
                     *  Return ['before', 'after'] to restrict available hitModes.
                     *  Any other return value will calc the hitMode from the cursor position.
                     */
                    // Prevent dropping a parent below another parent (only sort
                    // nodes under the same parent)
                    /*           if(node.parent !== data.otherNode.parent){
                          return false;
                        }
                        // Don't allow dropping *over* a node (would create a child)
                        return ["before", "after"];
                    */
                    return node.getLevel() <= 3;
                },
                dragDrop: function(node, data) {
                    /** This function MUST be defined to enable dropping of items on
                     *  the tree.
                     */
                    // console.log(node);
                    // console.log(data);
                    data.otherNode.moveTo(node, data.hitMode);
                    me.element.sort();
                }
            },
            // beforeSelect: function(event, data) {
            //     // A node is about to be selected: prevent this for folders:
            //     // if( data.node.isFolder() ){
            //     //   return false;
            //     // }
            //     return true;
            // }
        });
        // .on("mouseenter", "span.fancytree-title", function(event) {
        //     // Add a hover handler to all node titles (using event delegation)
        //     // var node = $.ui.fancytree.getNode(event);
        //     // node.info(event.type);
        //     console.log('hover');
        // })
        // .on("mouseleave", "span.fancytree-title", function(event) {
        //     // Add a hover handler to all node titles (using event delegation)
        //     // var node = $.ui.fancytree.getNode(event);
        //     // node.info(event.type);
        //     //console.log('leave');
        // });

        $("[data-menu-expandall]").bind("click", function() {
            $(me.baseElement).fancytree("getRootNode").visit(function(node) {
                node.setExpanded(true);
            });
        });

        $("[data-menu-collapseall]").bind("click", function() {
            $(me.baseElement).fancytree("getRootNode").visit(function(node) {
                node.setExpanded(false);
            });
        });

        $("[data-menu-add-element]").bind("click", function() {
            me.element.addElement();
        });

        $("[data-menu-add-subelement]").bind("click", function() {
            if (me.node.getActiveKey() == 0) {
                message.show('info', Laravel.lang.menu.choose_node);
            } else {
                me.element.addSubelement();
            }
        });

        $("[data-menu-edit-element]").bind("click", function() {
            if (me.node.getActiveKey() == 0) {
                message.show('info', Laravel.lang.menu.choose_node);
            } else {
                me.element.edit();
            }
        });

        $("[data-menu-delete-element]").bind("click", function() {
            if (me.node.getActiveKey() == 0) {
                message.show('info', Laravel.lang.menu.choose_node);
            } else {
                var node = me.node.getActiveNode();
                var question = Laravel.lang.menu.delete_question + "<b><i>" + node.title + "</i></b>";
                dialog.confirm(question, "", function() {
                    me.element.delete();
                });
            }
        });

        $("#nodeDetailsForm input[name='menu_element_type']").bind('click', function() {
            $("[data-menu-element-type]").each(function(i, val) {
                $(val).parent().find("label.error").remove();
            });
            $("[data-menu-element-type]").addClass('hidden');
            $("[data-menu-element-type]").removeClass('required url_http url');
            $("[data-menu-element-type='" + $(this).val() + "']").removeClass('hidden');
            $("[data-menu-element-type='" + $(this).val() + "']").addClass('required');

            if ($(this).val() == "link") {
                $("[data-menu-element-type='" + $(this).val() + "']").addClass('url_http url');
            }
        });

        me.form.submit();
    };
};
