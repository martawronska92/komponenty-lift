var update = new function(){

  var me = this;

  this.updateAmong = 0;
  this.currentUpdate = 0;
  this.break = 0;

  this.init = function(){
    $.post('/panel/version/getversions',function(response){
      var info = $.parseJSON(response);
      if(info.code){
        if(info.updateAmong > 0){
          me.updateAmong = info.updateAmong;
          me.update();
        }else{

        }
      }else{
        //bład pobierania wersji
        me.result.error();
      }
    },"json").error(function(){
      //błąd pobierania wersji
      me.result.error();
    });
  };

  this.update = function(){
    $.post('/panel/version/install',{currentUpdate:me.currentUpdate},function(response){
      var info = $.parseJSON(response);
      if(info.code){
        me.currentUpdate++;
        var percent = me.currentUpdate == me.updateAmong ? 100 : Math.ceil(me.currentUpdate/me.updateAmong*100);
        me.result.progress(percent);
      }else{
        me.break = 1;
      }
    },"json")
    .error(function(){
      //blad instalacji
      me.result.error();
    })
    .then(function(){
      if(!me.break){
        if(me.currentUpdate<me.updateAmong)
          me.update();
        else
          me.result.success();
      }else{
        me.result.error();
      }
    });
  };

  this.result = {
    "success": function(){
      $(".progress-bar").removeClass("active");
      $("[data-update-progressmsg]").find("p").html("Aktualizaja zakończyła się pomyślnie");
      $(".progress-bar").addClass("progress-bar-success");
    },
    "error": function(){
      $(".progress-bar").removeClass("active");
      $("[data-update-progressmsg]").find("p").html("Aktualizaja nie powiodła się");
      $(".progress-bar").addClass("progress-bar-danger");
    },
    "progress": function(percent){
      $(".progress-bar").css("width",percent);
      $(".progress-bar").html(percent+"%");
    }
  };

};
