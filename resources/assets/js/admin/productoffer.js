var productoffer = new function(){

  var me = this;

  this.checkedProducts = [];

  this.checkAll = function(direction){
    var el;
    for(var i=0;i<$("[data-productoffer-checkbox]").length;i++){
      el = $("[data-productoffer-checkbox]").eq(i);
      if(direction > 0){
        me.addToChecked(parseInt(el.val()));
        el.prop('checked',true);
      }else{
        me.removeFromChecked(parseInt(el.val()));
        el.prop('checked',false);
      }
    }
  };

  this.isChecked = function(id){
    return me.checkedProducts.indexOf(id) > -1;
  }

  this.markChecked = function(){
    var el;
    for(var i=0;i<$("[data-productoffer-checkbox]").length;i++){
      el = $("[data-productoffer-checkbox]").eq(i);
      $(el).prop("checked",me.isChecked(parseInt(el.val())));
    }
  };

  this.addToChecked = function(id){
    var index = me.checkedProducts.indexOf(id);
    if(index == -1)
      me.checkedProducts.push(id);
  };

  this.removeFromChecked = function(id){
    var index = me.checkedProducts.indexOf(id);
    if(index > -1)
      me.checkedProducts.splice(index, 1);
  };

  this.bindEvents = function(){
    console.log("bind events");

    $("body").on("click","[data-productoffer-checkboxall]",function(){
      me.checkAll($(this).is(':checked') ? 1 : -1);      
    });

    $("body").on("click","[data-productoffer-checkbox]",function(){
      var id = parseInt($(this).val());
      $(this).is(':checked') ? me.addToChecked(id) : me.removeFromChecked(id);
    });
  };

};
