var validator = new function() {
    this.methods = function() {
        jQuery.validator.addMethod("slug", function(value, element) {
            var slug = /^[a-zA-Z0-9\-]+$/;
            return slug.test(value);
        }, Laravel.lang.validation.url);

        jQuery.validator.addMethod("youtube_link", function(value, element) {
            var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
            if (value == "")
                return true;
            else
                return (value.match(p)) ? RegExp.$1 : false;
        }, Laravel.lang.validation.youtube_link);

        jQuery.validator.addMethod("date_greater_than", function(value, element) {
            var fromElementId, toELementId = $(element).attr('id');
            fromElementId = "#" + toELementId.split("_")[0] + "_from";
            var reg = /^[0-9]{2}-[0-9]{2}-[0-9]{4}$/;
            var toValue = value,
                fromValue = $(fromElementId).val();

            if (reg.test(fromValue) && reg.test(toValue)) {
                var fromValueParts = fromValue.split("-");
                fromValue = fromValueParts[2] + "-" + fromValueParts[1] + "-" + fromValueParts[0];
                var toValueParts = toValue.split("-");
                toValue = toValueParts[2] + "-" + toValueParts[1] + "-" + toValueParts[0];

                return new Date(toValue) >= new Date(fromValue);
            } else {
                return true;
            }

            // if (!/Invalid|NaN/.test(new Date(value))) {
            //
            // }

        }, Laravel.lang.validation.date_greater_than);

        jQuery.validator.addMethod("password", function(value, element) {
            var password = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d].*$/;
            return password.test(value) && value.length >= 8;
        }, Laravel.lang.validation.password);

        jQuery.validator.addMethod("current_password", function(value, element) {
            return value != $("#old_password").val();
        }, Laravel.lang.validation.new_password_must_be_different);

        jQuery.validator.addMethod("password_confirm", function(value, element) {
            return value == $("#new_password").val();
        }, Laravel.lang.validation.password_not_same);

        jQuery.validator.addMethod("valid_email", function(value, element) {
            var email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return email.test(value);
        }, Laravel.lang.validation.email);

        jQuery.validator.addMethod("newsletter_recipient", function(value, element) {
            var response = true;

            var email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            if (value != "" && email.test(value)) {
                $.ajax({
                    type: "POST",
                    url: "/panel/newsletter/checkrecipient",
                    data: {
                        "email": value,
                        "_token": Laravel.csrfToken,
                        "id": $(element).attr("data-id")
                    },
                    async: false,
                    success: function(data) {
                        response = parseInt(data) == 1 ? true : false;
                    }
                });
            }
            return response;
        }, Laravel.lang.validation.newsletter_recipient_exists);

        jQuery.validator.addMethod("nickname", function(value, element) {
            return /^[a-zA-Z0-9\_]+$/.test(value);
        }, Laravel.lang.validation.invalid_nickname);

        jQuery.validator.addMethod("unique_nickname", function(value, element) {
            var valid = true;
            $.post("/panel/user/checkunique", {
                "name": value,
                "id": $("#id").val(),
                "_token": Laravel.csrfToken
            }, function(response) {
                valid = parseInt(response.code) == 1 ? true : false;
            }, "json");
            return valid;
        }, Laravel.lang.validation.unique_nickname);

        jQuery.validator.addMethod("unique_email", function(value, element) {
            jQuery.validator.addMethod("unique_nickname", function(value, element) {
                var valid = true;
                $.post("/panel/user/checkunique", {
                    "name": value,
                    "id": $("#id").val(),
                    "_token": Laravel.csrfToken
                }, function(response) {
                    valid = parseInt(response.code) == 1 ? true : false;
                }, "json");
                return valid;
            }, Laravel.lang.validation.unique_nickname);
        }, Laravel.lang.validation.unique_email);

        jQuery.validator.addMethod("url_http", function(value, element) {
            return value != "" ? /^((http|https):\/\/)www.*$/.test(value) : true;
        }, Laravel.lang.validation.url_http);

        jQuery.validator.addMethod("url", function(value, element) {
            return /^((http|https):\/\/)www\.[a-zA-Z0-9\-\_\.]*\.[a-zA-Z]{2,5}.*$/.test(value);
        }, Laravel.lang.validation.url);

        jQuery.validator.addMethod("date_pl", function(value, element) {
            var valid = true;
            if (typeof value != "undefined" && value != "") {
                valid = /^[0-9]{2}-[0-9]{2}-[0-9]{4}$/.test(value);
                if (!valid)
                    return false;
                else {
                    var days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
                    var parts = value.split("-");
                    var day = parseInt(parts[0]),
                        month = parseInt(parts[1]),
                        year = parseInt(parts[2]);
                    if (month < 1 || month > 12) //miesiace
                        valid = false;
                    else {
                        var leap = ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
                        if (day == 29 && month == 2 && leap) { //rok przestępny
                            valid = false;
                        } else if (day > days[month - 1]) { //dni w miesiącu
                            valid = false;
                        }
                    }
                }
            }
            return valid;
        }, Laravel.lang.validation.invalid_date);

        jQuery.validator.addMethod("time", function(value, element) {
            var valid = /^[0-9]{2}:[0-9]{2}$/.test(value);
            if (!valid)
                return false;
            else {
                var parts = value.split(":");
                if (parseInt(parts[0]) < 0 || parseInt(parts[0]) > 24 || parseInt(parts[1]) < 0 || parseInt(parts[1]) > 59)
                    return false;
                else
                    return true;
            }
        }, Laravel.lang.validation.time);
    };

};

$(document).ready(function() {
    validator.methods();
});
