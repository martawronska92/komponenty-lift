var openinghours = new function() {

    var me = this;

    this.add = function(selectEl) {


        var id = me.generateId();

        var el;
        for(var i=0;i<$("[data-openinghours-item]:last input[data-lang-input]").length;i++){
          el = $("[data-openinghours-item]:last input[data-lang-input]").eq(i);
          $(el).prop({
              "name": "openinghours_name[" + id + "]["+el.attr('data-lang-id')+"]",
              "id": "openinghours_name[" + id + "]["+el.attr('data-lang-id')+"]"
          });
        }
        $("[data-openinghours-item]:last input[data-openinghours-from]").prop({
            "name": "openinghours_from[" + id + "]",
            "id": "openinghours_from[" + id + "]"
        });
        $("[data-openinghours-item]:last input[data-openinghours-to]").prop({
            "name": "openinghours_to[" + id + "]",
            "id": "openinghours_to[" + id + "]"
        });

        $("[data-openinghours-item]:last").find("[data-openinghours-label]").prop("for", "openinghours_open[" + id + "]");
        $("[data-openinghours-item]:last [data-openinghours-open]").prop({
            "id": "openinghours_open[" + id + "]",
            "name": "openinghours_open[" + id + "]"
        });



      function template(data, container) {
        var $state = $(
          '<span><img src="/img/langs/' + data.id.toLowerCase() + '.png"></span>'
        );
        return $state;
      }
      function formatState (state) {
        if (!state.id) { return state.text; }
        var $state = $(
          '<span><img src="/img/langs/' + state.element.value.toLowerCase() + '.png"></span>'
        );
        return $state;
      }

      $("[data-openinghours-box]").append($("[data-openinghours-template]").html());

          $("body").find("[data-openinghours-box] [data-lang-component]").select2({
            templateResult: formatState,
            templateSelection: template,
            minimumResultsForSearch: -1
          });

    };

    this.remove = function(el) {
        $(el).closest("[data-openinghours-item]").remove();
    };

    this.changeOpen = function(el) {
        var item = el.closest("[data-openinghours-item]");
        if (el.is(':checked')) {
            item.find("[data-openinghours-from],[data-openinghours-to]").removeAttr('disabled');
            item.find("[data-openinghours-from],[data-openinghours-to]").addClass("required");
            item.find("[data-openinghours-label] span").html(Laravel.lang.openinghours.open);
        } else {
            item.find("[data-openinghours-from],[data-openinghours-to]").attr('disabled', 'disabled');
            item.find("[data-openinghours-from],[data-openinghours-to]").val("");
            item.find("[data-openinghours-from],[data-openinghours-to]").removeClass("required");
            item.find("[data-openinghours-label] span").html(Laravel.lang.openinghours.close);
            item.find("label.error").remove();
        }
    };

    this.generateId = function() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 10; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    };

    this.reset = function() {
        $("[data-openinghours-template]").find("[data-lang-input]").each(function(i, val) {
            $(val).val("");
        });
        $("[data-openinghours-template]").find("[data-openinghours-from]").val("");
        $("[data-openinghours-template]").find("[data-openinghours-to]").val("");
    };

    this.bindEvents = function() {
        $("[data-openinghours-add]").bind("click", function() {
            me.add($(this));
        });

        $("body").on("click", "[data-openinghours-remove]", function() {
            me.remove($(this));
        });

        $("body").on("click", "[data-openinghours-open]", function() {
            me.changeOpen($(this));
        });

        //me.reset();
    };

};

$(document).ready(function() {
    openinghours.bindEvents();
});
