var category = new function() {

    var me = this;

    this.field = {
        "counter": 0,
        "add": function() {
            me.field.counter++;
            var name = "";
            $("[data-category-fieldpartial]").find("input").each(function(i, val) {
                name = $(val).attr('data-field') + "[" + me.field.counter + "][" + $(val).attr('data-language') + "]";
                $(val).attr('name', name);
            });
            $("[data-category-fieldbox]").append($("[data-category-fieldpartial]").html());
        },
        "remove": function(el) {
            if ($(el).closest(".newsBlock__item").find("[data-category-fieldid]").length > 0) {
                var removed = $("input[name='deleted_fields']").val() != "" ? $("input[name='deleted_fields']").val().split(",") : [];
                removed.push($(el).closest(".newsBlock__item").find("[data-category-fieldid]").val());
                $("input[name='deleted_fields']").val(removed.join());
            }
            $(el).closest(".newsBlock__item").remove();
        },
        "bindEvents": function() {
            me.field.counter = $("[data-category-fieldbox] [data-category-fieldrow]").length;
            $("[data-category-addfield]").bind("click", function() {
                me.field.add();
            });
            $("[data-category-fieldbox]").on("click", "[data-category-fielddelete]", function() {
                me.field.remove($(this));
            });
        }
    };

    this.bindEvents = function(){
      $("#show_modules").bind("click",function(){
        if($(this).is(':checked')){
          $("#tab_li_module").fadeIn();
        }else{
          $("#tab_li_module").fadeOut();
        }
      });
    }
};
