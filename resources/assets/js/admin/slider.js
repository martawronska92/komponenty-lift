var slider = new function() {

    var me = this;

    this.bindEvents = function() {

        $("#sliderForm input[name='external']").bind('click', function() {
            $("[data-slider-linktypecontainer]").find("label.error").remove();
            //$("[data-slider-linktype]").attr('class', 'form-control');
            $("[data-slider-linktypebox]").addClass('hidden');
            var value = parseInt($(this).val());

            if (value > -1) {
                $("[data-slider-linktypebox='" + value + "']").removeClass('hidden');
                //$("[data-slider-linktypebox='" + value + "']").addClass('col-lg-8')
                $("[data-slider-linktype='" + value + "']").addClass('required');

                if (value == 1) { //external
                    $("[data-slider-linktype='" + value + "']").addClass('url_http url');
                }
            }
        });

    };

};

$(document).ready(function() {
    slider.bindEvents();
});
