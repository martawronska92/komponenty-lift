var list = new function() {

    var me = this;

    this.sort = {
        "sortable": function() {
            var action = $("[data-list-sortable]").attr('data-list-sortable');
            $("[data-list-sortable]").sortable({
                'cursor': 'move',
                'update': function(ev, ui) {
                    var ids = [],
                        el = $("[data-list-sortable] [data-id]");
                    el.each(function(i, val) {
                        ids.push($(val).attr('data-id'));
                    });
                    if (typeof $(this).attr("data-list-sortablenoajax") == "undefined") {
                        $.post('/panel/' + action + '/setposition', {
                            'ids': ids,
                            '_token': Laravel.csrfToken
                        }).fail(function() {
                            loader.hide();
                            message.show('danger', Laravel.lang.operation_failure);
                        });;
                    }
                }
            });
        },
        "bindEvents": function() {
            me.sort.sortable();

            $("[data-list-container]").on("click", "[data-list-ordercolumnfield]", function() {
                var order = $.trim($(this).attr("data-list-ordercolumn"));
                var field = $(this).attr("data-list-ordercolumnfield");

                $("[data-list-ordercolumn]").attr("data-list-ordercolumn", "");
                $("[data-list-ordercolumnfield]").find("i").attr("class", "fa fa-sort");

                var new_order = (order == "" || order == "asc") ? "desc" : "asc";

                $(this).attr("data-list-ordercolumn", new_order);
                $(this).find("i").attr("class", "fa fa-sort-" + new_order);

                $("[data-list-globalorder]").val(new_order);
                $("[data-list-globalorderfield]").val(field);

                var page = $("[data-pagination-gotopage]").val() == "" ? 1 : $("[data-pagination-gotopage]").val();
                me.pagination.getPage($("[data-pagination-gotopage]").attr("data-pagination-baseurl"), {
                    'page': page
                });
            });
        }
    };

    this.callback;

    this.loadNextPage = function(el) {
        var url = el.attr('data-list-url');
        var page = el.attr('data-list-currentpage');
        var currentText = $(el).text();

        page++;

        $(el).html($(el).attr('data-loading-text'));

        $.get(url, {
            page: page
        }, function(response) {
            $("[data-list-body]").append($(response).find("[data-list-body]").html());
            var hasNextPage = $(response).find("[data-list-loadnextpage]").length;
            if (hasNextPage) {
                $("[data-list-loadnextpage]").attr("data-list-currentpage", page);
            } else {
                $("[data-list-loadnextpage]").fadeOut();
            }

            switchbutton.bindEvents();

            $(el).text(currentText);
        });
    };

    this.multiActions = {
        "execute": function() {
            var action = $("[data-multiaction-list] option:selected").val();
            var confirmQuestion = $("[data-multiaction-list] option:selected").attr("data-multiaction-confirm");

            if (action != "") {
                var ids = [];
                $("[data-list-checkbox]:checked").each(function(i, val) {
                    ids.push($(val).val());
                });

                if (ids.length == 0) {
                    message.show('info', Laravel.lang.list.no_rows_checked);
                } else {
                    action += "/" + ids.join(",");
                    confirmQuestion = confirmQuestion.replace(':among', '<b><i>' + ids.length + '</i></b>');

                    dialog.confirm(confirmQuestion, action);
                }
            } else {
                message.show('info', Laravel.lang.list.choose_multi_action);
            }
        },
        "bindEvents": function() {
            $("[data-list-checkboxall]").bind("click", function() {
                var checked = $(this).is(':checked');
                $("[data-list-checkbox]").each(function(i, val) {
                    $(val).prop("checked", checked);
                });
            });

            $("[data-multiaction-execute]").bind("click", function() {
                me.multiActions.execute();
            });

            $("[data-list-loadnextpage]").bind("click", function() {
                me.loadNextPage($(this));
            });
        }
    };

    this.pagination = {
        "getPage": function(url, params) {
            params.search_column = $("[data-list-container] select[name='search_column']").find("option:selected").val();
            params.search_value = $("[data-list-container] input[name='search_value']").val();

            if ($("[data-list-globalorder]").val() != "" && $("[data-list-globalorderfield]").val() != "") {
                params.order = $("[data-list-globalorder]").val();
                params.order_field = $("[data-list-globalorderfield]").val();
            }

            loader.show();
            $.get(url, params, function(response) {
                $("[data-list-container]").html(response);
                loader.hide();
                switchbutton.bindEvents();
                dialog.events();

                if(typeof me.callback != "undefined"){                  
                  me.callback();
                }
            });
        },
        "bindEvents": function() {
            $("[data-list-container]").on("click", "ul.pagination a", function() {
                //$("[data-list-container] ul.pagination a").bind("click", function() {
                me.pagination.getPage($(this).attr("href"), {});
                return false;
            });

            $("[data-list-container]").on("keypress", "[data-pagination-gotopage]", function(e) {
                //$("[data-list-container] [data-pagination-gotopage]").bind("keypress", function(e) {
                var page = $(this).val();
                var keycode = (e.keyCode ? e.keyCode : e.which);

                if (page != "" && keycode == '13' && (page % 1) === 0) {
                    me.pagination.getPage($(this).attr("data-pagination-baseurl"), {
                        'page': page
                    });

                    e.preventDefault();
                    return false;
                }
            });


            $("[data-list-container]").on("keypress", "input[name='search_value'], [data-pagination-rowsperpage]", function(e) {
                //$("[data-list-container] input[name='search_value'], [data-list-container] [data-pagination-rowsperpage]").bind("keypress", function(e) {
                var keycode = (e.keyCode ? e.keyCode : e.which);
                if (keycode == '13') {
                    var valid = true;
                    var params = {
                        'page': 1
                    };
                    if (typeof $(this).attr("data-pagination-rowsperpage") != "undefined") {
                        var per_page = $(this).val();
                        if (per_page == "" || (per_page % 1) !== 0)
                            valid = 0;
                        else
                            params.rows_per_page = per_page;
                    }
                    var url = $("[data-list-container] [data-pagination-search]").attr("data-pagination-baseurl");
                    if (url != "" && valid)
                        me.pagination.getPage(url, params);

                    e.preventDefault();
                    return false;
                }
            });

            $("[data-list-container]").on("click", "[data-pagination-search]", function() {
                //$("[data-list-container] [data-pagination-search]").bind("click", function() {
                me.pagination.getPage($(this).attr("data-pagination-baseurl"), {
                    'page': 1
                });
            });
        }
    };

    this.subrows = {
        "show" : function(el){
          $(el).attr('data-state',0);
          //$(el).prev().find("[data-list-showsubrow]").html("<i class='fa fa-list'></i> "+Laravel.lang.page.show_subpages);
          $(el).show();
        },
        "hide" : function(el){
          $(el).attr('data-state',1);
          //$(el).prev().find("[data-list-showsubrow]").html("<i class='fa fa-list'></i> "+Laravel.lang.page.hide_subpages);
          $(el).hide();
        },
        "bindEvents" : function(){
          $("[data-list-container]").on("click","[data-list-showsubrow]",function($response){
            var el = $(this).closest('tr').next();
            if(typeof $(el).attr('data-state') == "undefined" || $(el).attr('data-state') == 1){
              me.subrows.show(el);
            }else{
              me.subrows.hide(el);
            }
          });
        }
    };

    this.bindEvents = function() {
        me.sort.bindEvents();
        me.subrows.bindEvents();
        me.multiActions.bindEvents();
        me.pagination.bindEvents();
    };

};

$(document).ready(function() {
    list.bindEvents();

    $(".titleButtonsFlex + .resultsChanges").slideDown(500,function(){
        $(".titleButtonsFlex + .resultsChanges").delay(3500).slideUp();
    });

    $(".js-closeResult").on("click",function(){
        $(this).closest('.resultsChanges').slideUp();
    });
});
