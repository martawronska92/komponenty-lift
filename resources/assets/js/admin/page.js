var page = new function() {

    var me = this;

    this.common = {
        "checkIfExists": function(item, symbol, id) {
            var dataId = symbol != "" ? symbol + "_" + id : id;
            return $("[data-page-" + item + "box]").find("[data-id='" + dataId + "']").length > 0;
        },
        "remove": function(el) {
            if (typeof $(el).attr('data-question') != "undefined") {
                dialog.confirm($(el).attr('data-question'), "", function() {
                    me.common.removeCallback(el);
                });
            } else {
                me.common.removeCallback(el);
            }
        },
        "removeCallback": function(el) {
            loader.show();
            $.post('/panel/pagepost/removemodule', {
                'id': $(el).attr('data-page-removemodule'),
                '_token': Laravel.csrfToken
            }, function(response) {
                if (response.code == 1) {
                    $(el).closest("[data-page-modulerow]").remove();
                    if ($("[data-page-modulebox]").find("[data-page-modulerow]").length == 0) {
                        $("[data-page-moduleboxheader]").addClass("hidden");
                        $("[data-page-modulebox]").html('<div class="resultsChanges resultsChanges--info">' + Laravel.lang.module.added_list_empty + '</div>');
                    }

                    message.show('success', Laravel.lang.module.delete_success);
                } else {
                    message.show('danger', Laravel.lang.module.delete_failure);
                }
                loader.hide();
            }).fail(function() {
                loader.hide();
                message.show('danger', Laravel.lang.operation_failure);
            });
        },
        "add": function(params) {
            $("[data-page-modulepartial] [data-id]").attr("data-id", params.symbol + "_" + (params.module_entity_id != "" ? params.module_entity_id : params.module_id));
            $("[data-page-modulepartial] [data-page-modulename]").html(params.module_name);
            $("[data-page-modulepartial] [data-page-entitydetails]").html(params.entity_details);
            $("[data-page-modulepartial]").find("input[name='module_symbol[]']").val(params.symbol);
            $("[data-page-modulepartial]").find("input[name='module_id[]']").val(params.module_id);
            $("[data-page-modulepartial]").find("input[name='module_entity_id[]']").val(params.module_entity_id);

            if (params.item == 'category') {
                $("[data-page-modulepartial] [data-page-categorybutton='edit']").attr('href', '/panel/category/edit/' + params.module_entity_id);
                $("[data-page-modulepartial] [data-page-categorybutton='showposts']").attr('href', '/panel/post/category/' + params.module_entity_id);
                $("[data-page-modulepartial] [data-page-categorybutton]").removeClass("hidden");
                $("[data-page-modulepartial] [data-page-modulebutton]").addClass("hidden");
            } else {
                $("[data-page-modulepartial] [data-page-modulebutton]").attr('href', params.url);
                $("[data-page-modulepartial] [data-page-modulebutton]").removeClass("hidden");
                $("[data-page-modulepartial] [data-page-categorybutton]").addClass("hidden");
            }

            $("[data-page-" + params.item + "box]").append($("[data-page-modulepartial]").html());
        }
    };

    this.module = {
        "setPreview": function(params) {
            params._token = Laravel.csrfToken;
            params.entity_id = $("#id").val();
            params.entity_type = $("#entity_type").val();
            loader.show();
            $.post('/panel/pagepost/createmodule', params, function(response) {
                if (parseInt(response.code) == 1) {
                    if (response.msg != "") {
                        top.location.href = response.msg;
                    } else {
                        if ($("[data-page-modulebox]").find("[data-page-modulerow]").length == 0)
                            $("[data-page-modulebox]").html("");
                        message.show('success', Laravel.lang.module.add_success);
                        if (typeof response.view != "undefined") {
                            $("[data-page-moduleboxheader]").removeClass("hidden");
                            $("[data-page-modulebox]").append(response.view);
                            switchbutton.bindEvents();
                            list.sort.sortable();
                        }
                    }
                } else {
                    message.show('danger', response.msg);
                }
                loader.hide();
            }).fail(function() {
                loader.hide();
                message.show('danger', Laravel.lang.operation_failure);
            });
        },
        "add": function() {
            var bindEntity = parseInt($("[data-page-modulelist] option:selected").attr('data-bindentity'));
            var moduleId = $("[data-page-modulelist] option:selected").val();
            var moduleName = $("[data-page-modulelist] option:selected").text();
            var symbol = $("[data-page-modulelist] option:selected").attr('data-symbol');
            var url = $("[data-page-modulelist] option:selected").attr('data-url');
            /**
             * nie ma koniecznosci wybierania entity
             */
            if (bindEntity == 0) {
                if (me.common.checkIfExists('module', symbol, moduleId) > 0) {
                    message.show('danger', 'Moduł istnieje');
                } else {
                    me.module.setPreview({
                        "item": "module",
                        "module_name": moduleName,
                        "entity_name": "",
                        "symbol": symbol,
                        "module_id": moduleId,
                        "module_entity_id": "",
                        "url": url
                    });
                }
            } else {
                var moduleEntityId = $("[data-page-moduleid] option:selected").val();
                var moduleEntityName = $("[data-page-moduleid] option:selected").text();
                url = url + "/edit/" + moduleEntityId;

                if (moduleId == "") {
                    message.show('info', Laravel.lang.page.choose_module);
                } else if (typeof moduleEntityId == "undefined" || moduleEntityId == "") {
                    message.show('info', Laravel.lang.page.choose_entity);
                } else {
                    if (me.common.checkIfExists('module', symbol, moduleEntityId) > 0) {
                        message.show('danger', Laravel.lang.page.module_on_page);
                    } else {
                        me.module.setPreview({
                            "item": "module",
                            "module_name": moduleName,
                            "entity_name": moduleEntityName,
                            "symbol": symbol,
                            "module_id": moduleId,
                            "module_entity_id": moduleEntityId,
                            "url": url
                        });
                    }
                }
            }
        },
        "change": function() {
            var moduleId = $("[data-page-modulelist] option:selected").val();
            var bindEntity = $("[data-page-modulelist] option:selected").attr("data-bindentity");

            if (moduleId == "" || bindEntity == 0) {
                $("[data-page-moduleid]").addClass('hidden');
            } else {
                $("[data-page-moduleid]").html("");
                $("[data-page-moduleid]").removeClass('hidden');
                $.post("/panel/module/entity", {
                    'id': moduleId,
                    '_token': Laravel.csrfToken
                }, function(response) {
                    var html = "";
                    Object.keys(response).forEach(function(key, index) {
                        html = "<option value='" + response[key].id + "'>" + response[key].name + "</option>";
                        $("[data-page-moduleid]").append(html);
                    });
                }).fail(function() {
                    loader.hide();
                    message.show('danger', Laravel.lang.operation_failure);
                });
            }
        }
    };

    this.category = {
        "add": function() {
            var categoryId = $("[data-page-categorylist] option:selected").val();
            var categoryName = $("[data-page-categorylist] option:selected").text();
            var symbol = $("[data-page-categorylist] option:selected").attr('data-symbol');
            if (categoryId == "") {
                message.show('info', Laravel.lang.page.choose_category);
            } else {
                if (me.common.checkIfExists('category', 'category', categoryId) > 0) {
                    message.show('danger', Laravel.lang.page.category_on_page);
                } else {
                    me.common.add({
                        "item": "category",
                        "module_name": Laravel.lang.category.header,
                        "entity_name": categoryName,
                        "symbol": symbol,
                        "module_id": "",
                        "module_entity_id": categoryId
                    });
                }
            }
        }
    };

    this.init = function() {
        me.bindEvents();
    };

    this.generatePreview = function(type) {
        loader.show();

        var newWindow = window.open("/panel/" + type + "/waitforpreview", "_blank");

        $.post("/panel/" + type + "/generatepreview", {
            "form": $('[name!=thumbnail]', $("form")).serialize(), //$("form").serialize(),
            "_token": Laravel.csrfToken,
            "from_id": $("#id").val()
        }, function(response) {
            loader.hide();
            if (parseInt(response.code) == 1) {
                //top.location.href = response.msg;
                //window.open(response.msg, '_blank')
                newWindow.location.href = response.msg;
            } else {
                message.show('danger', response.msg);
            }
        }).fail(function() {
            loader.hide();
            message.show('danger', Laravel.lang.operation_failure);
        });
    };

    this.generatePreviewFromModule = function(moduleType){
        loader.show();

        var newWindow = window.open("/panel/page/waitforpreview", "_blank");

        $.post('/panel/'+moduleType+'/generatepreview',{
          '_token': Laravel.csrfToken,
          'form': $("[data-form-topreview]").serialize(),
          'pagePostModuleId': $("#pagePostModuleId").val(),
          'id': $("#id").val()
        },function(response){
          loader.hide();
          if (parseInt(response.code) == 1) {
              //top.location.href = response.msg;
              //window.open(response.msg, '_blank')
              newWindow.location.href = response.msg;
          } else {
              message.show('danger', response.msg);
          }
        }).fail(function() {
            loader.hide();
            message.show('danger', Laravel.lang.operation_failure);
        });
    };

    this.bindEvents = function() {
        $("[data-page-modulelist]").bind("change", function() {
            me.module.change();
        });

        var items = ['category', 'module'],
            item;
        for (var i = 0; i < items.length; i++) {
            item = items[i];
            if ($("[data-page-add" + item + "]").length) {
                $("[data-page-add" + item + "]").bind("click", {
                    item: item
                }, function(event) {
                    me[event.data.item].add();
                });

                $("[data-page-" + item + "box]").on("click", "[data-page-removemodule]", function() {
                    me.common.remove($(this));
                });
            }
        }

        $("[data-page-preview]").bind("click", function() {
            me.generatePreview("page");
        });

        $("[data-module-preview]").bind("click",function(){
           me.generatePreviewFromModule($(this).attr("data-module-preview"));
        });

        $("#parent_page_id").bind("change",function(){
          $("[data-linkgenerator-trigger]:visible").trigger("blur");
        });
    };

};
