var message = new function() {
    this.show = function(type, message) {
        $.notify({
            message: message
        }, {
            type: type,
            placement: {
                from: "bottom",
                align: "right"
            },
            delay: 5000
        });
    };
};

var form = new function() {
    this.validate = function() {
      if(typeof $("[data-formvalidation]").validate != "undefined"){
        $("[data-formvalidation]").validate({
            "ignore": [],
            "errorPlacement": function(error, element) {
                if (typeof element.attr("data-lang-input") !="undefined" && element.is(":hidden")){
                  //nie pokazujemy komunikatów dla ukrytych langów
                }else if (typeof element.attr("type") != "undefined" && element.attr("type") == "radio") {
                    error.insertAfter(element.parent().find('label'));
                }else if (typeof element.attr("type") != "undefined" && element.attr("type") == "file") {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            "invalidHandler": function(form, validator) {
                var errors = validator.numberOfInvalids();

                if (errors) {
                    var response = Laravel.lang.validation.invalid, msg;
                    if (validator.errorList.length > 0 && $(this).find("[data-formvalidation-label]").length > 0) {
                        response+= ":<ul>";
                        for (x = 0; x < validator.errorList.length; x++) {
                            msg = validator.errorList[x].message;
                            response+= "<li><b>"+$(validator.errorList[x].element).attr("data-formvalidation-label")+"</b> - "+msg[0].toLowerCase()+msg.slice(1,msg.length)+"</li>";
                        }
                        response+= "</ul>";
                    }

                    message.show('danger',response);
                }
            },
            "submitHandler": function(form){
              if(typeof $(form).attr("data-form-beforesubmit") != "undefined"){
                var functionName = $(form).attr("data-form-beforesubmit").split(".");
                var functionMethod = window;
                for(var i=0; i<functionName.length;i++){
                  functionMethod = functionMethod[functionName[i]];
                }

                return functionMethod();
              }
              return true;
            }
        });
      }
    };

    this.bind = function() {
        this.validate();
    };
};

$(document).ready(function() {
    form.bind();

    if($(".verticalcarousel").length > 0){
      $(".verticalcarousel").owlCarousel({
          //loop: true,
          items: 1,
          nav: true,
          animateOut: 'fadeOut'
      });
    }
});

if(typeof tippy !="undefined"){
  tippy(document.querySelectorAll('.js-popupTooltip'), {
      maxWidth:'350px',
      arrow:true
  });
}
