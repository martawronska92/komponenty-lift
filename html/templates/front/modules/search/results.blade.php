@if(\Request::has('s') && \Request::get('s')!="")
  {{-- wyniki --}}
  @if(count($results)>0)
    <div class="alert alert-info">{{trans('front.search.total_results')}}: {{$paginator->total()}}</div>

    <ul>
      @foreach($results as $result)
        <li><a href="{{$result[0]}}">{{$result[1]}}</a></li>
      @endforeach
    </ul>

    @if(\Request::has('s'))
      {{$paginator->appends(['s' => \Request::get('s')])->links()}}
    @endif
  @else
    <div class="alert alert-info">{{trans('front.search.no_results')}}</div>    
  @endif
@else
  {{trans('front.search.no_query')}}
@endif
