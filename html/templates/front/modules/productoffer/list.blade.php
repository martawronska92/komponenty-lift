@if(count($products) > 0)

  @if($offer->language && count($offer->language)>0 && $offer->language[0]->title!="")
    <h3>{{$offer->language[0]->title}}</h3>
  @endif

  <ul>
    @foreach($products as $product)
      <li>
        @if($product->getImage()!="")
          <img src="{{$product->getImage()}}" @if($product->language && count($product->language)>0 && $product->language[0]->title!="")alt="{{$product->language[0]->title}}"@endif/>
        @endif
        <a href="{{\App\Helpers\Url::getFrontPostUrl($product->id)}}">{{$product->language[0]->title}}</a>
      </li>
    @endforeach
  </ul>
@endif
