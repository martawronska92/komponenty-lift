$(document).ready(function(){
    $('.js-openMenu').on("click", function () {
      $('.mobileMenu').addClass('opened');
    });
    
    $('.js-close').on("click", function () {
        $('.mobileMenu').removeClass('opened');
    });

  $('body').on("click", '.js-prev', function () {
    var el = $(this);
    $(".mobileMenu__list.opened").removeClass("opened");
    $("[data-menu].actived").removeClass("actived");
    $(".js-prev").remove();
  });
    
    $("[data-menu]").on("click",function(e){
      if(!$(this).hasClass('actived')){
        e.preventDefault();
        var el = $(this);
        el.addClass("actived");
        el.next('ul').addClass("opened");
        el.next('ul').after("<span class='js-prev mobilePrev'></span>");
      }
     
    });
    
});
