var category = new function() {
    var me = this;
    this.containerAmong;
    
    this.showNext = function(el) {
        var categoryBox = $(el).closest("[data-category-box]");
        var currentPage = parseInt(categoryBox.attr("data-category-currentpage"));
        var container = categoryBox.find("[data-category-container]");
        if (container.eq(currentPage).length > 0) {
            container.eq(currentPage).fadeIn();
        }
        if (container.eq(currentPage + 1).length == 0) {
            $(el).fadeOut();
        }
        $(el).closest("[data-category-box]").attr("data-category-currentpage", currentPage + 1);
    };
    
    this.bindEvents = function() {
        $("[data-category-loadpage]").bind("click", function() {
            me.showNext($(this));
        });
    };

};

$(document).ready(function () {
    category.bindEvents();
    
    $('.loader').on('inview', function (event, isInView) {
        if (isInView) {
            var data = {
                category_id: $("[data-category-id]").data("category-id"),
                page_nr: parseInt($("[data-category-page]").data("category-page")) + 1,
                _token: Laravel.csrfToken
            };

            $.ajax({
                url: "/module/postfromcategory",
                method: "POST",
                data: data,
                dataType: "json",
                success: function (data) {
                    if (data.posts.length == 0) {
                        $(".loader").remove();
                    } else {
                        var template = $("#entry-template").html();
                        var stone = Handlebars.compile(template)(data);
                        var $items = $.parseHTML(stone);
                        container.append($items).isotope('appended', $items);
                        
                        $("[data-category-page]").data('category-page', data.page);
                        $(".loader").data("offset", data.offset);
                    }
                }
            });
        }
    });
});
