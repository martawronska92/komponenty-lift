$(document).ready(function(){
    // $('.js-videoSlider').slick({
    //     dots: false,
    //     vertical: true,
    //     slidesToShow: 3,
    //     slidesToScroll: 3,
    //     verticalSwiping: true,
    //     infinite: false
    // });
    
    $("[data-videourl]").on("click", function () {
        var el = $(this);
        var url = el.data("videourl");
        $('[data-target]').attr("src", url);
    });
    
    $("[data-videourl]").eq(0).trigger("click");
});